import { defineConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [uni()],
    // 打包部署后, 查找静态文件用相对路径
  base: './',
    // 配置代理, 开发时解决跨域.
  server: {
    proxy: {
        // 注意 变量名 同 import.meta.env.VITE_BASE_API
        '/api': {
          target: 'http://175.24.128.56:8069', // o17
          // target: 'http://124.220.156.33:8017', // o17
          // target: 'http://127.0.0.1:8069', // o17
          // target: 'http://192.168.56.2:8069', // o17
          // target: '/odoo',
  
          changeOrigin: true,
          rewrite: path => path.replace(/^\/api/, '')
        }
    }
  }
});
