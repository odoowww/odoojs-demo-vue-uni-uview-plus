import base from './base/index.js'
import account from './account/index.js'

const modules = {
  base,
  account
}

export default modules
