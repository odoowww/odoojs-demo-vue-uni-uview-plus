function ModelCreator(Model) {
  class ExtendModel extends Model {
    constructor(...args) {
      super(...args)
    }

    // async web2_read_one(res_id, specification, kwargs = {}) {
    //   const record = await super.web2_read_one(res_id, specification, kwargs)

    //   const invoice_line_ids = record.invoice_line_ids || []

    //   const AAAM = this.env.model('account.analytic.account')

    //   for (const line of invoice_line_ids) {
    //     const analytic_distribution = line.analytic_distribution
    //     if (!analytic_distribution) {
    //       continue
    //     }

    //     const keys = Object.keys(analytic_distribution).map(item =>
    //       Number(item)
    //     )

    //     const accs = await AAAM.read(keys, { display_name: {} })

    //     const aaas = accs.reduce((acc, cur) => {
    //       acc[cur.id] = cur
    //       return acc
    //     }, {})

    //     const ads = keys.reduce((acc, cur) => {
    //       acc[cur] = { ...aaas[cur], percent: analytic_distribution[cur] }
    //       return acc
    //     }, {})

    //     line.analytic_distribution = ads
    //   }

    //   return record
    // }
  }
  return ExtendModel
}

const metadata = {}

const AddonsModels = {
  'account.move': {
    ModelCreator,
    metadata
  }
}

export default AddonsModels

// activity_ids: {},
// amount_residual: {},
// amount_residual_signed: {},
// amount_tax_signed: {},
// amount_total_in_currency_signed: {},
// amount_total_signed: {},
// amount_untaxed_signed: {},
// auto_post: {},
// auto_post_until: {},
// commercial_partner_id: {},
// company_currency_id: {},
// company_id: {},
// country_code: {},
// currency_id: {},
// date: {},
// display_inactive_currency_warning: {},
// display_qr_code: {},
// duplicated_ref_ids: {},

// has_reconciled_entries: {},
// hide_post_button: {},
// highest_name: {},
// invoice_cash_rounding_id: {},
// invoice_date: {},
// invoice_date_due: {},
// invoice_filter_type_domain: {},
// invoice_has_outstanding: {},
// invoice_incoterm_id: {},
// invoice_line_ids: {},
// invoice_origin: {},
// invoice_outstanding_credits_debits_widget: {},
// invoice_partner_display_name: {},
// invoice_payment_term_id: {},
// invoice_payments_widget: {},
// invoice_source_email: {},
// invoice_user_id: {
//   domain: [['share', '=', false]]
// },

// is_move_sent: {},

// line_ids: {},
// made_sequence_hole: {},
// move_type: {},
// name: {},
// narration: {},
// partner_credit_warning: {},

// payment_id: {},
// payment_reference: {},
// payment_state: {},
// posted_before: {},
// qr_code_method: {},
// quick_edit_mode: {},
// quick_edit_total_amount: {},
// quick_encoding_vals: {},
// ref: {},
// restrict_mode_hash_table: {},
// reversed_entry_id: {},
// show_name_warning: {},
// show_reset_to_draft_button: {},
// state: {
//   selection: [
//     ['draft', 'Draft'],
//     ['posted', 'Posted'],
//     ['cancel', 'Cancelled']
//   ]
// },
// statement_line_id: {},
// suitable_journal_ids: {},
// tax_cash_basis_created_move_ids: {},
// tax_cash_basis_origin_move_id: {},
// tax_country_id: {},
// tax_lock_date_message: {},
// tax_totals: {},
// to_check: {},
// user_id: {}
