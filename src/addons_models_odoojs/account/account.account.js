function ModelCreator(Model) {
  class ModelExtend extends Model {
    constructor(...args) {
      super(...args)
    }

    // async action_open_in_payment(rids, kwargs = {}) {
    //   const { record } = kwargs
    //   const account_opening_date = record.company_id.account_opening_date
    //   console.log(account_opening_date)
    //   const result = {
    //     res_model: 'account.payment',
    //     type: 'ir.actions.act_window',
    //     target: 'current',
    //     domain: [['date', '<=', account_opening_date]]
    //   }

    //   return { result }
    // }

    // async call_button(method, rids, kwargs = {}) {
    //   if (method === 'action_open_in_payment') {
    //     return this.action_open_in_payment(rids, kwargs)
    //   } else {
    //     return super.call_button(method, rids, kwargs)
    //   }
    // }
  }

  return ModelExtend
}

const metadata = {}

const AddonsModels = {
  'account.account': {
    ModelCreator,
    metadata
  }
}

export default AddonsModels
