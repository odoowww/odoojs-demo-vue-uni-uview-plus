function ModelCreator(Model) {
  class ModelExtend extends Model {
    constructor(...args) {
      super(...args)
    }

    // async find_or_create(domain, vals) {
    //   const jul1 = await this.search_one(domain)
    //   if (jul1) {
    //     return jul1
    //   }
    //   return this.create(vals)
    // }

    // compute_code2({ record }) {
    //   return {
    //     code2: record.code
    //     // code3: record.code
    //   }
    // }
  }

  return ModelExtend
}

function StateModelCreator(Model) {
  class ModelExtend extends Model {
    constructor(...args) {
      super(...args)
    }

    // inverse_code2({ value, record, values, record_display }) {
    //   // console.log('inverse', value, record, values, record_display)
    //   return { value: { code: value } }
    // }
    // compute_code2({ record }) {
    //   return {
    //     code2: record.code,
    //     code3: record.code
    //   }
    // }
  }

  return ModelExtend
}

const metadata = {
  // code2: {
  //   type: 'char',
  //   string: 'code2',
  //   odoojs_extend: true,
  //   compute: 'compute_code2'
  // },
  // code3: {
  //   type: 'char',
  //   string: 'code3',
  //   odoojs_extend: true,
  //   compute: 'compute_code2'
  // }
}

const state_metadata = {
  // code2: {
  //   type: 'char',
  //   string: 'code2',
  //   odoojs_extend: true,
  //   compute: 'compute_code2',
  //   inverse: 'inverse_code2'
  // },
  // code3: {
  //   type: 'char',
  //   string: 'code3',
  //   odoojs_extend: true,
  //   compute: 'compute_code2'
  // }
}

const AddonsModels = {
  'res.country': { ModelCreator, metadata },
  'res.country.state': {
    ModelCreator: StateModelCreator,
    metadata: state_metadata
  }
}

export default AddonsModels
