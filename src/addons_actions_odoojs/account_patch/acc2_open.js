const view_form_acc2_open = {
  _odoo_model: 'ir.ui.view',
  model: 'acc2.open',
  type: 'form',
  arch: {
    button_box: {
      // button_action_open_related_taxes: {
      //   name: 'action_open_related_taxes',
      //   type: 'object',
      //   icon: 'fa-bars',
      //   attrs: {
      //     // 'invisible': [('related_taxes_amount', '=', 0)]
      //     invisible: [['related_taxes_amount', '=', 0]]
      //   },
      //   field_related_taxes_amount: { string: 'Taxes', widget: 'statinfo' }
      // },
      // button_account_action_move_line_select: {
      //   name: 'account.action_move_line_select',
      //   type: 'action',
      //   field_current_balance: { string: 'Balance', widget: 'statinfo' }
      // }
    },

    header: {
      button_action_post: {
        name: 'action_post',
        string: '锁定',
        type: 'object',
        invisible: [['state', '!=', 'draft']]
      },

      button_draft: {
        name: 'button_draft',
        string: '重置为草稿',
        type: 'object',
        invisible: [['state', '!=', 'posted']]
      },

      button_action_set_open_move: {
        name: 'action_set_open_move',
        string: '更新期初分录',
        type: 'object',
        invisible: [['state', '!=', 'draft']]
      },

      button_action_set_detail_move: {
        name: 'action_set_detail_move',
        string: '更新明细',
        type: 'object',
        invisible: [
          '|',
          ['state', '!=', 'draft'],
          ['account_open_type', 'in', ['no_detail', 'off_balance']]
          // [
          //   'account_open_type',
          //   'not in',
          //   ['out_invoice', 'in_invoice', 'in_payment', 'out_payment']
          // ]
        ]
      },

      field_state: { widget: 'statusbar', statusbar_visible: 'draft,posted' }
    },

    sheet: {
      field_company_id: {
        invisible: 1
        // fields: {
        //   account_opening_date: {},
        //   account_opening_move_id: {}
        // }
      },
      field_available_account_ids: { invisible: 1 },

      group: {
        group_left: {
          field_account_id: {
            readonly: [['id', '!=', false]],
            domain: [['id', 'in', { field_available_account_ids: {} }]]
          },
          field_account_code: { readonly: '1' },
          field_name: { readonly: '1' },
          field_account_open_type: { readonly: '1' },
          field_reversal_move_id: { readonly: '1' }
        },

        group_right: {
          field_debit: {},
          field_credit: {},
          field_balance: { invisible: '1' },

          field_opening_debit: {},
          field_opening_credit: {},
          field_opening_balance: { invisible: '1' },

          field_detail_debit: {
            invisible: [
              ['account_open_type', 'in', ['no_detail', 'off_balance']]
            ]
          },
          field_detail_credit: {
            invisible: [
              ['account_open_type', 'in', ['no_detail', 'off_balance']]
            ]
          }
        }
      },

      group_2: {},

      notebook: {
        page_aml_tab: {
          attr: {
            id: 'aml_tab',
            string: '明细',
            name: 'aml_tab',
            invisible: [
              ['account_open_type', 'in', ['no_detail', 'off_balance']]
            ]
          },
          field_detail_ids: {
            buttons: { create: true, edit: true, delete: false },

            views: {
              tree: {
                arch: {
                  sheet: {
                    field_company_id: { invisible: 1 },
                    field_account_open_type: { invisible: 1 },
                    field_account_id: {},
                    field_journal_id: {
                      column_invisible: [
                        ['parent.account_open_type', 'not in', ['entry_cash']]
                      ]
                    },
                    field_partner_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          [
                            'in_payment',
                            'out_payment',
                            'out_invoice',
                            'in_invoice',
                            'entry_in_receivable',
                            'entry_out_payable'
                          ]
                        ]
                      ]
                    },
                    field_date_maturity: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['out_invoice', 'in_invoice']
                        ]
                      ]
                    },
                    field_payment_journal_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ]
                    },
                    field_product_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['entry_stock', 'entry_fixed']
                        ]
                      ]
                    },
                    field_debit: {},
                    field_credit: {},
                    field_balance: { invisible: '1' },

                    field_move_id: {},
                    field_move_line_id: {},
                    field_payment_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ]
                    },
                    field_reversal_payment_move_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ]
                    }
                  },
                  kanban: {
                    card_title: { field_account_id: {} },
                    card_label: {},
                    card_value: {
                      field_partner_id: {},
                      field_journal_id: {},
                      field_product_id: {},
                      field_debit: {},
                      field_credit: {},
                      field_balance: { invisible: '1' }
                      // field_opening_debit: {},
                      // field_opening_credit: {},
                      // field_opening_balance: { invisible: '1' }
                      // widget_account_move_line_value: {
                      //   field_debit: {},
                      //   field_credit: {}
                      // }
                    }
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_company_id: { invisible: 1 },
                    field_account_open_type: { invisible: 1 },
                    field_account_id: {},
                    field_journal_id: {
                      invisible: [['account_open_type', '!=', 'entry_cash']],
                      domain: [
                        ['type', 'in', ['bank', 'cash']],
                        ['default_account_id', '=', { field_company_id: {} }],
                        ['company_id', '=', { field_company_id: {} }]
                      ]
                    },
                    field_partner_id: {
                      invisible: [
                        [
                          'account_open_type',
                          'not in',
                          [
                            'in_payment',
                            'out_payment',
                            'out_invoice',
                            'in_invoice',
                            'entry_in_receivable',
                            'entry_out_payable'
                          ]
                        ]
                      ]
                    },
                    field_date_maturity: {
                      invisible: [
                        [
                          'account_open_type',
                          'not in',
                          ['out_invoice', 'in_invoice']
                        ]
                      ]
                    },
                    field_payment_journal_id: {
                      invisible: [
                        [
                          'account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ],
                      domain: [
                        ['type', 'in', ['bank', 'cash']],
                        ['company_id', '=', { field_company_id: {} }]
                      ]
                    },

                    field_product_id: {
                      invisible: [
                        [
                          'account_open_type',
                          'not in',
                          ['entry_stock', 'entry_fixed']
                        ]
                      ]
                    },
                    field_debit: {},
                    field_credit: {},
                    field_balance: { invisible: '1' },

                    field_move_id: {},
                    field_move_line_id: {},
                    field_payment_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ]
                    },
                    field_reversal_payment_move_id: {
                      column_invisible: [
                        [
                          'parent.account_open_type',
                          'not in',
                          ['in_payment', 'out_payment']
                        ]
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_acc2_open = {
  _odoo_model: 'ir.ui.view',
  model: 'acc2.open',
  type: 'tree',
  arch: {
    sheet: {
      field_company_id: { invisible: 1 },
      field_account_id: {},
      // field_account_code: {},
      // field_name: {},
      field_account_open_type: {},
      field_available_account_ids: { invisible: 1 },
      field_debit: {},
      field_credit: {},
      field_balance: { optional: 'hide' },

      field_opening_debit: {},
      field_opening_credit: {},
      field_opening_balance: { optional: 'hide' },

      field_detail_debit: {},
      field_detail_credit: {},
      field_reversal_move_id: {}
    },

    kanban: {
      card_title: { field_account_id: {} },
      card_label: { field_account_code: {} },
      card_value: {
        field_opening_debit: {},
        field_opening_credit: {}
      }
    }
  }
}

const view_search_acc2_open = {
  _odoo_model: 'ir.ui.view',
  model: 'acc2.open',
  type: 'search',
  arch: {
    sheet: {
      field_name: {}
    }
  }
}

const action_acc2_open = {
  _odoo_model: 'ir.actions.act_window',
  name: '科目期初设置',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: true },
  res_model: 'acc2.open',
  search_view_id: 'view_search_acc2_open',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_acc2_open',
    form: 'view_form_acc2_open'
  }
}
export default {
  view_form_acc2_open,
  view_tree_acc2_open,
  view_search_acc2_open,
  action_acc2_open
}
