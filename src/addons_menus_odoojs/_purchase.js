export default {
  menu_purchase_action_res_partner: {
    _odoo_model: 'ir.ui.menu',
    parent: '_contacts.menu_contacts',
    name: '联系人(供应商设置)',
    icon: 'star',
    sequence: 60,
    action: 'purchase.action_res_partner'
  },

  menu_purchase_action_product_template: {
    _odoo_model: 'ir.ui.menu',
    parent: '_product.menu_product',
    name: '产品(采购设置)',
    sequence: 50,
    action: 'purchase.action_product_template'
  },

  menu_purchase: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '采购',
    icon: 'star',
    sequence: 61,
    children: {
      menu_contacts_divider_config: {
        name: '设置',
        action: 'divider'
      },

      menu_purchase2_action_res_partner: {
        _odoo_model: 'ir.ui.menu',
        name: '联系人(供应商设置)',
        action: 'purchase.action_res_partner'
      },

      menu_purchase2_action_product_template: {
        _odoo_model: 'ir.ui.menu',
        name: '产品(采购设置)',
        action: 'purchase.action_product_template'
      },

      menu_contacts_divider: {
        name: '订单',
        action: 'divider'
      },

      menu_action_purchase_order: {
        name: '采购订单',
        action: 'purchase.action_purchase_order'
      },

      menu_purchase_action_account_move_in_invoice: {
        action: 'account.action_account_move_in_invoice',
        name: '采购账单'
      },

      menu_purchase_action_account_payment_supplier_outbound: {
        action: 'account.action_account_payment_supplier_outbound',
        name: '采购付款'
      }

      // menu_purchase_rfq: {
      //   name: '询价单',
      //   action: 'purchase.purchase_rfq'
      // },

      // menu_action_purchase_history: {
      //   name: 'Purchase Lines',
      //   action: 'purchase.action_purchase_history'
      // }
    }
  }
}
