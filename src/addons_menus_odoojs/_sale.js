export default {
  menu_sale_action_res_partner: {
    _odoo_model: 'ir.ui.menu',
    parent: '_contacts.menu_contacts',
    name: '联系人(客户设置)',
    icon: 'star',
    sequence: 50,
    action: 'sale.action_res_partner'
  },

  menu_sale_action_product_template: {
    _odoo_model: 'ir.ui.menu',
    parent: '_product.menu_product',
    name: '产品(销售设置)',
    sequence: 50,
    action: 'sale.action_product_template'
  },

  menu_sale: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '销售',
    icon: 'star',
    sequence: 50,
    children: {
      menu_contacts_divider_config: {
        name: '设置',
        action: 'divider'
      },

      menu_sales_team_action_crm_tag: {
        name: '销售标签',
        action: 'sales_team.action_crm_tag'
      },
      menu_sales_team_action_crm_team: {
        name: '销售团队',
        action: 'sales_team.action_crm_team'
      },

      menu_sale2_action_res_partner: {
        _odoo_model: 'ir.ui.menu',
        name: '客户',
        icon: 'star',
        action: 'sale.action_res_partner'
      },

      menu_sale2_action_product_template: {
        _odoo_model: 'ir.ui.menu',
        name: '商品',
        action: 'sale.action_product_template'
      },

      menu_contacts_divider: {
        name: '订单',
        action: 'divider'
      },

      menu_action_sale_order: {
        name: '销售订单',
        action: 'sale.action_sale_order'
      },
      menu_sale_action_account_move_out_invoice: {
        action: 'account.action_account_move_out_invoice',
        name: '销售结算单'
      },
      menu_sale_action_account_payment_customer_inbound: {
        action: 'account.action_account_payment_customer_inbound',
        name: '销售收款'
      }
    }
  },

  menu_sale_analytic: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '销售分析',
    icon: 'star',
    sequence: 50,
    children: {}
  }
}
