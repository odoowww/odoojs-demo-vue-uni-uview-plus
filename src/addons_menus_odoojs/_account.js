export default {
  menu_account_action_res_partner: {
    _odoo_model: 'ir.ui.menu',
    parent: '_contacts.menu_contacts',
    name: '联系人(开票设置)',
    icon: 'star',
    sequence: 30,
    action: 'account.action_res_partner'
  },

  menu_account_action_product_category: {
    _odoo_model: 'ir.ui.menu',
    parent: '_product.menu_product',
    name: '产品类别(开票设置)',
    sequence: 30,
    action: 'account.action_product_category'
  },

  menu_account_action_product_template: {
    _odoo_model: 'ir.ui.menu',
    parent: '_product.menu_product',
    name: '产品(开票设置)',
    sequence: 30,
    action: 'account.action_product_template'
  },

  menu_account_config: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '开票设置',
    icon: 'star',
    sequence: 33,
    children: {
      menu_product_divider_config: {
        name: '设置',
        action: 'divider'
      },

      menu_action_account_account: {
        name: '科目表',
        action: 'account.action_account_account'
      },

      // menu_action_account_account_open: {
      //   name: '科目期初设置',
      //   action: 'account.action_account_account_open'
      // },

      menu_action_acc2_open: {
        name: '科目期初设置',
        action: 'account_patch.action_acc2_open'
      },

      menu_action_account_journal: {
        name: '日记账',
        action: 'account.action_account_journal'
      },

      menu_move_action_res_partner: {
        _odoo_model: 'ir.ui.menu',
        name: '联系人(开票设置)',
        icon: 'star',
        action: 'account.action_res_partner'
      },

      menu_move_action_product_category: {
        _odoo_model: 'ir.ui.menu',
        name: '产品类别(开票设置)',
        action: 'account.action_product_category'
      },

      menu_move_action_product_template: {
        _odoo_model: 'ir.ui.menu',
        name: '产品(开票设置)',
        action: 'account.action_product_template'
      }
    }
  },

  menu_account_move: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '开票',
    icon: 'star',
    sequence: 33,
    children: {
      menu_product_divider_move_readonly: {
        name: '凭证',
        action: 'divider'
      },

      menu_action_account_move: {
        action: 'account.action_account_move',
        name: '凭证(查看)'
      },

      menu_action_account_move_other: {
        action: 'account.action_account_move_other',
        name: '凭证(手工凭证)'
      },

      menu_product_divider_invoice: {
        name: '结算单及账单',
        action: 'divider'
      },

      menu_action_account_move_out_invoice: {
        action: 'account.action_account_move_out_invoice',
        name: '销售结算单'
      },
      menu_action_account_move_in_invoice: {
        action: 'account.action_account_move_in_invoice',
        name: '采购账单'
      },

      menu_product_divider_payment: {
        name: '收付款',
        action: 'divider'
      },
      menu_action_account_payment_customer_inbound: {
        action: 'account.action_account_payment_customer_inbound',
        name: '销售收款'
      },

      menu_action_account_payment_supplier_outbound: {
        action: 'account.action_account_payment_supplier_outbound',
        name: '采购付款'
      },
      menu_action_account_payment_transfer: {
        action: 'account.action_account_payment_transfer',
        name: '内部转账'
      }
    }
  }
}
