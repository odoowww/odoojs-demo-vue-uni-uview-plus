export default {
  menu_hr_expense: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '费用报销',
    icon: 'star',
    sequence: 80,
    children: {
      menu_action_hr_expense_product_product: {
        name: '费用类型',
        action: 'hr_expense.action_hr_expense_product_product'
      }
    }
  }
}
