export default {
  menu_mrp: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '生产',
    icon: 'star',
    sequence: 80,
    children: {
      menu_action_mrp_bom: {
        name: 'BoM',
        action: 'mrp.action_mrp_bom'
      },

      menu_action_mrp_production: {
        name: '制造订单',
        action: 'mrp.action_mrp_production'
      }
    }
  }
}
