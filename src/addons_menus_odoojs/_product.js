export default {
  menu_product: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '商品管理',
    icon: 'star',
    sequence: 24,
    children: {
      // menu_product_divider_config: {
      //   sequence: 2,
      //   name: '设置',
      //   action: 'divider'
      // },

      menu_action_uom_category: {
        name: '度量单位类别',
        sequence: 10,
        action: 'uom.action_uom_category'
      },
      menu_action_uom_uom: {
        name: '度量单位',
        sequence: 10,
        action: 'uom.action_uom_uom'
      },

      // menu_product_divider: {
      //   sequence: 11,
      //   name: '管理',
      //   action: 'divider'
      // },

      menu_action_product_tag: {
        name: '商品标签',
        sequence: 10,
        action: 'product.action_product_tag'
      },

      menu_action_product_category: {
        name: '商品类别',
        sequence: 11,
        action: 'product.action_product_category'
      },

      menu_action_product_template: {
        name: '商品模版',
        sequence: 20,
        action: 'product.action_product_template'
      }
      // menu_attribute_action: {
      //   name: '产品属性',
      //   action: 'product.attribute_action'
      // }
    }
  }
}
