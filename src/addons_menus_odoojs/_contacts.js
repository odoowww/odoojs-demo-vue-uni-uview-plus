export default {
  // menu_contacts1: {
  //   _odoo_model: 'ir.ui.menu',
  //   parent: '_base.menu_web_root',
  //   name: '联系人1',
  //   icon: 'star',
  //   icon2: 'star',
  //   sequence: 1,
  //   children: {
  //     menu_contacts_divider_config1: {
  //       sequence: 2,
  //       name: '配置',
  //       children: {
  //         menu_contacts_divider_config991222: {
  //           sequence: 2,
  //           name: '配置',
  //           children: {
  //             menu_action_res_partner_category991: {
  //               sequence: 2,
  //               name: '联系人标签1',
  //               action: 'base.action_res_partner_category'
  //             },
  //             menu_action_res_partner_category992: {
  //               sequence: 2,
  //               name: '联系人标签3222222',
  //               action: 'base.action_res_partner_category'
  //             }
  //           }
  //         },

  //         menu_action_res_partner_category1: {
  //           sequence: 2,
  //           name: '联系人标签1',
  //           action: 'base.action_res_partner_category'
  //         },
  //         menu_action_res_partner_category2: {
  //           sequence: 2,
  //           name: '联系人标签3222222',
  //           action: 'base.action_res_partner_category'
  //         }
  //       }
  //     }
  //   }
  // },

  menu_contacts: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: '联系人',
    icon: 'star',
    sequence: 14,
    children: {
      // menu_contacts_divider_config: {
      //   sequence: 2,
      //   name: '设置',
      //   action: 'divider'
      // },

      // menu_action_res_country: {
      //   sequence: 2,
      //   name: '国家',
      //   action: 'base.action_res_country'
      // },

      menu_contacts_divider_config: {
        sequence: 2,
        name: '配置',
        children: {
          menu_action_res_partner_category: {
            sequence: 2,
            name: '联系人标签',
            action: 'base.action_res_partner_category'
          },
          menu_action_res_partner_industry: {
            sequence: 2,
            name: '行业类型',
            action: 'base.action_res_partner_industry'
          }
        }
      },

      // menu_action_res_partner_category: {
      //   sequence: 2,
      //   name: '联系人标签',
      //   action: 'base.action_res_partner_category'
      // },

      // menu_action_res_partner_title: {
      //   sequence: 2,
      //   name: '联系人称谓',
      //   action: 'base.action_res_partner_title'
      // },

      // menu_contacts_divider: {
      //   sequence: 2,
      //   name: '管理',
      //   action: 'divider'
      // },

      menu_action_res_bank: {
        name: '银行',
        sequence: 4,
        action: 'base.action_res_bank'
      },
      // menu_action_res_partner_bank: {
      //   name: '银行账户',
      //   sequence: 6,
      //   action: 'base.action_res_partner_bank'
      // },

      menu_action_res_partner: {
        name: '联系人',
        sequence: 8,
        action: 'contacts.action_res_partner'
      }
    }
  }
}
