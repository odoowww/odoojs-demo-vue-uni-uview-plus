export default {
  //

  menu_hr: {
    _odoo_model: 'ir.ui.menu',
    parent: '_base.menu_web_root',
    name: 'HR',
    icon: 'star',
    sequence: 80,
    children: {
      menu_action_hr_contract_type: {
        name: '员工类型',
        action: 'hr.action_hr_contract_type'
      },

      menu_action_hr_employee_category: {
        name: '员工标签',
        action: 'hr.action_hr_employee_category'
      },

      menu_action_hr_job: {
        name: '岗位',
        action: 'hr.action_hr_job'
      },

      menu_action_hr_work_location: {
        name: '工作地点',
        action: 'hr.action_hr_work_location'
      },

      menu_action_hr_department: {
        name: '部门',
        action: 'hr.action_hr_department'
      },

      menu_action_hr_employee: {
        name: '员工',
        action: 'hr.action_hr_employee'
      }
    }
  }
}
