import _base from './_base.js'
import _contacts from './_contacts.js'
import _product from './_product.js'
import _account from './_account.js'
import _sale from './_sale.js'
import _purchase from './_purchase.js'
import _stock from './_stock.js'
import _mrp from './_mrp.js'

import _hr from './_hr.js'
import _hr_expense from './_hr_expense.js'

const modules = {
  _base,
  _contacts,
  _product,
  _account,
  _sale,
  _purchase,
  _stock,
  _mrp,
  _hr,
  _hr_expense
}

export default modules
