import { computed } from 'vue'

export function useNode({ props }) {
  const nodeFieldWidget = computed(() => {
    const { node } = props
    if (node.tagname !== 'field') {
      return
    } else {
      return node.widget
    }
  })

  const nodeFieldType = computed(() => {
    const { node } = props
    if (node.tagname !== 'field') {
      return
    } else {
      const { meta = {} } = node
      return meta.type
    }
  })

  const nodeWidgetName = computed(() => {
    const { node } = props
    if (node.tagname !== 'widget') {
      return
    } else {
      return node.name
    }
  })

  return {
    nodeFieldWidget,
    nodeFieldType,
    nodeWidgetName
  }
}
