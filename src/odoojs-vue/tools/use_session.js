import { computed, reactive, ref, toRaw } from 'vue'

export function useSession(kws = {}) {
  const { get_api } = kws

  const state = reactive({
    session_info: {},
    cids: []
  })

  function load_session() {
    const api = get_api()
    const info = api.env.load_session()
    console.log(info)
    state.session_info = { ...info }
    state.cids = info.cids || []
  }

  const company = computed(() => {
    const { user_companies = {} } = state.session_info
    const { current_company, allowed_companies = {} } = user_companies
    if (current_company) {
      return allowed_companies[current_company]
    } else {
      return { id: null, name: null }
    }
  })

  const user = computed(() => {
    const { uid, username, name, partner_id } = state.session_info

    const avatar = partner_id
      ? `/api/web/image?model=res.partner&field=image_128&id=${partner_id}`
      : null

    return { id: uid, name, login: username, avatar }
  })

  const server = computed(() => {
    const { server_version, db } = state.session_info
    return { version: server_version, db }
  })

  const allowed_companies_options = computed(() => {
    const { user_companies = {} } = state.session_info
    const { allowed_companies = {} } = user_companies

    return allowed_companies
  })

  const allowed_companies = computed(() => {
    const options = allowed_companies_options.value
    const cids = state.cids
    console.log(options, cids)
    return Object.values(options).filter(item => cids.includes(item.id))
  })

  const allowed_companies_checked = computed(() => {
    const options = allowed_companies_options.value
    const cids = state.cids

    return Object.values(options).map(item => {
      return { ...item, checked: cids.includes(item.id) }
    })

    // return Object.values(options).filter(item => cids.includes(item.id))
  })

  function set_cids(cids) {
    // console.log(cids)

    if (cids.length) {
      state.cids = cids

      const api = get_api()
      api.env.set_cids(cids)
    }
  }

  return {
    load_session,
    company,
    user,
    server,
    companies: allowed_companies,
    companies_options: allowed_companies_checked,
    set_cids
  }
}
