import { computed, reactive, ref, toRaw } from 'vue'

export function useMenu(kws = {}) {
  const { get_api, web, uniapp, no_web_menus = {}, HOME_PATH } = kws

  async function load_menus() {
    const api = get_api()
    const menus = api.menus
    const menus_tree = api.menus_tree

    return {
      menus_data: menus,
      menus_tree: menus_tree
    }
  }

  function select_menu(menu, from) {
    // console.log(menu, from)
    if (menu in no_web_menus) {
      if (from.name === menu) {
        return
      }
      const path = `${HOME_PATH}${menu}`

      const ret = { path, query: { menu }, meta: no_web_menus[menu] }
      return ret
    }

    return webMenuSelect(menu, from)
  }

  function webMenuSelect(name, from) {
    const api = get_api()
    const menus_data = api.menus
    // console.log(name, menus_data)
    const menu = menus_data[name]
    // console.log(name, menu, menus_data)
    if (menu.router) {
      return routerMenuSelect(menu)
    } else {
      return actionMenuSelect(menu, from)
    }
  }

  function routerMenuSelect(menu) {
    // console.log(menu, menus_data.value)
    const { path, action } = menu
    return { path, query: { action } }
  }

  function actionMenuSelect(menu, from) {
    const action_id = menu.action
    function get_action_info(action_id) {
      const api = get_api()
      console.log('actionMenuSelect action_id', action_id)
      const action_obj = api.action(action_id)
      console.log('actionMenuSelect action_obj', action_obj)
      return action_obj.config

      // try {
      //   const action_obj = api.action(action_id)
      //   // console.log('action_obj', action_obj)
      //   return action_obj.config
      // } catch (error) {
      //   console.log('actionMenuSelect', action_id)
      //   console.log('actionMenuSelect', error)

      //   return undefined
      // }
    }

    const action = get_action_info(action_id)

    if (!action) {
      console.log('action not defined', action_id)
      throw 'action not defined: ' + action_id
      //   return
    }

    const is_action_new =
      action.type === 'ir.actions.act_window' && action.target === 'new'

    if (is_action_new) {
      return
    }

    const view_type = action.view_type || 'tree'
    const xml_id = action.xml_id

    const path = web
    const query = { view_type, action: xml_id, menu: menu.xml_id, web }

    if (!uniapp) {
      const routeVal = from

      if (routeVal) {
        const is_me =
          routeVal.path === path &&
          routeVal.query.view_type === query.view_type &&
          routeVal.query.menu === query.menu &&
          Object.keys(routeVal.query).sort().toString() ==
            Object.keys(query).sort().toString()

        if (is_me) {
          return
        }
      }
    }

    return { path, query }
  }

  return {
    load_menus,
    select_menu
  }
}
