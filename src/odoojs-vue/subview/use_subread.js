import { computed } from 'vue'

function useSubRead({ props }) {
  const { get_api } = props.parentInfo

  function getRN() {
    const { relationNode } = props
    if (relationNode) {
      const api = get_api()
      return api.relation_node(relationNode)
    } else {
      return undefined
    }
  }

  const RN = computed(() => {
    const RN = getRN()
    return RN
  })

  const formview = computed(() => {
    const rnv = RN.value
    if (!rnv) {
      return undefined
    }
    return rnv.form
  })

  const sheet = computed(() => {
    if (!formview.value) {
      return {}
    }

    const record = props.record
    const values = {}

    const res = formview.value.get_sheet({
      editable: !props.readonly,
      parent_info: props.parentInfo,
      record,
      values
    })

    return res
  })

  const viewInfo = computed(() => {
    const view = formview.value

    if (!view) {
      return {}
    }

    const record = props.record
    const values = {}

    const info = {
      get_api,
      model: view.model,
      metadata: view.metadata,
      record,
      values,
      parent_info: props.parentInfo
    }
    // console.log(info)
    return info
  })

  return {
    sheet,
    viewInfo
  }
}

export function useO2mRead({ props }) {
  return useSubRead({ props })
}

export function useM2mRead({ props }) {
  return useSubRead({ props })
}
