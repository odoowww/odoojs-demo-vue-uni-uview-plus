import { useM2mTree } from './use_subtree'

export function useM2mNew({ props }) {
  return useM2mTree({ props })
}
