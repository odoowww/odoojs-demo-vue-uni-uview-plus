import { computed, watch, reactive, toRaw } from 'vue'

function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

export function useO2mEdit({ props }) {
  const { get_api } = props.parentInfo
  function getRN() {
    const { relationNode } = props
    if (relationNode) {
      const api = get_api()
      return api.relation_node(relationNode)
    } else {
      return undefined
    }
  }

  const RN = computed(() => {
    const RN = getRN()
    return RN
  })

  //

  const localState = {
    view: null,
    Editmodel: null
  }

  const state = reactive({
    record_display: {},
    record: {},
    values: {},
    data_ready: 0
  })

  function view_get() {
    if (state.data_ready) {
      return localState.view
    }
  }

  const sheet = computed(() => {
    const view = view_get()
    if (!view) {
      return {}
    }

    const record = state.record
    const values = state.values

    const res = view.get_sheet({
      editable: true,
      parent_info: toRaw(props.parentInfo),
      record,
      values
    })

    return res
  })

  const viewInfo = computed(() => {
    const view = view_get()

    if (!view) {
      return {}
    }

    const record = state.record
    const values = state.values

    const info = {
      get_api,
      editable: true,
      model: view.model,
      metadata: view.metadata,
      record,
      values,
      parent_info: props.parentInfo
    }
    // console.log(info)
    return info
  })

  // watch props.key
  watch(
    () => props.keyindex,
    async (newVal, oldVal) => {
      // console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        await sleep(100)
        await init()
      }
    },
    { immediate: true }
  )

  async function init() {
    const { record } = props

    state.record_display = { ...record }
    state.record = { ...record }
    state.values = {}

    const rnv = RN.value

    const formview = rnv.form
    localState.view = formview

    const editmodel = formview.new_editmodel()
    localState.Editmodel = editmodel

    state.record_display = editmodel.set_edit({
      record: { ...state.record },
      values: { ...state.values },
      parent_info: props.parentInfo || {}
    })

    const rid = record.id

    if (rid) {
      // edit
    } else {
      // new
      // new view. 获取 default
      const res = await editmodel.onchange()
      const { values: values2, domain, record_display } = res
      // console.log(res)
      state.record_display = record_display
      state.values = { ...values2 }
    }

    state.data_ready += 1
  }

  async function onChange(fname, value) {
    console.log('onChange ', fname, value)
    const editmodel = localState.Editmodel
    if (!editmodel) {
      return
    }

    const res3 = await editmodel.onchange(fname, value)
    const { values: values2, domain, record_display } = res3
    state.record_display = { ...record_display }
    state.values = { ...values2 }
    state.data_ready += 1
  }

  async function commit() {
    const editmodel = localState.Editmodel
    if (!editmodel) {
      return
    }

    const res3 = await editmodel.commit()

    const { values, record_display } = res3
    state.record_display = { ...record_display }
    state.values = { ...values }

    // const rid = state.record.id
    const val = state.values
    // console.log(rid, val)

    return val
  }

  return {
    record_display: computed(() => {
      return state.record_display
    }),
    sheet,

    viewInfo,
    onChange,
    commit
  }
}
