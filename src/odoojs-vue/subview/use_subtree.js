import { computed } from 'vue'

function useSubTree({ props }) {
  const { get_api } = props.parentInfo

  const Relation = computed(() => {
    if (!props.relationNode) {
      return undefined
    }

    const api = get_api()
    return api.relation_node(props.relationNode)
  })

  const treeview = computed(() => {
    if (!Relation.value) {
      return undefined
    }
    return Relation.value.tree
  })

  const viewInfo = computed(() => {
    const view = treeview.value

    if (!view) {
      return {}
    }

    return {
      get_api,
      model: view.model,
      metadata: view.metadata,
      parent_info: props.parentInfo
    }
  })

  const kanban = computed(() => {
    if (!treeview.value) {
      return {
        card_title: {},
        card_label: {},
        card_value: {}
      }
    }

    const kb = treeview.value.get_kanban({ parent_info: props.parentInfo })
    return kb.children
  })

  const sheet = computed(() => {
    if (!treeview.value) {
      return {}
    }
    return treeview.value.get_sheet({ parent_info: props.parentInfo })
  })

  const sheet_fields = computed(() => {
    return sheet.value.children || {}
  })

  function fields2cols(fields) {
    const cols = Object.keys(fields).map(fld => {
      const info = fields[fld] || {}
      //   console.log(fld, info)
      return {
        dataIndex: info.name,
        key: fld,
        title: info.string,
        ...info
        // sorter: 'sorter' in info ? info.sorter : true

        //   ellipsis: 'ellipsis' in meta ? meta.ellipsis : false,
        //   width: meta.web_col_width,
      }
    })

    return cols
  }

  const columns = computed(() => {
    const cols91 = fields2cols(sheet_fields.value)
    const cols92 = cols91.filter(item => item.optional !== 'hide')
    const cols = cols92.filter(item => item.tagname === 'field')
    // console.log('cols91', cols91)
    return cols
  })

  return {
    sheet_fields,
    table_columns: columns,
    kanban,
    viewInfo
  }
}

export function useO2mTree({ props }) {
  return useSubTree({ props })
}

export function useM2mTree({ props }) {
  return useSubTree({ props })
}
