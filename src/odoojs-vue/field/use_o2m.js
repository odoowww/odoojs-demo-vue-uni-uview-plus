import { useFieldTypeBase } from './use_field_type_base'

import { computed, toRaw } from 'vue'

export function useWOne2many({ props }) {
  const { value_raw, ...usedata } = useFieldTypeBase({ props })

  const value_display = computed(() => {
    const val = value_raw.value || []
    const { node = {} } = props
    if (!node.order) {
      return val
    }

    function val_get(one) {
      const odrs = node.order.split('.')
      return odrs.reduce((acc, cur) => {
        return acc[cur]
      }, one)
    }

    const val2 = [...val]
    val2.sort((a, b) => {
      return val_get(a) - val_get(b)
    })

    return val2
  })

  const infoAsParent = computed(() => {
    return {
      ...props.viewInfo,
      record: props.record
    }
  })

  const buttons = computed(() => {
    const { buttons = {} } = props.node
    const buttons2 = { ...buttons }
    if (!('create' in buttons)) {
      buttons2.create = true
    }
    if (!('edit' in buttons)) {
      buttons2.edit = true
    }
    if (!('delete' in buttons)) {
      buttons2.delete = true
    }

    return buttons2
  })

  const datastore = computed(() => {
    const fname = props.node.name
    const { record = {}, values = {} } = props.viewInfo || {}

    const records = record[fname] || []
    const values_me = values[fname] || []

    return {
      records,
      values: values_me
    }
  })

  const { get_api } = props.viewInfo

  function getRelationNode() {
    const { node } = props
    const api = get_api()
    return api.relation_node(node)
  }

  const Relation = computed(() => {
    const RN = getRelationNode()
    return RN.relation
  })

  function GetRelation() {
    const Rel = Relation.value
    Rel.set_edit({
      ...datastore.value
    })

    return Rel
  }

  function rowRemove(row) {
    // console.log('onRowRemove ')
    const Rel = GetRelation()
    if (Rel) {
      const RN = getRelationNode()
      const records_edit = RN.remove_one(toRaw(row), {
        ...toRaw(datastore.value)
      })

      return records_edit
    }
  }

  function rowCommit(row, val) {
    // console.log('onRowCommit ')
    const Rel = GetRelation()
    if (Rel) {
      const res = Rel.upinsert_one(row.id, val)
      //   console.log('rowCommit', res)

      const records_edit = Rel.records_edit
      return records_edit
    }
  }

  return {
    value_raw,
    ...usedata,
    value_display,
    buttons,
    infoAsParent,
    rowCommit,
    rowRemove
  }
}
