import { computed, ref, reactive } from 'vue'
import { useFieldTypeBase } from './use_field_type_base'

export function useWMany2one({ props }) {
  const {
    value_raw,
    change: superChange,
    ...usedata
  } = useFieldTypeBase({ props })

  const value_display = computed(() => {
    const val = value_raw.value || {}

    return val.display_name
  })

  const value_edit = computed(() => {
    const val = value_raw.value || {}

    const val2 = val.id ? { value: val.id, label: val.display_name } : undefined
    return val2
  })

  function formatInput(val) {
    if (val) {
      const { value = null, label, record = {} } = val
      // todo label 为 空
      const label2 = label && label.trim()
      const val2 = { id: value, ...record, display_name: label2 }
      return val2
    } else {
      const val2 = { id: null, display_name: null }
      return val2
    }
  }

  function change(val) {
    const val2 = formatInput(val)
    return superChange(val2)
  }

  const keyword = ref('')
  const options = ref([])
  const records = ref([])

  const pagination = ref({
    total: 0,
    current: 1,
    pageSize: 8
  })

  function _get_rn() {
    const { get_api } = props.viewInfo
    const api = get_api()
    return api.relation_node(props.node)
  }

  async function _loadSelectOptions(kw = {}) {
    const rel = _get_rn()
    const record = props.record || {}

    const res = await rel.load_select_options({ record, ...kw })

    const { total, current, pageSize, records } = res

    pagination.value = { total, current, pageSize }
    const ops1 = records
    return ops1.map(item => {
      return { value: item.id, label: item.display_name, record: item }
    })
  }

  async function _loadOptions(kw = {}) {
    const ops = await _loadSelectOptions(kw)

    records.value = ops
    return ops
  }

  async function loadOptions(kw = {}) {
    const { name = '' } = kw
    keyword.value = name
    const ops = await _loadOptions(kw)
    options.value = ops
  }

  async function loadPrev() {
    const name = keyword.value
    const cur = pagination.value.current
    if (cur > 1) {
      const current = pagination.value.current - 1
      await loadOptions({ current, name })
    }
  }

  async function loadNext() {
    const name = keyword.value
    const cur = pagination.value.current
    const len = pagination.value.total
    const lim = pagination.value.pageSize
    if (cur * lim < len) {
      const current = pagination.value.current + 1
      await loadOptions({ current, name })
    }
  }

  return {
    value_raw,
    ...usedata,
    value_display,
    value_edit,
    change,
    options,
    records,
    keyword,
    pagination,
    loadOptions,
    loadPrev,
    loadNext
  }
}
