import { computed, toRaw } from 'vue'

export function useFieldTypeBase({ props }) {
  const readonly = computed(() => {
    return !props.editable || props.node.readonly
  })

  const value_raw = computed(() => {
    const fname = props.node.name
    const record = props.record || {}
    const val = record[fname]
    return val
  })

  const placeholder = computed(() => {
    return props.node.placeholder || props.node.string
  })

  const label = computed(() => {
    if (props.node.nolabel) {
      return
    }

    const label_nodes = Object.keys(props.node).filter(
      item => item.split('_')[0] === 'label'
    )

    const inlbl = label_nodes.reduce((acc, cur) => {
      acc = props.node[cur].string
      return acc
    }, '')

    if (inlbl) {
      return inlbl
    }

    return props.node.string
  })

  const field_name = computed(() => {
    return props.node.name
  })

  function change(val) {
    // console.log(field_name.value, val)
    return [field_name.value, val]
  }

  return {
    readonly,
    label,
    value_raw,
    value_display: value_raw,
    value_edit: value_raw,
    placeholder,
    change
  }
}

export function useWBoolean({ props }) {
  return useFieldTypeBase({ props })
}

export function useWSelection({ props }) {
  const {
    value_raw,
    change: superChange,
    ...usedata
  } = useFieldTypeBase({ props })

  const value_display = computed(() => {
    // const { meta = {} } = props.node
    // const { selection = [] } = meta

    // const options = selection.reduce((acc, cur) => {
    //   acc[cur[0]] = cur[1]
    //   return acc
    // }, {})
    // const val = value_raw.value
    // return ops[val] || val

    const val = value_raw.value
    return value_edit.value.label || val
  })

  const value_edit = computed(() => {
    const ops = options.value.reduce((acc, cur) => {
      acc[cur.value] = cur
      return acc
    }, {})

    const val = value_raw.value
    return ops[val] || {}
  })

  const options = computed(() => {
    const { meta = {}, selection: ops_in_node } = props.node
    const { selection: ops_in_meta = [] } = meta

    const selection = ops_in_node ? ops_in_node : ops_in_meta

    return selection.map(item => {
      return {
        label: item[1],
        value: item[0]
      }
    })
  })

  function formatInput(val) {
    console.log(val)
    if (val) {
      const { value, label } = val
      const val2 = value || null
      return val2
    } else {
      const val2 = null
      return val2
    }
  }

  function change(val) {
    const val2 = formatInput(val)
    return superChange(val2)
  }

  return {
    value_raw,
    ...usedata,
    value_display,
    value_edit,
    change,
    options
  }
}

export function useWChar({ props }) {
  return useFieldTypeBase({ props })
}

export function useWImageUrl({ props }) {
  const usedata = useFieldTypeBase({ props })

  const { get_api } = props.viewInfo

  const image_url = computed(() => {
    const fname = props.node.name
    const record = props.record
    const val = record[fname]
    if (val) {
      const api = get_api()

      return `${api.baseURL}${val}`
    } else {
      return undefined
    }
  })

  return { ...usedata, image_url }
}

export function useWImage({ props }) {
  const usedata = useFieldTypeBase({ props })

  const { readonly } = usedata

  const image_url = computed(() => {
    const fname = props.node.name
    const record = props.record
    const val = record[fname]

    if (val && record.id) {
      if (readonly) {
        const { get_api, model } = props.viewInfo
        const api = get_api()

        const unique = Number(new Date())

        const params = { model, id: record.id, field: fname, unique }
        const params2 = Object.keys(params).map(
          item => `${item}=${params[item]}`
        )

        const params3 = params2.join('&')

        const url = `${api.baseURL}/web/image?${params3}`

        return url

        // http://localhost:8069/web/image?
        // model=res.partner
        // &id=3
        // &field=avatar_128
        // &unique=1710823899000
      } else {
        return undefined
      }

      // return `data:image/jpeg;base64,${val}`
    } else {
      return undefined
    }
  })

  return { ...usedata, image_url }
}

export function useWText({ props }) {
  return useFieldTypeBase({ props })
}

export function useWHtml({ props }) {
  return useFieldTypeBase({ props })
}

export function useWInteger({ props }) {
  return useFieldTypeBase({ props })
}
export function useWFloat({ props }) {
  return useFieldTypeBase({ props })
}
export function useWMonetary({ props }) {
  return useFieldTypeBase({ props })
}
