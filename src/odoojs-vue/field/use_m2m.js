import { useFieldTypeBase } from './use_field_type_base'

import { computed, ref } from 'vue'

export function useWMany2many({ props }) {
  const { value_raw, ...usedata } = useFieldTypeBase({ props })

  const value_display = computed(() => {
    const val = value_raw.value || []
    return val
  })

  const infoAsParent = computed(() => {
    return {
      ...props.viewInfo,
      record: props.record
    }
  })

  function rowRemove(row) {
    // console.log('onRowRemove ', row, value_display.value)
    const value2 = value_display.value.filter(item => item.id !== row.id)
    return value2
  }

  function rowSelect(rows) {
    // console.log('onRowSelect ', rows)
    const oldv = value_display.value
    const value2 = [...oldv, ...rows]
    return value2
  }

  const { get_api } = props.viewInfo

  function getRelationNode() {
    const { node } = props
    const api = get_api()
    return api.relation_node(node)
  }

  const RelationNode = computed(() => {
    const RN = getRelationNode()
    return RN
  })

  const treeview = computed(() => {
    if (!RelationNode.value) {
      return undefined
    }
    return RelationNode.value.tree
  })

  async function getTreeOptions() {
    if (!treeview.value) {
      return []
    }
    const view = treeview.value
    const ids = value_display.value.map(item => item.id)
    const domain = ['!', ['id', 'in', ids]]
    const records = await view.search_read(domain)
    return records
  }

  return {
    value_raw,
    ...usedata,
    value_display,
    infoAsParent,
    rowRemove,
    rowSelect,
    getTreeOptions
  }
}
