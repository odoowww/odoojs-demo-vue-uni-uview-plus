import { computed } from 'vue'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

import { useFieldTypeBase } from './use_field_type_base'

export function useWDate({ props }) {
  const usedata = useFieldTypeBase({ props })

  const { value_raw } = usedata

  function utc2local(str) {
    if (str) {
      const dt = dayjs.utc(str)
      const str2 = dt.local().format('YYYY-MM-DD')
      return str2
    }

    return str
  }

  const value_display = computed(() => {
    const val = value_raw.value

    const { meta = {} } = props.node

    if (meta.type === 'datetime') {
      const val2 = utc2local(val)
      return val2
    } else {
      return val
    }
  })

  const value_edit = computed(() => {
    const val = value_raw.value
    const val2 = utc2local(val)
    return val2
  })

  return {
    ...usedata,
    value_display,
    value_edit
  }
}

export function useWDatetime({ props }) {
  const usedata = useFieldTypeBase({ props })

  const { value_raw } = usedata

  function utc2local(str) {
    if (str) {
      const dt = dayjs.utc(str)
      const str2 = dt.local().format('YYYY-MM-DD HH:mm:ss')
      return str2
    }

    return str
  }

  const value_display = computed(() => {
    const val = value_raw.value
    const val2 = utc2local(val)
    return val2
  })

  const value_edit = computed(() => {
    const val = value_raw.value
    const val2 = utc2local(val)
    return val2
  })

  return {
    ...usedata,
    value_display,
    value_edit
  }
}
