import { computed, ref } from 'vue'
import { useFieldTypeBase } from './use_field_type_base'

export function useWM2mTags({ props }) {
  const {
    value_raw,
    change: superChange,
    ...usedata
  } = useFieldTypeBase({ props })

  const value_display = computed(() => {
    const val = value_raw.value || []
    const val2 = val.map(item => item.display_name)
    return val2
  })

  const value_edit = computed(() => {
    const val = value_raw.value || []
    return val.map(item => ({ value: item.id, label: item.display_name }))
  })

  function change(val) {
    const val2 = val.map(item => {
      return { id: item.value, display_name: item.label.trim() }
    })

    return superChange(val2)
  }

  const keyword = ref('')
  const options = ref([])
  const records = ref([])

  const pagination = ref({
    total: 0,
    current: 1,
    pageSize: 8
  })

  function _get_rn() {
    const { get_api } = props.viewInfo
    const api = get_api()
    return api.relation_node(props.node)
  }

  async function _loadSelectOptions(kw = {}) {
    const rel = _get_rn()
    const record = props.record || {}

    const res = await rel.load_select_options({ record, ...kw })

    const { total, current, pageSize, records } = res

    pagination.value = { total, current, pageSize }
    const ops1 = records
    return ops1.map(item => {
      return { value: item.id, label: item.display_name }
    })
  }

  async function _loadOptions(kw = {}) {
    const ops = await _loadSelectOptions(kw)
    records.value = ops
    return ops
  }

  async function loadOptions(kw = {}) {
    const { name = '' } = kw
    keyword.value = name
    const ops = await _loadOptions(kw)
    options.value = ops
  }

  async function loadPrev() {
    const name = keyword.value
    const cur = pagination.value.current
    if (cur > 1) {
      const current = pagination.value.current - 1
      await loadOptions({ current, name })
    }
  }

  async function loadNext() {
    const name = keyword.value
    const cur = pagination.value.current
    const len = pagination.value.total
    const lim = pagination.value.pageSize
    if (cur * lim < len) {
      const current = pagination.value.current + 1
      await loadOptions({ current, name })
    }
  }

  return {
    value_raw,
    ...usedata,
    value_display,
    value_edit,
    change,
    options,
    records,
    keyword,
    pagination,

    loadOptions,
    loadPrev,
    loadNext
  }
}
