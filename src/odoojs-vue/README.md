## odoojs-vue

logic components for odoojs.

## contact us

contact us: odoojs@outlook.com

## odoojs 前端开发流程

1. 创建 vue 项目
2. 完成三件事. 接口, 菜单, 路由
3. 使用物理组件 实现页面
4. 扩展物理组件
5. 物理组件的实现用到逻辑组件

## 客户端开发

### 客户端开发三件事

1. 定义 服务端接口
2. 定义 菜单
3. 定义 路由及页面

### 客户端开发三件事 详细说明

1. 定义 odoojs/index.js 文件. 参考 odoojs-api 说明
2. 定义路由: uniapp 是 "path": "pages/web/index",
3. 定义路由: pc 端 用 vue-router, 是 ......,
4. 组织 菜单, 使用 useMenu
5. 菜单点击后 使用 select_menu 跳转到上述路由
6. 定义 路由页面, 使用 useAction, 用物理组件 UWebView 实现 路由页面
7. 如果需要自定义扩展 新组件. 用自定义组件 WebView 替换这里的 UWebView
8. 关于自定义扩展 新组件, 见下节

### 客户端开发 自定义扩展 新组件的方法

1. 需要准备的组件 有 WebView, Node, UNodeField, NodeWidget
2. 使用到的 通用物理组件有 UWebView, UNode, UNodeField, UNodeWidget
3. 完成三件事. 仅最后一步 改为 用 组件 WebView 实现路由页面
4. 组件 WebView 中, 使用 UWebView, 用组件 Node 实现其插槽
5. 组件 Node 中, 使用 UNode,
   实现其插槽 sub-node, tree-field, field, tree-widget, widget
6. 插槽 sub-node 用 组件 Node
7. 插槽 tree-field, field 用 组件 NodeField
8. 插槽 tree-widget, widget 用 自定义组件 NodeWidget
9. 组件 NodeField 中, 使用 UNodeField.
   实现插槽 sub-tree, sub-read, sub-edit. 用组件 Node,
   实现插槽 todo, 这里扩展 新的组件
   其他插槽 可覆盖 已有的通用物理组件
10. 组件 NodeWidget 中, 使用 UNodeWidget.
    实现插槽 todo, 这里扩展 新的组件
    其他插槽 可覆盖 已有的通用物理组件

## 物理组件

1. 不同的 ui 库 , 做不同的 odoojs 物理组件库
2. 组件对外名称一致
3. 已经实现的 odoojs 物理组件库:
4. odoojs-uview-plus
5. odoojs-antdv
6. 若有自定义组件, 则需要自定义以下组件, 以代替组件库中的对应组件
7. WebView, 对应 组件库中的 UWebView
8. Node, 对应 组件库中的 UNode
9. NodeField, 对应 组件库中的 UNodeField
10. NodeWidget, 对应 组件库中的 UNodeWidget

### 组件列表

1. UWebView,
2. UNode,
3. UNodeField,
4. UNodeWidget

### 视图组件 UWebView

1. 用户视图组件 WebView 的 基础视图组件
2. useWeb

## 逻辑组件说明

1. odoojs 的逻辑组件库, 用于 vue 架构
2. useMenu
3. useAction
4. useWeb
5. useTree, useRead, useEdit
6. useNode
7. ONode, ONodeApp
8. useWChar 等. 名称 格式 'useW' + Field_type
9. useO2mTree, useO2mRead, useO2mEdit, useM2mTree, useM2mRead, useM2mNew

### 菜单管理 useMenu

#### 接口

1. useMenu
2. 入口参数: {get_api, web, uniapp}
3. get_api: 参考 odoojs-api 的初始化文件
4. web: 路由. 如 '/web', 'pages/web/index'
5. uniapp: 标志. 如果是 uniapp 架构. 值为 true
6. 返回值: {load_menus, select_menu}

#### load_menus

1. load_menus, 函数, 获得 菜单树
2. 入口参数 空
3. 返回 {menus_tree, menus_data}
4. menus_tree, 数组, 树状结构的菜单树
5. menus_data, 对象, 所有的菜单, 平行结构

#### select_menu

1. select_menu, 函数, 点击菜单的处理函数
2. 入口参数 menu, from
3. menu, 被点击的菜单 xml_id
4. from, 点击菜单前, 当前的菜单 xml_id
5. 返回 { path, query: { menu, action, view_type } }
6. 返回结果可以直接用于路由跳转.
7. path, 跳转的路由 如 '/web', 'pages/web/index'
8. action, action 的 xml_id
9. view_type, view 类型 如 'tree', 'form'

### Action 管理 useAction

#### 接口

1. useAction
2. 入口参数: {get_api}
3. get_api: 参考 odoojs-api 的初始化文件
4. 返回值: {load_action, config}
5. load_action 附后
6. config 附后
7. reload_action. 参数无, 返回值无. 在 onshow 事件中调用. 返回时刷新页面

#### load_action

1. load_action, 函数, 初始化 action 信息
2. 参数: { action, view_type }
3. 即是 路由的 query 参数

#### config

1. config: action 的配置信息, 可直接作为页面组件的参数
2. config = {action, metadata}
3. action: action 的 xml_id
4. metadata: 该 action 的所有字段

### 根页面组件 WebView 的 管理接口 useWeb

1. useWeb,
2. 入口参数 { props: config:{} }
3. config 是 useAction 返回的 config
4. 返回 { view_type, editable, config,  
   rowClick, rowNew, rowEdit, rowDel, rowCancel, rowCommit}
5. view_type: tree, form, 当前页面的类型
6. editable: view_type=form 时, 是否是编辑新增页面
7. config: 附后
8. rowClick: TreeView 的行点击事件, 参数 {rowid}. 返回 路由跳转参数
9. rowNew: TreeView 的新增按钮点击事件, 参数无. 返回 路由跳转参数
10. rowEdit: ReadView 的编辑按钮 点击事件, 参数无. 返回无
11. rowDel: ReadView 的删除按钮 点击事件, 参数无. 返回 路由跳转参数
12. rowCancel: ReadView 的返回按钮 点击事件, 参数无. 返回 路由跳转参数
13. rowCancel: EditView 的取消或返回按钮 点击事件, 参数无. 返回 路由跳转参数
14. rowCommit: EditView 的提交按钮 点击事件, 参数 rowid. 返回 路由跳转参数

#### config

1. 可以直接用于根页面的子组件 TreeView, ReadView, EditView 的参数
2. config={action, metadata, rowid}
3. action 当前的 action xml_id
4. metadata 当前 action 的所有字段
5. rowid 当前 form 页面的数据的 id

### 列表页面组件 TreeView 的 管理接口 useTree

1. useTree
2. 入口参数 { props: config:{} }
3. config 是 useWeb 返回的 config
4. 返回 { load_data, load_prev, load_next,  
   records, pagination, table_columns, kanban,
   buttons, title, viewInfo,
   toolbar, btnClick, wizard_config,
   searchSheet, searchDefault, searchSubmit}
5. buttons={create, edit, delete} 是否显示这三个按钮
6. title, 页面的标题
7. records: 数据, 可作为 table 的数据源
8. table_columns: Table 的 列
9. pagination={total, pageSize, current} 翻页控制
10. load_data: 翻页事件, 参数 { current, pageSize, order }
11. load_prev: 向前翻页, 已到首页, 则无作用
12. load_next: 向后翻页, 已到尾页, 则无作用
13. kanban={card_title, card_label, card_value}, 移动端或看板显示时, 看板的模版结构
14. viewInfo={model, metadata}, 渲染节点使用
15. searchSheet, 搜索的节点
16. searchDefault, 搜索的默认值
17. searchSubmit 搜索完成提交函数
18. toolbar, 工具条显示 按钮
19. btnClick, 工具条 按钮 点击后 调用
20. wizard_config. 工具条 按钮 若触发 wizard 窗口. 则 wizard_config 是窗口的参数

#### 搜索的使用

1. 用组件 SearchView 实现搜索

#### pc 端的页面渲染

1. pc 端 以 table 渲染.
2. table 的参数:
3. dataSource=records
4. pagination=pagination
5. columns=table_columns
6. change 事件 = load_data({ current, pageSize, order })
7. table 的 bodyCell 插槽, 参数 { column, record}
8. bodyCell 用 节点组件渲染. 参数:
9. {node:column, record, viewInfo, parentNodeType:"tree"}

#### 移动端或看板渲染.

1. 看板渲染 用组件 NodeTree
2. 组件 NodeTree 的参数: {records, kanban,viewInfo} 作为参数
3. 组件 NodeTree 的 默认插槽, 参数 { node, record, viewInfo}
4. 插槽 用 节点组件渲染. 参数 { node, record, viewInfo }

### 只读表单页面组件 ReadView 的 管理接口 useRead

1. useRead,
2. 入口参数 { props: config:{} }
3. config 是 useWeb 返回的 config
4. 返回 { record, sheet, viewInfo, buttons, title,
   toolbar, statusbar,
   onDelete,
   btnClick, reloadData, wizard_config }
5. title, 页面的标题
6. buttons={create, edit, delete} 是否显示这三个按钮
7. onDelete: ReadView 删除按钮触发, 返回 True
8. toolbar, 工具条 按钮
9. btnClick, 工具条 按钮 点击后 调用
10. wizard_config. 工具条 按钮 若触发 wizard 窗口. 则 wizard_config 是窗口的参数

#### PC 端 ReadView 页面的渲染

1. 布局用 form 标签 <form>. 内部各字段 用 <form-item>
2. <form> 内部, 用节点组件 参数:
3. {record, sheet, viewInfo, editable:false}

#### 移动端 ReadView 页面的渲染

1. 直接 渲染 节点组件, 参数:
2. {record, sheet, viewInfo, editable:false}

### Wizard 页面组件 WizardView 的 管理接口 useWizard

1. useWizard
2. 入口参数 { props: config:{} }
3. config 是 ReadView 或 TreeView 返回的 wizard_config
4. 返回 { record, sheet, viewInfo,onChange, toolbar, btnClick}
5. record: 数据
6. sheet 页面模版,
7. viewInfo={model, metadata}, 渲染节点使用
8. onChange: 字段编辑后触发, 返回 无
9. toolbar 工具条 按钮
10. btnClick 工具条 按钮 点击触发

### 编辑表单页面组件 EditView 的 管理接口 useEdit

1. useEdit,
2. 入口参数 { props: config:{} }
3. config 是 useWeb 返回的 config
4. 返回 { record, sheet, viewInfo, buttons, title,
   onChange, onCommit }
5. title, 页面的标题
6. onChange: EditView 字段编辑后触发, 返回 无
7. onCommit: EditView 提交按钮触发, 返回 数据 id
8. record: 数据
9. sheet 页面模版,
10. viewInfo={model, metadata}, 渲染节点使用

#### PC 端 EditView 页面的渲染

1. form 标签 <form> 的参数 {model:record}
2. <form> 内部, 用节点组件, 参数:
3. { record, sheet, viewInfo, editable:true, change: onChange}

#### 移动端 EditView 页面的渲染

1. form 标签 <form> 的参数 {model:record}
2. <form> 内部, 用节点组件, 参数:
3. { record, sheet, viewInfo, editable:true, change: onChange}

### 节点组件的管理接口 useNode

1. useNode
2. 入口参数 {props:{node} }
3. 返回 { nodeFieldWidget, nodeFieldType, nodeWidgetName }
4. nodeFieldWidget: field 类型的节点, 节点的 widget 值
5. nodeFieldType: field 类型的节点, 节点的 字段类型 值
6. nodeWidgetName: widget 类型的节点, 节点的 name 值

### 节点 逻辑组件 ONode

1. 是 PC 端 物理组件 Node 的基础
2. 是 移动端 逻辑组件 ONodeApp 的基础
3. 参数 { node, parentNodeType}
4. parentNodeType 的值域[ tree, notebook, grid, undefined]

#### 对外插槽:

1. notebook, 参数 { node } 用 Notebook 物理组件渲染
2. group-grid, 参数 { node } 用 Grid 物理组件渲染
3. group-title, 参数 { text }
4. sub-group-title, 参数 { text }
5. label, 参数 { text }
6. button, 参数 { node } 用 Node 物理组件渲染
7. widget, 参数 { node } 用 Node 物理组件渲染
8. field, 参数 { node } 用 Node 物理组件渲染
9. tree-button, 参数 { node } 用 Node 物理组件渲染
10. tree-widget, 参数 { node } 用 Node 物理组件渲染
11. tree-field, 参数 { node } 用 Node 物理组件渲染

### 移动端或看板用 节点 逻辑组件 ONodeApp

1. 是 移动端物理组件 Node 的基础
2. 参数 { node, parentNodeType}
3. parentNodeType 的值域[ tree, notebook, undefined]

#### 对外插槽:

1. 无 group-grid 插槽,
2. 其余 同 逻辑组件 ONode

### 字段 接口函数

1. 有很多. 每个类型的字段, 都有一个接口函数
2. 所有的函数, 入口参数 是一样的
3. 返回结果. 一些函数 有额外的值
4. 返回结果中无额外值的 接口函数, 称为 一般接口函数
5. 返回结果中有额外值的 接口函数, 称为 特殊接口函数

#### 一般接口函数

1. useWBoolean,
2. useWChar,
3. useWText,
4. useWHtml,
5. useWInteger,
6. useWFloat,
7. useWMonetary,

#### 特殊接口函数

1. useWImageUrl
2. useWImage
3. useWSelection
4. useWDate
5. useWDatetime
6. useWMany2one
7. useWM2mTags
8. useWMany2many
9. useWOne2many

#### 一般接口函数 说明

1. 入口参数: { node, editable, record, }
2. 返回结果为:
   { readonly, label, value_display, value_edit, placeholder, change }
3. readonly 当前字段是否可编辑
4. label 字段的标签
5. value_display, 只读时, 字段的显示值
6. value_edit, 编辑时, 字段的数据值
7. placeholder, 编辑时用
8. change, 字段编辑后触发
9. change 的参数 val
10. change 的 返回结果 [ field_name, val]
11. 返回值 field_name 是 字段名称
12. 返回值 val 是标准化以后的数据值

#### 图片字段的 接口函数 useWImageUrl useWImage

1. 额外返回 {image_url}, 可直接用于图片渲染
2. useWImage. 待扩展 图片上传下载函数

#### 选择字段的 接口函数 useWSelection

1. useWSelection, 额外返回 {options}, 可用于下拉框选项

#### 日期时间字段 接口函数 useWDate, useWDatetime

1. 返回结果中, 对数据做了 本地化转换处理

#### m2o 字段 接口函数 useWMany2one

1. 额外返回 { options, keyword, loadOptions,
   records, pagination, loadPrev, loadNext},
2. options 下拉框选项
3. keyword 搜索框的当前值
4. loadOptions 异步更新 options
5. 列表方式 展示 可选项时的数据值 records, pagination
6. loadPrev, loadNext 向前向后翻页, 更新 options

#### m2m 字段 Tags 方式的 接口函数 useWM2mTags

1. 与 useWMany2one 相同

#### m2m 字段 Tree 方式的 接口函数 useWMany2many

1. 额外返回 { infoAsParent, rowRemove, rowSelect, getTreeOptions}
2. infoAsParent={} 子页面视图 需要的 parentInfo
3. rowRemove 删除一行时的, 接口函数.
4. rowRemove 的参数 {id,...}.
5. rowRemove 的返回值 数组, 可直接用 字段的 onchange
6. rowSelect 添加行时的 接口函数
7. rowSelect 的参数 [{id,...}].
8. rowSelect 的返回值 数组, 可直接用 字段的 onchange
9. getTreeOptions 添加行时, 可选项 数据接口
10. getTreeOptions 的参数无
11. getTreeOptions 的返回值 数组, 可直接用 可选项目 列表展示

#### o2m 字段 Tree 方式的 接口函数 useWOne2many

1. 额外返回 { infoAsParent, rowRemove, rowCommit}
2. infoAsParent={} 子页面视图 需要的 parentInfo
3. rowRemove 删除一行时的, 接口函数.
4. rowRemove 的参数 {id,...}.
5. rowRemove 的返回值 数组, 可直接用 字段的 onchange
6. rowCommit 编辑或新增行时的 接口函数
7. rowCommit 的参数 {id }，val={...}.
8. rowCommit 的返回值 数组, 可直接用 字段的 onchange

### o2m, m2m 字段 子组件的 接口函数

1. useO2mTree, useO2mRead, useO2mEdit, useM2mTree, useM2mRead, useM2mNew
2. 物理组件 WOne2many WMany2many 的 子组件需要 的 接口函数
3. WMany2many 的子组件 包括 WM2mTree,WM2mRead,WM2mNew
4. WMany2many 的子组件 包括 WO2mTree,WO2mRead,WO2mEdit

#### WO2mTree 的 接口函数 useO2mTree

1. 入口参数 { props:{parentInfo, relationNode} }
2. 返回 { table_columns, kanban, viewInfo }
3. table_columns: Table 的 列
4. kanban={card_title, card_label, card_value}, 移动端或看板显示时, 看板的模版结构
5. viewInfo={model, metadata, parent_info}, 渲染节点使用
6. 节点渲染方式 参考 TreeView

#### WO2mRead 的 接口函数 useO2mRead

1. 入口参数 { props:{readonly, record, parentInfo, relationNode} }
2. 返回 { sheet, viewInfo }
3. sheet 页面模版,
4. viewInfo={model, metadata, record, values, parent_info}, 渲染节点使用
5. 节点渲染方式 参考 ReadView

#### WO2mEdit 的 接口函数 useO2mEdit

1. 入口参数 { props:{readonly, record, parentInfo, relationNode} }
2. 返回 { record_display, sheet, viewInfo,onChange,
   commit }
3. record_display 编辑后 当前显示数据
4. sheet 页面模版,
5. viewInfo={model, metadata, record, values, parent_info}, 渲染节点使用
6. 节点渲染方式 参考 EditView

#### WM2mTree 的 接口函数 useM2mTree

1. 同 useO2mTree

#### WM2mRead 的 接口函数 useM2mRead

1. 同 useO2mRead

#### WM2mNew 的 接口函数 useM2mNew

1. 同 useO2mTree
