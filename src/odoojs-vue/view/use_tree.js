import { watch, ref, computed, reactive, toRaw } from 'vue'

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

export function useTree(ctx = {}) {
  const { props } = ctx

  function get_api() {
    const { api } = ctx
    if (api) {
      return api
    } else {
      const { config = {} } = props
      return config.get_api()
    }
  }

  const localState = {
    view: null,
    model: undefined
  }

  const state = reactive({
    metadata: {},
    kanban: { children: {} },
    sheet: { children: {} },
    searchSheet: { children: {} },
    searchDefault: {},
    records: [],
    total: 0,
    current: 1,
    pageSize: 10,
    data_ready: 0
  })

  function view_get() {
    if (state.data_ready > 0) {
      return localState.view
    } else {
      return {}
    }
  }

  // watch props.key
  watch(
    () => (props.config || {}).keyindex,
    async (newVal, oldVal) => {
      // console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        await sleep(100)
        await watch_key()
      }
    },
    { immediate: true }
  )

  async function watch_key() {
    load_view()
    await load_data()
  }

  function load_view() {
    const { config } = props

    function info_get() {
      const info = {}
      const { domain, context } = config
      if (domain) {
        info.domain = domain
      }
      if (context) {
        info.context = context
      }

      return info
    }

    const { action, metadata } = config

    const info = info_get()
    // console.log(config, info)

    const api = get_api()
    const act = api.action(action, { ...info, metadata: toRaw(metadata) })
    const treeview = act.tree
    // console.log(treeview)

    state.metadata = { ...metadata }
    const sheet = treeview.get_sheet()
    state.sheet = sheet
    const kanban = treeview.get_kanban()
    state.kanban = kanban

    const searchSheet = treeview.get_search_sheet()
    state.searchSheet = searchSheet
    const searchDefault1 = treeview.get_search_default()

    // console.log('kanban', kanban)

    state.searchDefault = searchDefault1
    // console.log(searchSheet)
    localState.view = treeview
    localState.model = treeview.model
    treeview.search_change(searchDefault1)
  }

  async function reloadData() {
    const current = state.current
    await load_data({ current })
  }

  async function load_data(vals = {}) {
    const view = localState.view
    const result = await view.load_data({ ...vals })
    const { current, pageSize, total, records } = result

    state.records = records
    state.total = total
    state.current = current
    state.pageSize = pageSize
    // console.log('records', records)
    state.data_ready += 1
  }

  const viewInfo = computed(() => {
    return {
      get_api,
      model: localState.model,
      metadata: toRaw(state.metadata)
    }
  })

  const buttons = computed(() => {
    const view = view_get()
    return view.buttons || {}
  })

  const toolbar = computed(() => {
    const view = view_get()
    if (view.get_toolbar_action) {
      const ss = view.get_toolbar_action()
      // console.log(view, ss)
      return ss
    } else {
      return {}
    }
  })

  const title = computed(() => {
    const view = view_get()
    return view.title || ''
  })

  const pagination = computed(() => {
    return {
      total: state.total,
      pageSize: state.pageSize,
      current: state.current
    }
  })

  async function load_prev() {
    const cur = state.current
    if (cur > 1) {
      const current = state.current - 1
      await load_data({ current })
    }
  }

  async function load_next() {
    const cur = state.current
    const len = state.total
    const lim = state.pageSize
    if (cur * lim < len) {
      const current = state.current + 1
      await load_data({ current })
    }
  }

  const searchSheet = computed(() => {
    return state.searchSheet
  })

  const searchDefault = computed(() => {
    return state.searchDefault
  })

  async function searchSubmit(val) {
    // console.log('searchSubmit', val, searchSheet.value)
    const view = view_get()
    view.search_change(val)
    await load_data({ current: 1 })
  }

  const sheet_fields = computed(() => {
    return state.sheet.children
  })

  const kanban = computed(() => {
    return state.kanban.children
  })

  function fields2cols(fields) {
    const cols = Object.keys(fields).map(fld => {
      const info = fields[fld] || {}
      //   console.log(fld, info)
      return {
        dataIndex: info.name,
        key: fld,
        title: info.string,
        ...info,
        sorter: 'sorter' in info ? info.sorter : true

        //   ellipsis: 'ellipsis' in meta ? meta.ellipsis : false,
        //   width: meta.web_col_width,
      }
    })

    return cols
  }

  const columns = computed(() => {
    const cols91 = fields2cols(sheet_fields.value)
    const cols92 = cols91.filter(item => item.optional !== 'hide')
    const cols = cols92.filter(item => item.tagname === 'field')
    // console.log('cols91', cols91)
    return cols
  })

  const wizard_config = ref({
    get_api,
    show: false,
    action: {},
    title: '',
    keyindex: 0
  })

  async function btnClick(btn, ids) {
    // console.log(btn, ids)

    const view = view_get()
    const { type, name, action_map = {} } = btn
    const records = ids.map(item => ({ id: item }))

    const res = await view.call_button({ type, name, action_map, records })

    const { result, error } = res

    // console.log(res)

    if (error) {
      console.log('完成, 返回 error', res)
      return { error }
    }

    if (!result) {
      await reloadData()
      return
    }

    const { action_name, context = {} } = result
    if (!action_name) {
      console.log(result)
      throw 'error' + JSON.stringify(result)
    }

    const api = get_api()
    const act = api.action(action_name, { context })

    // console.log(result, act)

    wizard_config.value.title = act.title

    wizard_config.value.action = { ...result }
    wizard_config.value.show = true
    // await sleep(100)
    wizard_config.value.keyindex += 1

    return { result }
  }

  return {
    pagination,
    records: computed(() => {
      return state.records
    }),
    load_data,
    load_prev,
    load_next,

    buttons,
    title,
    searchSheet,
    searchDefault,
    searchSubmit,
    sheet_fields,
    table_columns: columns,
    kanban,
    viewInfo,

    toolbar,
    btnClick,
    wizard_config,
    reloadData
  }
}
