import { watch, computed, ref, reactive, toRaw } from 'vue'

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

export function useWeb(ctx = {}) {
  const { props } = ctx

  function get_api() {
    const { api } = ctx
    if (api) {
      return api
    } else {
      const { config = {} } = props
      return config.get_api()
    }
  }

  const keyindex = ref(0)

  const state = reactive({
    web: '',
    action: '',
    view_type: '',
    action_info: {},
    metadata: {},
    editable: false,
    currentRow: {}
  })

  const config = computed(() => {
    return {
      keyindex: keyindex.value,
      // view_type: state.view_type,
      get_api,
      action: state.action,
      metadata: state.metadata,
      ...toRaw(state.action_info),
      web: state.web,
      rowid: state.currentRow.id
    }
  })

  const routeConfig = computed(() => {
    return {
      web: state.web,
      action: state.action,
      view_type: state.view_type,
      rowid: state.currentRow.id,
      editable: state.editable ? 1 : undefined
    }
  })

  // watch props.key
  watch(
    () => (props.config || {}).keyindex,
    async (newVal, oldVal) => {
      // console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        // await sleep(100)
        await watch_key()
      }
    },
    { immediate: true }
  )

  async function watch_key() {
    // console.log('watch props.keyindex', toRaw(props))
    _load_action()
  }

  function _load_action() {
    const { config } = props

    function info_get() {
      const info = {}
      const { domain, context } = config
      if (domain) {
        info.domain = domain
      }
      if (context) {
        info.context = context
      }

      return info
    }

    const { action, metadata, web, view_type } = config
    // console.log(config)

    const info = info_get()
    // console.log(config, info)

    const api = get_api()
    const act = api.action(action, { ...info, metadata: toRaw(metadata) })
    state.action_info = info
    state.metadata = { ...metadata }
    state.action = action
    state.web = web
    state.view_type = view_type

    const { editable, rowid } = config
    state.editable = editable ? true : false
    if (rowid) {
      state.currentRow = { id: Number(rowid) }
    }

    // console.log(config)

    keyindex.value += 1
  }

  function rowClick(record) {
    // console.log(record)

    state.view_type = 'form'
    state.currentRow = { ...record }

    // keyindex.value += 1

    const cfg = { ...routeConfig.value }
    return cfg
  }

  function rowNew() {
    // console.log('row new')

    const cfg = {
      ...routeConfig.value,
      view_type: 'form',
      editable: 1,
      rowid: undefined
    }

    // state.editable = true
    // state.currentRow = {}
    // state.view_type = 'form'
    // // keyindex.value += 1

    // const cfg = { ...routeConfig.value }

    return cfg
  }

  function rowEdit() {
    state.editable = true
    keyindex.value += 1
    const cfg = { ...routeConfig.value }
    return cfg
  }

  function rowCommit(rid) {
    const old_rowid = state.currentRow.id

    // keyindex.value += 1

    if (old_rowid) {
      state.currentRow = { id: rid }
      state.editable = false
      return { type: undefined }
    } else {
      const cfg = {
        ...routeConfig.value,
        type: 'redirect',
        view_type: 'form',
        editable: undefined,
        rowid: rid
      }
      return cfg
    }
  }

  function rowCancel() {
    const cfg = { ...routeConfig.value }

    if (state.editable && state.currentRow.id) {
      state.editable = false
      cfg.type = undefined
      return cfg
    } else {
      state.currentRow = {}
      state.view_type = 'tree'
      cfg.type = 'back'
      return cfg
    }

    // keyindex.value += 1
  }

  function rowDel() {
    state.editable = false
    state.currentRow = {}
    state.view_type = 'tree'
    // keyindex.value += 1

    const cfg = { ...routeConfig.value }
    return cfg
  }

  return {
    config,
    view_type: computed(() => {
      return state.view_type
    }),
    editable: computed(() => {
      return state.editable
    }),

    rowClick,
    rowNew,
    rowEdit,
    rowDel,
    rowCancel,
    rowCommit
  }
}
