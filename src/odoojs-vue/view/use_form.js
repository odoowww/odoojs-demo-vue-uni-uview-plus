import { watch, ref, computed, reactive, toRaw } from 'vue'

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

export function useRead(ctx = {}) {
  const { props } = ctx

  function get_api() {
    const { api } = ctx
    if (api) {
      return api
    } else {
      const { config = {} } = props
      return config.get_api()
    }
  }

  const localState = {
    view: null,
    model: undefined
  }

  const state = reactive({
    metadata: {},
    sheet: {},
    record: {},
    data_ready: 0
  })

  function view_get() {
    if (state.data_ready > 0) {
      return localState.view
    } else {
      return {}
    }
  }

  // watch props.key
  watch(
    () => (props.config || {}).keyindex,
    async (newVal, oldVal) => {
      // console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        await sleep(100)
        await watch_key()
      }
    },
    { immediate: true }
  )

  async function watch_key() {
    load_view()
    const { config } = props
    const { rowid } = config

    await load_data(rowid)
  }

  function load_view() {
    const { config } = props
    const { action, metadata, web } = config
    const api = get_api()
    const act = api.action(action, { metadata: toRaw(metadata) })

    const formview = act.form
    localState.view = formview
    localState.model = formview.model
    state.metadata = { ...metadata }
    state.web = web
    state.sheet = formview.get_sheet()
  }

  async function reloadData() {
    const { config } = props
    const { rowid } = config

    await load_data(rowid)
  }

  async function load_data(resId) {
    const formview = localState.view

    const rid = Number(resId)
    const one = await formview.load_data(rid)

    state.record = { ...one }
    state.data_ready += 1

    state.sheet = formview.get_sheet({ record: state.record, values: {} })

    // console.log(state.sheet)
    return one
  }

  const viewInfo = computed(() => {
    return {
      get_api,
      model: localState.model,
      metadata: toRaw(state.metadata)
    }
  })

  const buttons = computed(() => {
    const view = view_get()
    return view.buttons || {}
  })

  const statusbar = computed(() => {
    const view = view_get()
    if (view.get_statusbar) {
      const ss = view.get_statusbar({ record: toRaw(state.record) })
      // console.log(ss)
      return ss
    } else {
      return [{}, {}]
    }
  })

  const toolbar = computed(() => {
    const view = view_get()
    if (view.get_toolbar) {
      const ss = view.get_toolbar({ record: toRaw(state.record) })

      return ss
    } else {
      return {}
    }
  })

  const box = computed(() => {
    const view = view_get()
    if (view.get_box) {
      const ss = view.get_box({ record: toRaw(state.record) })
      // console.log('box', ss)
      return ss
    } else {
      return {}
    }
  })

  const title = computed(() => {
    const view = view_get()
    if (view.get_title) {
      return view.get_title({ record: toRaw(state.record) })
    } else {
      return ''
    }
  })

  async function onDelete() {
    const view = view_get()
    const rid = state.record.id

    if (!view.unlink) return
    if (!rid) return
    await view.unlink(rid)

    return true
  }

  const wizard_config = ref({
    get_api,
    show: false,
    action: {},
    title: '',
    keyindex: 0
  })

  async function btnClick(btn) {
    // console.log(btn)

    const view = view_get()
    const { type, name, action_map = {} } = btn
    const record = toRaw(state.record)
    const res = await view.call_button({ type, name, record, action_map })

    const { result, error } = res
    // console.log(res)
    if (error) {
      console.log('完成, 返回 error', res)

      // throw 'error' + JSON.stringify(res)
      return { error }
    }

    if (!result) {
      await reloadData()
      return
    }

    if (result.type === 'ir.actions.act_window_close') {
      // console.log('act_window_close', result)

      const { params } = result
      if (params) {
        return { error: params }
      } else {
        await reloadData()
        return
      }
    }

    const { action_name } = result
    if (!action_name) {
      console.log('error, no action name', result)
      throw 'error' + JSON.stringify(result)
    } else {
      if (result.type !== 'ir.actions.act_window') {
        console.log(result)
        throw 'error' + JSON.stringify(result)
      }

      if (result.target === 'new') {
        wizard_config.value.title = result.name
        wizard_config.value.action = { ...result }
        wizard_config.value.show = true
        // await sleep(100)
        wizard_config.value.keyindex += 1
        return { result }
      } else if (result.target === 'current') {
        // console.log(result)
        // throw 'error' + JSON.stringify(result)
        // view_type, action: xml_id, menu: menu.xml_id, web

        let result2 = {
          view_type: result.view_type || result.view_mode || 'tree',
          action: action_name
        }

        if (result.res_id) {
          result2.rowid = result.res_id
        }
        if (result.domain) {
          result2.domain = JSON.stringify(result.domain)
        }

        if (result.context) {
          result2.context = JSON.stringify(result.context)
        }
        result2 = { ...result2, web: state.web, target: result.target }

        return { result: result2 }
      } else {
        console.log(result)
        throw 'error' + JSON.stringify(result)
      }
    }
  }

  return {
    record: computed(() => {
      return state.record
    }),
    sheet: computed(() => {
      return state.sheet
    }),
    viewInfo,
    title,
    buttons,
    box,
    toolbar,
    statusbar,
    onDelete,
    btnClick,
    reloadData,
    wizard_config
  }
}

export function useEdit(ctx = {}) {
  const { props } = ctx

  function get_api() {
    const { api } = ctx
    if (api) {
      return api
    } else {
      const { config = {} } = props
      return config.get_api()
    }
  }

  const localState = {
    view: null,
    model: undefined,
    Editmodel: null
  }

  const state = reactive({
    metadata: {},
    record_display: {},
    values: {},
    sheet: {},
    record: {},
    data_ready: 0
  })

  function view_get() {
    if (state.data_ready > 0) {
      return localState.view
    } else {
      return {}
    }
  }

  // watch props.key
  watch(
    () => (props.config || {}).keyindex,
    async (newVal, oldVal) => {
      // console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        await sleep(100)
        await watch_key()
      }
    },
    { immediate: true }
  )

  async function watch_key() {
    load_view()
    const { config } = props
    const { rowid } = config
    // console.log(config)
    await load_data(rowid)
  }

  function load_view() {
    const { config } = props
    const { action, metadata } = config
    const api = get_api()
    const act = api.action(action, { metadata: toRaw(metadata) })

    // console.log(act)
    const formview = act.form
    localState.view = formview
    localState.model = formview.model
    state.metadata = { ...metadata }
    state.sheet = formview.get_sheet()
  }

  async function load_data(resId) {
    const formview = localState.view
    const editmodel = formview.new_editmodel()
    localState.Editmodel = editmodel

    if (resId) {
      const rid = Number(resId)
      const one = await formview.load_data(rid)
      state.record = { ...one }
      state.record_display = editmodel.set_edit({
        record: { ...one },
        values: {}
      })
    } else {
      state.record_display = editmodel.set_edit({
        record: {},
        values: {}
      })
      // new view. 获取 default
      const res = await editmodel.onchange()
      const { record_display, values } = res
      state.record_display = record_display
      state.values = { ...values }
    }

    state.sheet = formview.get_sheet({
      editmodel,
      editable: true,
      record: state.record,
      values: state.values
    })

    state.data_ready += 1

    // console.log('formview record', state.record_display)
    // console.log('formview sheet', state.sheet)
  }

  const viewInfo = computed(() => {
    return {
      get_api,
      model: localState.model,
      metadata: toRaw(state.metadata),
      record: toRaw(state.record),
      values: toRaw(state.values)
    }
  })

  const buttons = computed(() => {
    const view = view_get()
    return view.buttons || {}
  })

  const title = computed(() => {
    const view = view_get()
    if (view.get_title) {
      return view.get_title({ record: state.record_display })
    } else {
      return ''
    }
  })

  async function onChange(fname, val) {
    console.log(fname, val)
    const editmodel = localState.Editmodel

    const res = await editmodel.onchange(fname, val)
    const { record_display, domain, values } = res
    state.record_display = { ...record_display }
    state.values = { ...values }

    const formview = view_get()

    state.sheet = formview.get_sheet({
      editmodel,
      editable: true,
      record: state.record,
      values: state.values
    })

    state.data_ready += 1

    // onchange 使用 form 自身的校验, 仅仅校验刚刚编辑的字段
    // 无需全部校验
    // 在提交时, 应做一次校验
    //  editRef.value.validate()
  }

  async function onCommit(validate) {
    const view = view_get()
    if (!view) return
    const editmodel = localState.Editmodel
    if (!editmodel) return
    const id_ret = await editmodel.commit(async done => {
      await sleep(100)
      // 在提交函数中执行 校验.
      // 提交函数 会进行排队. 等待 以前的 onchange 全部完成.
      // 以确保 当前页面中的 数据是 onchange 后的最新数据
      // 这里再等待100ms 是为了确保 前端页面完全刷新
      if (validate) {
        validate(done)
      } else {
        done(true)
      }
    })
    if (id_ret) {
      return id_ret
    } else {
      // 校验失败
    }
  }

  return {
    record: computed({
      get() {
        return state.record_display
      },
      set(val) {
        state.record_display = val
      }
    }),
    sheet: computed(() => {
      return state.sheet
    }),
    viewInfo,
    title,
    buttons,

    onChange,
    onCommit
  }
}

// function useForm(ctx = {}) {
//   const { editable } = ctx

//   if (editable) {
//     const todo = {
//       onDelete() {},
//       toolbar: {},
//       statusbar: {},
//       btnClick() {},
//       reloadData() {},
//       wizard_config: {}
//     }
//     const done = useEdit(ctx)
//     return { ...todo, ...done }
//   } else {
//     const todo = { onChange() {}, onCommit() {} }
//     const done = useRead(ctx)
//     return { ...todo, ...done }
//   }
// }
