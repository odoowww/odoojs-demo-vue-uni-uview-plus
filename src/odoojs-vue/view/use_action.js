import { computed, ref, reactive, toRaw } from 'vue'

export function useAction(ctx = {}) {
  const { get_api } = ctx

  const keyindex = ref(0)

  const state = reactive({
    web: '',
    action: '',
    view_type: '',
    metadata: {},
    action_info: {}
  })

  const config = computed(() => {
    return {
      keyindex: keyindex.value,
      get_api,
      action: state.action,
      metadata: state.metadata,
      view_type: state.view_type,
      web: state.web,
      editable: state.editable,
      rowid: state.rowid,
      ...toRaw(state.action_info)
    }
  })

  async function load_action(options) {
    // console.log(options)

    function info_get() {
      const info = {}
      const { domain, context } = options
      if (domain) {
        info.domain = JSON.parse(domain)
      }
      if (context) {
        info.context = JSON.parse(context)
      }

      return info
    }

    const { action, web, view_type, editable, rowid } = options

    const info = info_get()

    const api = get_api()
    const act = api.action(action, info)

    const res_id = await act.load_res_id()

    state.action = action
    state.web = web
    state.view_type = view_type
    state.editable = editable
    state.rowid = rowid || res_id
    state.action_info = info

    const metadata = await act.load_metadata()
    state.metadata = { ...metadata }
    keyindex.value += 1
  }

  function reload_action() {
    keyindex.value += 1
  }

  return {
    load_action,
    reload_action,
    config
  }
}
