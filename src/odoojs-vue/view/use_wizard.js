import { watch, ref, computed, reactive, toRaw } from 'vue'

function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

export function useWizard(ctx = {}) {
  const { props } = ctx

  function get_api() {
    const { api } = ctx
    if (api) {
      return api
    } else {
      const { config = {} } = props
      return config.get_api()
    }
  }

  const localState = {
    view: null,
    model: undefined,
    Editmodel: null
  }

  const state = reactive({
    metadata: {},
    record_display: {},
    values: {},
    sheet: {},
    record: {},
    data_ready: 0
  })

  function view_get() {
    if (state.data_ready > 0) {
      return localState.view
    } else {
      return {}
    }
  }

  const toolbar = computed(() => {
    const view = view_get()
    if (view.get_footer) {
      const ss = view.get_footer({
        record: toRaw(state.record),
        values: toRaw(state.values),
        editable: true
      })

      return ss
    } else {
      return {}
    }
  })

  // watch props.key
  watch(
    () => (props.config || {}).keyindex,
    async (newVal, oldVal) => {
      console.log('watch props.keyindex', newVal, oldVal)
      if (newVal) {
        await sleep(100)
        await watch_key()
      }
    },
    { immediate: true }
  )

  const a = ref({})
  async function watch_key() {
    console.log('watch props.keyindex', toRaw(props))
    await _load_action()
    await load_data()
  }

  async function _load_action() {
    const { config } = props
    const { action } = config
    const { action_name, context } = action
    const ctx = toRaw(context)

    const api = get_api()

    const act = api.action(action_name, { context: ctx })
    const metadata = await act.load_metadata()

    const formview = act.form
    localState.view = formview
    localState.model = formview.model
    state.sheet = formview.get_sheet()
    console.log('_load_action', action_name, act, metadata)
  }

  async function load_data() {
    const formview = localState.view
    const editmodel = formview.new_editmodel()
    localState.Editmodel = editmodel

    state.record_display = editmodel.set_edit({
      record: {},
      values: {}
    })
    // new view. 获取 default
    const res = await editmodel.onchange()
    const { record_display, values } = res
    state.record_display = record_display
    state.values = { ...values }

    state.sheet = formview.get_sheet({
      editmodel,
      editable: true,
      record: state.record,
      values: state.values
    })
    console.log(state)
    state.data_ready += 1
  }

  const viewInfo = computed(() => {
    return {
      get_api,
      model: localState.model,
      metadata: toRaw(state.metadata),
      record: toRaw(state.record),
      values: toRaw(state.values)
    }
  })

  async function onChange(fname, val) {
    console.log(fname, val)
    const editmodel = localState.Editmodel

    const res = await editmodel.onchange(fname, val)
    const { record_display, domain, values } = res
    state.record_display = { ...record_display }
    state.values = { ...values }

    const formview = view_get()

    state.sheet = formview.get_sheet({
      editmodel,
      editable: true,
      record: state.record,
      values: state.values
    })

    state.data_ready += 1
    console.log(editmodel, localState.Editmodel)
    // onchange 使用 form 自身的校验, 仅仅校验刚刚编辑的字段
    // 无需全部校验
    // 在提交时, 应做一次校验
    //  editRef.value.validate()
  }

  //   async function onCommit(validate) {
  //     const view = view_get()
  //     if (!view) return
  //     const editmodel = localState.Editmodel
  //     if (!editmodel) return

  //     console.log(' onCommit')
  //   }

  //

  async function btnClick(btn) {
    // console.log(btn)

    const view = view_get()
    const editmodel = localState.Editmodel
    const res_id = await editmodel.commit()

    const { type, name, action_map = {} } = btn
    const btn2 = { type, name, action_map }
    const res = await view.call_button({ ...btn2, record: { id: res_id } })

    const { error, result } = res

    if (error) {
      return { error }
    }

    if (!result) {
      return
    } else {
      if (result.type === 'ir.actions.act_window_close') {
        return
      }
      console.log('call_button1', btn, result)
      throw 'error. wizard call return action? ' + name + JSON.stringify(result)
    }
  }

  return {
    record: computed({
      get() {
        return state.record_display
      },
      set(val) {
        state.record_display = val
      }
    }),
    sheet: computed(() => {
      return state.sheet
    }),
    viewInfo,
    toolbar,
    onChange,
    btnClick
  }
}
