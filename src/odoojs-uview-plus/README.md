## odoojs-uview-plus

1. physic components for odoojs by uview-plus.
2. odoojs 的物理组件库, 用于 uview-plus ui 库实现

## contact us

contact us: odoojs@outlook.com

## 通用 物理组件说明

### 客户的 WebView 物理组件 的 基础物理组件: UWebView

1. 参数 props = { config }
2. 使用 逻辑组件库的 接口函数 useWeb
3. 对外插槽: default, 参数:
   { node, record, viewInfo, editable, onChange, parentNodeType}
4. 插槽中 用 物理组件 Node 实现

### 客户的 Node 物理组件 的 基础物理组件: UNode

1. 参数 props = { editable,node, record, viewInfo, parentNodeType }
2. 使用 逻辑组件 ONode 或 ONodeApp
3. 使用 物理组件 Notebook, Grid
4. 对外插槽, 附后

#### 对外插槽 sub-node

1. 参数 { node, parentNodeType}
2. 用 客户自己的 物理组件 Node 实现

#### 对外插槽 widget

1. 参数 { node, }
2. 用 客户自己的 物理组件 NodeWidget 实现

#### 对外插槽 field

1. 参数 { node, }
2. 用 客户自己的 物理组件 NodeWidget 实现

###

, , UNodeField, UNodeWidget
