// import { useMenu, useSession } from 'odoojs-api/lib/odoojs-vue'
// import { useAction } from 'odoojs-api/lib/odoojs-vue'

import { useMenu, useSession } from '@/odoojs-vue'
import { useAction } from '@/odoojs-vue'

export { useSession, useMenu, useAction }
