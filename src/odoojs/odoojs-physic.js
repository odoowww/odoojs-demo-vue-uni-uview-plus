import { UWebView, UNode } from '@/odoojs-uview-plus/index'
import { UNodeField, UNodeWidget } from '@/odoojs-uview-plus/index'

export { UWebView, UNode, UNodeField, UNodeWidget }
