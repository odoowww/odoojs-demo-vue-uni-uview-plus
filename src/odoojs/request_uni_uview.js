// uni.$u.http 的标准配置
// 封装为函数
// 允许由 odoojs-jsonrpc 配置:
// 1. baseURL, timeout
// 2. 请求头中 配置 session_id
// 3. 截获响应头, 处理 session_id
// 4. 对响应结果做 额外处理
// 5. 对 响应 error, 自行处理

export class Request {
  constructor(payload) {
    const { baseURL, timeout = 500000, messageError } = payload

    uni.$u.http.setConfig(config => {
      /* config 为默认全局配置*/
      config.baseURL = baseURL /* 根域名 */
      return config
    })

    // 请求拦截
    uni.$u.http.interceptors.request.use(
      config => {
        // 这里 在请求头中 增加 session_id
        const session_id = Request._sid
        if (session_id) {
          const url = config.url
          const url_auth = '/web/session/authenticate'
          if (url === url_auth) {
            delete config.header['X-Openerp-Session-Id']
          } else {
            config.header['X-Openerp-Session-Id'] = session_id
          }
        }
        return config
      },
      error => {
        console.log('request config error') // for debug
        if (messageError) {
          return messageError(error)
        } else {
          return Promise.reject(error)
        }
      }
    )

    // 响应拦截
    uni.$u.http.interceptors.response.use(
      response => {
        const url = response.config.url
        const url_auth = '/web/session/authenticate'
        const url_info = '/web/session/get_session_info'
        const url_destroy = '/web/session/destroy'
        if (url === url_auth || url === url_info) {
          // if run test or run from wx miniprograme, not cookie,
          // so wo set sid to call odoo
          // console.log('===url:', url)
          // console.log('===response:', response)
          const headers2 = response.header
          const cookie = headers2['set-cookie']
          // const cookie = headers2['Set-Cookie']

          // console.log('===headers2:', headers2)
          // console.log('===cookie:', cookie)
          if (cookie) {
            const cookie2 = cookie[0]
            const session_id = cookie2.slice(11, 51)
            // console.log('===session_id:', session_id)
            Request._sid = session_id
          }
        } else if (url === url_destroy) {
          Request._sid = undefined
        }

        return response
      },
      error => {
        console.log('request response error', error) // for debug
        const url = (error.config || {}).url
        const url_check = '/web/session/check'
        const url_info = '/web/session/get_session_info'
        const url_destroy = '/web/session/destroy'

        console.log('request response error', error) // for debug
        if ([url_check, url_info, url_destroy].includes(url)) {
          return Promise.reject(error)
        } else if (messageError) {
          return messageError(error)
        } else {
          return Promise.reject(error)
        }
      }
    )
  }

  localStorage = {
    getItem(item) {
      uni.getStorageSync(item)
    },
    setItem(item, val) {
      uni.setStorageSync(item, val)
    }
  }

  call_post(url, data) {
    const http = uni.$u.http
    return http.post(url, data)
  }
}

Request._sid = undefined
