import { Request } from './request_uni_uview.js'
import { get_odoojs_rpc } from 'odoojs-rpc'
import { get_odoojs_api } from 'odoojs-api'

// import { get_odoojs_rpc } from '@/odoojs-rpc/index.js'
// import { get_odoojs_api } from '@/odoojs-api/index.js'

import addons_models_odoojs from '@/addons_models_odoojs/index.js'
import addons_actions_odoo from '@/addons_actions_odoo/index.js'
import addons_actions_odoojs from '@/addons_actions_odoojs/index.js'
import addons_menus_odoo from '@/addons_menus_odoo/index.js'
import addons_menus_odoojs from '@/addons_menus_odoojs/index.js'

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}

function messageError(error) {
    console.log('error', error)
    console.log('show error', error.message)

    uni.showModal({
      title: error.message,
      content: error.data.message,
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定');
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      }
    });

    // uni.showToast({
    //   title: '标题',
    //   duration: 2000
    // });

    return Promise.reject(error)
}

  // 开发时, 值为 '/api', 打包部署后, 值为 '/odoo'
const baseURL = import.meta.env.VITE_BASE_API
const timeout = 50000
const run_by_server = true // to set true if addons of web_for_odoojs installed

const addons_dict = { 
  addons_models_odoojs,
  addons_actions_odoo, 
  addons_actions_odoojs,
  addons_menus_odoo, 
  addons_menus_odoojs,
}
  
const modules_installed = [
    // 'base',
    // 'product',
    // 'analytic',
    // 'account',
    // 'contacts',
    // 'sale',
    // 'purchase'
]
  
const debug = 1
// load from session storage
const lang = 'zh_CN'


export function get_api(){
  const rpc = get_odoojs_rpc({ Request, baseURL, timeout, run_by_server, messageError })
  const api = get_odoojs_api(rpc, { debug, addons_dict, lang, modules_installed }) 
  return api  
}

export async function session_check() {
  const api =  get_api() 
  const res = await api.session_check()
  if (!res) {
    console.log('errr, goto  login ', )
    uni.$u.route('pages/user/login')
  }
  return res
}

