// import { createSSRApp } from "vue";
// import App from "./App.vue";
// export function createApp() {
//   const app = createSSRApp(App);
//   return {
//     app,
//   };
// }

import uviewPlus from 'uview-plus'

import { createSSRApp } from 'vue'

import App from './App.vue'
import {i18n} from './i18n'


export function createApp() {
  const app = createSSRApp(App)
  app.use(uviewPlus)
  app.use(i18n)
  return {
    app
  }
}
