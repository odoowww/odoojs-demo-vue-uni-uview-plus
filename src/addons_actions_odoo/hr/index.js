import hr_contract_type from './hr_contract_type.js'
import hr_department from './hr_department.js'
import hr_employee_category from './hr_employee_category.js'
import hr_work_location from './hr_work_location.js'
import hr_job from './hr_job.js'

import hr_employee from './hr_employee.js'
// import res_partner from './res_partner.js'
// import res_users from './res_users.js'

export default {
  ...hr_contract_type,
  ...hr_employee_category,
  ...hr_work_location,
  ...hr_job,
  ...hr_department,
  ...hr_employee
  // ...res_partner,
  // ...res_users
}
