const view_form_hr_employee_category = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.employee.category',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group: {
          field_name: {},
          field_color: {},
          field_employee_ids: { widget: 'many2many_tags' }
        }
      }
    }
  }
}

const view_tree_hr_employee_category = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.employee.category',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_color: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_color: {} },
      card_value: {}
    }
  }
}

const action_hr_employee_category = {
  _odoo_model: 'ir.actions.act_window',
  type: 'ir.actions.act_window',
  name: '员工标签',
  res_model: 'hr.employee.category',
  // search_view_id: 'view_search_hr_employee_category',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_employee_category',
    form: 'view_form_hr_employee_category'
  }
}

export default {
  view_form_hr_employee_category,
  view_tree_hr_employee_category,

  action_hr_employee_category
}
