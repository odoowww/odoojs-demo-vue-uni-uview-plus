const view_form_hr_employee = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.employee',
  type: 'form',
  arch: {
    button_box: {},

    header: {
      // button_plan_wizard_action: {
      //   attr: {
      //     name: 'plan_wizard_action',
      //     type: 'action',
      //     string: 'Launch Plan',
      //     groups: 'hr.group_hr_user'
      //   }
      // }
    },
    sheet: {
      field_active: { invisible: '1' },
      field_user_id: { invisible: '1' },
      field_user_partner_id: { invisible: '1' },
      field_image_128: { invisible: '1' },
      field_company_id: { invisible: '1' },
      field_last_activity_time: { invisible: '1' },
      field_last_activity: { invisible: '1' },
      field_work_contact_id: { invisible: '1' },

      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      field_avatar_128: { invisible: '1' },
      field_name: { required: 1, placeholder: "Employee's Name" },
      field_job_title: { placeholder: 'Job Position' },
      field_category_ids: {
        widget: 'many2many_tags',
        groups: 'hr.group_hr_user',
        placeholder: 'Tags'
      },

      field_image_1920: { widget: 'image' },
      field_show_hr_icon_display: { invisible: '1' },
      field_hr_icon_display: {
        widget: 'hr_presence_status'
        // invisible: 'not show_hr_icon_display or not id'
      },

      group: {
        group: {
          field_mobile_phone: { widget: 'phone' },
          field_work_phone: { widget: 'phone' },
          field_work_email: { widget: 'email' },
          field_company_id: { groups: 'base.group_multi_company' },
          field_company_country_id: { invisible: '1' },
          field_company_country_code: { invisible: '1' }
        },
        group_139: {
          field_department_id: {},
          field_job_id: {},
          field_parent_id: { widget: 'many2one_avatar_user' },
          field_coach_id: { widget: 'many2one_avatar_user' }
        }
      },

      notebook: {
        page_public: {
          attr: { name: 'public', string: '工作信息' },

          group: {
            group: {
              attr: { string: '工作地点' },
              field_address_id: {
                context: { show_address: 1 },
                always_reload: true,
                highlight_first_line: true
              },
              field_work_location_id: {
                context: {
                  default_address_id: { field_address_id: {} }
                }
              }
            },
            group_managers: {
              attr: {
                invisible: '1',
                name: 'managers',
                string: 'Approvers',
                class: 'hide-group-if-empty'
              }
            },
            group_departure: {
              attr: {
                name: 'departure',
                string: 'Departure',
                invisible: [['active', '=', true]]
              },
              field_departure_reason_id: {},
              field_departure_description: {},
              field_departure_date: {}
            },
            group_296: {
              attr: { string: 'Schedule' },
              field_resource_calendar_id: { required: '1' },
              field_id: { invisible: '1' },
              field_tz: { required: [['id', '!=', false]] }
            }
          }
        },
        page_personal_information: {
          attr: {
            name: 'personal_information',
            string: '个人信息',
            groups: 'hr.group_hr_user'
          },
          group: {
            group: {
              attr: { string: 'Private Contact' },

              field_private_street: { placeholder: 'Street...' },
              field_private_street2: { placeholder: 'Street 2...' },
              field_private_city: { placeholder: 'City' },
              field_private_state_id: { placeholder: 'State' },
              field_private_zip: { placeholder: 'ZIP' },
              field_private_country_id: { placeholder: 'Country' },

              field_private_email: { string: 'Email' },
              field_private_phone: { string: 'Phone' },
              field_bank_account_id: {
                readonly: [['id', '=', false]]
              },
              field_lang: { string: 'Language' },
              field_km_home_work: {}
            },

            group_297: {
              attr: { string: 'Family Status' },
              field_marital: {},
              field_spouse_complete_name: {
                // invisible: [['marital', 'not in', ['married', 'cohabitant']]]
              },
              field_spouse_birthdate: {
                // invisible: [['marital', 'not in', ['married', 'cohabitant']]]
              },
              field_children: {}
            },

            group_2973: {
              attr: { string: 'Emergency', name: 'emergency' },
              field_emergency_contact: {},
              field_emergency_phone: {}
            },
            group_588: {
              attr: { string: 'Education' },
              field_certificate: {},
              field_study_field: {},
              field_study_school: {},
              widget_separator__has_work_permit: {
                attr: { name: 'has_work_permit', string: 'Work Permit' }
              },
              field_visa_no: {},
              field_permit_no: {},
              field_visa_expire: {},
              field_work_permit_expiration_date: {},
              field_has_work_permit: { widget: 'work_permit_upload' }
            },
            group_756: {
              attr: { string: 'Citizenship' },
              field_country_id: { no_open: true, no_create: true },
              field_identification_id: {},
              field_passport_id: {},
              field_gender: {},
              field_birthday: {},
              field_place_of_birth: {},
              field_country_of_birth: {}
            }
          }
        },
        page_hr_settings: {
          attr: {
            name: 'hr_settings',
            string: 'HR Settings',
            groups: 'hr.group_hr_user'
          },
          group: {
            group_active_group: {
              attr: { name: 'active_group', string: 'Status' },
              field_employee_type: {},
              field_user_id: {
                widget: 'many2one_avatar_user',
                string: 'Related User',
                domain: [
                  ['company_ids', 'in', { field_company_id: {} }],
                  ['share', '=', false]
                ]
              }
            },
            group_identification_group: {
              attr: {
                name: 'identification_group',
                string: 'Attendance/Point of Sale'
              },
              field_pin: { string: 'PIN Code' },

              group_barcode: {
                field_barcode: {}
                // button_generate_random_barcode: {
                //   attr: {
                //     name: 'generate_random_barcode',
                //     type: 'object',
                //     string: 'Generate',
                //     invisible: [['barcode', '!=', false]],
                //     class: 'btn btn-link'
                //   }
                // },
                // button_hr_employee_print_badge: {
                //   attr: {
                //     name: 'hr_employee_print_badge',
                //     type: 'action',
                //     string: 'Print Badge',
                //     invisible: [['barcode', '=', false]],
                //     class: 'btn btn-link'
                //   }
                // }
              }
            },
            group_payroll_group: {
              attr: { name: 'payroll_group', string: 'Payroll', invisible: '1' }
            },
            group_application_group: {
              attr: {
                name: 'application_group',
                string: 'Application Settings',
                invisible: '1'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_hr_employee = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.employee',
  type: 'tree',
  arch: {
    sheet: {
      // _header: {
      //   _button_plan_wizard_action: {
      //     _attr: {
      //       name: 'plan_wizard_action',
      //       type: 'action',
      //       string: 'Launch Plan',
      //       context: { action_plan: true }
      //     }
      //   }
      // },
      field_name: { readonly: '1' },
      field_work_phone: { readonly: '1' },
      field_work_email: {},
      // activity_ids: { widget: 'list_activity' },
      // activity_user_id: {
      //   string: 'Activity by',
      //   widget: 'many2one_avatar_user',
      //   optional: 'hide'
      // },
      // activity_date_deadline: {
      //   widget: 'remaining_days',
      //   allow_order: '1'
      // },
      field_company_id: { groups: 'base.group_multi_company', readonly: '1' },
      field_department_id: {},
      field_job_id: {},
      field_parent_id: { widget: 'many2one_avatar_user' },
      field_address_id: { invisible: '1' },
      // company_id: { invisible: '1' },
      field_work_location_id: { optional: 'hide' },
      field_coach_id: { invisible: '1' },
      field_active: { invisible: '1' },
      field_category_ids: {
        widget: 'many2many_tags',
        optional: 'hide',
        color_field: 'color'
      },
      field_country_id: { optional: 'hide' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_department_id: {} },
      card_value: { field_work_email: {} }
    }
  }
}

const view_search_hr_employee = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.employee',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '员工',
        filter_domain: [
          '|',
          ['work_email', 'ilike', { self: {} }],
          ['name', 'ilike', { self: {} }]
        ]
      },
      field_parent_id: { string: 'Manager' },
      field_job_id: {},
      field_coach_id: {},
      field_category_ids: { groups: 'hr.group_hr_user' },
      field_private_car_plate: {},

      filter_my: {
        my_team: {
          name: 'my_team',
          string: 'My Team',
          domain: [['parent_id.user_id', '=', { session_uid: {} }]]
        },
        my_department: {
          name: 'my_department',
          string: 'My Department',
          domain: [['member_of_department', '=', true]]
        }
      },
      filter_newly_hired: {
        newly_hired: {
          name: 'newly_hired',
          string: 'Newly Hired',
          domain: [['newly_hired', '=', true]]
        }
      },
      filter_inactive: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      },

      groupby: {
        group_manager: {
          string: '管理者',
          domain: '[]',
          context: { group_by: 'parent_id' }
        },
        group_department: {
          string: '部门',
          domain: '[]',
          context: { group_by: 'department_id' }
        },

        group_job: {
          string: '岗位',
          domain: '[]',
          context: { group_by: 'job_id' }
        },
        group_start: {
          string: '开始时间',
          domain: '[]',
          context: { group_by: 'create_date' }
        },
        group_category_ids: {
          string: '标签',
          domain: '[]',
          context: { group_by: 'category_ids' }
        }
      }
    }
  }
}

const action_hr_employee = {
  _odoo_model: 'ir.actions.act_window',
  type: 'ir.actions.act_window',
  name: '员工',
  res_model: 'hr.employee',
  search_view_id: 'view_search_hr_employee',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_employee',
    form: 'view_form_hr_employee'
  }
}

export default {
  view_form_hr_employee,
  view_tree_hr_employee,
  view_search_hr_employee,
  action_hr_employee
}
