const view_form_hr_work_location = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.work.location',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group: {
          field_active: { invisible: '1' },
          field_name: {},
          field_address_id: {},
          field_location_type: { widget: 'hr_homeworking_radio_image' }
        },

        group_375: {
          field_company_id: { groups: 'base.group_multi_company' }
        }
      }
    }
  }
}

const view_tree_hr_work_location = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.work.location',
  type: 'tree',
  arch: {
    sheet: {
      field_active: { invisible: '1' },
      field_name: {},
      field_location_type: { widget: 'hr_homeworking_radio_image' },
      field_company_id: { groups: 'base.group_multi_company' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {
        field_location_type: { widget: 'hr_homeworking_radio_image' }
      },
      card_value: {}
    }
  }
}

const action_hr_work_location = {
  _odoo_model: 'ir.actions.act_window',
  type: 'ir.actions.act_window',
  name: '工作地点',
  res_model: 'hr.work.location',
  // search_view_id: 'view_search_hr_work_location',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_work_location',
    form: 'view_form_hr_work_location'
  }
}

export default {
  view_form_hr_work_location,
  view_tree_hr_work_location,

  action_hr_work_location,
  model_hr_work_location: {
    _odoo_model: 'ir.model',
    fields: {
      // active: { string: '启用' },
      location_type: {
        string: '类型',
        selection: [
          ['home', '居家'],
          ['office', '办公室'],
          ['other', '其他']
        ]
      }
    }
  }
}
