const view_form_hr_contract_type = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.contract.type',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group: {
          field_sequence: { widget: 'handle' },
          field_name: {},
          field_code: { groups: 'base.group_no_one' },
          field_country_id: {}
        }
      }
    }
  }
}

const view_tree_hr_contract_type = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.contract.type',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_code: { groups: 'base.group_no_one' },
      field_country_id: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_code: { groups: 'base.group_no_one' } },
      card_value: {}
    }
  }
}

const action_hr_contract_type = {
  _odoo_model: 'ir.actions.act_window',
  type: 'ir.actions.act_window',
  name: '员工类型',
  res_model: 'hr.contract.type',
  // search_view_id: 'view_search_hr_contract_type',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_contract_type',
    form: 'view_form_hr_contract_type'
  }
}

export default {
  view_form_hr_contract_type,
  view_tree_hr_contract_type,

  action_hr_contract_type
}
