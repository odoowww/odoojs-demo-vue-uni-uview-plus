const view_form_hr_job = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.job',
  type: 'form',
  arch: {
    button_box: {},
    sheet: {
      field_active: { invisible: '1' },
      field_company_id: { invisible: '1' },

      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      field_name: { placeholder: 'e.g. Sales Manager' },

      notebook: {
        page_recruitment_page: {
          attr: { name: 'recruitment_page', string: '招聘' },
          group: {
            group_recruitment: {
              attr: { name: 'recruitment' },
              field_company_id: { groups: 'base.group_multi_company' },
              field_department_id: {},
              field_contract_type_id: {}
            },
            group_recruitment2: {
              attr: { name: 'recruitment2' },
              field_no_of_recruitment: { class: 'col-3 ps-0' }
            }
          }
        },
        page_job_description_page: {
          attr: { name: 'job_description_page', string: '职位描述' },
          field_description: {}
        }
      }
    }
  }
}

const view_tree_hr_job = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.job',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_department_id: {},
      field_no_of_recruitment: {},
      field_no_of_employee: { optional: 'hide' },
      field_expected_employees: { optional: 'hide' },
      field_no_of_hired_employee: { optional: 'hide' },
      field_message_needaction: { invisible: '1' },
      field_company_id: { groups: 'base.group_multi_company', optional: 'hide' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_department_id: {} },
      card_value: { field_no_of_recruitment: {} }
    }
  }
}

const view_search_hr_job = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.job',
  type: 'search',
  arch: {
    sheet: {
      field_name: { string: '岗位' },
      field_department_id: { operator: 'child_of' },

      // group_message_needaction: {
      //   message_needaction: {
      //     name: 'message_needaction',
      //     string: 'Unread Messages',
      //     domain: [['message_needaction', '=', true]]
      //   }
      // },
      filter_archived: {
        archived: {
          name: 'archived',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      },

      groupby: {
        group_department: {
          string: '部门',
          domain: '[]',
          context: { group_by: 'department_id' }
        },
        group_company: {
          groups: 'base.group_multi_company',
          string: '公司',
          domain: '[]',
          context: { group_by: 'company_id' }
        },
        group_employment_type: {
          string: '类型',
          domain: '[]',
          context: { group_by: 'contract_type_id' }
        }
      }
    }
  }
}

const action_hr_job = {
  _odoo_model: 'ir.actions.act_window',
  name: '岗位',
  res_model: 'hr.job',
  search_view_id: 'view_search_hr_job',
  domain: [],
  context: {
    search_default_Current: 1
  },
  views: {
    tree: 'view_tree_hr_job',
    form: 'view_form_hr_job'
  }
}

export default {
  view_form_hr_job,
  view_tree_hr_job,
  view_search_hr_job,
  action_hr_job,
  model_hr_job: {}
}
