const view_form_hr_department = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.department',
  type: 'form',

  title: { field_name: {} },

  arch: {
    button_box: {
      // _button_hr__act_employee_from_department: {
      //   _attr: {
      //     name: 'hr.act_employee_from_department',
      //     type: 'action',
      //     icon: 'fa-users',
      //     class: 'oe_stat_button'
      //   },
      //   total_employee: { string: 'Employees', widget: 'statinfo' }
      // },
      // _button_action_plan_from_department: {
      //   _attr: {
      //     name: 'action_plan_from_department',
      //     type: 'object',
      //     icon: 'fa-list-ul',
      //     class: 'oe_stat_button'
      //   },
      //   plans_count: { string: 'Plans', widget: 'statinfo' }
      // }
    },

    header: {},
    sheet: {
      field_company_id: { invisible: '1' },
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      field_active: { invisible: 1 },

      group: {
        group: {
          field_name: {},
          field_manager_id: {},
          field_parent_id: {},
          field_company_id: {
            groups: 'base.group_multi_company',
            no_create: true
          }
        }
      }
    }
  }
}

const view_tree_hr_department = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.department',
  type: 'tree',
  arch: {
    sheet: {
      field_display_name: {},
      field_company_id: { groups: 'base.group_multi_company' },
      field_manager_id: {},
      field_total_employee: { string: 'Employees' },
      field_parent_id: {}
    },
    kanban: {
      card_title: { field_display_name: {} },
      card_label: { field_parent_id: {} },
      card_value: { field_manager_id: {} }
    }
  }
}

const view_search_hr_department = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.department',
  type: 'search',
  arch: {
    sheet: {
      field_name: { string: '部门' },
      field_manager_id: {},

      // filter_message_needaction: {
      //   message_needaction: {
      //     groups:"mail.group_mail_notification_type_inbox",
      //     name: 'message_needaction',
      //     string: 'Unread Messages',
      //     domain: [['message_needaction', '=', true]]
      //   }
      // },

      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_hr_department = {
  _odoo_model: 'ir.actions.act_window',
  type: 'ir.actions.act_window',
  name: '部门',
  res_model: 'hr.department',
  search_view_id: 'view_search_hr_department',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_department',
    form: 'view_form_hr_department'
  }
}

export default {
  view_form_hr_department,
  view_tree_hr_department,
  view_search_hr_department,
  action_hr_department
}
