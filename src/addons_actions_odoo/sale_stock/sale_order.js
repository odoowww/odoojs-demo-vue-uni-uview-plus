const view_form_sale_order = {
  _odoo_model: 'ir.ui.view',
  inherit_patch: 'sale.view_form_sale_order',
  arch: {
    button_box: {
      button_action_view_delivery: {
        name: 'action_view_delivery',
        type: 'object',
        groups: 'stock.group_stock_user',
        // 'invisible': [('delivery_count', '=', 0)]
        invisible: [['delivery_count', '=', 0]],
        action_map: {
          'stock.picking': 'stock.action_stock_picking_outgoing'
        },

        field_delivery_count: { string: '出库单', widget: 'statinfo' }
      }
    }
  }
}
export default {
  view_form_sale_order
  // view_tree_sale_order,
  // view_search_sale_order,
  // action_sale_order
}
