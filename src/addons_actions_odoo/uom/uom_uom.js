const view_form_uom_uom = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.uom',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group_uom_details: {
          field_name: {},
          field_category_id: {},
          field_uom_type: { readonly: 1 },

          group_factor: {
            attr: {
              // 'invisible':[('uom_type','!=','smaller')]
              invisible: [['uom_type', '!=', 'smaller']]
            },
            field_factor: {
              // 'readonly':[('uom_type','=','bigger')]
              readonly: [['uom_type', '=', 'bigger']]
            },

            widget_text: 'e.g: 1*(reference unit)=ratio*(this unit)'
          },

          group_factor_inv: {
            attr: {
              // 'invisible':[('uom_type','!=','bigger')]
              invisible: [['uom_type', '!=', 'bigger']]
            },
            field_factor_inv: {
              // 'readonly':[('uom_type','!=','bigger')]
              readonly: [['uom_type', '!=', 'bigger']]
            },

            widget_text: 'e.g: 1*(this unit)=ratio*(reference unit)'
          }
        }
      },

      group_active_rounding: {
        field_active: { widget: 'boolean_toggle' },
        field_rounding: {}
      }
    }
  }
}

const view_tree_uom_uom = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.uom',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_category_id: {},
      field_uom_type: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_category_id: {} },
      card_value: { field_uom_type: {} }
    }
  }
}

const view_search_uom_uom = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.uom',
  type: 'search',
  arch: {
    fields: {
      name: {}
    },

    filters: {
      group_active: {
        inactive: { string: '已归档', domain: [['active', '=', false]] }
      }
    }
  }
}

const action_uom_uom = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Units of Measure',
  type: 'ir.actions.act_window',
  res_model: 'uom.uom',
  search_view_id: 'view_search_uom_uom',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_uom_uom',
    form: 'view_form_uom_uom'
  }
}

export default {
  view_form_uom_uom,
  view_tree_uom_uom,
  view_search_uom_uom,
  action_uom_uom
}
