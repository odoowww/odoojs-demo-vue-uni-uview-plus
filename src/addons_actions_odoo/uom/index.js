import uom_category from './uom_category.js'
import uom_uom from './uom_uom.js'

export default {
  ...uom_category,
  ...uom_uom
}
