const view_form_uom_category = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.category',
  type: 'form',
  arch: {
    sheet: {
      group_name: {
        // attr: {
        //   // string: 'group_name'
        // },
        field_name: {},
        field_reference_uom_id: { invisible: '1' }
      },

      notebook: {
        // page_uom_lines1: {
        //   attr: { string: 'Units', name: 'uom_lines1' }
        // },
        page_uom_lines: {
          attr: { string: '度量单位', name: 'uom_lines' },
          // group_1: {
          //   attr: {
          //     string: 'Group_1'
          //   }
          // },
          // field_name: {},
          field_uom_ids: {
            nolabel: 1,
            // context: ({ record }) => {
            //   return {
            //     default_uom_type: 'smaller',
            //     default_category_id: record.id
            //   }
            // },
            views: {
              tree: {
                fields: {},
                arch: {
                  sheet: {
                    field_display_name: {
                      invisible: 1,
                      help: '为treeview 准备的'
                    },
                    field_name: {},
                    field_uom_type: {},
                    field_factor: { invisible: 0 },
                    field_factor_inv: { invisible: 0 },
                    field_ratio: {},
                    field_active: {},
                    field_rounding: {}
                  },

                  kanban: {
                    card_title: {
                      field_name: {}
                    },
                    card_label: {
                      field_uom_type: {}
                    },
                    card_value: {
                      field_ratio: {}
                    }
                  }
                }
              },

              form: {
                arch: {
                  sheet: {
                    group_name: {
                      field_name: {},
                      field_uom_type: {},
                      field_factor: { invisible: 0 },
                      field_factor_inv: { invisible: 0 },
                      field_ratio: {
                        // string: 'Ratio',

                        // 'readonly': [('uom_type', '=', 'reference')]}"
                        readonly: [['uom_type', '=', 'reference']]
                      },
                      field_active: {},
                      field_rounding: {}
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_uom_category = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.category',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_uom_ids: {
        widget: 'many2many_tags'
      }
    },

    kanban: {
      card_title: {
        field_name: {}
      },
      card_label: {},
      card_value: {
        field_uom_ids: { widget: 'many2many_tags' }
      }
    }
  }
}

const view_search_uom_category = {
  _odoo_model: 'ir.ui.view',
  model: 'uom.category',
  type: 'search',
  arch: {
    fields: {
      name: {},
      uom_ids: {}
    },

    filters: {}
  }
}

const action_uom_category = {
  _odoo_model: 'ir.actions.act_window',
  name: '度量单位类别',
  type: 'ir.actions.act_window',
  res_model: 'uom.category',
  search_view_id: 'view_search_uom_category',
  domain: [],
  context: { allow_to_change_reference: 1 },
  views: {
    tree: 'view_tree_uom_category',
    form: 'view_form_uom_category'
  }
}

export default {
  view_form_uom_category,
  view_tree_uom_category,
  view_search_uom_category,
  action_uom_category
}
