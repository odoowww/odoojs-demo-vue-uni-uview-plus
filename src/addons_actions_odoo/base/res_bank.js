const view_form_res_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.bank',
  type: 'form',

  title: { field_name: {} },

  arch: {
    header: {},
    sheet: {
      field_active: { invisible: 1 },
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },
      group_bank_details: {
        // attr: { string: 'Name' },
        field_name: {},
        field_bic: {}
      },
      group: {
        // attr: { string: 'Details' },
        group_address_details: {
          // attr: { string: 'Details2' },
          label: { for: 'street', string: '地址' },
          group_address: {
            field_street: { nolabel: 0, placeholder: 'Street...' },
            field_street2: { nolabel: 0, placeholder: 'Street 2...' },
            field_city: { nolabel: 0, placeholder: 'City' },
            field_state: { nolabel: 0, placeholder: 'State' },
            field_zip: { nolabel: 0, placeholder: 'ZIP' },
            field_country: { placeholder: 'Country' }
          }
        },
        group_communication_details: {
          field_phone: {},
          field_email: { widget: 'email' },
          field_active: { invisible: '1' }
        }
      }
    }
  }
}

const view_tree_res_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.bank',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_bic: {},
      field_country: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_bic: {} },
      card_value: { field_email: {} }
    }
  }
}

const view_search_res_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.bank',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_res_bank = {
  _odoo_model: 'ir.actions.act_window',
  name: '银行',
  type: 'ir.actions.act_window',
  res_model: 'res.bank',
  search_view_id: 'view_search_res_bank',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_bank',
    form: 'view_form_res_bank'
  }
}

export default {
  view_form_res_bank,
  view_tree_res_bank,
  view_search_res_bank,
  action_res_bank,

  model_res_bank: {
    _odoo_model: 'ir.model',
    fields: {
      active: { string: '启用' },
      bic: { string: '代码' },
      city: { string: '市县' },
      country: { string: '国家' },
      display_name: { string: '全名' },
      email: { string: '邮箱' },
      name: { string: '名称' },
      phone: { string: '电话' },
      state: { string: '省' },
      street: { string: '街道' },
      street2: { string: '详细地址' },
      zip: { string: '邮政编码' }
    }
  }
}
