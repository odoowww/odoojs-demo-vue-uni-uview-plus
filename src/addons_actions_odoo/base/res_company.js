const view_company_form = {
  _odoo_model: 'ir.ui.view',
  model: 'res.company',
  type: 'form',
  arch: {
    sheet: {
      field_logo: { widget: 'image' },
      field_name: { placeholder: 'e.g. My Company' },

      notebook: {
        page_general_info: {
          attr: {
            name: 'general_info',
            string: 'General Information'
          },
          group: {
            group: {
              field_partner_id: {
                string: 'Contact',
                groups: 'base.group_no_one',
                required: 0,
                readonly: '1'
              },
              group_address: {
                label_address_text: { string: 'Address' },
                field_street: { placeholder: 'Street...' },
                field_street2: { placeholder: 'Street 2...' },
                field_city: { placeholder: 'City' },
                field_state_id: { placeholder: 'State' },
                field_zip: { placeholder: 'ZIP' },
                field_country_id: { placeholder: 'Country' }
              },

              field_vat: {},
              field_company_registry: {},
              field_currency_id: { context: { active_test: false } }
            },

            group_2: {
              field_phone: {},
              field_mobile: {},
              field_email: {},
              field_website: {
                widget: 'url',
                string: 'Website',
                placeholder: 'e.g. https://www.odoo.com'
              },
              field_parent_id: { groups: 'base.group_multi_company' },
              field_sequence: { invisible: 1 }
              // field_favicon: {
              //   widget: 'image',
              //   size: 'small',
              //   groups: 'base.group_no_one'
              // }
            },
            group_social_media: {
              attr: { name: 'social_media' }
            }
          }
        }
      }
    }
  }
}

const view_company_tree = {
  _odoo_model: 'ir.ui.view',
  model: 'res.company',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_partner_id: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_partner_id: {} }
    }
  }
}

const view_form_res_company = view_company_form
const view_tree_res_company = view_company_tree

const action_res_company = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Companies',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'res.company',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_company',
    form: 'view_form_res_company'
  }
}

export default {
  view_form_res_company,
  view_tree_res_company,
  action_res_company
}
