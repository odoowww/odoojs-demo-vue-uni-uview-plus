const view_form_res_partner_category = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.category',
  type: 'form',

  title: { field_display_name: {} },

  arch: {
    sheet: {
      group_1: {
        field_name: {
          // placeholder: 'e.g. "Consulting Services"'
        },
        // field_color: { widget: 'color_picker' },
        field_parent_id: {},
        field_active: { widget: 'boolean_toggle' }
      }
    }
  }
}

const view_tree_res_partner_category = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.category',
  type: 'tree',
  arch: {
    sheet: {
      field_display_name: {},
      field_name: {},
      field_parent_id: {},
      field_active: { widget: 'boolean_toggle' }
      // field_color: {
      //   widget: 'color_picker'
      // }
    },
    kanban: {
      card_title: { field_display_name: {} },
      card_label: { field_color: {} },
      card_value: { field_parent_id: {} }
    }
  }
}

const view_search_res_partner_category = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.category',
  type: 'search',
  arch: {
    sheet: {
      field_name: { string: '名称' },
      field_display_name: { string: '全名' },
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_res_partner_category = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人标签',
  type: 'ir.actions.act_window',
  res_model: 'res.partner.category',
  search_view_id: 'view_search_res_partner_category',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_partner_category',
    form: 'view_form_res_partner_category'
  }
}

export default {
  view_form_res_partner_category,
  view_tree_res_partner_category,
  view_search_res_partner_category,
  action_res_partner_category,

  model_res_partner_category: {
    _odoo_model: 'ir.model',
    fields: {
      active: { string: '激活' },
      color: {},
      display_name: { string: '全名', disable_field_onchange: 1 },
      name: { string: '名称', required: true, placeholder: '名称' },
      parent_id: { string: '上级' },
      child_ids: {
        string: '下级',
        readonly: 1,
        fields: {
          name: { string: '名称', required: true, placeholder: '名称' },
          display_name: { string: '全名' },
          partner_ids: {
            string: '联系人',
            readonly: 1,
            fields: {
              display_name: { string: '名称' }
            }
          }
        }
      },
      partner_ids: {
        string: '联系人',
        readonly: 1,
        fields: {
          display_name: { string: '名称' }
        }
      }
    }
  }
}
