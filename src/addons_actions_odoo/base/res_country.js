const view_form_res_country = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country',
  type: 'form',
  title: { field_name: {} },

  arch: {
    sheet: {
      field_image_url: { widget: 'image_url' },

      group_main_group: {
        group_country_details: {
          field_name: {},
          field_currency_id: { domain: [] },
          field_code: {}
        },

        group_phone_vat_settings: {
          field_phone_code: {},
          field_vat_label: {},
          field_zip_required: {},
          field_state_required: {}
        }
      },

      group_advanced_address_formatting: {
        attr: {
          name: 'advanced_address_formatting',
          string: 'Advanced Address Formatting',
          groups: 'base.group_no_one'
        },

        field_address_view_id: {},

        widget_text:
          'Choose a subview of partners that includes only address fields, to change the way users can input addresses.',
        field_address_format: { placeholder: 'Address format...' },
        widget_text__2: 'Change the way addresses are displayed in reports',
        field_name_position: {}
      },

      label: { for: 'state_ids', string: '州省' },

      field_state_ids: {
        nolabel: 1,
        context({ record }) {
          const { state_ids = [] } = record
          return {
            state_ids: state_ids
          }
        },
        views: {
          tree: {
            arch: {
              sheet: {
                field_name: {},
                field_code: {}
              },
              kanban: {
                card_title: { field_name: {} },
                card_label: {},
                card_value: { field_code: {} }
              }
            }
          },
          form: {
            arch: {
              sheet: { field_name: {}, field_code: {} }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_country = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_code: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_code: {} }
    }
  }
}

const action_res_country = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Countries',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: true, delete: false },
  res_model: 'res.country',
  domain: [],
  order: 'code',
  context: { ctxtest: 1 },
  fields: {
    name: {},
    state_ids: {
      fields: {
        name: { a: 1 }
      }
    }
  },
  views: {
    tree: 'view_tree_res_country',
    form: 'view_form_res_country'
  }
}

export default {
  view_form_res_country,
  view_tree_res_country,
  action_res_country,

  model_res_country: {
    _odoo_model: 'ir.model',
    fields: {
      address_format: {},
      address_view_id: {},
      code: { string: '编码', readonly: 1, required: true },
      currency_id: {},
      image_url: { string: '国旗', widget: 'image_url' },
      name: {
        string: '名称',
        readonly: 1,
        required: true,
        placeholder: '名称'
      },
      name_position: {
        selection: [
          ['before', '地址前'],
          ['after', '地址后']
        ]
      },
      phone_code: {},
      state_ids: { string: '州省' },
      state_required: {},
      vat_label: {},
      zip_required: {}
    }
  },

  model_res_country_state: {
    _odoo_model: 'ir.model',
    fields: {
      code: {},
      country_id: {},
      name: {}
    }
  }
}
