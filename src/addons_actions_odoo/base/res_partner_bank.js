const view_form_res_partner_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.bank',
  type: 'form',
  arch: {
    header: {},
    sheet: {
      widget: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      group: {
        group_name: {
          field_sequence: { invisible: '1' },
          field_acc_type: { invisible: '1' },
          field_acc_number: {},
          field_company_id: { groups: 'base.group_multi_company' },
          field_partner_id: {},
          field_acc_holder_name: {}
        },

        group_bank: {
          field_bank_id: {},
          // field_currency_id: { groups: 'base.group_multi_currency' },
          field_allow_out_payment: { widget: 'boolean_toggle' },
          field_active: { invisible: '1' }
        }
      }
    }
  }
}

const view_tree_res_partner_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.bank',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_acc_number: {},
      field_bank_name: {},
      field_company_id: { groups: 'base.group_multi_company' },
      field_partner_id: {},
      field_acc_holder_name: { invisible: '1' },
      field_allow_out_payment: { widget: 'boolean_toggle' }
    },

    kanban: {
      card_title: { field_partner_id: {} },
      card_label: { field_bank_name: {} },
      card_value: { field_acc_number: {} }
    }
  }
}

const view_search_res_partner_bank = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.bank',
  type: 'search',
  arch: {
    sheet: {
      field_bank_name: {
        default: 1,
        string: 'Bank Name',
        filter_domain: [
          '|',
          ['bank_name', 'ilike', { self: {} }],
          ['acc_number', 'ilike', { self: {} }]
        ]
      },
      field_company_id: {
        string: 'Company',
        // invisible="context.get['company_hide', True]"
        invisible: { context_company_hide: true }
      },
      field_partner_id: { string: 'Partner' }
    }
  }
}

const action_res_partner_bank = {
  _odoo_model: 'ir.actions.act_window',
  name: '银行账户',
  type: 'ir.actions.act_window',
  res_model: 'res.partner.bank',
  search_view_id: 'view_search_res_partner_bank',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_partner_bank',
    form: 'view_form_res_partner_bank'
  }
}

export default {
  view_form_res_partner_bank,
  view_tree_res_partner_bank,
  view_search_res_partner_bank,
  action_res_partner_bank,

  model_res_partner_bank: {
    _odoo_model: 'ir.model',
    fields: {
      acc_holder_name: { string: '账户名称' },
      acc_number: { string: '账号' },
      acc_type: {},
      active: {},
      allow_out_payment: {},
      bank_id: {},
      bank_name: {},
      company_id: {},
      currency_id: {},
      partner_id: { string: '账户' },
      sequence: {}
    }
  }
}
