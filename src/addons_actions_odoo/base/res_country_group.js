const view_tree_res_country_group = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country.group',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {}
    },
    kanban: {
      card_title: { field_name: {} }
    }
  }
}

const view_form_res_country_group = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country.group',
  type: 'form',

  arch: {
    sheet: {
      group_title: {
        group: {
          field_name: { string: 'Group Name', placeholder: 'e.g. Europe' }
        }
      },

      group_country_group: {
        field_country_ids: {
          widget: 'many2many_tags',
          domain_test: [
            '|',
            ['name', '=', 1],
            ['name', '=', []],
            ['name', '=', { field_id: {} }]
          ]
        }
      }
    }
  }
}

const action_res_country_group = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Country Group',
  type: 'ir.actions.act_window',
  res_model: 'res.country.group',

  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_country_group',
    form: 'view_form_res_country_group'
  }
}

export default {
  view_tree_res_country_group,
  view_form_res_country_group,
  action_res_country_group
}
