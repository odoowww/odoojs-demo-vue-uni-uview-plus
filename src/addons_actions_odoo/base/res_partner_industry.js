const view_form_res_partner_industry = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.industry',
  type: 'form',
  arch: {
    sheet: {
      group: {
        field_name: {},
        field_full_name: {},
        field_active: { widget: 'boolean_toggle' }
      }
    }
  }
}

const view_tree_res_partner_industry = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.industry',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_full_name: {},
      field_active: { invisible: 1 }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_full_name: {} },
      card_value: { field_active: {} }
    }
  }
}

const view_search_res_partner_industry = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.industry',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},
      field_full_name: {},
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_res_partner_industry = {
  _odoo_model: 'ir.actions.act_window',
  name: '行业类型',
  type: 'ir.actions.act_window',
  res_model: 'res.partner.industry',
  search_view_id: 'view_search_res_partner_industry',
  domain: [],
  order: 'full_name',
  context: {},
  views: {
    tree: 'view_tree_res_partner_industry',
    form: 'view_form_res_partner_industry'
  }
}

export default {
  view_form_res_partner_industry,
  view_tree_res_partner_industry,
  view_search_res_partner_industry,
  action_res_partner_industry,

  model_res_partner_industry: {
    _odoo_model: 'ir.model',
    fields: {
      name: {},
      full_name: {},
      active: {}
    }
  }
}
