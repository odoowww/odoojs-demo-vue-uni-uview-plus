const module_form = {
  _odoo_model: 'ir.ui.view',
  model: 'ir.module.module',
  type: 'form',
  arch: {
    header: {},
    sheet: {
      field_icon_image: { widget: 'image' },
      field_shortdesc: { placeholder: 'Module Name' },
      field_author: { nolabel: 1, placeholder: 'Author Name' },
      field_state: { invisible: '1' },
      field_to_buy: { invisible: '1' },
      field_has_iap: { invisible: '1' },
      button_immediate_install: {
        invisible: '1',
        name: 'button_immediate_install',
        type: 'object',
        string: 'Activate',
        groups: 'base.group_system'

        // invisible: ['|', ['to_buy', '=', true], ['state', '!=', 'uninstalled']]
      },

      widget_text__: {
        attr: {
          // 'invisible': [('has_iap', '=', False)]
          invisible: [['has_iap', '=', false]],
          text: 'Contains In-App Purchases'
        }
      },

      notebook: {
        attr: { groups: 'base.group_no_one' },
        page_information: {
          attr: { name: 'information', string: 'Information' },
          group: {
            group: {
              field_website: {
                widget: 'url',
                invisible: [['website', '=', false]]
              },
              field_category_id: {},
              field_summary: {}
            },
            group_2: {
              field_name: {},
              field_license: {},
              field_installed_version: {}
            }
          }
        },
        page_technical_data: {
          attr: {
            name: 'technical_data',
            string: 'Technical Data'
          },
          group: {
            attr: { col: 24 },
            field_demo: {},
            field_application: {},
            field_state: {}
          },
          group_2: {
            attr: {
              string: 'Created Views',
              // 'invisible':[('state','!=','installed')]
              invisible: [['state', '!=', 'installed']]
            }
          },
          widget_text__: {
            attr: {
              text: '-This module does not create views.',
              // 'invisible':
              // ['|',('views_by_module','not in',[None,False]),
              // ('state','!=','installed')]

              invisible: [
                '|',
                ['views_by_module', 'not in', [null, false]],
                ['state', '!=', 'installed']
              ]
            }
          },

          field_views_by_module: {},
          group_164: {
            attr: {
              string: 'Dependencies'
            }
          },

          widget_text__3333: {
            attr: {
              text: '-This module does not depends on any other module.',
              // invisible: [['dependencies_id', 'not in', [None, false]]],
              invisible: [['dependencies_id', 'not in', [null, false]]]
            }
          },

          field_dependencies_id: {
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_state: {}
                  },
                  kanban: {
                    card_title: { field_name: {} },
                    card_label: {},
                    card_value: { field_state: {} }
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_state: {}
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const module_tree = {
  _odoo_model: 'ir.ui.view',
  model: 'ir.module.module',
  type: 'tree',
  arch: {
    sheet: {
      field_author: {},
      field_name: {},
      field_author: {},
      field_website: {},
      field_installed_version: {},
      field_state: { widget: 'badge' },
      field_category_id: { invisible: '1' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_author: {} },
      card_value: { field_state: {} }
    }
  }
}

const view_module_filter = {
  _odoo_model: 'ir.ui.view',
  model: 'ir.module.module',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Module',
        filter_domain: [
          '|',
          '|',
          ['summary', 'ilike', { self: {} }],
          ['shortdesc', 'ilike', { self: {} }],
          ['name', 'ilike', { self: {} }]
        ]
      },
      field_category_id: {},

      filter_app: {
        app: {
          name: 'app',
          string: 'Apps',
          domain: [['application', '=', true]]
        },
        extra: {
          name: 'extra',
          string: 'Extra',
          domain: [['application', '=', false]]
        }
      },
      filter_state: {
        installed: {
          name: 'installed',
          string: 'Installed',
          domain: [['state', 'in', ['installed', 'to upgrade', 'to remove']]]
        },
        not_installed: {
          name: 'not_installed',
          string: 'Not Installed',
          domain: [
            ['state', 'in', ['uninstalled', 'uninstallable', 'to install']]
          ]
        }
      }
    }
  }
}

const view_form_ir_module_module = module_form
const view_tree_ir_module_module = module_tree
const view_search_ir_module_module = view_module_filter

const action_ir_module_module = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Apps',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'ir.module.module',
  search_view_id: 'view_search_ir_module_module',

  domain: [],
  context: { search_default_app: 1 },
  views: {
    tree: 'view_tree_ir_module_module',
    form: 'view_form_ir_module_module'
  }
}

export default {
  view_form_ir_module_module,
  view_tree_ir_module_module,
  view_search_ir_module_module,
  action_ir_module_module
}
