import ir_module_module from './ir_module_module.js'
import res_company from './res_company.js'
import res_users from './res_users.js'

import res_currency from './res_currency.js'
import res_lang from './res_lang.js'

import res_country_group from './res_country_group.js'
import res_country from './res_country.js'

import res_partner_category from './res_partner_category.js'
import res_partner_industry from './res_partner_industry.js'
import res_partner_title from './res_partner_title.js'

import res_bank from './res_bank.js'
import res_partner_bank from './res_partner_bank.js'
import res_partner from './res_partner.js'

export default {
  ...ir_module_module,
  ...res_company,
  ...res_users,

  ...res_country_group,
  ...res_country,
  ...res_currency,
  ...res_lang,
  ...res_partner_category,
  ...res_partner_industry,
  ...res_partner_title,

  ...res_bank,
  ...res_partner_bank,
  ...res_partner
}
