const view_form_res_partner = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner',
  type: 'form',
  title: { field_name: {} },
  arch: {
    alert_div: {
      // _div_same_vat_partner_id: {
      //   _attr: {
      //     attrs: {
      //       invisible({ record, editable }) {
      //         // oe_edit_only
      //         // 'invisible': [('same_vat_partner_id', '=', False)]
      //         const { same_vat_partner_id } = record
      //         return !editable || !same_vat_partner_id
      //       }
      //     },
      //     text({ record }) {
      //       const { same_vat_partner_id } = record
      //       return `A partner with the same Tax ID already exists (${same_vat_partner_id}), are you sure to create a new one?`
      //     }
      //   }
      // },
      // _div_same_company_registry_partner_id: {
      //   _attr: {
      //     attrs: {
      //       invisible({ record, editable }) {
      //         // oe_edit_only
      //         //'invisible': [('same_company_registry_partner_id', '=', False)]
      //         const { same_company_registry_partner_id } = record
      //         return !editable || !same_company_registry_partner_id
      //       }
      //     },
      //     text({ record }) {
      //       const { same_company_registry_partner_id } = record
      //       return `A partner with the same Company Registry already exists (${same_company_registry_partner_id}), are you sure to create a new one?`
      //     }
      //   }
      // },
    },

    button_box: {},

    sheet: {
      field_active: { invisible: 1 },

      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      field_user_ids: { invisible: 1 },

      field_is_company: { invisible: 1 },
      field_commercial_partner_id: { invisible: 1 },

      field_company_id: { invisible: 1 },
      field_country_code: { invisible: 1 },

      group_name: {
        group_name: {
          group: {
            field_company_type: {
              string: '联系人类型',
              widget: 'radio',
              invisible: [['type', '!=', 'contact']]
            },

            field_name: {
              string: '名称',
              required: [['type', '=', 'contact']],
              placeholder: '名称'
            }
          }
        },

        group_img: {
          field_avatar_128: { invisible: 1 },
          field_image_1920: {
            string: '',
            widget: 'image',
            preview_image: 'avatar_128'
          }
        }
      },

      group_parent: {
        group: {
          group_parent: {
            field_parent_id__company: {
              string: '上级公司',
              invisible: 1,
              // invisible: [['is_company', '!=', true]],
              placeholder: '上级公司'
            },

            field_parent_id__person: {
              string: '所属公司',
              invisible: [
                '|',
                ['is_company', '=', true],
                ['type', '!=', 'contact']
              ],
              placeholder: '所属公司',
              domain: [['is_company', '=', true]]
            },

            field_parent_id__address: {
              string: '联系人',
              invisible: [['type', '=', 'contact']],
              placeholder: '联系人',
              domain: [['type', '=', 'contact']]
            },

            // field_parent_id: {
            //   widget: 'res_partner_many2one',
            //   placeholder: 'Company Name...',
            //   // context="{'default_is_company': True, 'show_vat': True, 'default_user_id': user_id}"
            //   attrs: {
            //     // invisible:
            //     // ['|',
            //     // '&amp;',
            //     // ('is_company','=', True),
            //     // ('parent_id', '=', False),
            //     // ('company_name', '!=', False),
            //     // ('company_name', '!=', '')]

            //     invisible: [
            //       '|',
            //       '&',
            //       ['is_company', '=', true],
            //       ['parent_id', '=', false],
            //       ['company_name', '!=', false],
            //       ['company_name', '!=', '']
            //     ]
            //   },
            //   // domain: [('is_company', '=', True)]
            //   domain: [['is_company', '=', true]]
            // },

            field_company_name: {
              invisible: 1
              //invisible: ['|',
              // '|',
              // ('company_name', '=', False),
              // ('company_name', '=', ''),
              // ('is_company', '=', True)]

              // invisible: [
              //   '|',
              //   '|',
              //   ['company_name', '=', false],
              //   ['company_name', '=', ''],
              //   ['is_company', '=', true]
              // ]
            },

            button_create_company: {
              invisible: 1,
              name: 'create_company',
              string: 'Create company',
              type: 'object'
              // 'invisible': ['|', '|',
              // ('is_company','=', True),
              // ('company_name', '=', ''),
              // ('company_name', '=', False)]

              // invisible: [
              //   '|',
              //   '|',
              //   ['is_company', '=', true],
              //   ['company_name', '=', ''],
              //   ['company_name', '=', false]
              // ]
            }
          }
        }
      },

      field_vat__company: {
        string: '公司税号',
        placeholder: '公司税号',
        invisible: [['is_company', '!=', true]],
        // 'readonly': [('parent_id','!=',False)]
        readonly: [['parent_id', '!=', false]]
      },

      field_vat__person: {
        string: '个人身份证号',
        placeholder: '个人身份证号',
        invisible: 1
        // invisible: ['|', ['type', '!=', 'contact'], ['is_company', '=', true]]
      },

      group_slot: {},

      group_info: {
        group_address: {
          field_type__address: {
            widget: 'radio',
            invisible: [['type', '=', 'contact']],

            selection: [
              // ['contact', 'Contact'],
              ['invoice', '开票地址'],
              ['delivery', '送货地址'],
              ['private', '家庭地址'],
              ['other', '其他地址']
            ]
          },

          field_type___odoo_bak_here: {
            invisible: 1,
            widget: 'radio',
            // invisible: [('is_company','=', True)],
            // invisible: ['|', { editable: false }, ['is_company', '=', true]],
            // 'required': [('is_company','!=', True)],
            required: [['is_company', '!=', true]],

            // 'readonly': [('user_ids', '!=', [])]
            readonly: [['user_ids', '!=', []]]
          },

          group_address: {
            label_address_text: {
              string: '地址',
              // invisible: [('is_company','=', False)],
              invisible: [
                '&',
                ['is_company', '=', false],
                ['type', '!=', 'contact']
              ]
            },

            label_address_type: {
              field_type: {},
              // invisible: [('is_company','=', True)],
              invisible: [
                '|',
                ['is_company', '=', true],
                '&',
                ['is_company', '=', false],
                ['type', '=', 'contact']
              ]
            },

            field_street: {
              placeholder: 'Street...',
              // 'readonly': [('type', '=', 'contact'), ('parent_id', '!=', False)]

              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ]
            },
            field_street2: {
              placeholder: 'Street 2...',
              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ]
            },
            field_city: {
              placeholder: 'City',
              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ]
            },
            field_state_id: {
              placeholder: 'State',
              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ],
              // context: "{'country_id': country_id, 'default_country_id': country_id, 'zip': zip}"
              context: {
                country_id: { field_country_id: {} },
                default_country_id: { field_country_id: {} },
                zip: { field_zip: {} }
              }
            },
            field_zip: {
              placeholder: 'ZIP',
              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ]
            },
            field_country_id: {
              placeholder: 'Country',
              readonly: [
                ['type', '=', 'contact'],
                ['parent_id', '!=', false]
              ]
            }
          }
        },

        group_comunication: {
          field_function: {
            // invisible: [('is_company','=', True)]
            // invisible: [['is_company', '=', true]]

            invisible: [
              '|',
              ['type', '!=', 'contact'],
              ['is_company', '=', true]
            ]
          },
          field_phone: { widget: 'phone' },
          field_mobile: { widget: 'phone' },
          field_user_ids: { invisible: 1 },
          field_email: {
            // context: "{'gravatar_image': True}",
            // 'required': [('user_ids','!=', [])]
            required: [['user_ids', '!=', []]]
          },
          field_website: {
            widget: 'url',
            placeholder: 'e.g. https://www.odoo.com'
          },
          field_title: {
            placeholder: 'e.g. Mister',
            // invisible: [('is_company','=', True)]
            // invisible: [['is_company', '=', true]],
            invisible: [
              '|',
              ['type', '!=', 'contact'],
              ['is_company', '=', true]
            ]
          },
          field_active_lang_count: { invisible: 1 },
          field_lang: {
            invisible: 1
            // 'invisible': [('active_lang_count', '&lt;=', 1)]
            // invisible: [['active_lang_count', '<=', 1]]
          },
          field_category_id: {
            widget: 'many2many_tags',
            placeholder: 'e.g. "B2B", "VIP", "Consulting", ...'
          }
        }
      },

      notebook: {
        page_contact_addresses: {
          attr: {
            string: '联系人及地址',
            invisible: [['type', '!=', 'contact']]
          },
          field_child_ids: {
            //   // context="{
            //   // 'default_parent_id': active_id,
            //   // 'default_street': street, 'default_street2': street2,
            //   // 'default_city': city, 'default_state_id': state_id,
            //   // 'default_zip': zip, 'default_country_id': country_id,
            //   // 'default_lang': lang, 'default_user_id': user_id,
            //   // 'default_type': 'other'}">
            //   //

            context: {
              default_parent_id: { field_id: {} },
              default_street: { field_street: {} },
              default_street2: { field_street2: {} },
              default_city: { field_city: {} },
              default_state_id: { field_state_id: {} },
              default_zip: { field_zip: {} },
              default_country_id: { field_country_id: {} },
              default_lang: { field_lang: {} },
              default_user_id: { field_user_id: {} },
              default_type: 'other'
            },

            views: {
              tree: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_type: {},
                    field_function: {},
                    field_email: {},
                    field_zip: { invisible: 1 },
                    field_city: {},
                    field_state_id: { invisible: 1 },
                    field_country_id: { invisible: 1 },
                    field_phone: { invisible: 1 },
                    field_mobile: { invisible: 1 }
                  },
                  kanban: {
                    card_title: { field_name: {} },
                    card_label: { field_type: {} },
                    card_value: { field_email: {} }
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_type: { required: '1', widget: 'radio' },
                    field_parent_id: { invisible: 1 },

                    subgroup: {
                      attr: {
                        invisible: { editable: false }
                      },

                      widget_text__contact: {
                        attr: {
                          text: 'Use this to organize the contact details of employees of a given company (e.g. CEO, CFO, ...).',

                          // 'invisible': [('type', '!=', 'contact')]
                          invisible: [['type', '!=', 'contact']]
                        }
                      },

                      widget_text__invoice: {
                        attr: {
                          text: 'Preferred address for all invoices. Selected by default when you invoice an order that belongs to this company.',

                          // 'invisible': [('type', '!=', 'invoice')]
                          invisible: [['type', '!=', 'invoice']]
                        }
                      },
                      widget_text__delivery: {
                        attr: {
                          text: 'Preferred address for all deliveries. Selected by default when you deliver an order that belongs to this company.',

                          // 'invisible': [('type', '!=', 'delivery')]
                          invisible: [['type', '!=', 'delivery']]
                        }
                      },
                      widget_text__private: {
                        attr: {
                          text: 'Private addresses are only visible by authorized users and contain sensitive data (employee home addresses, ...).',

                          // 'invisible': [('type', '!=', 'private')]
                          invisible: [['type', '!=', 'private']]
                        }
                      },

                      widget_text__other: {
                        attr: {
                          text: 'Other address for the company (e.g. subsidiary, ...)',

                          // 'invisible': [('type', '!=', 'other')]
                          invisible: [['type', '!=', 'other']]
                        }
                      }
                    },

                    // _hr: {},

                    group: {
                      group_name: {
                        field_name: {
                          string: '名称',
                          // required: [('type', '=', 'contact')]
                          required: [['type', '=', 'contact']]
                        },
                        field_title: {
                          //'invisible': [('type','!=', 'contact')]
                          invisible: [['type', '!=', 'contact']]
                        },
                        field_function: {
                          placeholder: 'e.g. Sales Director',
                          //'invisible': [('type','!=', 'contact')]
                          invisible: [['type', '!=', 'contact']]
                        },

                        group_address: {
                          attr: {
                            // widget: 'partner_address',
                            //'invisible': [('type','=', 'contact')]
                            invisible: [['type', '=', 'contact']]
                          },
                          label_address: {
                            string: '地址'
                          },

                          group: {
                            field_street: { placeholder: 'Street...' },
                            field_street2: { placeholder: 'Street 2...' },
                            field_city: { placeholder: 'City' },
                            field_state_id: { placeholder: 'State' },
                            field_zip: { placeholder: 'ZIP' },
                            field_country_id: { placeholder: 'Country' }
                          }
                        }
                      },
                      group_comunication: {
                        field_email: { widget: 'email' },
                        field_phone: { widget: 'phone' },
                        field_mobile: { widget: 'phone' },
                        field_company_id: { invisible: 1 }
                      }
                    },
                    group_comment: {
                      field_comment: {}
                    },
                    field_lang: { invisible: 1 },
                    field_user_id: { invisible: 1 }
                  }
                }
              }
            }
          }
        },

        page_internal_notes: {
          attr: { name: 'internal_notes', string: '备注' },
          field_comment: { placeholder: 'Internal notes...' },
          group_slot: {}
        }
      }
    }
  }
}

const view_tree_res_partner = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner',
  type: 'tree',
  arch: {
    sheet: {
      field_display_name: {},
      field_company_type: { invisible: '1' },
      field_type: { widget: 'partner_type' },
      field_function: { invisible: '1' },
      field_phone: { optional: 'show' },
      field_email: { optional: 'show' },
      field_user_id: { optional: 'show' },
      field_city: { optional: 'show' },
      field_state_id: { optional: 'hide', readonly: '1' },
      field_country_id: { optional: 'show', readonly: '1' },
      field_vat: { optional: 'hide', readonly: '1' },
      field_category_id: { optional: 'show', widget: 'many2many_tags' },
      field_company_id: { readonly: '1', groups: 'base.group_multi_company' },
      field_is_company: { invisible: '1' },
      field_parent_id: { invisible: '1', readonly: '1' },
      field_active: { invisible: '1' }
    },

    kanban: {
      card_title: { field_display_name: {} },
      card_label: { field_type: { widget: 'partner_type' } },
      card_value: { field_phone: {}, field_email: {} }
    }
  }
}

const view_search_res_partner = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '名称',
        filter_domain: [
          '|',
          '|',
          ['display_name', 'ilike', { self: {} }],
          ['ref', 'ilike', { self: {} }],
          ['email', 'ilike', { self: {} }]
        ]
      },
      field_parent_id: {
        // domain: [['is_company', '=', true]],
        operator: 'child_of'
      },

      field_email: {
        filter_domain: [['email', 'ilike', { self: {} }]]
      },

      field_phone: {
        filter_domain: [
          '|',
          ['phone', 'ilike', { self: {} }],
          ['mobile', 'ilike', { self: {} }]
        ]
      },
      field_category_id: {
        filter_domain: [['category_id', 'child_of', { self: {} }]]
      },

      field_user_id: {},

      filter_type: {
        type_address: {
          name: 'type_address',
          string: '地址',
          domain: ['&', ['is_company', '=', false], ['type', '!=', 'contact']]
        },

        type_person: {
          name: 'type_person',
          string: '个人',
          // domain: [['is_company', '=', false]]
          domain: ['&', ['is_company', '=', false], ['type', '=', 'contact']]
        },

        type_company: {
          name: 'type_company',
          string: '公司',
          domain: [['is_company', '=', true]]
        }
      },

      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

export default {
  view_form_res_partner,
  view_tree_res_partner,
  view_search_res_partner
}
