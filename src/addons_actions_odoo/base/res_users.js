const view_users_form = {
  _odoo_model: 'ir.ui.view',
  model: 'res.users',
  type: 'form',
  arch: {
    sheet: {
      // _div_button_box: {
      //   // todo
      //   // accesses_count: { string: 'Access Rights' },
      // },

      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible: [('active', '=', true)]
          invisible: [['active', '=', true]]
        }
      },
      field_active_partner: { required: 0, readonly: '1', invisible: '1' },

      field_avatar_128: { invisible: '1' },
      field_image_1920: { widget: 'image', preview_image: 'avatar_128' },

      field_name: { required: '1', placeholder: 'e.g. John Doe' },

      field_email: { invisible: '1' },
      field_login: {
        string: 'Email Address',
        placeholder: 'e.g. email@yourcompany.com'
      },

      group: {
        group: {
          field_partner_id: {
            nolabel: 0,
            readonly: '1',
            groups: 'base.group_no_one',
            // 'invisible': [('id', '=', False)]
            invisible: [['id', '=', false]]
          },
          field_share: { invisible: '1' }
        }
      },

      notebook: {
        page_access_rights: {
          attr: { name: 'access_rights', string: 'Access Rights' },
          group_access_rights: {
            attr: {
              string: 'Multi Companies',
              // 'invisible': [('companies_count', '&lt;=', 1)]
              invisible: [['companies_count', '<=', 1]]
            },
            field_company_ids: {
              widget: 'many2many_tags',
              string: 'Allowed Companies'
            },
            field_company_id: {
              string: 'Default Company',
              context: { user_preference: 0 }
            },
            field_companies_count: {
              invisible: '1',
              string: 'Companies count'
            }
          },
          field_groups_id: {
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_full_name: {},
                    field_category_id: {}
                  },
                  kanban: {
                    card_title: { field_full_name: {} },
                    card_label: {},
                    card_value: {}
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_name: {}
                  }
                }
              }
            }
          }
        },

        page_preferences: {
          attr: { name: 'preferences', string: 'Preferences' },
          group: {
            group_preferences: {
              attr: { name: 'preferences', string: 'Localization' },
              field_active: { invisible: '1', string: 'Active' },

              field_lang: { required: '1' },
              button_lang: {
                invisible: '1',
                type: 'action',
                name: 'base.action_view_base_language_install',
                title: 'Add a language'
              },
              field_tz: { widget: 'timezone_mismatch' },
              field_tz_offset: { invisible: '1' }
            },
            group_2: {
              attr: {
                string: 'Menus Customization',
                groups: 'base.group_no_one',
                // 'invisible': [('share', '=', True)]
                invisible: [['share', '=', true]]
              },
              field_action_id: {}
            }
          },
          group_messaging: {
            field_signature: {},
            field_login_date: {}
          }
        }
      }
    }
  }
}

const view_users_tree = {
  _odoo_model: 'ir.ui.view',
  model: 'res.users',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_login: {},
      field_lang: {},
      field_login_date: {},
      field_company_id: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_login: {} },
      card_value: { field_login_date: {} }
    }
  }
}

const view_users_search = {
  _odoo_model: 'ir.ui.view',
  model: 'res.users',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'User',
        filter_domain: [
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['login', 'ilike', { self: {} }],
          ['email', 'ilike', { self: {} }]
        ]
      },

      field_company_ids: { string: 'Company' },

      filter_share: {
        filter_no_share: {
          name: 'filter_no_share',
          string: 'Internal Users',
          domain: [['share', '=', false]]
        },
        filter_share: {
          name: 'filter_share',
          string: 'Portal Users',
          domain: [['share', '=', true]]
        }
      },

      filter_active: {
        inactive: {
          name: 'Inactive',
          string: 'Inactive Users',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const view_form_res_users = view_users_form
const view_tree_res_users = view_users_tree
const view_search_res_users = view_users_search

const action_res_users = {
  _odoo_model: 'ir.actions.act_window',
  name: '用户',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'res.users',
  search_view_id: 'view_search_res_users',
  domain: [],
  context: {
    search_default_no_share: 1,
    show_user_group_warning: true
  },
  views: {
    tree: 'view_tree_res_users',
    form: 'view_form_res_users'
  }
}

export default {
  view_form_res_users,
  view_tree_res_users,
  view_search_res_users,
  action_res_users
}
