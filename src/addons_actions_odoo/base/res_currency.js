const view_search_res_currency = {
  _odoo_model: 'ir.ui.view',
  model: 'res.currency',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Currency',

        // ('|','|','|','|',
        //  ('name', 'ilike', self),
        //    ('full_name', 'ilike', self),
        //    ('symbol', 'ilike', self),
        //    ('currency_unit_label', 'ilike', self),
        //    ('currency_subunit_label', 'ilike', self),
        //    )

        filter_domain: [
          '|',
          '|',
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['full_name', 'ilike', { self: {} }],
          ['symbol', 'ilike', { self: {} }],
          ['currency_unit_label', 'ilike', { self: {} }],
          ['currency_subunit_label', 'ilike', { self: {} }]
        ]
      },

      filter_active: {
        active: {
          string: 'Active',
          domain: [['active', '=', true]]
        },
        inactive: {
          string: 'Inactive',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const view_tree_res_currency = {
  _odoo_model: 'ir.ui.view',
  model: 'res.currency',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_symbol: {},
      field_full_name: { string: 'Name' },
      field_date: { string: 'Last Update' },
      field_rate: {},
      field_inverse_rate: {},
      field_active: { widget: 'boolean_toggle' }
    },

    kanban: {
      card_title: { field_name: {} },
      card_label: { field_symbol: {} },
      card_value: { field_date: { string: 'Last Update' }, field_rate: {} }
    }
  }
}

const view_form_res_currency = {
  _odoo_model: 'ir.ui.view',
  model: 'res.currency',
  type: 'form',
  arch: {
    sheet: {
      field_is_current_company_currency: { invisible: '1' },
      // _div: {
      //   _attr: {
      //     groups: 'base.group_no_one',
      //     // oe_edit_only
      //     invisible({ editable }) {
      //       return !editable
      //     },
      //     text: 'You cannot reduce the number of decimal places of a currency already used on an accounting entry.'
      //   }
      // },

      // _div_2: {
      //   _attr: {
      //     attrs: {
      //       invisible({ record }) {
      //         // 'invisible': [('is_current_company_currency','=',False)]
      //         const { is_current_company_currency } = record
      //         return !is_current_company_currency
      //       }
      //     },

      //     text: "This is your company's currency."
      //   }
      // },

      group_1: {
        group_11: {
          field_name: {},
          field_full_name: { string: 'Name' },
          field_active: { widget: 'boolean_toggle' }
        },
        group_12: {
          field_currency_unit_label: {},
          field_currency_subunit_label: {}
        }
      },

      group_2: {
        attr: {
          groups: 'base.group_no_one'
        },
        group_21: {
          field_rounding: {},
          field_decimal_places: {}
        },

        group_22: {
          field_symbol: {},
          field_position: {}
        }
      },

      notebook: {
        attr: {
          // 'invisible': [('is_current_company_currency','=',True)]}"
          invisible: [['is_current_company_currency', '=', true]]
        },

        page_rates: {
          attr: { string: 'Rates' },
          field_rate_ids: {
            nolabel: '1',
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_company_id: { groups: 'base.group_multi_company' },
                    field_company_rate: {},
                    field_inverse_company_rate: {},
                    field_rate: { optional: 'hide' },
                    field_write_date: { optional: 'hide' }
                  },

                  kanban: {
                    card_title: { field_name: {} },
                    card_label: {},
                    card_value: { field_company_rate: {} }
                  }
                }
              },

              form: {
                arch: {
                  sheet: {
                    field_name: {},
                    field_company_id: { groups: 'base.group_multi_company' },
                    field_company_rate: {},
                    field_inverse_company_rate: {},
                    field_rate: {},
                    field_write_date: {}
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const action_res_currency = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Currencies',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'res.currency',
  search_view_id: 'view_search_res_currency',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_currency',
    form: 'view_form_res_currency'
  }
}

export default {
  view_search_res_currency,
  view_tree_res_currency,
  view_form_res_currency,
  action_res_currency
}
