const view_form_res_lang = {
  _odoo_model: 'ir.ui.view',
  model: 'res.lang',
  type: 'form',
  arch: {
    sheet: {
      // _div_button_box: {
      //   _button: {
      //     _attr: {
      //       name: 'base.action_view_base_language_install',
      //       type: 'action',
      //       string: 'Activate and Translate',
      //       icon: 'fa-refresh'
      //     }
      //   }
      // },

      field_flag_image: { widget: 'image' },
      field_name: { placeholder: 'e.g. French' },

      group_1: {
        group_1: {
          field_code: {},
          field_iso_code: {},
          field_url_code: { invisible: 1, required: 0 },
          field_active: { widget: 'boolean_toggle' }
        },

        group_2: {
          field_direction: {},
          field_grouping: {},
          field_decimal_point: {},
          field_thousands_sep: {},
          field_date_format: {},
          field_time_format: {},
          field_week_start: {}
        }
      },

      widget_text__1: 'Legends for supported Date and Time Formats',
      widget_text__11: '%a - Abbreviated day of the week.',
      widget_text__112: '%A - Full day of the week.',
      widget_text__113: '%b - Abbreviated month name.',
      widget_text__114: '%B - Full month name."',
      widget_text__115: '%d - Day of the month [01,31]."',
      widget_text__116: '%j - Day of the year [001,366]."',
      widget_text__117: '%H - Hour (24-hour clock) [00,23]."',
      widget_text__118: '%I - Hour (12-hour clock) [01,12]."',

      widget_text__1181: '%M - Minute [00,59]."',
      widget_text__1182: '%p - Equivalent of either AM or PM."',
      widget_text__1183: '%S - Seconds [00,61]."',
      widget_text__1184: '%w - Day of the week number [0(Sunday),6]."',
      widget_text__1185: '%y - Year without century [00,99]."',
      widget_text__1186: '%Y - Year with century."',
      widget_text__1187: '%m - Month number [01,12]."',

      widget_text__11891: 'Examples',

      widget_text__11892: '1. %b, %B         ==> Dec, December',
      widget_text__11893: '2. %a ,%A         ==> Fri, Friday',
      widget_text__11894: '3. %y, %Y         ==> 08, 2008',
      widget_text__11895: '4. %d, %m         ==> 05, 12',
      widget_text__11896: '5. %H:%M:%S      ==> 18:25:20',
      widget_text__11897: '6. %I:%M:%S %p  ==> 06:25:20 PM',
      widget_text__11898: '7. %j              ==> 340',
      widget_text__118902: '8. %S              ==> 20',
      widget_text__118903: '9. %w              ==> 5 ( Friday is the 6th day)'
    }
  }
}

const view_tree_res_lang = {
  _odoo_model: 'ir.ui.view',
  model: 'res.lang',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_code: { groups: 'base.group_no_one' },
      field_iso_code: { groups: 'base.group_no_one' },
      field_url_code: { groups: 'base.group_no_one', invisible: 1 },
      field_direction: { groups: 'base.group_no_one' },
      field_active: {}
    },

    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_code: {} }
    }
  }
}

const view_search_res_lang = {
  _odoo_model: 'ir.ui.view',
  model: 'res.lang',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Language',
        filter_domain: [
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['code', 'ilike', { self: {} }],
          ['iso_code', 'ilike', { self: {} }]
        ]
      },
      field_direction: {},

      filter_active: {
        active: {
          name: 'active',
          string: 'Active',
          domain: [['active', '=', true]]
        }
      }
    }
  }
}

const action_res_lang = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Languages',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'res.lang',
  search_view_id: 'view_search_res_lang',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_lang',
    form: 'view_form_res_lang'
  }
}

export default {
  view_form_res_lang,
  view_tree_res_lang,
  view_search_res_lang,
  action_res_lang
}
