const view_form_res_partner_title = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.title',
  type: 'form',
  arch: {
    sheet: {
      group_name: {
        field_name: {},
        field_shortcut: {}
      }
    }
  }
}

const view_tree_res_partner_title = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner.title',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_shortcut: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_shortcut: {} }
    }
  }
}

const action_res_partner_title = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人头衔',
  type: 'ir.actions.act_window',
  res_model: 'res.partner.title',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_partner_title',
    form: 'view_form_res_partner_title'
  }
}

export default {
  view_form_res_partner_title,
  view_tree_res_partner_title,
  action_res_partner_title,

  model_res_partner_title: {
    _odoo_model: 'ir.model',
    fields: {
      name: { string: '名称', required: true, placeholder: '名称' },
      shortcut: { string: '缩写' }
    }
  }
}
