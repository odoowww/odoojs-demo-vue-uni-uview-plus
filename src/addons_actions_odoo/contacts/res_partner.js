const action_res_partner = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人',
  type: 'ir.actions.act_window',
  res_model: 'res.partner',
  search_view_id: 'base.view_search_res_partner',
  domain: [],
  context: {
    default_is_company: true,
    default_type: 'contact',
    search_default_type_company: 1,
    search_default_type_person: 1
  },

  views: {
    tree: 'base.view_tree_res_partner',
    form: 'base.view_form_res_partner'
  }
}

export default {
  action_res_partner
}
