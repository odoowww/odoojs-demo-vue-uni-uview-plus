import mrp_bom from './mrp_bom.js'
import mrp_workorder from './mrp_workorder.js'
import mrp_production from './mrp_production.js'

export default {
  ...mrp_bom,
  ...mrp_workorder,
  ...mrp_production
}
