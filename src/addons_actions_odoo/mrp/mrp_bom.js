const view_form_mrp_bom_sheet = {
  group: {
    group_1: {
      field_active: { invisible: '1' },

      field_company_id: { invisible: '1' },
      field_product_tmpl_id: {},
      field_product_uom_category_id: { invisible: '1' },
      field_allow_operation_dependencies: { invisible: '1' },
      field_product_id: { groups: 'product.group_product_variant' },
      field_product_qty: {},
      field_product_uom_id: { groups: 'uom.group_uom' }
    },

    group_2: {
      field_code: {},
      field_type: { widget: 'radio' },
      field_company_id: { groups: 'base.group_multi_company' }
    }
  },
  notebook: {
    page_components: {
      attr: { name: 'components', string: '组件' },
      field_bom_line_ids: {
        widget: 'one2many',
        context: {
          default_parent_product_tmpl_id: { field_product_tmpl_id: {} },
          default_product_id: false,
          default_bom_id: { field_id: {} }
        },
        views: {
          tree: {
            arch: {
              sheet: {
                field_company_id: { column_invisible: '1' },
                // field_sequence: { widget: 'handle' },
                field_product_id: {},
                field_product_tmpl_id: { column_invisible: '1' },

                // <button name="action_see_attachments" type="object" icon="fa-files-o" aria-label="Product Attachments" title="Product Attachments" class="float-end" column_invisible="context.get('parent_production_id')"/>
                // field_attachments_count: {
                //   column_invisible: "context.get('parent_production_id')"
                // },

                field_product_qty: {},

                field_product_uom_category_id: { column_invisible: '1' },
                field_parent_product_tmpl_id: { column_invisible: '1' },

                field_product_uom_id: { groups: 'uom.group_uom' },
                field_possible_bom_product_template_attribute_value_ids: {
                  column_invisible: '1'
                },
                // field_bom_product_template_attribute_value_ids: {
                //   optional: 'hide',
                //   widget: 'many2many_tags',
                //   column_invisible: 'parent.product_id',
                //   groups: 'product.group_product_variant'
                // },
                field_allowed_operation_ids: { column_invisible: '1' },

                // field_operation_id: {
                //   groups: 'mrp.group_mrp_routings',
                //   column_invisible: [
                //     [{ field_parent_type: {} }, 'not in', ['normal', 'phantom']]
                //   ],

                //   optional: 'hidden'
                // },

                field_manual_consumption_readonly: { column_invisible: '1' },
                field_manual_consumption: {
                  optional: 'hide',
                  readonly: 'manual_consumption_readonly',
                  force_save: '1'
                }
              },
              kanban: {
                card_title: { field_product_id: {} },
                card_label: { field_product_qty: {} },
                card_value: {}
              }
            }
          },
          form: {
            arch: {
              sheet: {
                field_company_id: { invisible: '1' },
                field_sequence: { widget: 'handle' },
                field_product_id: {},
                field_product_tmpl_id: { invisible: '1' },

                // // <button name="action_see_attachments" type="object" icon="fa-files-o" aria-label="Product Attachments" title="Product Attachments" class="float-end" column_invisible="context.get('parent_production_id')"/>
                // field_attachments_count: {
                //   invisible: "context.get('parent_production_id')"
                // },

                field_product_qty: {},

                field_product_uom_category_id: { invisible: '1' },
                field_parent_product_tmpl_id: { invisible: '1' },

                field_product_uom_id: { groups: 'uom.group_uom' },
                field_possible_bom_product_template_attribute_value_ids: {
                  invisible: '1'
                },
                // field_bom_product_template_attribute_value_ids: {
                //   optional: 'hide',
                //   widget: 'many2many_tags',
                //   invisible: 'parent.product_id',
                //   groups: 'product.group_product_variant'
                // },
                field_allowed_operation_ids: { invisible: '1' },

                // field_operation_id: {
                //   groups: 'mrp.group_mrp_routings',
                //   invisible: [
                //     [{ field_parent_type: {} }, 'not in', ['normal', 'phantom']]
                //   ],
                //   optional: 'hidden'
                // },

                field_manual_consumption_readonly: { invisible: '1' }
                // field_manual_consumption: {
                //   optional: 'hide',
                //   readonly: 'manual_consumption_readonly',
                //   force_save: '1'
                // }
              }
            }
          }
        }
      }
    },

    page_operations: {
      attr: {
        name: 'operations',
        string: '操作',
        invisible: [['type', 'not in', ['normal', 'phantom']]],
        groups: 'mrp.group_mrp_routings'
      },

      field_operation_ids: {
        invisible: [['type', 'not in', ['normal', 'phantom']]],
        groups: 'mrp.group_mrp_routings',
        context: {
          bom_id_invisible: true,
          default_bom_id: { field_id: {} }
          // tree_view_ref: 'mrp.mrp_routing_workcenter_bom_tree_view'
        }
      }
    },

    page_by_products: {
      attr: {
        name: 'by_products',
        string: 'By-products',
        invisible: [['type', '!=', 'normal']],

        groups: 'mrp.group_mrp_byproducts'
      },

      field_byproduct_ids: {
        context: {
          default_bom_id: { field_id: {} }
          // tree_view_ref: 'mrp.mrp_bom_byproduct_form_view'
        },
        views: {
          tree: {
            arch: {
              sheet: {
                field_company_id: { column_invisible: '1' },
                field_product_uom_category_id: { column_invisible: '1' },
                field_sequence: { widget: 'handle' },
                field_product_id: {},
                field_product_qty: {},
                field_product_uom_id: { groups: 'uom.group_uom' },
                field_cost_share: { optional: 'hide' },
                field_allowed_operation_ids: { column_invisible: '1' },
                field_operation_id: { groups: 'mrp.group_mrp_routings' },
                field_possible_bom_product_template_attribute_value_ids: {
                  column_invisible: '1'
                },
                field_bom_product_template_attribute_value_ids: {
                  optional: 'hide',
                  widget: 'many2many_tags',
                  column_invisible: 'parent.product_id',
                  groups: 'product.group_product_variant'
                }
              },
              kanban: {
                card_title: { field_product_id: {} },
                card_label: { product_qty: {} },
                card_value: {}
              }
            }
          },
          form: {
            arch: {
              sheet: {
                field_company_id: { column_invisible: '1' },
                field_product_uom_category_id: { column_invisible: '1' },
                field_sequence: { widget: 'handle' },
                field_product_id: {},
                field_product_qty: {},
                field_product_uom_id: { groups: 'uom.group_uom' },
                field_cost_share: { optional: 'hide' },
                field_allowed_operation_ids: { column_invisible: '1' },
                field_operation_id: { groups: 'mrp.group_mrp_routings' },
                field_possible_bom_product_template_attribute_value_ids: {
                  column_invisible: '1'
                },
                field_bom_product_template_attribute_value_ids: {
                  optional: 'hide',
                  widget: 'many2many_tags',
                  column_invisible: 'parent.product_id',
                  groups: 'product.group_product_variant'
                }
              }
            }
          }
        }
      }
    },

    page_miscellaneous: {
      attr: { name: 'miscellaneous', string: '杂项' },
      group: {
        group: {
          field_ready_to_produce: {
            invisible: [['type', '=', 'phantom']],
            string: 'Manufacturing Readiness',
            widget: 'radio',
            groups: 'mrp.group_mrp_routings'
          },
          field_consumption: {
            invisible: [['type', '=', 'phantom']],
            widget: 'radio'
          },
          field_allow_operation_dependencies: {
            groups: 'mrp.group_mrp_workorder_dependencies'
          }
        },
        group_2: {
          field_picking_type_id: {
            invisible: [['type', '=', 'phantom']],
            string: 'Routing',
            groups: 'stock.group_adv_location'
          },
          field_produce_delay: { string: 'Manuf. Lead Time' },
          field_days_to_prepare_mo: {},
          button_action_compute_bom_days: {
            string: 'Compute',
            type: 'object',
            help: 'Compute the days required to resupply all components from BoM, by either buying or manufacturing the components and/or subassemblies. Also note that purchase security lead times will be added when appropriate.'
          }
        }
      }
    }
  }
}

const view_form_mrp_bom = {
  _odoo_model: 'ir.ui.view',
  model: 'mrp.bom',
  type: 'form',

  title: { field_product_tmpl_id: {} },

  arch: {
    sheet: view_form_mrp_bom_sheet
  }
}

const view_tree_mrp_bom = {
  _odoo_model: 'ir.ui.view',
  model: 'mrp.bom',
  type: 'tree',
  arch: {
    sheet: {
      field_active: { column_invisible: '1' },
      field_sequence: { widget: 'handle' },
      field_product_tmpl_id: {},
      field_code: { optional: 'show' },
      field_type: {},
      field_product_id: {
        groups: 'product.group_product_variant',
        optional: 'hide'
      },
      field_company_id: {
        groups: 'base.group_multi_company',
        optional: 'show'
      },
      field_product_qty: { optional: 'hide' },
      field_product_uom_id: { groups: 'uom.group_uom', optional: 'hide' }
    },
    kanban: {
      card_title: { field_product_tmpl_id: {} },
      card_label: { field_code: {} },
      card_value: { field_type: {} }
    }
  }
}

const view_search_mrp_bom = {
  _odoo_model: 'ir.ui.view',
  model: 'mrp.bom',
  type: 'search',
  arch: {
    sheet: {
      field_code: {
        string: 'Bom',
        filter_domain: [
          '|',
          ['code', 'ilike', { self: {} }],
          ['product_tmpl_id', 'ilike', { self: {} }]
        ]
      },
      field_product_tmpl_id: { string: '产品' },
      field_bom_line_ids: { string: '组件' },
      filter_type: {
        normal: {
          name: 'normal',
          string: '制造',
          domain: [['type', '=', 'normal']]
        },

        phantom: {
          name: 'phantom',
          string: '套件',
          domain: [['type', '=', 'phantom']]
        }
      },
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      },

      groupby: {
        groupby_product: {
          string: '产品',
          domain: '[]',
          context: { group_by: 'product_tmpl_id' }
        },
        group_by_type: {
          string: 'BoM 类型',
          domain: '[]',
          context: { group_by: 'type' }
        },
        group_by_default_unit_of_measure: {
          string: '度量单位',
          domain: '[]',
          context: { group_by: 'product_uom_id' }
        }
      }
    }
  }
}

const action_mrp_bom = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Bom',
  type: 'ir.actions.act_window',
  res_model: 'mrp.bom',
  search_view_id: 'view_search_mrp_bom',
  domain: [],
  context: {
    default_company_id: { session_current_company_id: {} }
  },
  views: {
    tree: 'view_tree_mrp_bom',
    form: 'view_form_mrp_bom'
  }
}

export default {
  view_form_mrp_bom,
  view_tree_mrp_bom,
  view_search_mrp_bom,
  action_mrp_bom,

  model_mrp_bom: {
    _odoo_model: 'ir.model',
    fields: {}
  }
}
