const view_form_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_form_product_template',
  model: 'product.template',

  type: 'form',
  arch: {
    button_box: {
      button_action_view_po: {
        invisible: 1,
        name: 'action_view_po',
        type: 'object',
        icon: 'fa-credit-card',
        groups: 'purchase.group_purchase_user',

        // invisible: [['purchase_ok', '=', false]],
        field_purchased_product_qty: { widget: 'statinfo' },
        field_uom_name: { invisible: 1 }
      }
    },

    sheet: {
      field_service_type: { widget: 'radio', invisible: true },
      field_visible_expense_policy: { invisible: '1' },

      notebook: {
        page_general_information: {},
        page_purchase: {
          attr: {
            name: 'purchase',
            string: '采购',
            invisible: 0
            //  'invisible':[('purchase_ok','=',False)]
            // invisible: [['purchase_ok', '=', false]]
          },

          field_seller_ids: {
            invisible: 1,
            // invisible: [['product_variant_count', '>', 1]],
            readonly: [['product_variant_count', '>', 1]],
            // context: {
            //   todo_ctx:
            //     "{'default_product_tmpl_id':context.get('product_tmpl_id',active_id), 'product_template_invisible_variant': True, 'tree_view_ref':'purchase.product_supplierinfo_tree_view2'}"
            // }

            views: {
              tree: {
                arch: {
                  sheet: {},
                  kanban: {}
                }
              },
              form: {
                arch: {
                  sheet: {},
                  kanban: {}
                }
              }
            }
          },

          field_variant_seller_ids: {
            invisible: 1,
            // invisible: [['product_variant_count', '<=', 1]],
            readonly: [['product_variant_count', '<=', 1]],
            // context: {
            //   todo_ctx:
            //     "{'model': active_model, 'active_id': active_id, 'tree_view_ref':'purchase.product_supplierinfo_tree_view2'}"
            // }

            views: {
              tree: {
                arch: {
                  sheet: {},
                  kanban: {}
                }
              },
              form: {
                arch: {
                  sheet: {},
                  kanban: {}
                }
              }
            }
          },

          group_purchase: {
            attr: { name: 'purchase' },
            group_bill: {
              attr: {
                name: 'bill',
                string: 'Vendor Bills',
                groups: 'purchase.group_purchase_manager'
              },
              field_purchase_method: { widget: 'radio' }
            },

            group: {
              attr: { col: '24' },
              group: {
                attr: { string: 'Purchase Description' },
                field_description_purchase: {
                  placeholder: 'This note is added to purchase orders.'
                }
              },
              group_383: {
                attr: {
                  string: 'Warning when Purchasing this Product',
                  groups: 'purchase.group_warning_purchase'
                },
                field_purchase_line_warn: {},
                field_purchase_line_warn_msg: {
                  invisible: [['purchase_line_warn', '=', 'no-message']],
                  required: [['purchase_line_warn', '!=', 'no-message']],
                  readonly: [['purchase_line_warn', '=', 'no-message']],
                  placeholder: 'Type a message...'
                }
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_tree_product_template'
}

const view_search_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_search_product_template',
  arch: {
    sheet: {
      field_type: { invisible: '1' },
      field_location_id: {
        // filter_domain: [],
        context: {
          // todo_ctx: "{'location': self}"
        }
      },
      field_warehouse_id: {
        // filter_domain: [],
        context: {
          // todo_ctx: "{'warehouse': self}"
        }
      },

      filter_real_stock: {
        real_stock_available: {
          name: 'real_stock_available',
          string: 'Available Products',
          domain: [['qty_available', '>', 0]]
        },
        real_stock_negative: {
          name: 'real_stock_negative',
          string: 'Negative Forecasted Quantity',
          domain: [['virtual_available', '<', 0]]
        }
      }
    }
  }
}

const action_product_template = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品(采购设置)',
  type: 'ir.actions.act_window',
  res_model: 'product.template',
  search_view_id: 'view_search_product_template',
  domain: [],
  context: {
    search_default_filter_to_purchase: 1,
    purchase_product_template: 1
  },
  views: {
    tree: 'view_tree_product_template',
    form: 'view_form_product_template'
  }
}

export default {
  view_form_product_template,
  view_tree_product_template,
  view_search_product_template,
  action_product_template
}
