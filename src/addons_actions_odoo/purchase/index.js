import res_partner from './res_partner.js'
import product_template from './product_template.js'
import purchase_order from './purchase_order.js'

// import account_analytic_account from './account_analytic_account.js'
// import account_move from './account_move.js'
// import purchase_order_line from './purchase_order_line.js'

export default {
  ...res_partner,
  ...product_template,
  ...purchase_order
  // ...account_analytic_account,
  // ...account_move,
  // ...purchase_order_line,
}
