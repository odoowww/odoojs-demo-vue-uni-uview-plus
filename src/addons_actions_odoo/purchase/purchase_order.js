const puchase_order_line = {
  widget: 'section_and_note_one2many',
  context: { default_state: 'draft' },
  readonly: [['state', 'in', ['done', 'cancel']]],
  views: {
    tree: {
      arch: {
        sheet: {
          field_display_type: { invisible: 1 },
          field_currency_id: { invisible: 1 },
          field_state: { invisible: 1 },
          field_product_type: { invisible: 1 },
          //  field_product_uom: { invisible: 1 },
          field_product_uom_category_id: { invisible: 1 },
          field_invoice_lines: { invisible: 1 },
          field_sequence: { widget: 'handle' },
          field_product_id: {
            // attrs: {
            //   // 'readonly': [('state', 'in', ('purchase', 'to approve','done', 'cancel'))],
            //   readonly: [
            //     ['state', 'in', ['purchase', 'to approve', 'done', 'cancel']]
            //   ],
            //   // 'required': [('display_type', '=', False)],
            //   required: [['display_type', '=', false]]
            // },
            // // context="{'partner_id':parent.partner_id, 'quantity':product_qty, 'company_id': parent.company_id}"
            // context: {
            //   // partner_id: parent.partner_id,
            //   // quantity: product_qty,
            //   // company_id: parent.company_id
            // },
            // // domain="[('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id)]"/>
            // domain: [
            //   ['purchase_ok', '=', true],
            //   '|',
            //   ['company_id', '=', false],
            //   ['company_id', '=', { field_parent_company_id: {} }]
            // ],
            // force_save: '1'
          },

          field_name: { widget: 'section_and_note_text' },
          field_date_planned: {
            widget: 'date',
            optional: 'hide',
            required: [['display_type', '=', false]],
            force_save: '1'
          },
          field_analytic_distribution: {
            widget: 'analytic_distribution',
            optional: 'hide'
          },

          field_product_qty: {},
          field_qty_received_manual: { invisible: 1 },
          field_qty_received_method: { invisible: 1 },
          field_qty_received: {
            string: 'Received',
            optional: 'show',
            //'column_invisible': [('parent.state', 'not in', ('purchase', 'done'))],
            column_invisible: [
              ['parent.state', 'not in', ['purchase', 'done']]
            ],
            // 'readonly': [('qty_received_method', '!=', 'manual')]
            readonly: [['qty_received_method', '!=', 'manual']]
          },
          field_qty_invoiced: {
            string: 'Billed',
            optional: 'show',
            //'column_invisible': [('parent.state', 'not in', ('purchase', 'done'))]

            column_invisible: [['parent.state', 'not in', ['purchase', 'done']]]
          },
          field_product_uom: {
            force_save: '1',
            optional: 'show',
            // 'readonly': [('state', 'in', ('purchase', 'done', 'cancel'))],
            // 'required': [('display_type', '=', False)]

            readonly: [['state', 'in', ['purchase', 'done', 'cancel']]],
            // 'required': [('display_type', '=', False)],
            required: [['display_type', '=', false]]
          },

          field_product_packaging_qty: {
            groups: 'product.group_stock_packaging',
            optional: 'show',
            // invisible':
            // ['|', ('product_id', '=', False),
            // ('product_packaging_id', '=', False)]}"
            invisible: [
              '|',
              ['product_id', '=', false],
              ['product_packaging_id', '=', false]
            ]
          },
          field_product_packaging_id: {
            optional: 'show',
            //'invisible': [('product_id', '=', False)]}"
            invisible: [['product_id', '=', false]]
          },
          field_price_unit: {
            readonly: [['qty_invoiced', '!=', 0]]
          },
          // <button name="action_purchase_history" type="object" icon="fa-history" title="Purchase History" attrs="{'invisible': [('id', '=', False)]}"/>

          field_taxes_id: {
            widget: 'many2many_tags',
            optional: 'show',

            // domain="[('type_tax_use','=','purchase'), ('company_id', '=', parent.company_id), ('country_id', '=', parent.tax_country_id)]"
            domain: [
              ['type_tax_use', '=', 'purchase'],
              ['company_id', '=', { field_parent_company_id: {} }],
              ['country_id', '=', { field_parent_tax_country_id: {} }]
            ]

            // context="{'default_type_tax_use': 'purchase', 'search_view_ref': 'account.account_tax_view_search'}"
          },
          field_price_subtotal: { widget: 'monetary' },
          field_price_total: { invisible: 1 },
          field_price_tax: { invisible: 1 }
        },

        kanban: {
          card_title: { field_name: {} },
          card_label: { field_taxes_id: { widget: 'many2many_tags' } },
          card_value: {
            widget_sale_order_line_value: {
              field_display_type: {},
              field_product_qty: {},
              field_price_unit: {},
              field_price_total: {}
            }
          }
        }
      }
    },

    form: {
      arch: {
        sheet: {
          field_state: { invisible: 1 },
          field_display_type: { invisible: 1 },
          field_sequence: {},
          group: {
            attr: {
              //'invisible':[('display_type', '!=', False)]
              invisible: [['display_type', '!=', false]]
            },

            group: {
              field_product_uom_category_id: { invisible: 1 },

              field_product_id: {
                widget: 'many2one_barcode',
                readonly: [
                  ['state', 'in', ['purchase', 'to approve', 'done', 'cancel']]
                ],

                required: [['display_type', '=', false]],
                // domain="[('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id)]"
                domain: [
                  ['purchase_ok', '=', true],
                  '|',
                  ['company_id', '=', false],
                  ['company_id', '=', { field_parent_company_id: {} }]
                ],
                context: {
                  // partner_id: parent.partner_id,
                },
                force_save: '1'
              },

              field_product_qty: {},

              field_product_uom: {
                string: 'UoM',
                groups: 'uom.group_uom',
                force_save: '1',
                //  'readonly':
                // [('state', 'in', ('purchase', 'done', 'cancel'))],

                readonly: [['state', 'in', ['purchase', 'done', 'cancel']]],

                // 'required': [('display_type', '=', False)]
                required: [['display_type', '=', false]]
              },

              field_qty_received_method: { invisible: 1 },
              field_qty_received: {
                string: 'Received Quantity',
                //'invisible': [('parent.state', 'not in', ('purchase', 'done'))],
                invisible: [['parent.state', 'not in', ['purchase', 'done']]],
                // 'readonly': [('qty_received_method', '!=', 'manual')]
                readonly: [['qty_received_method', '!=', 'manual']]
              },
              field_qty_invoiced: {
                string: 'Billed Quantity',
                //'invisible': [('parent.state', 'not in', ('purchase', 'done'))],
                invisible: [['parent.state', 'not in', ['purchase', 'done']]]
              },
              field_product_packaging_qty: {
                groups: 'product.group_stock_packaging',
                // invisible':
                // ['|', ('product_id', '=', False),
                // ('product_packaging_id', '=', False)]}"
                invisible: [
                  '|',
                  ['product_id', '=', false],
                  ['product_packaging_id', '=', false]
                ]
              },
              field_product_packaging_id: {
                groups: 'product.group_stock_packaging',
                // invisible': [('product_id', '=', False)]
                invisible: [['product_id', '=', false]],
                // context="{
                // 'default_product_id': product_id,
                // 'tree_view_ref':'product.product_packaging_tree_view',
                // 'form_view_ref':'product.product_packaging_form_view'}"
                context: {
                  // default_product_id: product_id,
                  // tree_view_ref: 'product.product_packaging_tree_view',
                  // form_view_ref: 'product.product_packaging_form_view'
                }
              },

              field_price_unit: {
                // 'readonly': [('qty_invoiced', '!=', 0)]
                readonly: [['qty_invoiced', '!=', 0]]
              },

              field_taxes_id: {
                widget: 'many2many_tags',
                // domain="[('type_tax_use','=','purchase'), ('company_id', '=', parent.company_id), ('country_id', '=', parent.tax_country_id)]"
                domain: [
                  ['type_tax_use', '=', 'purchase'],
                  ['company_id', '=', { field_parent_company_id: {} }],
                  ['country_id', '=', { field_parent_tax_country_id: {} }]
                ],
                //  context="{'default_type_tax_use': 'purchase', 'search_view_ref': 'account.account_tax_view_search'}"
                context: {
                  // default_type_tax_use: 'purchase',
                  // search_view_ref: 'account.account_tax_view_search'
                }
              }
            },

            group_2: {
              field_date_planned: {
                widget: 'date',
                // 'required': [('display_type', '=', False)]
                required: [['display_type', '=', false]],
                force_save: '1'
              },
              field_analytic_distribution: {
                widget: 'analytic_distribution',
                groups: 'analytic.group_analytic_accounting',
                options: {
                  product_field: 'product_id',
                  business_domain: 'purchase_order'
                }
              }
            }
          },

          notebook: {
            page_notes: {
              attr: { string: 'Notes', name: 'notes' },
              field_name: {}
            },
            page_invoices_incoming_shiptments: {
              attr: {
                string: 'Invoices and Incoming Shipments',
                name: 'invoices_incoming_shiptments'
              },
              field_invoice_lines: {
                // todo
                //
                widget: 'many2many_tags'
              }
            }
          },

          group_name: {
            field_name__line: {
              string: '',
              // 'invisible': [('display_type', '=', False)]
              invisible: [['display_type', '!=', false]]
            },

            field_name__line_section: {
              string: 'Section Name (eg. Products, Services)',
              // 'invisible': [('display_type', '!=', 'line_section')]
              invisible: [['display_type', '!=', 'line_section']]
            },

            field_name__line_note: {
              for: 'name',
              string: 'Note',
              // 'invisible': [('display_type', '!=', 'line_note')]
              invisible: [['display_type', '!=', 'line_note']]
            }
          }
        }
      }
    }
  }
}

const view_form_purchase_order_sheet = {
  group_name: {
    group_name: {
      widget_text__RFQ: {
        attr: {
          text: 'Request for Quotation',
          // 'invisible': [('state','not in',('draft','sent'))]
          invisible: [['state', 'not in', ['draft', 'sent']]]
        }
      },

      widget_text__PO: {
        attr: {
          text: 'Purchase Order',
          // 'invisible': [('state','in',('draft','sent'))]
          invisible: [['state', 'in', ['draft', 'sent']]]
        }
      },

      // priority: { widget: 'priority' },
      field_name: { readonly: '1' }
    }
  },

  group: {
    group: {
      field_partner_id: {
        widget: 'res_partner_many2one',
        context: { res_partner_search_mode: 'supplier', show_vat: true },
        placeholder: 'Name, TIN, Email, or Reference'
      },
      field_partner_ref: {},
      field_currency_id: {
        groups: 'base.group_multi_currency',
        force_save: '1'
      },
      field_company_id: { invisible: '1' }
      // currency_id: {
      //   invisible: '1',
      //   groups: '!base.group_multi_currency'
      // }
    },

    group_2: {
      field_date_order: {
        // 'invisible': [('state','in',('purchase','done'))]
        invisible: [['state', 'in', ['purchase', 'done']]]
      },

      group_date_approve: {
        attr: {
          // 'invisible': [('state','not in',('purchase','done'))]
          invisible: [['state', 'not in', ['purchase', 'done']]]
        },
        field_date_approve: {},
        field_mail_reception_confirmed: { invisible: '1' },
        widget_text: {
          attr: {
            text: '(confirmed by vendor)',
            // 'invisible': [('mail_reception_confirmed','=', False)]
            invisible: [['mail_reception_confirmed', '=', false]]
          }
        }
      },

      group_date_planned: {
        field_date_planned: {
          // 'readonly': [('state', 'not in',
          // ('draft', 'sent', 'to approve', 'purchase'))]

          readonly: [
            ['state', 'not in', ['draft', 'sent', 'to approve', 'purchase']]
          ]
        },
        field_mail_reminder_confirmed: { invisible: '1' },
        widget_text: {
          attr: {
            text: '(confirmed by vendor)',
            // 'invisible':
            // [('mail_reminder_confirmed', '=', False)]
            invisible: [['mail_reception_confirmed', '=', false]]
          }
        }
      },

      group_receipt_reminder_email: {
        attr: { groups: 'purchase.group_send_reminder' },
        field_receipt_reminder_email: {},
        field_reminder_date_before_receipt: {}
      }
    }
  },

  notebook: {
    page_products: {
      attr: { string: 'Products', name: 'products' },
      field_tax_country_id: { invisible: '1' },
      field_order_line: puchase_order_line,

      group: {
        group: {
          field_notes: { placeholder: 'Define your terms and conditions ...' }
        },

        group_2: {
          field_tax_totals: {
            widget: 'account-tax-totals-field',
            nolabel: '1',
            readonly: '1'
          }
        }
      }
    },

    page_purchase_delivery_invoice: {
      attr: { string: '其他信息', name: 'purchase_delivery_invoice' },

      group: {
        group_other_info: {
          field_user_id: { widget: 'many2one_avatar_user' },
          field_company_id: { groups: 'base.group_multi_company' },
          field_origin: {}
        },

        group_invoice_info: {
          field_invoice_status: {
            // 'invisible': [('state', 'in',
            // ('draft', 'sent', 'to approve', 'cancel'))]
            invisible: [
              ['state', 'in', ['draft', 'sent', 'to approve', 'cancel']]
            ]
          },
          field_payment_term_id: {
            // 'readonly': ['|', ('invoice_status','=', 'invoiced'),
            // ('state', '=', 'done')]}"

            readonly: [
              '|',
              ['invoice_status', '=', 'invoiced'],
              ['state', '=', 'done']
            ]
          },
          field_fiscal_position_id: {
            // 'readonly': ['|', ('invoice_status','=', 'invoiced'),
            // ('state', '=', 'done')]}"

            readonly: [
              '|',
              ['invoice_status', '=', 'invoiced'],
              ['state', '=', 'done']
            ]
          }
        }
      }
    }
  }
}

const view_form_purchase_order = {
  _odoo_model: 'ir.ui.view',
  model: 'purchase.order',
  type: 'form',
  title: { field_name: {} },
  // toolbar: {
  //   action: {},
  //   print: {}
  // },

  arch: {
    button_box: {
      button_action_view_invoice: {
        name: 'action_view_invoice',
        type: 'object',
        icon: 'fa-pencil-square-o',
        //  'invisible':['|',
        // ('invoice_count', '=', 0),
        // ('state', 'in', ('draft','sent','to approve'))]

        invisible: [
          '|',
          ['invoice_count', '=', 0],
          ['state', 'in', ['draft', 'sent', 'to approve']]
        ],
        field_invoice_count: { widget: 'statinfo', string: 'Vendor Bills' },
        field_invoice_ids: { invisible: '1' }
      }
    },
    header: {
      button_button_confirm: {
        name: 'button_confirm',
        type: 'object',
        string: 'Confirm Order',
        btn_type: 'primary',
        invisible: [['state', '!=', 'sent']]
      },

      button_button_confirm2: {
        name: 'button_confirm',
        type: 'object',
        string: 'Confirm Order',
        invisible: [['state', '!=', 'draft']]
      },

      button_button_approve: {
        groups: 'purchase.group_purchase_manager',
        name: 'button_approve',
        type: 'object',
        string: 'Approve Order',
        btn_type: 'primary',
        invisible: [['state', '!=', 'to approve']]
      },

      button_button_draft: {
        name: 'button_draft',
        type: 'object',
        string: 'Set to Draft',
        invisible: [['state', '!=', 'cancel']]
      },

      button_button_cancel: {
        name: 'button_cancel',
        type: 'object',
        string: 'Cancel',
        invisible: [
          '!',
          ['state', 'in', ['draft', 'to approve', 'sent', 'purchase']]
        ]
      },
      button_button_done: {
        name: 'button_done',
        type: 'object',
        string: 'Lock',
        invisible: ['!', ['state', 'in', ['purchase']]]
      },
      button_button_unlock: {
        groups: 'purchase.group_purchase_manager',
        name: 'button_unlock',
        type: 'object',
        string: 'Unlock',
        invisible: ['!', ['state', 'in', ['done']]]
      },

      button_action_create_invoice: {
        name: 'action_create_invoice',
        type: 'object',
        string: 'Create Bill',
        btn_type: 'primary',
        context: { create_bill: true },
        invisible: [
          '|',
          '!',
          ['state', 'in', ['purchase', 'done']],
          ['invoice_status', 'in', ['no', 'invoiced']]
        ]
      },

      button_action_create_invoice2: {
        name: 'action_create_invoice',
        type: 'object',
        string: 'Create Bill',
        context: { create_bill: true },
        invisible: [
          '|',
          '|',
          '!',
          ['state', 'in', ['purchase', 'done']],
          '!',
          ['invoice_status', 'in', ['no', 'invoiced']],
          ['order_line', '=', []]
        ]
      },

      button_action_rfq_send: {
        invisible: 1,
        name: 'action_rfq_send',
        type: 'object',
        string: 'Send by Email',
        btn_type: 'primary',
        context: { send_rfq: true }
        // invisible: [['state', '!=', 'draft']]
      },

      button_action_rfq_send2: {
        invisible: 1,
        name: 'action_rfq_send',
        type: 'object',
        string: 'Re-Send by Email',
        context: { send_rfq: true }
        // invisible: [['state', '!=', 'sent']]
      },

      button_print_quotation: {
        invisible: 1,
        name: 'print_quotation',
        type: 'object',
        string: 'Print RFQ',
        groups: 'base.group_user',
        btn_type: 'primary',
        context: { send_rfq: true }
        // invisible: [['state', '!=', 'draft']]
      },

      button_action_rfq_send22: {
        invisible: 1,
        name: 'action_rfq_send',
        type: 'object',
        string: 'Send PO by Email',
        context: { send_rfq: false }
        // invisible: [['state', '!=', 'purchase']]
      },

      button_print_quotation2: {
        invisible: 1,
        groups: 'base.group_user',
        name: 'print_quotation',
        type: 'object',
        string: 'Print RFQ',
        context: { send_rfq: true }
        // invisible: [['state', '!=', 'sent']]
      },

      button_confirm_reminder_mail: {
        invisible: 1,
        groups: 'base.group_no_one',
        name: 'confirm_reminder_mail',
        type: 'object',
        string: 'Confirm Receipt Date'
        // invisible: [
        //   '|',
        //   '|',
        //   '!',
        //   ['state', 'in', ['purchase', 'done']],
        //   ['mail_reminder_confirmed', '=', true],
        //   ['date_planned', '=', false]
        // ]
      },

      field_state: {
        widget: 'statusbar',
        statusbar_visible: 'draft,sent,purchase'
      }
    },

    sheet: { ...view_form_purchase_order_sheet }
  }
}

const view_tree_purchase_order = {
  _odoo_model: 'ir.ui.view',
  model: 'purchase.order',
  type: 'tree',
  arch: {
    sheet: {
      //       field_priority: { optional: 'show', widget: 'priority' },
      field_partner_ref: { optional: 'hide' },
      field_name: { string: 'Reference' },
      field_date_approve: {
        widget: 'date',
        optional: 'show'
        // invisible="context.get('quotation_only', False)"
      },
      field_date_planned: {
        optional: 'show'
        // invisible="context.get('quotation_only', False)"
      },

      field_date_order: {
        optional: 'show'
        // invisible="not context.get('quotation_only', False)"
      },

      field_partner_id: {},
      field_company_id: { optional: 'show' },

      field_user_id: { widget: 'many2one_avatar_user', optional: 'show' },

      field_origin: { optional: 'show' },
      field_amount_untaxed: {
        sum: 'Total Untaxed amount',
        string: 'Untaxed',
        widget: 'monetary',
        optional: 'hide'
      },
      field_amount_total: {
        sum: 'Total amount',
        widget: 'monetary',
        optional: 'show'
      },
      field_currency_id: { invisible: '1' },
      field_state: { invisible: '1' },
      field_invoice_status: {
        widget: 'badge',
        optional: 'show'
        // decoration-success="invoice_status == 'invoiced'"
        // decoration-info="invoice_status == 'to invoice'" optional="show"
      }
    },
    kanban: {
      card_title: {
        widget_one_row: {
          field_name: {},
          widget_text: ' ',
          field_partner_id: {}
        }
      },
      card_label: {
        field_date_order: {
          // widget: 'date'
        }
      },
      card_value: { field_state: {}, field_amount_total: {} }
    }
  }
}

const view_search_purchase_order = {
  _odoo_model: 'ir.ui.view',
  model: 'purchase.order',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '订单',
        filter_domain: [
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['partner_ref', 'ilike', { self: {} }],
          ['partner_id', 'child_of', { self: {} }]
        ]
      },
      field_partner_id: { operator: 'child_of' },
      field_user_id: {},
      field_product_id: {},
      field_origin: {},

      filter_me: {
        my_purchases: {
          name: 'my_purchases',
          string: '我的订单',
          domain: [['user_id', '=', { session_uid: {} }]]
        }
        // starred: {
        //   name: 'starred',
        //   string: 'Starred',
        //   domain: [['priority', '=', '1']]
        // }
      },

      filter_state: {
        draft: {
          name: 'draft',
          string: '询价单',
          domain: [['state', 'in', ['draft', 'sent']]]
        },
        to_approve: {
          name: 'to_approve',
          string: '申批中',
          domain: [['state', 'in', ['to approve']]]
        },
        approved: {
          name: 'approved',
          string: '销售订单',
          domain: [['state', 'in', ['purchase', 'done']]]
        }
      },

      filter_invoice: {
        not_invoiced: {
          name: 'not_invoiced',
          string: 'Waiting Bills',
          domain: [['invoice_status', '=', 'to invoice']]
        },
        invoiced: {
          name: 'invoiced',
          string: 'Bills Received',
          domain: [['invoice_status', '=', 'invoiced']]
        }
      },

      filter_date: {
        order_date: {
          name: 'order_date',
          string: 'Order Date',
          date: 'date_order'
        }
      }

      // filter_type: {
      //   // draft_rfqs: {
      //   //   name: 'draft_rfqs',
      //   //   string: 'Draft RFQs',
      //   //   domain: [['state', '=', 'draft']]
      //   // },
      //   // waiting_rfqs: {
      //   //   name: 'waiting_rfqs',
      //   //   string: 'Waiting RFQs',
      //   //   domain: ({ env }) => {
      //   //     const today = env.date_tools.today
      //   //     return [
      //   //       ['state', '=', 'sent'],
      //   //       ['date_order', '>=', today]
      //   //     ]
      //   //   }
      //   // },
      //   // late_rfqs: {
      //   //   name: 'late_rfqs',
      //   //   string: 'Late RFQs',
      //   //   domain: ({ env }) => {
      //   //     const today = env.date_tools.today
      //   //     return [
      //   //       ['state', 'in', ['draft', 'sent', 'to approve']],
      //   //       ['date_order', '<', today]
      //   //     ]
      //   //   }
      //   // }
      // }
    }
  }
}

const action_purchase_order = {
  _odoo_model: 'ir.actions.act_window',
  name: '采购订单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'purchase.order',
  search_view_id: 'view_search_purchase_order',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_purchase_order',
    form: 'view_form_purchase_order'
  }
}

const model_purchase_order = {
  _odoo_model: 'ir.model',
  fields: {
    state: {
      selection: [
        ['draft', 'RFQ'],
        ['sent', 'RFQ Sent'],
        ['to approve', 'To Approve'],
        ['purchase', 'Purchase Order'],
        ['done', 'Locked'],
        ['cancel', 'Cancelled']
      ]
    }
  }
}

export default {
  view_form_purchase_order,
  view_tree_purchase_order,
  view_search_purchase_order,
  action_purchase_order
}
