const view_form_res_partner = {
  inherit_id: 'account.view_form_res_partner',
  _odoo_model: 'ir.ui.view',
  model: 'res.partner',
  type: 'form',
  title: { field_name: {} },
  arch: {
    button_box: {
      button_purchase__act_res_partner_2_supplier_invoices: {
        name: 'purchase.act_res_partner_2_supplier_invoices',
        type: 'action',
        // groups: 'account.group_account_invoice',

        field_supplier_invoice_count: {
          string: 'Vendor Bills',
          widget: 'statinfo'
        }
      },
      button_purchase__act_res_partner_2_purchase_order: {
        name: 'purchase.act_res_partner_2_purchase_order',
        type: 'action',
        groups: 'purchase.group_purchase_user',

        field_purchase_order_count: {
          string: 'Purchases',
          widget: 'statinfo'
        }
      }
    },

    sheet: {
      notebook: {
        page_contact_addresses: {},
        page_sales_purchases: {
          attr: { name: 'sales_purchases', string: '采购' },
          group_container_row_2: {
            attr: { name: 'container_row_2' },
            group_sale: { attr: { invisible: 1 } },
            group_purchase: {
              attr: { name: 'purchase', string: 'Purchase' },
              group_receipt_reminder: {
                attr: {
                  name: 'receipt_reminder',
                  groups: 'purchase.group_send_reminder'
                },

                field_receipt_reminder_email: {},

                group_reminder_date: {
                  field_reminder_date_before_receipt: {},
                  widget_text: {
                    attr: {
                      invisible: [['receipt_reminder_email', '=', false]],
                      text: 'day(s) before'
                    }
                  }
                }
              },
              field_property_purchase_currency_id: {
                groups: 'base.group_multi_currency'
              }
            }
          }
        },
        page_internal_notes: {
          group_slot: {},
          group_invoice: {},
          group_purchase: {
            attr: { groups: 'purchase.group_purchase_user' },
            group_purchase2: {
              attr: { groups: 'purchase.group_warning_purchase' },
              widget_separator: 'Warning on the Purchase Order',
              field_purchase_warn: { required: '1' },
              field_purchase_warn_msg: {
                invisible: [['purchase_warn', 'in', (false, 'no-message')]],
                required: [
                  ['purchase_warn', '!=', false],
                  ['purchase_warn', '!=', 'no-message']
                ],
                placeholder: 'Type a message...'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_tree_res_partner'
}

const view_search_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_search_res_partner'
}

const action_res_partner = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人(供应商设置)',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'res.partner',
  search_view_id: 'view_search_res_partner',
  domain: [],
  context: {
    search_default_supplier: 1,
    res_partner_search_mode: 'supplier',
    default_is_company: true,
    default_supplier_rank: 1
  },
  views: {
    tree: 'view_tree_res_partner',
    form: 'view_form_res_partner'
  }
}

export default {
  view_form_res_partner,
  view_tree_res_partner,
  view_search_res_partner,
  action_res_partner
}
