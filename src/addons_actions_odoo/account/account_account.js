const view_form_account_account = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account',
  type: 'form',
  arch: {
    button_box: {
      button_action_open_related_taxes: {
        name: 'action_open_related_taxes',
        type: 'object',
        icon: 'fa-bars',
        // 'invisible': [('related_taxes_amount', '=', 0)]
        invisible: [['related_taxes_amount', '=', 0]],

        field_related_taxes_amount: { string: 'Taxes', widget: 'statinfo' }
      },

      button_account_action_move_line_select: {
        name: 'account.action_move_line_select',
        type: 'action',
        field_current_balance: { string: 'Balance', widget: 'statinfo' }
      }
    },

    sheet: {
      field_company_id: { invisible: 1 },
      field_code: { string: '代码' },
      field_name: { string: '名称' },

      notebook: {
        page: {
          attr: { name: 'accounting', string: '开票' },
          group: {
            group_type: {
              field_account_type: { widget: 'account_type_selection' },

              field_tax_ids: {
                widget: 'many2many_tags',
                // 'invisible': [('internal_group', '=', 'off_balance')]
                invisible: [['internal_group', '=', 'off_balance']],
                domain: [['company_id', '=', { field_company_id: {} }]]
              },
              field_tag_ids: {
                widget: 'many2many_tags',
                domain: [['applicability', '=', 'accounts']]
              },
              field_allowed_journal_ids: {
                widget: 'many2many_tags',
                domain: [['company_id', '=', { field_company_id: {} }]]
              }
            },
            group_type2: {
              field_internal_group: { invisible: 1, readonly: '1' },
              field_currency_id: { groups: 'base.group_multi_currency' },
              field_deprecated: {},
              field_group_id: {},
              field_company_id: { groups: 'base.group_multi_company' }
            }
          }
        }
      }
    }
  }
}

const view_tree_account_account = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account',
  type: 'tree',
  arch: {
    sheet: {
      field_company_id__1: { invisible: 1 },
      field_code: {},
      field_name: {},
      field_account_type: { widget: 'account_type_selection' },
      field_group_id: { optional: 'hide' },
      field_internal_group: { invisible: 1 },
      field_reconcile: {
        widget: 'boolean_toggle',
        // 'invisible':
        // ['|', ('account_type', 'in',
        // ('asset_cash', 'liability_credit_card')),
        // ('internal_group', '=', 'off_balance')]

        invisible: [
          '|',
          ['account_type', 'in', ['asset_cash', 'liability_credit_card']],
          ['internal_group', '=', 'off_balance']
        ]
      },
      field_non_trade: {
        widget: 'boolean_toggle',
        optional: 'hide',
        // 'invisible':
        // [('account_type', 'not in',
        // ('liability_payable', 'asset_receivable'))]

        invisible: [
          ['account_type', 'not in', ['liability_payable', 'asset_receivable']]
        ]
      },
      field_tax_ids: { optional: 'hide', widget: 'many2many_tags' },
      field_tag_ids: { optional: 'hide', widget: 'many2many_tags' },
      field_allowed_journal_ids: {
        optional: 'hide',
        widget: 'many2many_tags'
      },
      field_currency_id: { groups: 'base.group_multi_currency' },
      field_company_id: { groups: 'base.group_multi_company' },
      field_current_balance: { string: 'Balance' }
    },

    kanban: {
      card_title: { field_name: {} },
      card_label: { field_code: {} },
      card_value: {
        field_account_type: {},
        field_current_balance: {}
      }
    }
  }
}

const view_search_account_account = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '名称或代码',
        // ['|', ('name','ilike',self), ('code','ilike',self)]
        filter_domain: [
          '|',
          ['name', 'ilike', self],
          ['code', '=like', { self: {} }]
        ]
      },

      filter_type: {
        receivableacc: {
          name: 'receivableacc',
          string: '应收',
          domain: [['account_type', '=', 'asset_receivable']]
        },

        payableacc: {
          name: 'payableacc',
          string: '应付',
          domain: [['account_type', '=', 'liability_payable']]
        },

        equityacc: {
          name: 'equityacc',
          string: '权益',
          domain: [['internal_group', '=', 'equity']]
        },

        assetsacc: {
          name: 'assetsacc',
          string: '资产',
          domain: [['internal_group', '=', 'asset']]
        },
        liabilityacc: {
          name: 'liabilityacc',
          string: '负债',
          domain: [['internal_group', '=', 'liability']]
        },
        incomeacc: {
          name: 'incomeacc',
          string: '收入',
          domain: [['internal_group', '=', 'income']]
        },
        expensesacc: {
          name: 'expensesacc',
          string: '费用',
          domain: [['internal_group', '=', 'expense']]
        }
      },

      filter_active: {
        used: {
          name: 'used',
          string: '已使用的',
          domain: [['used', '=', true]]
        },
        activeacc: {
          name: 'activeacc',
          string: '可用的',
          domain: [['deprecated', '=', false]]
        }
      }
    }
  }
}

const action_account_account = {
  _odoo_model: 'ir.actions.act_window',
  name: '科目表',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'account.account',
  search_view_id: 'view_search_account_account',
  domain: [],
  context: { search_default_activeacc: true },
  views: {
    tree: 'view_tree_account_account',
    form: 'view_form_account_account'
  }
}

export default {
  view_form_account_account,
  view_tree_account_account,
  view_search_account_account,
  action_account_account
}
