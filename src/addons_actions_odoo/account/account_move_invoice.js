const invoice_line_ids_form_sheet = {
  field_display_type: { invisible: '1' },
  field_company_id: { invisible: '1' },
  field_partner_id: { invisible: '1' },

  group_product: {
    field_product_id__out: {
      widget: 'many2one_barcode',
      domain: [
        ['sale_ok', '=', true],
        '|',
        ['company_id', '=', false],
        ['company_id', '=', { field_parent_company_id: {} }]
      ]
    },

    field_product_id__in: {
      widget: 'many2one_barcode',
      domain: [
        ['purchase_ok', '=', true],
        '|',
        ['company_id', '=', false],
        ['company_id', '=', { field_parent_company_id: {} }]
      ]
    },
    field_quantity: {},
    field_product_uom_category_id: { invisible: '1' },
    field_product_uom_id: {},
    field_price_unit: {},
    field_discount: { string: 'Disc.%' }
  },
  group_account: {
    field_account_id: {
      // groups: 'account.group_account_readonly',
      // readonly: '1',
      domain: [
        ['deprecated', '=', false],
        ['account_type', 'not in', ['asset_receivable', 'liability_payable']],
        ['company_id', '=', { field_parent_company_id: {} }],
        ['is_off_balance', '=', false]
      ],
      context: {
        partner_id: { field_partner_id: {} },
        move_type: { field_parent_move_type: {} }
      }
    },

    field_tax_ids: {
      widget: 'many2many_tags',
      domain: [
        ['type_tax_use', '=?', { field_parent_invoice_filter_type_domain: {} }],
        ['company_id', '=', { field_parent_company_id: {} }],
        ['country_id', '=', { field_parent_tax_country_id: {} }]
      ],
      // context="{'append_type_to_tax_name': not parent.invoice_filter_type_domain}"
      context: {
        // append_type_to_tax_name: not parent.invoice_filter_type_domain
      }
    },
    field_analytic_line_ids: {
      invisible: '1',
      fields: {
        account_id: {},
        auto_account_id: {}
      }
    },
    // field_analytic_account_id: {},
    field_analytic_distribution: {
      invisible: '1',
      widget: 'analytic_distribution',
      groups: 'analytic.group_analytic_accounting'
      // readonly: '1'
    }
  },
  group_name: {
    field_name: {
      widget: 'text',

      label_Description: {
        for: 'name',
        string: '描述',
        // 'invisible': [('display_type', 'in',
        // ('line_note', 'line_section'))]
        invisible: [['display_type', 'in', ['line_note', 'line_section']]]
      },

      label_Section: {
        for: 'name',
        string: 'Section',
        // 'invisible': [('display_type', '!=', 'line_section')]
        invisible: [['display_type', '!=', 'line_section']]
      },

      label_Note: {
        for: 'name',
        string: 'Note',
        // 'invisible': [('display_type', '!=', 'line_note')]
        invisible: [['display_type', '!=', 'line_note']]
      }
    },

    group_amount: {
      field_price_subtotal: {},
      field_price_total: {}
    }
  }
}

const view_move_form_sheet = {
  field_company_id: { invisible: 1 },
  field_journal_id: { invisible: 1 },
  field_show_name_warning: { invisible: 1 },
  field_posted_before: { invisible: 1 },
  field_move_type: { invisible: 1 },
  field_payment_state: { invisible: 1 },
  field_invoice_filter_type_domain: { invisible: 1 },
  field_suitable_journal_ids: { invisible: 1 },
  field_currency_id: { invisible: 1 },
  field_company_currency_id: { invisible: 1 },
  field_commercial_partner_id: { invisible: 1 },
  field_bank_partner_id: { invisible: 1 },
  field_display_qr_code: { invisible: 1 },
  field_show_reset_to_draft_button: { invisible: 1 },

  field_invoice_has_outstanding: { invisible: 1 },
  field_is_move_sent: { invisible: 1 },
  field_has_reconciled_entries: { invisible: 1 },
  field_restrict_mode_hash_table: { invisible: 1 },
  field_country_code: { invisible: 1 },
  field_display_inactive_currency_warning: { invisible: 1 },
  field_statement_line_id: { invisible: 1 },
  field_payment_id: { invisible: 1 },
  field_tax_country_id: { invisible: 1 },
  field_tax_cash_basis_created_move_ids: { invisible: 1 },
  field_quick_edit_mode: { invisible: 1 },
  field_hide_post_button: { invisible: 1 },
  field_duplicated_ref_ids: { invisible: 1 },
  field_quick_encoding_vals: { invisible: 1 },

  group_title: {
    group: {
      field_move_type: { invisible: 1 },
      field_name: {}
    }
  },

  group: {
    group_header_left_group: {
      field_partner_id: {
        label_customer: {
          for: 'partner_id',
          string: '客户',
          invisible: [
            [
              'move_type',
              'not in',
              ['out_invoice', 'out_refund', 'out_receipt']
            ]
          ]
        },
        label_vendor: {
          for: 'partner_id',
          string: '供应商',
          invisible: [
            ['move_type', 'not in', ['in_invoice', 'in_refund', 'in_receipt']]
          ]
        }
      },

      field_partner_shipping_id: {
        // invisible: 1,
        groups: 'account.group_delivery_invoice_address',
        // 'invisible': [
        // ('move_type', 'not in', ('out_invoice', 'out_refund', 'out_receipt'))],
        invisible: [
          'move_type',
          'not in',
          ['out_invoice', 'out_refund', 'out_receipt']
        ],
        // 'readonly': [('state', '!=', 'draft')]
        readonly: [['state', '!=', 'draft']]
      },

      field_quick_edit_total_amount: {
        // invisible: 1,
        // 'invisible': [
        // '|', ('move_type', '=', 'entry'),
        // ('quick_edit_mode', '=', False)],
        invisible: [
          '|',
          ['move_type', '=', 'entry'],
          ['quick_edit_mode', '=', false]
        ],
        // 'readonly': [('state', '!=', 'draft')]
        readonly: [['state', '!=', 'draft']]
      },

      field_ref__in: {
        invisible: [
          ['move_type', 'not in', ['in_invoice', 'in_receipt', 'in_refund']]
        ],
        string: '账单号码'
      },

      field_ref: {
        invisible: [
          [
            'move_type',
            'in',
            [
              'in_invoice',
              'in_receipt',
              'in_refund',
              'out_invoice',
              'out_refund'
            ]
          ]
        ],
        string: '业务单'
      },

      field_tax_cash_basis_origin_move_id: {
        // invisible: 1,
        // 'invisible':
        //  [('tax_cash_basis_origin_move_id', '=', False)]
        invisible: [['tax_cash_basis_origin_move_id', '=', false]]
      },

      field_invoice_vendor_bill_id: {
        // invisible: 1,
        string: 'Auto-Complete',
        context: { show_total_amount: true },
        placeholder: 'Select an old vendor bill',
        // invisible: { editable: false },
        // class="oe_edit_only"
        // 'invisible':
        // ['|', ('state', '!=', 'draft'), ('move_type', '!=', 'in_invoice')]
        invisible: [
          '|',
          { editable: false },
          '|',
          ['state', '!=', 'draft'],
          ['move_type', '!=', 'in_invoice']
        ],

        domain: [
          ['company_id', '=', { field_company_id: {} }],
          ['partner_id', 'child_of', [{ field_partner_id: {} }]],
          ['move_type', '=', 'in_invoice']
        ]
      }
    },

    group_header_right_group: {
      attr: { id: 'header_right_group' },
      field_invoice_date: {
        // 在 执行 action_post 时, odoo 服务端检查 该字段是否有值, 且直接报错
        // required : [['in_moves', 'in', ['in_invoice', 'in_refund', 'in_receipt'] ]]
        required: [
          ['in_moves', 'in', ['in_invoice', 'in_refund', 'in_receipt']]
        ],

        label_invoice: {
          for: 'invoice_date',
          string: '开票日期',
          invisible: [
            [
              'move_type',
              'not in',
              ['out_invoice', 'out_refund', 'out_receipt']
            ]
          ]
        },

        label_bill: {
          for: 'invoice_date',
          string: '账单日期',
          invisible: [
            ['move_type', 'not in', ['in_invoice', 'in_refund', 'in_receipt']]
          ]
        }
      },

      field_date: {
        string: '账期',
        invisible: [
          ['move_type', 'in', ['out_invoice', 'out_refund', 'out_receipt']],
          ['quick_edit_mode', '=', false]
        ],
        readonly: [['state', '!=', 'draft']]
      },

      field_payment_reference: { string: '收付款单号' },

      field_partner_bank_id: {
        string: '收款对方账号',
        invisible: [
          ['move_type', 'not in', ['in_invoice', 'in_refund', 'in_receipt']]
        ]
      },

      field_invoice_date_due: { string: '收付款期限' },
      field_invoice_payment_term_id: { string: '支付条款' },
      group_journal_div: {
        attr: {
          // groups: 'account.group_account_readonly',
          // invisible="
          // context.get('default_journal_id') and
          // context.get('move_type', 'entry') != 'entry'"
          invisible: [
            '&',
            { context_default_journal_id: false },
            [{ context_move_type: 'entry' }, '!=', 'entry']
          ]
        },
        field_journal_id: {
          // readonly: { editable: false },
          // 'readonly': [('posted_before', '=', True)]
          readonly: [['posted_before', '=', true]]
        },
        // widget_text__span_journal_id: {
        //   attr: {
        //     groups: 'base.group_multi_currency',
        //     text: 'in',
        //       // 'invisible': [('move_type', '=', 'entry')]
        //       invisible: [['move_type', '=', 'entry']]
        //   }
        // },

        field_currency_id: {
          // invisible: 1,
          groups: 'base.group_multi_currency',
          // 'invisible': [('move_type', '=', 'entry')]
          invisible: [['move_type', '=', 'entry']],
          // 'readonly': [('state', '!=', 'draft')]
          readonly: [['state', '!=', 'draft']]
        }
      },

      field_currency_id: {
        invisible: 1,
        // groups: '!account.group_account_readonly,base.group_multi_currency',
        // 'readonly': [('state', '!=', 'draft')]
        readonly: [['state', '!=', 'draft']]
      }
    }
  },

  notebook: {
    page_invoice_tab: {
      attr: { id: 'invoice_tab', name: 'invoice_tab', string: '明细' },
      field_invoice_line_ids: {
        widget: 'section_and_note_one2many',
        context: {
          default_move_type: { context_default_move_type: null },
          journal_id: { field_journal_id: {} },
          default_partner_id: { field_commercial_partner_id: {} },
          // default_currency_id: currency_id or company_currency_id,
          default_display_type: 'product',
          quick_encoding_vals: { field_quick_encoding_vals: {} }
        },

        views: {
          tree: {
            arch: {
              sheet: {
                field_sequence: { widget: 'handle', invisible: 1 },
                field_product_id: { optional: 'show' },
                field_name: {
                  optional: 'show',
                  widget: 'section_and_note_text'
                },

                field_account_id: {
                  //  groups: 'account.group_account_readonly'
                },
                // field_analytic_account_id: {},
                field_analytic_distribution: {
                  widget: 'analytic_distribution',
                  optional: 'show'
                },
                field_quantity: { optional: 'show' },
                field_product_uom_category_id: { invisible: '1' },
                field_product_uom_id: {
                  string: 'UoM',
                  groups: 'uom.group_uom',
                  optional: 'show'
                },
                field_price_unit: { string: '单价' },
                field_discount: { string: 'Disc.%', optional: 'hide' },
                field_tax_ids: { widget: 'many2many_tags', optional: 'show' },
                field_price_subtotal: {
                  string: '不含税金额',
                  groups: 'account.group_show_line_subtotals_tax_excluded'
                },
                field_price_total: {
                  string: '含税金额',
                  groups: 'account.group_show_line_subtotals_tax_included'
                },

                field_partner_id: { invisible: '1' },
                field_currency_id: { invisible: '1' },
                field_company_id: { invisible: '1' },
                field_company_currency_id: { invisible: '1' },
                field_display_type: { invisible: '1' },
                field_product_uom_id__22: { invisible: '1' }
              },
              kanban: {
                card_title: { field_product_id: {} },
                card_label: { field_account_id: {} },
                card_value: {
                  widget_account_move_invoice_line_value: {
                    field_quantity: {},
                    field_price_unit: {},
                    field_price_total: {}
                  }
                }
              }
            }
          },
          form: { arch: { sheet: { ...invoice_line_ids_form_sheet } } }
        }
      },
      group_oe_invoice_lines_tab: {
        group_narration_8: {
          field_narration: { nolabel: 1, placeholder: 'Terms and Conditions' }
        },
        group__footer: {
          group_div_oe_subtotal_footer: {
            attr: {
              invisible: [['payment_state', '=', 'invoicing_legacy']]
            },

            field_tax_totals: {
              nolabel: 1,
              widget: 'account-tax-totals-field',
              // 'readonly': ['|', ('state', '!=', 'draft'),
              // '&amp;', ('move_type', 'not in', ('in_invoice', 'in_refund')),
              // ('quick_edit_mode', '=', False)]

              readonly: [
                '|',
                ['state', '!=', 'draft'],
                '&',
                ['move_type', 'not in', ['in_invoice', 'in_refund']],
                ['quick_edit_mode', '=', false]
              ]
            },
            field_invoice_payments_widget: { nolabel: 1, widget: 'payment' },
            field_amount_residual: {
              string: 'Residual',
              readonly: '1',
              // 'invisible':  [('state', '=', 'draft')]
              invisible: [['state', '=', 'draft']]
            }
          },
          field_invoice_outstanding_credits_debits_widget: {
            nolabel: 1,
            widget: 'payment',
            // 'invisible': ['|',
            // ('state', '!=', 'posted'),
            // ('move_type', 'in', ('out_receipt', 'in_receipt'))]

            invisible: [
              '|',
              ['state', '!=', 'posted'],
              ['move_type', 'in', ['out_receipt', 'in_receipt']]
            ]
          }
        }
      }
    },
    page_other_tab: {
      attr: {
        id: 'other_tab',
        string: '其他信息',
        name: 'other_info',
        invisible: [
          [
            'move_type',
            'not in',
            ['out_invoice', 'out_refund', 'in_invoice', 'in_refund']
          ]
        ]
      },
      group_other_tab_group: {
        attr: { id: 'other_tab_group' },
        group_sale_info_group: {
          attr: {
            name: 'sale_info_group',
            string: '开票',
            invisible: [['move_type', 'not in', ['out_invoice', 'out_refund']]]
          },
          field_ref: { string: '客户参考' },
          field_user_id: { invisible: '1' },
          field_invoice_user_id: {
            widget: 'many2one_avatar_user',
            string: '销售员'
          },
          field_invoice_origin: { invisible: '1', string: '原单' },
          field_partner_bank_id: {},
          field_qr_code_method: {
            // invisible: 1,
            // context="{'default_partner_id': bank_partner_id}"
            // domain:[('partner_id', '=', bank_partner_id)]",
            // 'invisible': [('display_qr_code', '=', False)]
            invisible: [['display_qr_code', '=', false]],
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          }
        },

        group_accounting_info_group: {
          attr: {
            name: 'accounting_info_group',
            string: 'Accounting',
            // 'invisible':
            // [('move_type', 'not in',
            // ('out_invoice', 'out_refund', 'in_invoice', 'in_refund'))]

            invisible: [
              [
                'move_type',
                'not in',
                ['out_invoice', 'out_refund', 'in_invoice', 'in_refund']
              ]
            ]
          },
          field_company_id: {},
          field_invoice_incoterm_id: {},
          field_fiscal_position_id: {},
          field_invoice_cash_rounding_id: {
            groups: 'account.group_cash_rounding'
          },
          field_invoice_source_email: {
            widget: 'email',
            // 'invisible': ['|',
            // ('move_type', 'not in', ('in_invoice', 'in_refund')),
            // ('invoice_source_email', '=', False)]

            invisible: [
              '|',
              ['move_type', 'not in', ['in_invoice', 'in_refund']],
              ['invoice_source_email', '=', false]
            ]
          },
          field_auto_post: {
            // 'readonly': [('state','!=','draft')]
            readonly: [['state', '!=', 'draft']]
          },
          field_auto_post_until: {
            // 'invisible': [('auto_post', 'in', ('no', 'at_date'))],
            invisible: [['auto_post', 'in', ['no', 'at_date']]],
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          },
          field_to_check: {}
        }
      }
    }
  }
}

const view_form_account_move_invoice = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'form',
  arch: {
    alert_div: {
      // _div_open_duplicated_ref_bill_view: {
      //   _attr: {
      //     class: 'alert alert-warning mb-0',
      //     text: 'Warning: this bill might be a duplicate of',
      //     attrs: {
      //       invisible({ record }) {
      //         // invisible: [
      //         //   '|',
      //         //   ['state', '!=', 'draft'],
      //         //   ['duplicated_ref_ids', '=', []]
      //         // ],
      //         const { state, duplicated_ref_ids = [] } = record
      //         return state !== 'draft' || !duplicated_ref_ids.length
      //       }
      //     }
      //   },
      //   _button_open_duplicated_ref_bill_view: {
      //     _attr: {
      //       name: 'open_duplicated_ref_bill_view',
      //       type: 'object',
      //       string: 'one of those bills',
      //       class: 'btn btn-link p-0'
      //     }
      //   }
      // },
      // _div_tax_lock_date_message: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     class: 'alert alert-warning mb-0',
      //     attrs: {
      //       invisible({ record }) {
      //         // invisible: [
      //         //   '|',
      //         //   ['state', '!=', 'draft'],
      //         //   ['tax_lock_date_message', '=', false]
      //         // ],
      //         const { state, tax_lock_date_message } = record
      //         return state !== 'draft' || !tax_lock_date_message
      //       }
      //     }
      //   },
      //   tax_lock_date_message: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly'
      //   }
      // },
      // _div_customer_outstanding_credits: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     attrs: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['move_type', '!=', 'out_invoice'],
      //         ['invoice_has_outstanding', '=', false],
      //         ['payment_state', 'not in', ('not_paid', 'partial')]
      //       ]
      //     },
      //     class: 'alert alert-info mb-0',
      //     text: [
      //       'You have',
      //       'for this customer. You can allocate them to mark this invoice as paid.'
      //     ]
      //   },
      //   _bold: {
      //     _a: {
      //       _attr: {
      //         href: '#outstanding',
      //         class: 'alert-link',
      //         text: 'outstanding credits'
      //       }
      //     }
      //   }
      // },
      // _div_vendor_outstanding_debits: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     attrs: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['move_type', '!=', 'in_invoice'],
      //         ['invoice_has_outstanding', '=', false],
      //         ['payment_state', 'not in', ('not_paid', 'partial')]
      //       ]
      //     },
      //     class: 'alert alert-info mb-0',
      //     text: [
      //       'You have',
      //       'for this vendor. You can allocate them to mark this bill as paid.'
      //     ]
      //   },
      //   _bold: {
      //     _a: {
      //       _attr: {
      //         href: '#outstanding',
      //         class: 'alert-link',
      //         text: 'outstanding debits'
      //       }
      //     }
      //   }
      // },
      // _div_customer_outstanding_debits: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     attrs: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['move_type', '!=', 'out_refund'],
      //         ['invoice_has_outstanding', '=', false],
      //         ['payment_state', 'not in', ('not_paid', 'partial')]
      //       ]
      //     },
      //     class: 'alert alert-info mb-0',
      //     text: [
      //       'You have',
      //       'for this customer. You can allocate them to mark this credit note as paid.'
      //     ]
      //   },
      //   _bold: {
      //     _a: {
      //       _attr: {
      //         href: '#outstanding',
      //         class: 'alert-link',
      //         text: 'outstanding debits'
      //       }
      //     }
      //   }
      // },
      // _div_vendor_outstanding_credits: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     attrs: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['move_type', '!=', 'in_refund'],
      //         ['invoice_has_outstanding', '=', false],
      //         ['payment_state', 'not in', ('not_paid', 'partial')]
      //       ]
      //     },
      //     class: 'alert alert-info mb-0',
      //     text: [
      //       'You have',
      //       'for this vendor. You can allocate them to mark this credit note as paid.'
      //     ]
      //   },
      //   _bold: {
      //     _a: {
      //       _attr: {
      //         href: '#outstanding',
      //         class: 'alert-link',
      //         text: 'outstanding credits'
      //       }
      //     }
      //   }
      // },
      // _div_date: {
      //   _attr: {
      //     class: 'alert alert-info mb-0',
      //     text: [
      //       'This move is configured to be posted automatically at the accounting date:',
      //       '.'
      //     ],
      //     attrs: {
      //       invisible({ record }) {
      //         // invisible: ['|', ['state', '!=', 'draft'],
      //         // ['auto_post', '!=', 'at_date']],
      //         const { state, auto_post } = record
      //         return state !== 'draft' || auto_post !== 'at_date'
      //       }
      //     }
      //   },
      //   date: { readonly: '1' }
      // },
      // _div_auto_post: {
      //   _attr: {
      //     attrs: {
      //       invisible({ record }) {
      //         // invisible: [
      //         //   '|',
      //         //   '|',
      //         //   ['state', '!=', 'draft'],
      //         //   ['auto_post', '=', 'no'],
      //         //   ['auto_post', '=', 'at_date']
      //         // ],
      //         const { state, auto_post } = record
      //         return (
      //           state !== 'draft' || auto_post === 'no' || auto_post === 'at_date'
      //         )
      //       }
      //     },
      //     class: 'alert alert-info mb-0',
      //     text: ['auto-posting enabled. Next accounting date:', '.']
      //   },
      //   auto_post: { readonly: '1' },
      //   date: { readonly: '1' },
      //   _span: {
      //     _attr: {
      //       attrs: {
      //         invisible: [['auto_post_until', '=', false]]
      //       },
      //       text: ['The recurrence will end on', '(included).']
      //     },
      //     auto_post_until: { readonly: '1' }
      //   }
      // },
      // _div_partner_credit_warning: {
      //   _attr: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly',
      //     class: 'alert alert-warning mb-0',
      //     attrs: {
      //       invisible({ record }) {
      //         // invisible: [['partner_credit_warning', '=', '']],
      //         const { partner_credit_warning } = record
      //         return !partner_credit_warning
      //       }
      //     }
      //   },
      //   partner_credit_warning: {
      //     groups: 'account.group_account_invoice,account.group_account_readonly'
      //   }
      // },
      // _div_bill_action_activate_currency: {
      //   _attr: {
      //     attrs: {
      //       invisible: [
      //         '|',
      //         ['display_inactive_currency_warning', '=', false],
      //         ['move_type', 'not in', ('in_invoice', 'in_refund', 'in_receipt')]
      //       ]
      //     },
      //     class: 'alert alert-warning mb-0',
      //     text: [
      //       'In order to validate this bill, you must',
      //       ". The journal entries need to be computed by Odoo before being posted in your company's currency."
      //     ]
      //   },
      //   _button_action_activate_currency: {
      //     _attr: {
      //       name: 'action_activate_currency',
      //       type: 'object',
      //       class: 'oe_link',
      //       text: 'activate the currency of the bill'
      //     }
      //   }
      // },
      // _div_invoice_action_activate_currency: {
      //   _attr: {
      //     attrs: {
      //       invisible: [
      //         '|',
      //         ['display_inactive_currency_warning', '=', false],
      //         ['move_type', 'not in', ('out_invoice', 'out_refund', 'out_receipt')]
      //       ]
      //     },
      //     class: 'alert alert-warning mb-0',
      //     text: [
      //       'In order to validate this invoice, you must',
      //       ". The journal entries need to be computed by Odoo before being posted in your company's currency."
      //     ]
      //   },
      //   _button_action_activate_currency: {
      //     _attr: {
      //       name: 'action_activate_currency',
      //       type: 'object',
      //       class: 'oe_link',
      //       text: 'activate the currency of the invoice'
      //     }
      //   }
      // },
      // _div_o_attachment_preview: {
      //   _attr: {
      //     attrs: {
      //       invisible: [
      //         '|',
      //         [
      //           'move_type',
      //           'not in',
      //           ('out_invoice', 'out_refund', 'in_invoice', 'in_refund')
      //         ],
      //         ['state', '!=', 'draft']
      //       ]
      //     },
      //     class: 'o_attachment_preview'
      //   }
      // },
      // _widget_web_ribbon_paid: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Paid',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': ['|', ('payment_state', '!=', 'paid'),
      //         // ('move_type', 'not in',
      //         // ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'))]
      //         const { payment_state, move_type } = record
      //         const types = [
      //           'out_invoice',
      //           'out_refund',
      //           'in_invoice',
      //           'in_refund',
      //           'out_receipt',
      //           'in_receipt'
      //         ]
      //         return payment_state !== 'paid' || !types.includes(move_type)
      //       }
      //     }
      //   }
      // },
      // _widget_web_ribbon_in_payment: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'In Payment',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': ['|', ('payment_state', '!=', 'in_payment'),
      //         // ('move_type', 'not in',
      //         // ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'))]
      //         const { payment_state, move_type } = record
      //         const types = [
      //           'out_invoice',
      //           'out_refund',
      //           'in_invoice',
      //           'in_refund',
      //           'out_receipt',
      //           'in_receipt'
      //         ]
      //         return payment_state !== 'in_payment' || !types.includes(move_type)
      //       }
      //     }
      //   }
      // },
      // _widget_web_ribbon_partial: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Partial',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': ['|', ('payment_state', '!=', 'partial'),
      //         // ('move_type', 'not in',
      //         // ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'))]
      //         const { payment_state, move_type } = record
      //         const types = [
      //           'out_invoice',
      //           'out_refund',
      //           'in_invoice',
      //           'in_refund',
      //           'out_receipt',
      //           'in_receipt'
      //         ]
      //         return payment_state !== 'partial' || !types.includes(move_type)
      //       }
      //     }
      //   }
      // },
      // _widget_web_ribbon_reversed: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Reversed',
      //     bg_color: 'bg-danger',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': [('payment_state', '!=', 'reversed')]
      //         const { payment_state } = record
      //         return payment_state !== 'reversed'
      //       }
      //     }
      //   }
      // },
      // _widget_web_ribbon_invoicing_legacy: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Invoicing App Legacy',
      //     bg_color: 'bg-info',
      //     tooltip:
      //       "This entry has been generated through the Invoicing app, before installing Accounting. It has been disabled by the 'Invoicing Switch Threshold Date' setting so that it does not impact your accounting.",
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': [('payment_state', '!=', 'invoicing_legacy')]
      //         const { payment_state } = record
      //         return payment_state !== 'invoicing_legacy'
      //       }
      //     }
      //   }
      // },
    },

    button_box: {},

    header: {
      button_action_post2: {
        name: 'action_post',
        string: '确认',
        type: 'object',
        // groups: 'account.group_account_invoice',
        btn_type: 'primary',
        context: { validate_analytic: true },
        // 'invisible': ['|', '|', ('hide_post_button', '=', True), ('move_type', '=', 'entry'), ('display_inactive_currency_warning','=',True)]
        invisible: [
          '|',
          '|',
          ['hide_post_button', '=', true],
          ['move_type', '=', 'entry'],
          ['display_inactive_currency_warning', '=', true]
        ]
      },

      //   action_invoice_sent: {
      //     name: 'action_invoice_sent',
      //     string: 'Send & Print',
      //     type: 'object',
      //     btn_type: 'primary',
      //     attrs: {
      //       invisible({ record }) {
      //         const { state, is_move_sent, move_type } = record
      //         return (
      //           state !== 'posted' ||
      //           is_move_sent ||
      //           !['out_invoice', 'out_refund'].includes(move_type)
      //         )
      //       }
      //     }
      //   },
      //   action_invoice_sent2: {
      //     name: 'action_invoice_sent',
      //     string: 'Send & Print',
      //     type: 'object',
      //     attrs: {
      //       invisible({ record }) {
      //         const { state, is_move_sent, move_type } = record
      //         return (
      //           state !== 'posted' ||
      //           !is_move_sent ||
      //           ![
      //             'out_invoice',
      //             'out_refund',
      //             'in_invoice',
      //             'in_refund'
      //           ].includes(move_type)
      //         )
      //       }
      //     }
      //   },

      button_action_register_payment: {
        name: 'action_register_payment',
        string: '登记支付',
        type: 'object',
        // groups: 'account.group_account_invoice',
        btn_type: 'primary',
        // context="{'dont_redirect_to_payments': True}"
        context: { dont_redirect_to_payments: true },
        // 'invisible':
        // ['|', '|', ('state', '!=', 'posted'), ('payment_state', 'not in', ('not_paid', 'partial')),
        // ('move_type', 'not in', ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'))]

        invisible: [
          '|',
          '|',
          ['state', '!=', 'posted'],
          ['payment_state', 'not in', ['not_paid', 'partial']],
          [
            'move_type',
            'not in',
            [
              'out_invoice',
              'out_refund',
              'in_invoice',
              'in_refund',
              'out_receipt',
              'in_receipt'
            ]
          ]
        ]
      },

      button_preview_invoice: {
        invisible: 1,
        name: 'preview_invoice',
        string: 'Preview',
        type: 'object',
        btn_type: 'primary'

        // 'invisible': [('move_type', 'not in', ('out_invoice', 'out_refund'))]
        // invisible: [['move_type', 'not in', ['out_invoice', 'out_refund']]]
      },

      button_action_reverse: {
        invisible: 1,
        name: 'action_reverse',
        string: 'Add Credit Note',
        type: 'object'
        // groups: 'account.group_account_invoice',

        // 'invisible': ['|', ('move_type', 'not in', ('out_invoice', 'in_invoice')), ('state', '!=', 'posted')]
        // invisible: [
        //   '|',
        //   ['move_type', 'not in', ['out_invoice', 'in_invoice']],
        //   ['state', '!=', 'posted']
        // ]
      },

      button_cancel2: {
        name: 'button_cancel',
        string: '取消',
        type: 'object',
        // groups: 'account.group_account_invoice',

        // 'invisible' : ['|', '|', ('id', '=', False), ('state', '!=', 'draft'), ('move_type', '==', 'entry')]
        invisible: [
          '|',
          '|',
          ['id', '=', false],
          ['state', '!=', 'draft'],
          ['move_type', '=', 'entry']
        ]
      },

      button_draft: {
        name: 'button_draft',
        string: '重置为草稿',
        type: 'object',
        // groups: 'account.group_account_invoice',

        //  'invisible' : [('show_reset_to_draft_button', '=', False)]
        invisible: [['show_reset_to_draft_button', '=', false]]
      },

      field_state: { widget: 'statusbar', statusbar_visible: 'draft,posted' }
    },
    sheet: {
      ...view_move_form_sheet
    }
  }
}

const view_tree_account_move_invoice = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'tree',
  arch: {
    sheet: {
      // field_made_sequence_hole: { invisible: 1 },
      // field_name: {},
      field_invoice_partner_display_name__vendor: {
        string: '供应商',
        invisible: [
          [
            { context_default_move_type: '' },
            'not in',
            ['in_invoice', 'in_refund', 'in_receipt']
          ]
        ]
      },
      field_invoice_partner_display_name__customer: {
        string: '客户',
        invisible: [
          [
            { context_default_move_type: '' },
            'not in',
            ['out_invoice', 'out_refund', 'out_receipt']
          ]
        ]
      },

      field_invoice_date__vendor: {
        string: '账单日期',
        invisible: [
          [
            { context_default_move_type: '' },
            'not in',
            ['in_invoice', 'in_refund', 'in_receipt']
          ]
        ]
      },

      field_invoice_date__customer: {
        string: '开票日期',
        invisible: [
          [
            { context_default_move_type: '' },
            'not in',
            ['out_invoice', 'out_refund', 'out_receipt']
          ]
        ]
      },

      field_date: { optional: 'hide' },

      field_invoice_date_due: {
        optional: 'show',
        widget: 'remaining_days',

        invisible: [['payment_state', 'in', ['paid', 'in_payment', 'reversed']]]
      },
      field_invoice_origin: { optional: 'hide' },
      field_payment_reference: {
        optional: 'hide',
        // invisible="context.get('default_move_type') in ('out_invoice', 'out_refund','out_receipt')"

        invisible: [
          [
            { context_default_move_type: '' },
            'in',
            ['out_invoice', 'out_refund', 'out_receipt']
          ]
        ]
      },

      field_ref: { optional: 'show' },
      field_invoice_user_id: {
        optional: 'hide',
        widget: 'many2one_avatar_user',

        invisible: [
          [
            { context_default_move_type: '' },
            'not in',
            ['out_invoice', 'out_refund', 'out_receipt']
          ]
        ]
      },
      // activity_ids: { widget: 'list_activity', optional: 'show' },

      field_company_id: { optional: 'hide' },
      field_amount_untaxed_signed: { string: '不含税金额', optional: 'show' },
      field_amount_tax_signed: { string: '税', optional: 'hide' },
      field_amount_total_signed: { string: '含税金额', optional: 'show' },
      field_amount_residual_signed: {
        string: 'Amount Due',
        optional: 'hide'
      },
      field_currency_id: { optional: 'hide' },
      field_company_currency_id: { invisible: '1' },
      field_to_check: { widget: 'boolean_toggle', optional: 'hide' },
      field_payment_state: {
        optional: 'show',
        widget: 'badge',

        invisible: [['payment_state', 'in', ['invoicing_legacy']]]
      },

      field_state: { widget: 'badge', optional: 'show' },
      field_move_type: {
        invisible: {
          context_default_move_type: true
        }
      }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_invoice_partner_display_name: {} },
      card_value: {
        field_state: {},
        // field_date: {},
        field_amount_total_signed: {}
      }
    }
  }
}

const view_search_account_move_invoice = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Invoice',
        filter_domain: [
          '|',
          '|',
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['invoice_origin', 'ilike', { self: {} }],
          ['ref', 'ilike', { self: {} }],
          ['payment_reference', 'ilike', { self: {} }],
          ['partner_id', 'ilike', { self: {} }]
        ]
      },

      field_journal_id: {},
      field_partner_id: { operator: 'child_of' },
      field_invoice_user_id: {
        string: 'Salesperson',
        domain: [['share', '=', false]]
      },
      // date:{ string: 'Period' },
      field_line_ids: { string: 'Invoice Line' },
      filter_me: {
        myinvoices: {
          name: 'myinvoices',
          help: 'My Invoices',
          // domain="[('invoice_user_id', '=', uid)]"
          domain: [['invoice_user_id', '=', { session_uid: {} }]]
        }
      },

      filter_state: {
        // string: 'state',
        draft: {
          name: 'draft',
          string: 'Draft',
          domain: [['state', '=', 'draft']]
        },
        posted: {
          name: 'posted',
          string: '已过账',
          domain: [['state', '=', 'posted']]
        },
        cancel: {
          name: 'cancel',
          string: 'Cancelled',
          domain: [['state', '=', 'cancel']]
        }
      },

      filter_to_check: {
        string: 'to_check',
        to_check: {
          name: 'to_check',
          string: 'To Check',
          domain: [['to_check', '=', true]]
        }
      },
      filter_payment: {
        string: 'payment_state',
        open: {
          name: 'open',
          string: 'Unpaid',
          domain: [
            ['state', '=', 'posted'],
            ['payment_state', 'in', ['not_paid', 'partial']]
          ]
        },
        closed: {
          name: 'closed',
          string: 'Paid',
          domain: [
            ['state', '=', 'posted'],
            ['payment_state', 'in', ['in_payment', 'paid']]
          ]
        },
        late: {
          name: 'late',
          string: 'Overdue',
          domain: [
            ['invoice_date_due', '<', { datetime_today: {} }],
            ['state', '=', 'posted'],
            ['payment_state', 'in', ['not_paid', 'partial']]
          ]
        }
      },
      filter_date: {
        invoice_date: {
          name: 'invoice_date',
          string: 'Invoice Date',
          date: 'invoice_date'
        },
        date: {
          name: 'date',
          string: 'Accounting Date',
          date: 'date',
          invisible: [
            [
              {
                context_default_move_type: ''
              },
              'in',
              ['out_invoice', 'out_refund', 'out_receipt']
            ]
          ]
        },
        due_date: {
          name: 'due_date',
          string: 'Due Date',
          date: 'invoice_date_due'
        }
      }
    }
  }
}

const action_account_move_out_invoice = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售结算单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['out_invoice']]],
  context: { default_move_type: 'out_invoice' },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

const action_account_move_out_refund = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售退货单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['out_refund']]],
  context: { default_move_type: 'out_refund' },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

const action_account_move_out_receipt = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售收据',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['out_receipt']]],
  context: {
    default_move_type: 'out_receipt'
  },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

const action_account_move_in_invoice = {
  _odoo_model: 'ir.actions.act_window',
  name: '采购账单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['in_invoice']]],
  context: { default_move_type: 'in_invoice' },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

const action_account_move_in_refund = {
  _odoo_model: 'ir.actions.act_window',
  name: '采购退货单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['in_refund']]],
  context: { default_move_type: 'in_refund' },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

const action_account_move_in_receipt = {
  _odoo_model: 'ir.actions.act_window',
  name: '采购收据',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_invoice',
  domain: [['move_type', 'in', ['in_receipt']]],
  context: { default_move_type: 'in_receipt' },
  views: {
    tree: 'view_tree_account_move_invoice',
    form: 'view_form_account_move_invoice'
  }
}

export default {
  view_form_account_move_invoice,
  view_tree_account_move_invoice,
  view_search_account_move_invoice,

  action_account_move_out_invoice,
  action_account_move_out_refund,
  action_account_move_out_receipt,

  action_account_move_in_invoice,
  action_account_move_in_refund,
  action_account_move_in_receipt
}
