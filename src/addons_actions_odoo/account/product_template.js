const view_form_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_form_product_template',
  model: 'product.template',
  type: 'form',

  arch: {
    sheet: {
      notebook: {
        page_general_information: {
          attr: { string: '基本信息', name: 'general_information' },

          group_general_information: {
            group_group_standard_price: {
              field_uom_name: { help2: 'next, extend' },
              field_tax_string: {},

              field_taxes_id: {
                widget: 'many2many_tags',
                // context="{
                // 'default_type_tax_use':'sale',
                // 'search_default_sale': 1,
                // 'search_default_service': type == 'service',
                // 'search_default_goods': type == 'consu'}"

                context: {
                  // default_type_tax_use: 'sale',
                  // search_default_sale: 1,
                  // search_default_service: type == 'service',
                  // search_default_goods: type == 'consu'
                }
              }
            }
          }
        },

        page_purchase: {
          attr: {
            name: 'purchase',
            string: '采购',

            //  'invisible':[('purchase_ok','=',False)]
            invisible: [['purchase_ok', '=', false]]
          },

          group_purchase: {
            attr: { name: 'purchase' },
            group_bill: {
              attr: { name: 'bill', string: 'Vendor Bills' },

              field_supplier_taxes_id: {
                // 安装 采购模块后, 设置只读
                readonly: [['purchase_ok', '=', 0]],
                widget: 'many2many_tags',
                // context="{'default_type_tax_use':'purchase',
                // 'search_default_purchase': 1,
                // 'search_default_service': type == 'service',
                // 'search_default_goods': type == 'consu'}"

                context: {
                  // default_type_tax_use: 'purchase',
                  // search_default_purchase: 1,
                  // search_default_service: type == 'service',
                  // search_default_goods: type == 'consu'
                }
              }
            }
          }
        },

        page_invoicing: {
          attr: {
            string: '开票设置',
            name: 'invoicing'
            // groups:
            //   'account.group_account_readonly,account.group_account_invoice'
          },

          group_properties: {
            attr: {
              name: 'properties'
              // groups: 'account.group_account_readonly'
            },

            group_receivables: {
              attr: { string: '销售' },
              field_property_account_income_id: {
                // groups: 'account.group_account_readonly'
              }
            },
            group_payables: {
              attr: { name: 'payables', string: '采购' },
              field_property_account_expense_id: {
                // 安装 采购模块后, 设置只读
                readonly: [['purchase_ok', '=', 0]]

                // groups: 'account.group_account_readonly'
              }
            }
          },
          group_accounting: {
            attr: {
              name: 'accounting'
              // groups:
              //   'account.group_account_readonly,account.group_account_invoice'
            }
          }
        }
      }
    }
  }
}

const view_tree_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_tree_product_template',
  model: 'product.template',
  type: 'tree',
  arch: {
    sheet: {
      field_default_code: {},
      field_name: {},
      field_list_price: { widget: 'monetary' },
      field_taxes_id: { widget: 'many2many_tags' },
      field_supplier_taxes_id: { widget: 'many2many_tags' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {
        field_type: {
          selection: [
            ['service', '服务或费用'],
            ['consu', '易耗品'],
            ['product', '库存产品']
          ]
        }
      },
      card_value: { field_default_code: {}, field_barcode: {} }
    }
  }
}

const view_search_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_search_product_template'
}

const action_product_template = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品(财务设置)',
  type: 'ir.actions.act_window',
  res_model: 'product.template',
  search_view_id: 'view_search_product_template',
  domain: [],
  context: {
    search_default_filter_to_sell: 1,
    search_default_filter_to_purchase: 1
  },
  views: {
    tree: 'view_tree_product_template',
    form: 'view_form_product_template'
  }
}

export default {
  view_form_product_template,
  view_tree_product_template,
  view_search_product_template,
  action_product_template
}
