const view_form_account_journal = {
  _odoo_model: 'ir.ui.view',
  model: 'account.journal',
  type: 'form',
  arch: {
    button_box: {
      // button_action_account_moves_all_a: {
      //   string: 'Journal Entries',
      //   type: 'action',
      //   name: 'action_account_moves_all_a',
      //   // context="{'search_default_journal_id':active_id}",
      //   context: { search_default_journal_id: { field_id: {} } },
      //   widget_statinfo: {
      //     widget_text__1: '会计',
      //     widget_text__2: '分录'
      //   }
      // }
    },

    sheet: {
      field_company_id: { invisible: '1' },
      field_bank_statements_source: { invisible: '1' },
      field_active: {},
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },
      field_name: { placeholder: 'e.g. Customer Invoices' },

      group: {
        group_type: {
          field_active: { invisible: '1' },
          field_type: {}
        },
        group_country: {
          field_company_id: { groups: 'base.group_multi_company' },
          field_country_code: { invisible: '1' }
        }
      },

      notebook: {
        page_bank_account: {
          attr: { name: 'bank_account', string: '分录' },
          group: {
            group_Accounting_Information: {
              attr: { string: '开票信息' },
              field_default_account_type: { invisible: '1' },

              field_default_account_id: {
                // groups: 'account.group_account_readonly',
                invisible: [['type', '=', false]],
                // 'required': [
                // '|',
                // '&amp;',
                // ('id', '!=', False),
                // ('type', 'in', ('bank', 'cash')),
                // ('type', 'in', ('sale', 'purchase'))]

                required: [
                  '|',
                  '&',
                  ['id', '!=', false],
                  ['type', 'in', ['bank', 'cash']],
                  ['type', 'in', ['sale', 'purchase']]
                ],

                label_bank: {
                  for: 'default_account_id',
                  string: 'Bank Account',
                  invisible: [['type', '!=', 'bank']]
                },
                label_cash: {
                  for: 'default_account_id',
                  string: 'Cash Account',
                  invisible: [['type', '!=', 'cash']]
                },

                label_sale: {
                  for: 'default_account_id',
                  string: 'Default Income Account',
                  invisible: [['type', '!=', 'sale']]
                },

                label_purchase: {
                  for: 'default_account_id',
                  string: 'Default Expense Account',
                  invisible: [['type', '!=', 'purchase']]
                },

                label_general: {
                  for: 'default_account_id',
                  string: 'Default Account',
                  help: 'If set, this account is used to automatically balance entries.',
                  invisible: [['type', '!=', 'general']]
                }
              },

              field_suspense_account_id: {
                // groups: 'account.group_account_readonly',
                // invisible: [('type', 'not in', ('bank', 'cash'))]
                invisible: [['type', 'not in', ['bank', 'cash']]],
                //required:  [('type', 'in', ('bank', 'cash'))]
                required: [['type', 'in', ['bank', 'cash']]]
              },
              field_profit_account_id: {
                // invisible:['!', ('type', 'in', ('cash', 'bank'))]
                invisible: ['!', ['type', 'in', ['bank', 'cash']]]
              },
              field_loss_account_id: {
                // invisible:['!', ('type', 'in', ('cash', 'bank'))]
                invisible: ['!', ['type', 'in', ['bank', 'cash']]]
              },

              field_refund_sequence: {
                // invisible:['!', ('type', 'not in', ('sale', 'purchase'))]
                invisible: ['!', ['type', 'not in', ['sale', 'purchase']]]
              },
              field_payment_sequence: {
                // invisible: [('type', 'not in', ('bank', 'cash'))]
                invisible: [['type', 'not in', ['bank', 'cash']]]
              },
              field_code: { placeholder: 'e.g. INV' },
              field_currency_id: { groups: 'base.group_multi_currency' }
            },
            group_bank_account_number: {
              attr: {
                name: 'bank_account_number',
                string: '银行账号',
                //  'invisible': [('type', '!=', 'bank')]
                invisible: [['type', '!=', 'bank']]
              },
              field_company_partner_id: { invisible: 1 },
              field_bank_account_id: {
                string: '银行账号'
                // context="{'default_partner_id': company_partner_id}"
              },
              field_bank_id: {
                // 'invisible': [('bank_account_id', '=', False)]
                invisible: [['bank_account_id', '=', false]]
              },

              field_bank_statements_source: {
                widget: 'radio',
                groups: 'account.group_account_readonly',
                // 'required': [('type', '=', 'bank')]
                required: [['type', '=', 'bank']]
              }
            }
          }
        },
        page_inbound_payment_settings: {
          attr: {
            id: 'inbound_payment_settings',
            string: '销售收款',
            // invisible: [('type', 'not in', ('bank', 'cash'))]
            invisible: [['type', 'not in', ['bank', 'cash']]]
          },
          field_available_payment_method_ids: { invisible: 1 },
          field_inbound_payment_method_line_ids: {
            context: { default_payment_type: 'inbound' },
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_available_payment_method_ids: { invisible: 1 },
                    field_payment_type: { invisible: 1 },
                    field_company_id: { invisible: 1 },
                    field_sequence: { widget: 'handle' },
                    field_payment_method_id: {},
                    field_name: {},
                    field_payment_account_id: {
                      optional: 'hide',
                      string: 'Outstanding Receipts accounts',
                      // groups: 'account.group_account_readonly',
                      placeholder:
                        'Leave empty to use the default outstanding account',
                      no_quick_create: true
                    }
                  },
                  kanban: {
                    card_title: { field_name: {} },
                    card_label: {},
                    card_value: {}
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_available_payment_method_ids: { invisible: 1 },
                    field_payment_type: { invisible: 1 },
                    field_company_id: { invisible: 1 },
                    field_sequence: { widget: 'handle' },
                    field_payment_method_id: {},
                    field_name: {},
                    field_payment_account_id: {}
                  }
                }
              }
            }
          }
        },

        page_outbound_payment_settings: {
          attr: {
            id: 'outbound_payment_settings',
            string: '支付',
            // invisible: [('type', 'not in', ('bank', 'cash'))]
            invisible: [['type', 'not in', ['bank', 'cash']]]
          },
          field_outbound_payment_method_line_ids: {
            context: { default_payment_type: 'outbound' },
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_available_payment_method_ids: { invisible: 1 },
                    field_payment_type: { invisible: 1 },
                    field_company_id: { invisible: 1 },
                    field_sequence: { widget: 'handle' },
                    field_payment_method_id: {},
                    field_name: {},
                    field_payment_account_id: {
                      optional: 'hide',
                      string: 'Outstanding Payments accounts',
                      // groups: 'account.group_account_readonly',
                      placeholder:
                        'Leave empty to use the default outstanding account',
                      no_quick_create: true
                    }
                  },
                  kanban: {
                    card_title: { field_name: {} },
                    card_label: {},
                    card_value: {}
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_available_payment_method_ids: { invisible: 1 },
                    field_payment_type: { invisible: 1 },
                    field_company_id: { invisible: 1 },
                    field_sequence: { widget: 'handle' },
                    field_payment_method_id: {},
                    field_name: {},
                    field_payment_account_id: {}
                  }
                }
              }
            }
          },

          field_selected_payment_method_codes: { invisible: 1 },
          group_outgoing_payment: {
            attr: { name: 'outgoing_payment' }
          }
        },
        page_advanced_settings: {
          attr: { name: 'advanced_settings', string: 'Advanced Settings' },
          group: {
            group_Control_Access: {
              attr: {
                string: 'Control-Access',
                groups: 'account.group_account_manager'
              },

              widget_text: 'Keep empty for no control',

              field_account_control_ids: { widget: 'many2many_tags' },
              field_restrict_mode_hash_table: {
                // groups: 'account.group_account_readonly',
                // invisible: [('type', 'in', ('bank', 'cash'))]
                invisible: [['type', 'in', ['bank', 'cash']]]
              }
            },
            group_group_alias_ro: {
              attr: {
                name: 'group_alias_ro',
                string: 'Create Invoices upon Emails',
                // 'invisible':
                // ['|', ('type', 'not in',  ('sale' ,'purchase')),
                // ('alias_domain', '=', False)]

                invisible: [
                  '|',
                  ['type', 'not in', ['sale', 'purchase']],
                  ['alias_domain', '=', false]
                ]
              },
              field_alias_id: {}
            },
            group_group_alias_no_domain: {
              attr: {
                name: 'group_alias_no_domain',
                string: 'Create Invoices upon Emails',
                // 'invisible': ['|', ('type', 'not in',  ('sale' ,'purchase')),
                //  ('alias_domain', '!=', False)]

                invisible: [
                  '|',
                  ['type', 'not in', ['sale', 'purchase']],
                  ['alias_domain', '!=', false]
                ]
              },

              button: {
                invisible: 1,
                type: 'action',
                name: 'action_open_settings',
                string: 'Configure Email Servers'
              }
            },

            group_group_alias_edit: {
              attr: {
                name: 'group_alias_edit',
                string: 'Create Invoices upon Emails',

                // class: 'oe_edit_only',
                // 'invisible': ['|', ('type', 'not in',  ('sale' ,'purchase')),
                //  ('alias_domain', '!=', False)]

                invisible: [
                  '|',
                  { editable: false },
                  '|',
                  ['type', 'not in', ['sale', 'purchase']],
                  ['alias_domain', '!=', false]
                ]
              },

              field_alias_name: {
                label_alias_name: {
                  for: 'alias_name',
                  string: 'Email Alias'
                }
              },
              widget_text: '@',
              field_alias_domain: { readonly: '1' }
            },

            group_Payment_Communications: {
              attr: {
                string: 'Payment Communications',
                // 'invisible': [('type', '!=', 'sale')]
                invisible: [['type', '!=', 'sale']]
              },
              field_invoice_reference_type: {},
              field_invoice_reference_model: {
                //'invisible': [('invoice_reference_type', '=', 'none')]
                invisible: [['invoice_reference_type', '=', 'none']]
              }
            },
            group_Follow_Customer_Payments: {
              attr: {
                string: 'Follow Customer Payments',
                // 'invisible': [('type', '!=', 'sale')]
                invisible: [['type', '!=', 'sale']]
              },
              field_sale_activity_type_id: {},
              field_sale_activity_user_id: {
                //'invisible': [('sale_activity_type_id', '=', False)]
                invisible: [['sale_activity_type_id', '=', false]]
              },
              field_sale_activity_note: {
                placeholder: 'e.g. Give a phone call, check with others , ...',
                //'invisible': [('sale_activity_type_id', '=', False)]
                invisible: [['sale_activity_type_id', '=', false]]
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_account_journal = {
  _odoo_model: 'ir.ui.view',
  model: 'account.journal',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_type: {},
      field_journal_group_ids: {
        widget: 'many2many_tags',
        readonly: '1',
        optional: 'show'
      },
      field_currency_id: { optional: 'hide' },
      field_code: { optional: 'show' },
      field_default_account_id: { optional: 'show' },
      field_active: { optional: 'hide' },
      field_company_id: { optional: 'hide' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_code: {} },
      card_value: { field_default_account_id: {} }
    }
  }
}

const view_search_account_journal = {
  _odoo_model: 'ir.ui.view',
  model: 'account.journal',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '名称或代码',
        filter_domain: [
          '|',
          ['name', 'ilike', { self: {} }],
          ['code', '=like', { self: {} }]
        ]
      },

      // filter_dashboard: {
      //   dashboard: {
      //     name: 'dashboard',
      //     string: 'Favorites',
      //     domain: [['show_on_dashboard', '=', true]]
      //   }
      // },

      filter_type: {
        sales: {
          name: 'sales',
          string: '销售',
          domain: [['type', '=', 'sale']]
        },
        purchases: {
          name: 'purchases',
          string: '采购',
          domain: [['type', '=', 'purchase']]
        },
        liquidity: {
          name: 'liquidity',
          string: '流水',
          domain: ['|', ['type', '=', 'cash'], ['type', '=', 'bank']]
        },
        miscellaneous: {
          name: 'miscellaneous',
          string: '其他',
          domain: [['type', 'not in', ['sale', 'purchase', 'cash', 'bank']]]
        }
      },

      filter_active: {
        inactive: {
          name: 'inactive',
          string: '已归档',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_account_journal = {
  _odoo_model: 'ir.actions.act_window',
  name: '账簿',
  type: 'ir.actions.act_window',
  // buttons: { create: false, edit: false, delete: false },
  res_model: 'account.journal',
  search_view_id: 'view_search_account_journal',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_account_journal',
    form: 'view_form_account_journal'
  }
}

export default {
  view_form_account_journal,
  view_tree_account_journal,
  view_search_account_journal,
  action_account_journal
}
