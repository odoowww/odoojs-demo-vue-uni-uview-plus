const view_form_account_account_open = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account',
  type: 'form',
  arch: {
    button_box: {
      // button_action_open_related_taxes: {
      //   name: 'action_open_related_taxes',
      //   type: 'object',
      //   icon: 'fa-bars',
      //     // 'invisible': [('related_taxes_amount', '=', 0)]
      //     invisible: [['related_taxes_amount', '=', 0]]
      //   field_related_taxes_amount: { string: 'Taxes', widget: 'statinfo' }
      // },
      // button_account_action_move_line_select: {
      //   name: 'account.action_move_line_select',
      //   type: 'action',
      //   field_current_balance: { string: 'Balance', widget: 'statinfo' }
      // }
    },

    header: {
      button_action_open_in_payment: {
        name: 'action_open_in_payment',
        string: '打开收款单',
        type: 'object',
        action_map: {
          'account.payment': 'account.action_account_payment_customer_inbound'
        },

        invisible: [['code', '!=', '112300']]
      }
    },

    sheet: {
      field_company_id: {
        invisible: 1,
        fields: {
          account_opening_date: {},
          account_opening_move_id: {}
        }
      },
      field_code: { readonly: '1' },
      field_name: { readonly: '1' },
      field_account_type: { widget: 'account_type_selection', readonly: '1' },

      field_reconcile: { widget: 'boolean_toggle', readonly: '1' },
      field_opening_debit: {},
      field_opening_credit: {},
      field_opening_balance: { invisible: '1' }
    }
  }
}

const view_tree_account_account_open = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account',
  type: 'tree',
  arch: {
    sheet: {
      field_company_id: { invisible: 1 },
      field_code: {},
      field_name: {},
      field_account_type: { widget: 'account_type_selection' },
      field_reconcile: { widget: 'boolean_toggle', optional: 'hide' },
      field_opening_debit: {},
      field_opening_credit: {},
      field_opening_balance: { optional: 'hide' },
      field_tax_ids: { optional: 'hide', widget: 'many2many_tags' },
      field_tag_ids: { optional: 'hide', widget: 'many2many_tags' },
      field_allowed_journal_ids: { optional: 'hide', widget: 'many2many_tags' }
    },

    kanban: {
      card_title: { field_name: {} },
      card_label: { field_code: {} },
      card_value: {
        field_account_type: {},
        field_current_balance: {}
      }
    }
  }
}

const acc_codes = ['100202', '100203', '100204', '101201', '999999']

const action_account_account_open = {
  _odoo_model: 'ir.actions.act_window',
  name: '科目期初设置',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: true, delete: false },
  res_model: 'account.account',
  search_view_id: 'view_search_account_account',
  domain: [
    ['code', 'not in', [...acc_codes]],
    ['include_initial_balance', '=', true]
  ],
  context: { search_default_activeacc: true },
  views: {
    tree: 'view_tree_account_account_open',
    form: 'view_form_account_account_open'
  }
}
export default {
  view_form_account_account_open,
  view_tree_account_account_open,
  action_account_account_open
}
