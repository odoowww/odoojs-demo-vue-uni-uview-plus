const view_form_account_payment = {
  _odoo_model: 'ir.ui.view',
  model: 'account.payment',
  type: 'form',

  toolbar: {
    action: {},
    print: {}
  },

  arch: {
    alert_div: {
      // widget_text__alert_is_internal_transfer: {
      //   attr: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['paired_internal_transfer_payment_id', '!=', false],
      //         ['is_internal_transfer', '=', false],
      //         ['state', '!=', 'draft']
      //       ] ,
      //     text: 'A second payment will be created automatically in the destination journal.'
      //   }
      // },
      // widget_text__alert_is_internal_transfer_partner_bank_id: {
      //   attr: {
      //       invisible: [
      //         '|',
      //         '|',
      //         ['is_internal_transfer', '=', false],
      //         ['require_partner_bank_account', '=', false],
      //         ['partner_bank_id', '!=', false]
      //       ] ,
      //     text: 'The selected payment method requires a bank account but none is set on'
      //   }
      //   // _button_action_open_destination_journal: {
      //   //   _attr: {
      //   //     name: 'action_open_destination_journal',
      //   //     type: 'object',
      //   //     class: 'oe_link alert-link',
      //   //     text: 'the destination journal'
      //   //   }
      //   // }
      // },
    },

    button_box: {
      button_button_open_invoices: {
        invisible: 1,
        name: 'button_open_invoices',
        type: 'object',
        class: 'oe_stat_button',
        icon: 'fa-bars',

        // 'invisible': [('reconciled_invoices_count','=', 0)]

        field_reconciled_invoices_count: {
          // string: 'Invoice'
          //'invisible': [('reconciled_invoices_type', '!=', 'invoice')]
          //
          // text: 'Credit Note',
          //'invisible': [('reconciled_invoices_type', '=', 'invoice')]
        }
      },

      button_button_open_bills: {
        invisible: 1,
        name: 'button_open_bills',
        type: 'object',
        class: 'oe_stat_button',
        icon: 'fa-bars',

        // 'invisible': [('reconciled_bills_count','=', 0)]

        field_reconciled_bills_count: { string: 'Bill' }
      },

      button_button_open_statement_lines: {
        invisible: 1,
        name: 'button_open_statement_lines',
        type: 'object',
        class: 'oe_stat_button',
        icon: 'fa-bars',

        // 'invisible': [('reconciled_statement_lines_count','=', 0)]

        field_reconciled_statement_lines_count: { string: 'Transaction' }
      },

      button_button_open_journal_entry: {
        invisible: 1,
        string: 'Journal Entry',
        name: 'button_open_journal_entry',
        type: 'object',
        class: 'oe_stat_button',
        icon: 'fa-bars'
      }
    },

    header: {
      // button_action_post: {
      //   name: 'action_post',
      //   type: 'object',
      //   string: 'Confirm',
      //   btn_type: 'primary',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': [('state', '!=', 'draft')]
      //       const { state } = record
      //       return state !== 'draft'
      //     }
      //   }
      // },
      // button_action_draft: {
      //   name: 'action_draft',
      //   type: 'object',
      //   string: 'Reset To Draft',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': [('state', 'not in', ('posted', 'cancel'))]
      //       const { state } = record
      //       return !['posted', 'cancel'].includes(state)
      //     }
      //   }
      // },
      // button_action_cancel: {
      //   name: 'action_cancel',
      //   type: 'object',
      //   string: 'Cancel',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': [('state', '!=', 'draft')]
      //       const { state } = record
      //       return state !== 'draft'
      //     }
      //   }
      // },
      // button_mark_as_sent: {
      //   name: 'mark_as_sent',
      //   type: 'object',
      //   string: 'Mark as Sent',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': ['|', '|',
      //       // ('state', '!=', 'posted'),
      //       // ('is_move_sent', '=', True),
      //       // ('payment_method_code', '!=', 'manual')]
      //       const { state, is_move_sent, payment_method_code } = record
      //       return (
      //         state !== 'posted' ||
      //         is_move_sent ||
      //         payment_method_code !== 'manual'
      //       )
      //     }
      //   }
      // },
      // button_unmark_as_sent: {
      //   name: 'unmark_as_sent',
      //   type: 'object',
      //   string: 'Unmark as Sent',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': ['|', '|',
      //       // ('state', '!=', 'posted'),
      //       // ('is_move_sent', '=', False),
      //       // ('payment_method_code', '!=', 'manual')]
      //       const { state, is_move_sent, payment_method_code } = record
      //       return (
      //         state !== 'posted' ||
      //         !is_move_sent ||
      //         payment_method_code !== 'manual'
      //       )
      //     }
      //   }
      // },

      field_state: { widget: 'statusbar', statusbar_visible: 'draft,posted' }
    },

    sheet: {
      field_is_move_sent: { invisible: 1 },
      field_is_reconciled: { invisible: 1 },
      field_is_matched: { invisible: 1 },
      field_payment_method_code: { invisible: 1 },
      field_show_partner_bank_account: { invisible: 1 },
      field_require_partner_bank_account: { invisible: 1 },
      field_available_payment_method_line_ids: { invisible: 1 },
      field_available_partner_bank_ids: { invisible: 1 },
      field_suitable_journal_ids: { invisible: 1 },
      field_country_code: { invisible: 1 },
      field_partner_type: { invisible: 1 },
      field_posted_before: { invisible: 1 },
      field_reconciled_invoices_type: { invisible: 1 },
      field_company_id: { invisible: 1 },
      field_paired_internal_transfer_payment_id: { invisible: 1 },
      field_available_journal_ids: { invisible: 1 },

      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          bg_color: 'bg-info',

          invisible: [['state', '!=', 'invoicing_legacy']]
        }
      },

      widget_text___name1: {
        attr: {
          //'invisible': [('state', '!=', 'draft')]
          invisible: [['state', '!=', 'draft']],
          text: 'Draft'
        }
      },

      field_name: {
        //'invisible': [('state', '=', 'draft')]
        invisible: [['state', '=', 'draft']],
        readonly: '1'
      },

      group_name: {
        group_group1: {
          attr: { name: 'group1' },

          field_partner_type: {
            readonly: '1',

            invisible: [['is_internal_transfer', '=', 'true']],
            help: '额外增加的字段'
          },

          field_is_internal_transfer: { force_save: '1', readonly: 1 },
          field_payment_type: {
            widget: 'radio',

            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          },

          field_partner_id: {
            invisible: [['is_internal_transfer', '=', true]],
            label_customer: {
              string: '客户',

              invisible: [
                '|',
                ['partner_type', '!=', 'customer'],
                ['is_internal_transfer', '=', true]
              ]
            },
            label_supplier: {
              string: '供应商',

              invisible: [
                '|',
                ['partner_type', '!=', 'supplier'],
                ['is_internal_transfer', '=', true]
              ]
            }
          },

          field_amount: {
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          },

          field_currency_id: {
            string: 'Payment Currency',
            groups: 'base.group_multi_currency',
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']],
            required: '1'
          },
          field_date: {
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          },
          field_ref: { string: 'Memo' }
        },

        group_group2: {
          attr: { name: 'group2' },
          field_journal_id: {
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']],
            // domain="[('id', 'in', available_journal_ids)]"

            domain: [['id', 'in', { field_available_journal_ids: {} }]]
          },

          field_payment_method_line_id: {
            required: '1',

            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']],
            //  domain="[('id', 'in', available_payment_method_line_ids)]"
            domain: [
              ['id', 'in', { field_available_payment_method_line_ids: {} }]
            ]
          },

          field_partner_bank_id: {
            readonly: [['payment_type', '!=', 'outbound']],
            required: [
              ['require_partner_bank_account', '=', true],
              ['is_internal_transfer', '=', false]
            ],

            invisible: [
              '|',
              ['show_partner_bank_account', '=', false],
              ['is_internal_transfer', '=', true]
            ],
            label_customer: {
              string: '客户银行账户',

              invisible: [
                '|',
                ['partner_type', '!=', 'customer'],
                ['payment_type', '=', 'inbound']
              ]
            },

            label_supplier: {
              string: '供应商银行账户',

              invisible: [
                '|',
                ['partner_type', '!=', 'supplier'],
                ['payment_type', '=', 'inbound']
              ]
            },

            label_company: {
              string: '源账户',

              invisible: [['payment_type', '=', 'outbound']]
            }
          },

          field_destination_journal_id: {
            string: '目标账簿',

            // 'invisible': [('is_internal_transfer', '=', False)],
            invisible: [['show_partner_bank_account', '=', false]],
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [('state', '!=', 'draft')],

            // 'required': [('is_internal_transfer', '=', True),('state', '=', 'draft')]
            required: [
              ['is_internal_transfer', '=', true],
              ['state', '=', 'draft']
            ],

            // domain="[('type', 'in', ('bank','cash')),
            //   ('company_id', '=', company_id), ('id', '!=', journal_id)]",

            domain: [
              ['type', 'in', ('bank', 'cash')],
              ['company_id', '=', { field_company_id: {} }],
              ['id', '!=', { field_journal_id: {} }]
            ]
          }
        }
      },

      group_group3: {
        field_qr_code: {
          widget: 'html',

          // 'invisible': [('qr_code', '=', False)]
          invisible: [['qr_code', '=', false]]
        }
      }
    }
  }
}

const view_tree_account_payment = {
  _odoo_model: 'ir.ui.view',
  model: 'account.payment',
  type: 'tree',
  arch: {
    sheet: {
      // _header: {
      //   _button_action_post: {
      //     _attr: {
      //       name: 'action_post',
      //       type: 'object',
      //       string: 'Confirm'
      //     }
      //   }
      // },

      field_company_currency_id: { invisible: '1' },
      field_date: {},
      field_name: {},
      field_journal_id: {},
      field_payment_method_line_id: {},

      field_partner_id__inbound: {
        string: '客户',
        invisible: [[{ context_default_payment_type: '' }, '!=', 'inbound']]
      },
      field_partner_id__outbound: {
        string: '供应商',
        invisible: [[{ context_default_payment_type: '' }, '!=', 'outbound']]
      },

      // field_amount_signed: {
      //   string: 'Amount in Currency',
      //   optional: 'hide',
      //   groups: 'base.group_multi_currency'
      // },
      // field_currency_id: {
      //   string: 'Payment Currency',
      //   optional: 'hide',
      //   groups: 'base.group_multi_currency'
      // },
      field_amount_company_currency_signed: {
        widget: 'monetary',
        string: 'Amount'
      },

      field_state: {
        widget: 'badge'
        // decoration-info="state == 'draft'" decoration-success="state == 'posted'"
      }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_partner_id: {}, field_journal_id: {} },
      card_value: { field_state: {}, field_amount_signed: {} }
    }
  }
}

const view_search_account_payment = {
  _odoo_model: 'ir.ui.view',
  model: 'account.payment',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '名称',

        // filter_domain="
        // ['|', '|', '|', '|',
        // ('name', 'ilike', self),
        // ('partner_id', 'ilike', self),
        // ('ref', 'ilike', self),
        // ('amount_company_currency_signed' , 'ilike', self),
        // ('amount', 'ilike', self)]

        filter_domain: [
          '|',
          '|',
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['partner_id', 'ilike', { self: {} }],
          ['ref', 'ilike', { self: {} }],
          ['partner_id', 'ilike', { self: {} }],
          ['amount_company_currency_signed', 'ilike', { self: {} }],
          ['amount', 'ilike', { self: {} }]
        ]
      },
      field_partner_id: {
        string: '客户/供应商'
        // domain: ['|', ['parent_id', '=', false], ['is_company', '=', true]]
      },
      field_journal_id: {
        // domain="[('id', 'in', available_journal_ids)]"
      },
      // is_internal_transfer: {}
      field_company_id: { groups: 'base.group_multi_company' },

      filter_type: {
        inbound_filter: {
          name: 'inbound_filter',
          string: '收款',
          domain: [
            ['partner_type', '=', 'customer'],
            ['is_internal_transfer', '=', false]
          ]
        },
        outbound_filter: {
          name: 'outbound_filter',
          string: '付款',
          domain: [
            ['partner_type', '=', 'supplier'],
            ['is_internal_transfer', '=', false]
          ]
        },
        transfers_filter: {
          name: 'transfers_filter',
          string: '内部转账',
          domain: [['is_internal_transfer', '=', true]]
        }
      },

      filter_state: {
        state_draft: {
          name: 'state_draft',
          string: '草稿',
          domain: [['state', '=', 'draft']]
        },
        state_posted: {
          name: 'state_posted',
          string: '已过账',
          domain: [['state', '=', 'posted']]
        }
      },
      filter_status: {
        // state_sent: {
        //   name: 'state_sent',
        //   string: 'Sent',
        //   domain: [['is_move_sent', '=', true]]
        // },
        // matched: {
        //   name: 'matched',
        //   string: 'Bank Matched',
        //   domain: [['is_matched', '=', true]]
        // },
        reconciled: {
          name: 'reconciled',
          string: '已核销',
          domain: [['is_reconciled', '=', true]]
        }
      },

      filter_date: {
        date: {
          name: 'date',
          string: '支付日期',
          date: 'date'
        }
      }
    }
  }
}

const action_account_payment_customer_inbound = {
  _odoo_model: 'ir.actions.act_window',
  name: '收款单',
  type: 'ir.actions.act_window',
  res_model: 'account.payment',
  search_view_id: 'view_search_account_payment',
  domain: [],
  context: {
    default_payment_type: 'inbound',
    default_partner_type: 'customer',
    search_default_inbound_filter: 1,
    default_move_journal_types: ['bank', 'cash']
  },
  views: {
    tree: 'view_tree_account_payment',
    form: 'view_form_account_payment'
  }
}

const action_account_payment_supplier_outbound = {
  _odoo_model: 'ir.actions.act_window',
  name: '付款单',
  type: 'ir.actions.act_window',
  res_model: 'account.payment',
  search_view_id: 'view_search_account_payment',
  domain: [],
  context: {
    default_payment_type: 'outbound',
    default_partner_type: 'supplier',
    search_default_outbound_filter: 1,
    default_move_journal_types: ['bank', 'cash']
  },
  views: {
    tree: 'view_tree_account_payment',
    form: 'view_form_account_payment'
  }
}

const action_account_payment_transfer = {
  _odoo_model: 'ir.actions.act_window',
  name: '内部转账单',
  type: 'ir.actions.act_window',
  res_model: 'account.payment',
  search_view_id: 'view_search_account_payment',
  domain: [],
  context: {
    default_is_internal_transfer: true,
    default_payment_type: 'outbound',
    search_default_transfers_filter: 1
  },
  views: {
    tree: 'view_tree_account_payment',
    form: 'view_form_account_payment'
  }
}

export default {
  view_form_account_payment,
  view_tree_account_payment,
  view_search_account_payment,

  action_account_payment_customer_inbound,
  action_account_payment_supplier_outbound,
  action_account_payment_transfer
}
