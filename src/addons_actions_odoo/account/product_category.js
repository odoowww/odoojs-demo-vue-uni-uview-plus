const view_form_product_category = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_form_product_category',
  model: 'product.category',
  type: 'form',
  arch: {
    sheet: {
      group_first: {},
      group_account_property: {
        attr: { name: 'account_property' },
        group_account_property: {
          attr: {
            string: 'Account Properties'
            // groups: 'account.group_account_readonly'
          },
          field_property_account_income_categ_id: {},
          field_property_account_expense_categ_id: {}
        }
      }
    }
  }
}

const view_tree_product_category = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_tree_product_category'
}

const view_search_product_category = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_search_product_category'
}

const action_product_category = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品类别(开票设置)',
  type: 'ir.actions.act_window',
  res_model: 'product.category',
  search_view_id: 'view_search_product_category',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_product_category',
    form: 'view_form_product_category'
  }
}

export default {
  view_form_product_category,
  view_tree_product_category,
  view_search_product_category,
  action_product_category
}
