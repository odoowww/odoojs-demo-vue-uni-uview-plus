const view_form_account_account_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account.tag',
  type: 'form',
  arch: {
    sheet: {
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      group_name: {
        field_active: { invisible: '1' },
        field_name: {},
        field_applicability: {},
        field_tax_negate: {
          readonly: '1',
          // 'invisible': [('applicability', '!=', 'taxes')]
          invisible: [['applicability', '!=', 'taxes']]
        },
        field_country_id: {
          // 'invisible': [('applicability', '!=', 'taxes')]
          invisible: [['applicability', '!=', 'taxes']]
        }
      }
    }
  }
}

const view_tree_account_account_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account.tag',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_applicability: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_applicability: {} }
    }
  }
}

const view_search_account_account_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'account.account.tag',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},

      filter_active: {
        archived: {
          name: 'archived',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_account_account_tag = {
  _odoo_model: 'ir.actions.act_window',
  name: '科目标签',
  type: 'ir.actions.act_window',
  res_model: 'account.account.tag',
  search_view_id: 'view_search_account_account_tag',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_account_account_tag',
    form: 'view_form_account_account_tag'
  }
}

export default {
  view_form_account_account_tag,
  view_tree_account_account_tag,
  view_search_account_account_tag,
  action_account_account_tag
}
