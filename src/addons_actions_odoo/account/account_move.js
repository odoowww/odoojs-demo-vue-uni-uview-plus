const line_ids_form_sheet = {
  group_name: {
    field_account_id: {
      // domain: [
      //   ['company_id', '=', { field_parent_company_id: {} }],
      //   ['deprecated', '=', false]
      // ]
    },
    field_partner_id: {
      // readonly: [['move_type', '!=', 'entry']],
      // domain: ['|', ['parent_id', '=', false], ['is_company', '=', true]]
    },
    field_name: {},
    field_analytic_distribution: { widget: 'analytic_distribution' },
    field_amount_currency: {
      groups: 'base.group_multi_currency',
      readonly: '1'
    },
    field_company_currency_id: { invisible: 1 },
    field_company_id: { invisible: 1 },
    field_currency_id: {},
    field_debit: {},
    field_credit: {},
    field_balance: { invisible: 1 },
    field_date_maturity: {
      required: 0,
      invisible: { context_view_no_maturity: false }
    }
  }
}

const view_move_form_sheet = {
  field_company_id: { invisible: 1 },
  field_journal_id: { invisible: 1 },
  field_show_name_warning: { invisible: 1 },
  field_posted_before: { invisible: 1 },
  field_move_type: { invisible: 1 },
  field_payment_state: { invisible: 1 },
  field_invoice_filter_type_domain: { invisible: 1 },
  field_suitable_journal_ids: { invisible: 1 },
  field_currency_id: { invisible: 1 },
  field_company_currency_id: { invisible: 1 },
  field_commercial_partner_id: { invisible: 1 },
  field_bank_partner_id: { invisible: 1 },
  field_display_qr_code: { invisible: 1 },
  field_show_reset_to_draft_button: { invisible: 1 },

  field_invoice_has_outstanding: { invisible: 1 },
  field_is_move_sent: { invisible: 1 },
  field_has_reconciled_entries: { invisible: 1 },
  field_restrict_mode_hash_table: { invisible: 1 },
  field_country_code: { invisible: 1 },
  field_display_inactive_currency_warning: { invisible: 1 },
  field_statement_line_id: { invisible: 1 },
  field_payment_id: { invisible: 1 },
  field_tax_country_id: { invisible: 1 },
  field_tax_cash_basis_created_move_ids: { invisible: 1 },
  field_quick_edit_mode: { invisible: 1 },
  field_hide_post_button: { invisible: 1 },
  field_duplicated_ref_ids: { invisible: 1 },
  field_quick_encoding_vals: { invisible: 1 },

  group_title: {
    group: {
      // widget_move_type: {
      //   field_move_type: { invisible: 1 },
      //   field_payment_id: {
      //     invisible: 1,
      //     fields: { partner_type: {}, payment_type: {} }
      //   }

      //   // 销售或采购: move_type != entry
      //   // next:     move_type=entry,
      //   // 手工凭证: payment_id=false
      //   // next:    payment_id=true,
      //   // 内部转账:  payment_id.is_internal_transfer=true
      //   // next:     payment_id.is_internal_transfer=false
      //   // 客户收款:  payment_id.partner_type=customer, payment_id.payment_type=inbound
      //   // 客户退款:  payment_id.partner_type=customer, payment_id.payment_type=outbound
      //   // 供应商付款:  payment_id.partner_type=supplier, payment_id.payment_type=outbound
      //   // 供应商退款:  payment_id.partner_type=supplier, payment_id.payment_type=inbound
      //   //
      // },

      field_name: {}
    }
  },

  group: {
    group_header_left_group: {
      field_ref__in: {
        invisible: [
          ['move_type', 'not in', ['in_invoice', 'in_receipt', 'in_refund']]
        ],
        string: '账单号码'
      },

      field_ref: {
        invisible: [
          [
            'move_type',
            'in',
            [
              'in_invoice',
              'in_receipt',
              'in_refund',
              'out_invoice',
              'out_refund'
            ]
          ]
        ],
        string: '业务单'
      }
    },

    group_header_right_group: {
      attr: { id: 'header_right_group' },

      field_date: {
        string: '账期',

        invisible: [
          ['move_type', 'in', ['out_invoice', 'out_refund', 'out_receipt']],
          ['quick_edit_mode', '=', false]
        ]
      },
      field_journal_id: {}
    }
  },

  notebook: {
    page_aml_tab: {
      attr: {
        id: 'aml_tab',
        string: '分录',
        name: 'aml_tab'
        // groups: 'account.group_account_readonly'
      },
      field_line_ids: {
        views: {
          tree: {
            arch: {
              sheet: {
                field_account_id: { fields: { account_type: {} } },

                field_payment_id: { fields: { journal_id: {} } },
                field_journal_id: {},
                field_partner_id: {},
                field_name: {},
                field_date_maturity: {},
                field_debit: {},
                field_credit: {},
                field_balance: {}
              },
              kanban: {
                card_title: { field_account_id: {} },
                card_label: {
                  widget_account_move_line_label: {
                    field_partner_id: {},
                    field_payment_id: {}
                  }
                },
                card_value: {
                  widget_account_move_line_value: {
                    field_debit: {},
                    field_credit: {}
                  }
                }
              }
            }
          },
          form: { arch: { sheet: { ...line_ids_form_sheet } } }
        }
      }
    },
    page_other_tab_entry: {
      attr: {
        id: 'other_tab_entry',
        string: '其他信息',
        name: 'other_info_2'
      },
      group_other_tab_entry_group: {
        attr: { id: 'other_tab_entry_group' },
        group_misc_group: {
          attr: { name: 'misc_group' },
          field_auto_post: {},
          field_reversed_entry_id: {},
          field_auto_post_until: {},
          field_to_check: {}
        },

        group_other_tab_entry_group__2: {
          field_fiscal_position_id: {},
          field_company_id: {
            required: '1',
            groups: 'base.group_multi_company'
          }
        }
      },
      field_narration: {
        nolabel: 1,
        placeholder: 'Add an internal note...'
      }
    }
  }
}

const view_form_account_move = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'form',
  toolbar: {
    action: {
      // 在数据库中 找到 所有绑定到该模型的 action
      // select * from ir_actions where binding_model_id = ?
      // model_account_move
      //
      //
      // action_invoice_order_generate_link
    },
    print: {
      // odoo 原生是 report kanban
      // 需要 前端自定义
    }
  },
  arch: {
    alert_div: {},

    button_box: {
      button_action_open_business_doc: {
        invisible: 1,
        string: '1 Payment',
        name: 'action_open_business_doc',
        type: 'object'

        // 'invisible': ['|', '|',
        // ('move_type', '!=', 'entry'),
        // ('id', '=', False), ('payment_id', '=', False)]}"
      },

      button_open_reconcile_view: {
        invisible: 1,
        string: 'Reconciled Items',
        name: 'open_reconcile_view',
        icon: 'fa-bars',
        type: 'object'

        // 'invisible': ['|', '|', ('move_type', '!=', 'entry'),
        // ('id', '=', False), ('has_reconciled_entries', '=', False)]
      },

      button_open_created_caba_entries: {
        invisible: 1,
        string: 'Cash Basis Entries',
        name: 'open_created_caba_entries',
        icon: 'fa-usd',
        type: 'object'

        // 'invisible': [('tax_cash_basis_created_move_ids', '=', [])]
      }
    },
    header: {},
    sheet: { ...view_move_form_sheet }
  }
}

const view_tree_account_move = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'tree',
  arch: {
    sheet: {
      field_made_sequence_hole: { invisible: 1 },
      field_date: {},
      field_name: {},
      field_partner_id: { optional: 'show' },
      field_ref: { optional: 'show' },
      field_journal_id: {},
      field_company_id: { optional: 'show' },
      field_amount_total_signed: { string: 'Total' },
      field_state: {
        widget: 'badge'
        // decoration-info="state == 'draft'" decoration-success="state == 'posted'"
      },
      field_currency_id: { invisible: 1 },
      field_to_check: { widget: 'boolean_toggle', optional: 'hide' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {
        field_partner_id: {}
        // field_journal_id: {},
        // field_payment_id: {}
      },
      card_value: { field_state: {}, field_date: {} }
    }
  }
}

const view_search_account_move = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'search',
  arch: {
    sheet: {
      field_display_name: {
        string: '名称',
        filter_domain: [
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['ref', 'ilike', { self: {} }],
          ['partner_id', 'ilike', { self: {} }]
        ]
      },
      field_name: {},
      field_ref: {},
      // date: {},
      field_partner_id: {},
      // field_journal_id: {},

      filter_state: {
        string: 'State',
        unposted: {
          name: 'unposted',
          string: '未过账',
          domain: [['state', '=', 'draft']]
        },
        posted: {
          name: 'posted',
          string: '已过账',
          domain: [['state', '=', 'posted']]
        }
      },

      // filter_payment: {
      //   reversed: {
      //     name: 'reversed',
      //     string: 'Reversed',
      //     domain: [['payment_state', '=', 'reversed']]
      //   }
      // },
      // filter_to_check: {
      //   to_check: {
      //     name: 'to_check',
      //     string: 'To Check',
      //     domain: [['to_check', '=', true]]
      //   }
      // },

      filter_move_type: {
        sales: {
          name: 'move_out',
          string: '销售',
          domain: [
            ['move_type', 'in', ['out_invoice', 'out_refund', 'out_receipt']]
          ]
        },

        purchases: {
          name: 'move_in',
          string: '采购',
          domain: [
            ['move_type', 'in', ['in_invoice', 'in_refund', 'in_receipt']]
          ]
        },

        other: {
          name: 'other',
          string: '手工',
          domain: ['&', ['move_type', '=', 'entry'], ['payment_id', '=', false]]
        },

        payment_internal: {
          name: 'payment_internal',
          string: '内部转账',
          domain: [
            '&',
            ['move_type', '=', 'entry'],
            ['payment_id.is_internal_transfer', '=', true]
          ]
        },

        payment_customer: {
          name: 'payment_customer',
          string: '收款',
          domain: [
            '&',
            ['move_type', '=', 'entry'],
            '&',
            ['payment_id.is_internal_transfer', '=', false],
            ['payment_id.partner_type', '=', 'customer']
          ]
        },

        payment_supplier: {
          name: 'payment_supplier',
          string: '付款',
          domain: [
            '&',
            ['move_type', '=', 'entry'],
            '&',
            ['payment_id.is_internal_transfer', '=', false],
            ['payment_id.partner_type', '=', 'supplier']
          ]
        },

        other: {
          name: 'other',
          string: '手工',
          domain: ['&', ['move_type', '=', 'entry'], ['payment_id', '=', false]]
        }
      },

      // filter_type: {
      //   sales: {
      //     name: 'sales',
      //     string: '销售',
      //     domain: [['journal_id.type', '=', 'sale']],
      //     context: { default_journal_type: 'sale' }
      //   },

      //   purchases: {
      //     name: 'purchases',
      //     string: '采购',
      //     domain: [['journal_id.type', '=', 'purchase']],
      //     context: { default_journal_type: 'purchase' }
      //   },

      //   bankoperations: {
      //     name: 'bankoperations',
      //     string: '银行',
      //     domain: [['journal_id.type', '=', 'bank']],
      //     context: { default_journal_type: 'bank' }
      //   },

      //   cashoperations: {
      //     name: 'cashoperations',
      //     string: '现金',
      //     domain: [['journal_id.type', '=', 'cash']],
      //     context: { default_journal_type: 'cash' }
      //   },

      //   misc_filter: {
      //     name: 'misc_filter',
      //     string: '其他',
      //     domain: [['journal_id.type', '=', 'general']],
      //     context: { default_journal_type: 'general' }
      //   }
      // },

      filter_date: {
        date: { name: 'date', string: 'Date', date: 'date' }
      }
    }
  }
}

const action_account_move = {
  _odoo_model: 'ir.actions.act_window',
  name: '凭证(所有)',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: false, delete: false },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move',
  domain: [['journal_id.code', 'not in', ['OPEN', 'RPTM']]],
  context: {},
  views: {
    tree: 'view_tree_account_move',
    form: 'view_form_account_move'
  }
}

const actions_by_move_type = {
  action_account_move_payment_customer_inbound: {
    _odoo_model: 'ir.actions.act_window',
    name: '凭证(客户收款单)',
    type: 'ir.actions.act_window',
    buttons: { create: false, edit: false, delete: false },
    res_model: 'account.move',
    search_view_id: 'view_search_account_move',
    domain: [
      '&',
      ['move_type', '=', 'entry'],
      '&',
      ['payment_id.partner_type', '=', 'customer'],
      ['payment_id.payment_type', '=', 'inbound']
    ],
    context: {},
    views: {
      tree: 'view_tree_account_move',
      form: 'view_form_account_move'
    }
  },

  action_account_move_payment_supplier_outbound: {
    _odoo_model: 'ir.actions.act_window',
    name: '凭证(供应商付款单)',
    type: 'ir.actions.act_window',
    buttons: { create: false, edit: false, delete: false },
    res_model: 'account.move',
    search_view_id: 'view_search_account_move',
    domain: [
      '&',
      ['move_type', '=', 'entry'],
      '&',
      ['payment_id.partner_type', '=', 'supplier'],
      ['payment_id.payment_type', '=', 'outbound']
    ],
    context: {},
    views: {
      tree: 'view_tree_account_move',
      form: 'view_form_account_move'
    }
  },

  action_account_move_out: {
    _odoo_model: 'ir.actions.act_window',
    name: '凭证(销售结算单)',
    type: 'ir.actions.act_window',
    buttons: { create: false, edit: false, delete: false },
    res_model: 'account.move',
    search_view_id: 'view_search_account_move',
    domain: [['move_type', 'in', ['out_invoice', 'out_refund', 'out_receipt']]],
    context: {},
    views: {
      tree: 'view_tree_account_move',
      form: 'view_form_account_move'
    }
  },

  action_account_move_in: {
    _odoo_model: 'ir.actions.act_window',
    name: '凭证(采购账单)',
    type: 'ir.actions.act_window',
    buttons: { create: false, edit: false, delete: false },
    res_model: 'account.move',
    search_view_id: 'view_search_account_move',
    domain: [['move_type', 'in', ['in_invoice', 'in_refund', 'in_receipt']]],
    context: {},
    views: {
      tree: 'view_tree_account_move',
      form: 'view_form_account_move'
    }
  }
}

export default {
  view_form_account_move,
  view_tree_account_move,
  view_search_account_move,
  action_account_move
}
