const view_form_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'base.view_form_res_partner',
  model: 'res.partner',
  type: 'form',
  title: { field_name: {} },
  arch: {
    sheet: {
      group_slot: {},
      field_customer_rank: {},
      field_supplier_rank: {},

      notebook: {
        page_contact_addresses: {},
        page_sales_purchases: {
          attr: { name: 'sales_purchases', string: '销售和采购' },
          group_container_row_2: {
            attr: { name: 'container_row_2' },
            group_sale: {
              attr: {
                name: 'sale',
                string: 'Sales'
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly'
              },

              field_property_payment_term_id: {
                // string: '支付条款',
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly'
              }
            },
            group_purchase: {
              attr: {
                name: 'purchase',
                string: 'Purchase'
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly'
              },
              field_property_supplier_payment_term_id: {
                // string: '支付条款',
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly'
              }
            },
            group_fiscal_information: {
              attr: {
                name: 'fiscal_information',
                string: 'Fiscal Information'
              },
              field_property_account_position_id: {}
            },
            group_misc: {
              attr: { name: 'misc', string: 'Misc' },
              field_company_registry: {
                // 'invisible': [('parent_id','!=',False)]
                invisible: [['parent_id', '!=', false]]
              },
              field_ref: { string: 'Reference' },
              field_company_id: {
                groups: 'base.group_multi_company',
                force_save: '1',

                // 'readonly': [('parent_id','!=',False)]
                readonly: [['parent_id', '!=', false]]
              },
              field_industry_id: {
                // 'invisible': [('is_company','=',False)]
                invisible: [['is_company', '=', false]]
              }
            }
          }
        },
        page_accounting: {
          attr: {
            name: 'accounting',
            string: '会计',

            // 'invisible': [('is_company','=',False),('parent_id','!=',False)],
            invisible: [
              ['is_company', '=', false],
              ['parent_id', '!=', false]
            ]

            // groups:
            //   'account.group_account_invoice,account.group_account_readonly'
          },

          field_duplicated_bank_account_partners_count: { invisible: 1 },
          field_show_credit_limit: { invisible: 1 },

          group: {
            group: {
              attr: {
                name: 'banks',
                string: '银行账户'
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly'
              },

              // label: { string: '' },
              field_bank_ids: {
                nolabel: '1',
                context: { default_allow_out_payment: true },
                views: {
                  tree: {
                    arch: {
                      sheet: {
                        field_sequence: { widget: 'handle' },
                        field_bank_id: {},
                        field_acc_number: {},
                        field_allow_out_payment: { widget: 'boolean_toggle' },
                        field_acc_holder_name: { invisible: '1' }
                      },
                      kanban: {
                        card_title: { field_bank_id: {} },
                        card_label: {},
                        card_value: { field_acc_number: {} }
                      }
                    }
                  },
                  form: {
                    arch: {
                      sheet: {
                        field_sequence: { widget: 'handle' },
                        field_bank_id: {},
                        field_acc_number: {},
                        field_allow_out_payment: { widget: 'boolean_toggle' },
                        field_acc_holder_name: { invisible: '1' }
                      }
                    }
                  }
                }
              }

              // todo: button_base.action_res_partner_bank_account_form
            },

            group_accounting_entries: {
              attr: {
                string: 'Accounting Entries',
                name: 'accounting_entries'
                // groups: 'account.group_account_readonly'
              },
              field_currency_id: { invisible: '1' },
              field_property_account_receivable_id: {},
              field_property_account_payable_id: {}
            },
            group_credit_limits: {
              attr: {
                string: 'Credit Limits',
                name: 'credit_limits',
                // groups:
                //   'account.group_account_invoice,account.group_account_readonly',

                // 'invisible': [('show_credit_limit', '=', False)]
                invisible: [['show_credit_limit', '=', false]]
              },
              field_credit: {},
              field_use_partner_credit_limit: {},
              field_credit_limit: {
                // 'invisible': [('use_partner_credit_limit', '=', False)]
                invisible: [['use_partner_credit_limit', '=', false]]
              }
            }
          }
        },
        page_accounting_disabled: {
          attr: {
            name: 'accounting_disabled',
            string: '会计',

            // 'invisible': ['|',('is_company','=',True),('parent_id','=',False)]
            invisible: [
              '|',
              ['is_company', '=', true],
              ['parent_id', '=', false]
            ]

            // groups:
            //   'account.group_account_invoice,account.group_account_readonly'
          }
        },
        page_internal_notes: {
          group_slot: {},
          group_invoice: {
            attr: {
              // groups:
              //   'account.group_account_invoice,account.group_account_readonly'
            },
            group_invoice2: {
              attr: {
                groups: 'account.group_warning_account'
              },
              widget_separator: 'Warning on the Invoice',
              field_invoice_warn: { required: '1' },
              field_invoice_warn_msg: {
                nolabel: '1',

                // 'invisible':[('invoice_warn','in',(False,'no-message'))]
                invisible: [['invoice_warn', 'in', [false, 'no-message']]],
                // 'required':
                // [('invoice_warn','!=', False),
                // ('invoice_warn','!=','no-message')],

                required: [
                  ['invoice_warn', '!=', false],
                  ['invoice_warn', '!=', 'no-message']
                ],
                placeholder: 'Type a message...'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'base.view_tree_res_partner'
}

const view_search_res_partner = {
  _odoo_model: 'ir.ui.view',
  model: 'res.partner',
  inherit_id: 'base.view_search_res_partner',
  type: 'search',
  arch: {
    sheet: {
      filter_sell_purchase: {
        customer: {
          string: '客户',
          domain: [['customer_rank', '>', 0]]
        },
        supplier: {
          string: '供应商',
          domain: [['supplier_rank', '>', 0]]
        }
      }
    }
  }
}

const action_res_partner = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人(开票设置)',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'res.partner',
  search_view_id: 'view_search_res_partner',
  domain: [],
  context: {
    default_is_company: true,
    default_type: 'contact',
    search_default_type_company: 1,
    search_default_type_person: 1
  },
  views: {
    tree: 'view_tree_res_partner',
    form: 'view_form_res_partner'
  }
}

export default {
  view_form_res_partner,
  view_tree_res_partner,
  view_search_res_partner,
  action_res_partner
}
