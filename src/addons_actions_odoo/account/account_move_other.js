const line_ids_form_sheet = {
  group_name: {
    field_account_id: {
      // domain="[('company_id', 'parent_of', parent.company_id), ('deprecated', '=', False)]"
      //        "[('company_id', '=',         parent.company_id), ('deprecated', '=', False)]
      domain: [
        ['company_id', 'parent_of', { field_parent_company_id: {} }],
        ['deprecated', '=', false]
      ]
    },
    field_partner_id: {
      // 'readonly':[('move_type', '!=', 'entry')]
      // readonly: [['move_type', '!=', 'entry']]
      // domain="['|', ('parent_id', '=', False), ('is_company', '=', True)]
      // domain: ['|', ['parent_id', '=', false], ['is_company', '=', true]]
    },
    field_name: {},
    field_analytic_distribution: { widget: 'analytic_distribution' },
    field_amount_currency: {
      groups: 'base.group_multi_currency',
      readonly: '1'
    },
    field_company_currency_id: { invisible: 1 },
    field_company_id: { invisible: 1 },
    field_currency_id: {},
    field_debit: {},
    field_credit: {},
    field_balance: { invisible: 1 },
    field_tax_ids: {
      invisible: 1,
      string: 'Taxes Applied',
      widget: 'autosave_many2many_tags',

      //   'readonly': [
      //   '|', '|',
      //   ('display_type', 'in', ('line_section', 'line_note')),
      //   ('tax_line_id', '!=', False),
      //   '&amp;',
      //   ('parent.move_type', 'in', (
      // 'out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'
      //   )),
      //   ('account_type', 'in', ('asset_receivable', 'liability_payable')),
      //   ]}"

      readonly: [
        '|',
        '|',
        ['display_type', 'in', ['line_section', 'line_note']],
        ['tax_line_id', '!=', false],
        '&',
        [
          'parent.move_type',
          'in',
          [
            'out_invoice',
            'out_refund',
            'in_invoice',
            'in_refund',
            'out_receipt',
            'in_receipt'
          ]
        ],
        ['account_type', 'in', ['asset_receivable', 'liability_payable']]
      ]

      // domain="[('type_tax_use', '=?', parent.invoice_filter_type_domain)]"
      // domain: [
      //   ['type_tax_use', '=?', { field_parent_invoice_filter_type_domain: {} }]
      // ]
    },
    field_date_maturity: {
      required: 0,

      //invisible: context.get('view_no_maturity', False)
      invisible: { context_view_no_maturity: false }
    }
  }
}

const view_move_form_sheet = {
  field_company_id: { invisible: 1 },
  field_journal_id: { invisible: 1 },
  field_show_name_warning: { invisible: 1 },
  field_posted_before: { invisible: 1 },
  field_move_type: { invisible: 1 },
  field_payment_state: { invisible: 1 },
  field_invoice_filter_type_domain: { invisible: 1 },
  field_suitable_journal_ids: { invisible: 1 },
  field_currency_id: { invisible: 1 },
  field_company_currency_id: { invisible: 1 },
  field_commercial_partner_id: { invisible: 1 },
  field_bank_partner_id: { invisible: 1 },
  field_display_qr_code: { invisible: 1 },
  field_show_reset_to_draft_button: { invisible: 1 },

  field_invoice_has_outstanding: { invisible: 1 },
  field_is_move_sent: { invisible: 1 },
  field_has_reconciled_entries: { invisible: 1 },
  field_restrict_mode_hash_table: { invisible: 1 },
  field_country_code: { invisible: 1 },
  field_display_inactive_currency_warning: { invisible: 1 },
  field_statement_line_id: { invisible: 1 },
  field_payment_id: { invisible: 1 },
  field_tax_country_id: { invisible: 1 },
  field_tax_cash_basis_created_move_ids: { invisible: 1 },
  field_quick_edit_mode: { invisible: 1 },
  field_hide_post_button: { invisible: 1 },
  field_duplicated_ref_ids: { invisible: 1 },
  field_quick_encoding_vals: { invisible: 1 },

  group_title: {
    group: {
      field_move_type: { invisible: 1 },
      field_name: {
        placeholder: 'Draft',

        invisible: [
          ['name', '=', '/'],
          ['posted_before', '=', false],
          ['quick_edit_mode', '=', false]
        ],
        //   'readonly': [('state', '!=', 'draft')]
        readonly: [['state', '!=', 'draft']]
      },
      group_noname: {
        attr: {
          // 'invisible': ['|', '|', ('state', '!=', 'draft'),
          // ('name', '!=', '/'), ('quick_edit_mode', '=', True)]
          invisible: [
            '|',
            '|',
            ['state', '!=', 'draft'],
            ['name', '!=', '/'],
            ['quick_edit_mode', '=', true]
          ]
        },
        widget_text: 'Draft'
      }
    }
  },

  group: {
    group_header_left_group: {
      field_partner_id: { invisible: 1 },
      field_ref: {}
    },
    group_header_right_group: {
      attr: { id: 'header_right_group' },

      field_date: {
        string: '账期',
        readonly: [['state', '!=', 'draft']]
      },

      group_journal_div: {
        attr: {
          invisible: 1
          // groups: 'account.group_account_readonly',
          // invisible="
          // context.get('default_journal_id') and
          // context.get('move_type', 'entry') != 'entry'"
          // invisible: [
          //   '&',
          //   { context_default_journal_id: false },
          //   [{ context_move_type: 'entry' }, '!=', 'entry']
          // ]
        },
        field_journal_id: {
          // readonly: { editable: false },

          // 'readonly': [('posted_before', '=', True)]
          readonly: [['posted_before', '=', true]]
        },
        // widget_text__span_journal_id: {
        //   attr: {
        //     groups: 'base.group_multi_currency',
        //     text: 'in',

        //       // 'invisible': [('move_type', '=', 'entry')]
        //       invisible: [['move_type', '=', 'entry']]

        //   }
        // },

        field_currency_id: {
          groups: 'base.group_multi_currency',

          // 'invisible': [('move_type', '=', 'entry')]
          invisible: [['move_type', '=', 'entry']],
          // 'readonly': [('state', '!=', 'draft')]
          readonly: [['state', '!=', 'draft']]
        }
      },
      field_currency_id: {
        invisible: 1,
        // groups: '!account.group_account_readonly,base.group_multi_currency',

        // 'readonly': [('state', '!=', 'draft')]
        readonly: [['state', '!=', 'draft']]
      }
    }
  },

  notebook: {
    page_aml_tab: {
      attr: {
        id: 'aml_tab',
        string: '会计分录',
        name: 'aml_tab'
        // groups: 'account.group_account_readonly'
      },
      field_line_ids: {
        // context="{
        //     'default_move_type': context.get('default_move_type'),
        //     'line_ids': line_ids,
        //     'journal_id': journal_id,
        //     'default_partner_id': commercial_partner_id,
        //     'default_currency_id': currency_id or company_currency_id,
        //     'kanban_view_ref': 'account.account_move_line_view_kanban_mobile',
        // }"

        context: {
          default_move_type: { context_default_move_type: null },
          line_ids: { field_line_ids: {} },
          journal_id: { field_journal_id: {} },
          default_partner_id: { field_commercial_partner_id: {} }
          // default_currency_id: {
          //   logic: [
          //     '|',
          //     { field_currency_id: {} },
          //     { field_company_currency_id: {} }
          //   ]
          // }

          // kanban_view_ref: 'account.account_move_line_view_kanban_mobile'
        },

        views: {
          tree: {
            arch: {
              sheet: {
                field_account_id: {
                  // 'invisible': [('display_type', 'in', ('line_section', 'line_note'))],
                  invisible: [
                    ['display_type', 'in', ['line_section', 'line_note']]
                  ],
                  fields: { account_type: {} }
                },
                field_partner_id: { optional: 'show' },
                field_name: {
                  optional: 'show',
                  widget: 'section_and_note_text'
                },
                // field_analytic_distribution: {
                //   widget: 'analytic_distribution',
                //   optional: 'show'
                // },
                field_date_maturity: {
                  optional: 'hide'

                  // invisible="context.get('view_no_maturity')"
                  //  'invisible':
                  // [('display_type', 'in', ('line_section', 'line_note'))]
                },
                // field_amount_currency: {
                //   groups: 'base.group_multi_currency',
                //   readonly: '1',
                //   optional: 'hide'
                // },
                // field_currency_id: {
                //   optional: 'hide',

                // },
                // field_tax_ids: {
                //   widget: 'autosave_many2many_tags',
                //   optional: 'hide'
                // },
                field_debit: {
                  // 'invisible': [('display_type', 'in', ('line_section', 'line_note'))],
                  invisible: [
                    ['display_type', 'in', ['line_section', 'line_note']]
                  ]
                },
                field_credit: {
                  // 'invisible': [('display_type', 'in', ('line_section', 'line_note'))],
                  invisible: [
                    ['display_type', 'in', ['line_section', 'line_note']]
                  ]
                },
                field_balance: { invisible: '1' },
                // field_discount_date: {
                //   optional: 'hide',
                //   string: 'Discount Date'
                // },
                // field_discount_amount_currency: {
                //   optional: 'hide',
                //   string: 'Discount Amount'
                // },
                // field_tax_tag_ids: {
                //   widget: 'many2many_tags',
                //   optional: 'show',
                //   string: 'Tax Grids'

                //   // domain="[
                //   //     ('applicability', '=', 'taxes'),
                //   //     '|', ('country_id', '=', parent.tax_country_id),
                //   //     ('country_id', '=', False),
                //   // ]"

                //   // domain: [
                //   //   ['applicability', '=', 'taxes'],
                //   //   '|',
                //   //   [
                //   //     'country_id',
                //   //     '=',
                //   //     { field_parent_tax_country_id: {} }
                //   //   ],
                //   //   ['company_id', '=', false]
                //   // ]
                // },
                // field_tax_tag_invert: {
                //   optional: 'hide',
                //   readonly: '1',
                //   groups: 'base.group_no_one'
                // },
                // <button name="action_automatic_entry"
                // type="object"
                // icon="fa-calendar"
                // string="Cut-Off"
                // aria-label="Change Period"
                // class="float-end"
                // attrs="{
                // 'invisible': [('account_internal_group', 'not in', ('income', 'expense'))],
                // 'column_invisible': ['|', ('parent.move_type', '=', 'entry'), ('parent.state', '!=', 'posted')]}"
                // context="{'hide_automatic_options': 1, 'default_action': 'change_period'}"/>
                // field_tax_line_id: { invisible: '1' },
                // field_company_currency_id: { invisible: '1' },
                field_display_type: { invisible: '1', force_save: '1' },
                field_company_id: { invisible: '1' },
                field_sequence: { invisible: '1' },
                field_account_internal_group: { invisible: '1' },
                field_account_type: { invisible: '1' }
              },
              kanban: {
                card_title: { field_account_id: {} },
                card_label: { field_name: {} },
                card_value: {
                  widget_account_move_line_value: {
                    field_debit: {},
                    field_credit: {}
                  }
                }
              }
            }
          },
          form: { arch: { sheet: { ...line_ids_form_sheet } } }
        }
      },

      widget_text__alert: {
        attr: {
          invisible: 1,

          // invisible: [
          //   '|',
          //   ['payment_state', '!=', 'invoicing_legacy'],
          //   ['move_type', '=', 'entry']
          // ],

          text: 'This entry has been generated through the Invoicing app, before installing Accounting. Its balance has been imported separately.'
        }
      }
    },

    page_other_tab_entry: {
      attr: {
        id: 'other_tab_entry',
        string: '其他信息',
        name: 'other_info_2'
      },
      group_other_tab_entry_group: {
        attr: { id: 'other_tab_entry_group' },
        group_misc_group: {
          attr: { name: 'misc_group' },
          field_auto_post: {
            readonly: [['state', '!=', 'draft']]
          },
          field_reversed_entry_id: {},
          field_auto_post_until: {
            readonly: [['state', '!=', 'draft']]
          },
          field_to_check: {}
        },

        group_other_tab_entry_group__2: {
          field_fiscal_position_id: {},
          field_company_id: {
            required: '1',
            groups: 'base.group_multi_company'
          }
        }
      },
      field_narration: {
        nolabel: 1,
        placeholder: 'Add an internal note...'
      }
    }
  }
}

const view_form_account_move_other = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'form',
  toolbar: {
    action: {},
    print: {}
  },

  arch: {
    alert_div: {},
    button_box: {},
    header: {
      button_action_post: {
        name: 'action_post',
        string: 'Post',
        type: 'object',
        // groups: 'account.group_account_invoice',
        btn_type: 'primary',
        context: { validate_analytic: true },

        // 'invisible':  ['|', ('hide_post_button', '=', True), ('move_type', '!=', 'entry')]
        invisible: [
          '|',
          ['hide_post_button', '=', true],
          ['move_type', '!=', 'entry']
        ]
      },

      button_action_view_account_move_reversal: {
        name: 'action_view_account_move_reversal',
        string: 'Reverse Entry',
        type: 'action',
        // groups: 'account.group_account_invoice',
        invisible: 1

        // 'invisible': ['|', ('move_type', '!=', 'entry'), '|', ('state', '!=', 'posted'), ('payment_state', '=', 'reversed')]
        // invisible: [
        //   '|',
        //   ['move_type', '!=', 'entry'],
        //   '|',
        //   ['state', '!=', 'posted'],
        //   ['payment_state', '=', 'reversed']
        // ]
      },

      button_cancel: {
        name: 'button_cancel',
        string: 'Cancel Entry',
        type: 'object',
        // groups: 'account.group_account_invoice',

        // 'invisible' : ['|', '|', ('id', '=', False),  ('state', '!=', 'draft'),('move_type', '!=', 'entry')]
        invisible: [
          '|',
          '|',
          ['id', '=', false],
          ['state', '!=', 'draft'],
          ['move_type', '!=', 'entry']
        ]
      },

      button_draft: {
        name: 'button_draft',
        string: '重置为草稿',
        type: 'object',
        // groups: 'account.group_account_invoice',

        //  'invisible' : [('show_reset_to_draft_button', '=', False)]
        invisible: [['show_reset_to_draft_button', '=', false]]
      },

      //   button_set_checked: {
      //     name: 'button_set_checked',
      //     string: 'Set as Checked',
      //     type: 'object',
      //     groups: 'account.group_account_invoice',
      //     attrs: {
      //       invisible({ record }) {
      //         // attrs="{'invisible' : [('to_check', '=', False)]}" data-hotkey="k" />
      //         const { to_check } = record
      //         return !to_check
      //       }
      //     }
      //   }

      field_state: { widget: 'statusbar', statusbar_visible: 'draft,posted' }
    },
    sheet: { ...view_move_form_sheet }
  }
}

const view_tree_account_move_other = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'tree',
  arch: {
    sheet: {
      field_made_sequence_hole: { invisible: 1 },
      field_date: {},
      field_name: {},
      // field_partner_id: { optional: 'show' },
      field_ref: { optional: 'show' },
      field_journal_id: {},
      field_company_id: { optional: 'show' },
      field_amount_total_signed: { string: 'Total' },
      field_state: {
        widget: 'badge'
        // decoration-info="state == 'draft'" decoration-success="state == 'posted'"
      },
      field_currency_id: { invisible: 1 },
      field_to_check: { widget: 'boolean_toggle', optional: 'hide' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {
        field_date: {},
        field_ref: {}
        // field_partner_id: {}
        // field_journal_id: {},
        // field_payment_id: {}
      },
      card_value: {
        field_state: {},
        field_amount_total_signed: { string: 'Total' }
      }
    }
  }
}

const view_search_account_move_other = {
  _odoo_model: 'ir.ui.view',
  model: 'account.move',
  type: 'search',
  arch: {
    sheet: {
      field_display_name: {
        string: '名称',
        filter_domain: [
          '|',
          ['name', 'ilike', { self: {} }],
          ['ref', 'ilike', { self: {} }]
        ]
      },
      field_name: {},
      field_ref: {},
      // date: {},
      // field_partner_id: {},
      field_journal_id: {},

      filter_state: {
        string: 'State',
        unposted: {
          name: 'unposted',
          string: '未过账',
          domain: [['state', '=', 'draft']]
        },
        posted: {
          name: 'posted',
          string: '已过账',
          domain: [['state', '=', 'posted']]
        }
      },

      filter_date: {
        date: { name: 'date', string: 'Date', date: 'date' }
      }
    }
  }
}

const action_account_move_other = {
  _odoo_model: 'ir.actions.act_window',
  name: '手工凭证',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: true },
  res_model: 'account.move',
  search_view_id: 'view_search_account_move_other',
  domain: [
    '&',
    ['move_type', '=', 'entry'],
    '&',
    ['payment_id', '=', false],
    ['journal_id.code', 'not in', ['OPEN', 'RPTM']]
  ],
  context: {
    default_move_type: 'entry'
    // search_default_misc_filter: 1,
    // view_no_maturity: true
  },

  views: {
    tree: 'view_tree_account_move_other',
    form: 'view_form_account_move_other'
  }
}

export default {
  view_form_account_move_other,
  view_tree_account_move_other,
  view_search_account_move_other,
  action_account_move_other
}
