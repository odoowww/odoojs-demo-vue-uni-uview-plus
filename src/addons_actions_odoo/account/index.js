// import res_company from './res_company.js'
import res_partner from './res_partner.js'
import product_category from './product_category.js'
import product_template from './product_template.js'

import account_account_tag from './account_account_tag.js'
import account_account from './account_account.js'
import account_account_open from './account_account_open.js'

// import account_journal_group from './account_journal_group.js'
import account_journal from './account_journal.js'

// import account_move_line from './account_move_line.js'
import account_move from './account_move.js'

import account_move_other from './account_move_other.js'
import account_move_invoice from './account_move_invoice.js'
import account_payment from './account_payment.js'

// import account_analytic_account from './account_analytic_account.js'
// import account_analytic_distribution_model from './account_analytic_distribution_model.js'
// import account_analytic_line from './account_analytic_line.js'
// import account_analytic_plan from './account_analytic_plan.js'
// import account_bank_statement from './account_bank_statement.js'
// import account_cash_rounding from './account_cash_rounding.js'
// import account_fiscal_position_template from './account_fiscal_position_template.js'
// import account_fiscal_position from './account_fiscal_position.js'
// import account_full_reconcile from './account_full_reconcile.js'
// import account_group from './account_group.js'
// import account_incoterms from './account_incoterms.js'
// import account_payment_term from './account_payment_term.js'

// import account_tax_group from './account_tax_group.js'
// import account_tax_template from './account_tax_template.js'
// import account_tax from './account_tax.js'

export default {
  // ...res_partner,
  ...product_category,
  ...product_template,
  // ...res_company,

  ...account_account_tag,
  ...account_account,
  ...account_account_open,

  // ...account_journal_group,
  ...account_journal,
  ...account_move,

  ...account_move_other,
  ...account_move_invoice,
  // ...account_move_line,
  ...account_payment,

  // ...account_analytic_account,
  // ...account_analytic_distribution_model,
  // ...account_analytic_line,
  // ...account_analytic_plan,
  // ...account_bank_statement,
  // ...account_cash_rounding,
  // ...account_fiscal_position_template,
  // ...account_fiscal_position,
  // ...account_full_reconcile,
  // ...account_group,
  // ...account_incoterms,
  // ...account_payment_term,

  // ...account_tax_group,
  // ...account_tax_template,
  // ...account_tax,

  ...res_partner
}
