const view_form_product_template = {
  _odoo_model: 'ir.ui.view',
  model: 'product.template',
  type: 'form',
  arch: {
    button_box: {
      button_open_pricelist_rules: {
        invisible: 1,
        name: 'open_pricelist_rules',
        type: 'object',
        groups: 'product.group_product_pricelist',

        field_pricelist_item_count__many: {
          widget: 'statinfo',
          string: 'Extra Prices', //  '额外价格'
          // 'invisible': [('pricelist_item_count', '=', 1)]
          invisible: [['pricelist_item_count', '=', 1]]
        },

        field_pricelist_item_count__one: {
          widget: 'statinfo',
          string: 'Extra Price', //  '额外价格'
          //'invisible': [('pricelist_item_count', '!=', 1)]
          invisible: [['pricelist_item_count', '!=', 1]]
        }
      },

      button_product_product_variant_action: {
        // groups: 'product.group_product_variant',
        invisible: 1,
        name: 'product.product_variant_action',
        type: 'action',
        // 'invisible': [('product_variant_count', '&lt;=', 1)]
        // invisible: [['product_variant_count', '&lt;=', 1]],
        field_product_variant_count: { widget: 'statinfo', string: 'Variants' }
      }
    },

    header: {
      button_action_open_label_layout: {
        invisible: 1,
        name: 'action_open_label_layout',
        type: 'object',
        string: 'Print Labels'
        // 'invisible': [('detailed_type', '==', 'service')]
        // invisible: [['detailed_type', '==', 'service']]
      }
    },

    sheet: {
      field_product_variant_count: { invisible: 1 },
      field_is_product_variant: { invisible: 1 },
      field_attribute_line_ids: { invisible: '1' },
      field_type: { invisible: 1 },
      field_company_id: { invisible: 1 },
      field_active: { invisible: 1 },
      group: {
        group_left: {
          field_name: {
            // string: 'Product Name'
            // placeholder: 'e.g. Cheese Burger'
          },

          field_sale_ok: {},
          field_purchase_ok: {},
          field_priority: { invisible: 1, nolabel: 0, widget: 'priority' },
          field_active: {},
          widget_web_ribbon: {
            attr: {
              name: 'web_ribbon',
              title: 'Archived',
              bg_color: 'bg-danger',
              // invisible': [('active', '=', True)]
              invisible: [['active', '=', true]]
            }
          }
        },
        group_right: {
          field_image_1920: { widget: 'image', preview_image: 'image_128' }
        }
      },

      notebook: {
        page_general_information: {
          attr: { string: '基本信息', name: 'general_information' },
          group_general_information: {
            group_group_general: {
              attr: { name: 'group_general' },

              field_detailed_type: {},
              field_product_tooltip: { string: '' },
              field_uom_id: { groups: 'uom.group_uom', readonly: '1' },
              field_uom_po_id: { groups: 'uom.group_uom' }
            },

            group_group_standard_price: {
              group_pricing: {
                attr: { name: 'pricing' },
                field_list_price: {
                  widget: 'monetary',
                  currency_field: 'currency_id',
                  field_digits: true
                }
              },

              group_standard_price: {
                attr: {
                  // 'invisible':
                  // [('product_variant_count', '&gt;', 1),
                  // ('is_product_variant', '=', False)]

                  invisible: [
                    ['product_variant_count', '>', 1],
                    ['is_product_variant', '=', false]
                  ]
                },
                field_standard_price: {
                  widget: 'monetary',
                  currency_field: 'cost_currency_id',
                  field_digits: true
                  // readonly: '1'
                },
                field_uom_name: { groups: 'uom.group_uom' }
              },

              field_categ_id: {},
              field_default_code: {
                // 'invisible': [('product_variant_count', '>', 1)]
                invisible: [['product_variant_count', '>', 1]]
              },
              field_barcode: {
                // readonly: [['product_variant_count', '>', 1]]
                readonly: [['product_variant_count', '>', 1]],
                // 'invisible': [('product_variant_count', '>', 1)]
                invisible: [['product_variant_count', '>', 1]]
              },
              field_product_tag_ids: {
                widget: 'many2many_tags',
                color_field: 'color'
              },
              field_company_id: { groups: 'base.group_multi_company' },
              field_currency_id: { invisible: 1 },
              field_cost_currency_id: { invisible: 1 },
              field_product_variant_id: { invisible: 1 }
            }
          },
          group_note: {
            attr: { string: '备注' },
            field_description: {
              nolabel: '1',
              placeholder: 'This note is only for internal purposes.'
            }
          }
        },

        page_variants: {
          attr: {
            // invisible: 1,
            name: 'variants',
            string: '属性和变体',
            groups: 'product.group_product_variant'
          },

          field_attribute_line_ids: {
            // invisible: 1,
            // context: { show_attribute: false },
            context: { show_attribute: false },

            views: {
              tree: {
                arch: {
                  sheet: {
                    field_value_count: { invisible: 1 },
                    field_attribute_id: {},
                    field_value_ids: { widget: 'many2many_tags' }
                    // _button: {
                    //   _attr: {
                    //     groups: 'product.group_product_variant',
                    //     name: 'action_open_attribute_values',
                    //     string: 'Configure',
                    //     type: 'object',
                    //     class: 'float-end btn-secondary'
                    //   }
                    // }
                  },
                  kanban: {
                    card_title: { field_attribute_id: {} },
                    card_label: {},
                    card_value: { field_value_ids: {} }
                  }
                }
              },

              form: {
                arch: {
                  sheet: {
                    field_value_count: { invisible: 1 },
                    field_attribute_id: {
                      // 'readonly': [('id', '!=', False)]
                      readonly: [['id', '!=', false]]
                    },
                    field_value_ids: { widget: 'many2many_tags' },
                    button_action_open_attribute_values: {
                      invisible: 1,
                      groups: 'product.group_product_variant',
                      name: 'action_open_attribute_values',
                      string: 'Configure',
                      type: 'object'
                    }
                  }
                }
              }
            }
          },

          group_1: {
            attr: {
              invisible: 1
            },

            group_: {
              attr: {
                invisible: { editable: true }
              },
              widget_text:
                'Warning: adding or deleting attributes will delete and recreate existing variants and lead to the loss of their possible customizations.'
            }
          }
        }
      }
    }
  }
}

const view_tree_product_template = {
  _odoo_model: 'ir.ui.view',
  model: 'product.template',
  type: 'tree',
  arch: {
    sheet: {
      // _header: {
      //   _button_action_open_label_layout: {
      //     _attr: {
      //       name: 'action_open_label_layout',
      //       type: 'object',
      //       string: 'Print Labels'
      //     }
      //   }
      // },
      field_product_variant_count: { invisible: '1' },
      field_sale_ok: { invisible: '1' },
      field_currency_id: { invisible: '1' },
      field_cost_currency_id: { invisible: '1' },
      field_priority: { widget: 'priority', optional: 'hide' },
      field_name: {},
      field_default_code: { optional: 'show' },
      field_product_tag_ids: {
        widget: 'many2many_tags',
        optional: 'show',
        color_field: 'color'
      },
      field_barcode: { optional: 'hide' },
      field_company_id: {
        optional: 'hide',
        no_create: true,
        groups: 'base.group_multi_company'
      },
      field_list_price: {
        widget: 'monetary',
        optional: 'show',
        currency_field: 'currency_id'
      },
      field_standard_price: {
        widget: 'monetary',
        optional: 'show',
        currency_field: 'cost_currency_id'
      },
      field_categ_id: { optional: 'hide' },
      field_detailed_type: { optional: 'hide' },
      field_type: { invisible: '1' },
      field_uom_id: { optional: 'show' },
      field_active: { invisible: '1' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {
        field_type: {
          selection: [
            ['service', '服务或费用'],
            ['consu', '易耗品'],
            ['product', '库存产品']
          ]
        }
      },
      card_value: { field_default_code: {}, field_barcode: {} }
    }
  }
}

const view_search_product_template = {
  _odoo_model: 'ir.ui.view',
  model: 'product.template',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Product',
        filter_domain: [
          '|',
          '|',
          '|',
          ['default_code', 'ilike', { self: {} }],
          ['product_variant_ids.default_code', 'ilike', { self: {} }],
          ['name', 'ilike', { self: {} }],
          ['barcode', 'ilike', { self: {} }]
        ]
      },

      field_categ_id: {
        filter_domain: [['categ_id', 'child_of', { self: {} }]]
      },

      filter_type: {
        services: {
          name: 'services',
          string: 'Services',
          domain: [['type', '=', 'service']]
        },
        consumable: {
          name: 'consumable',
          string: 'Products',
          domain: [['type', 'in', ['consu']]]
        },
        products: {
          name: 'products',
          string: '库存产品',
          domain: [['type', 'in', ['product']]]
        }
      },

      filter_sell_purchase: {
        filter_to_sell: {
          name: 'filter_to_sell',
          string: 'Can be Sold',
          domain: [['sale_ok', '=', true]]
        },
        filter_to_purchase: {
          name: 'filter_to_purchase',
          string: 'Can be Purchased',
          domain: [['purchase_ok', '=', true]]
        }
      },
      filter_favorites: {
        favorites: {
          name: 'favorites',
          string: 'Favorites',
          domain: [['priority', '=', '1']]
        }
      },
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_product_template = {
  _odoo_model: 'ir.actions.act_window',
  name: '商品模版',
  type: 'ir.actions.act_window',
  res_model: 'product.template',
  search_view_id: 'view_search_product_template',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_product_template',
    form: 'view_form_product_template'
  }
}

const model_product_template = {
  _odoo_model: 'ir.model',
  fields: {
    detailed_type: {
      selection: [
        ['service', '服务或费用'],
        ['consu', '易耗品'],
        ['product', '库存产品']
      ]
    },
    default_code: { string: '编码' }
    // active: { string: '启用' },
    // name: { string: '名称' },
  }
}

export default {
  view_form_product_template,
  view_tree_product_template,
  view_search_product_template,
  action_product_template,
  model_product_template
}
