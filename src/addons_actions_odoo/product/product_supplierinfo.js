const view_form_product_supplierinfo = {
  _odoo_model: 'ir.ui.view',
  model: 'product.supplierinfo',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group_vendor: {
          attr: { name: 'vendor', string: 'Vendor' },
          field_partner_id: {
            context: { res_partner_search_mode: 'supplier' }
          },
          field_product_name: {},
          field_product_code: {},

          field_delay: {},
          widget_text: 'days'
        },

        group_pricelist: {
          attr: { string: 'Pricelist' },
          field_product_tmpl_id: {
            string: 'Product',
            // invisible="context.get('visible_product_tmpl_id', True)"
            invisible: { context_visible_product_tmpl_id: true }
          },
          field_product_id: { groups: 'product.group_product_variant' },
          group_qty: {
            field_min_qty: {},
            field_product_uom: { groups: 'uom.group_uom' }
          },
          group_price: {
            field_price: { string: 'Unit Price' },
            field_currency_id: { groups: 'base.group_multi_currency' }
          },
          group_date: {
            field_date_start: { string: 'Validity' },
            widget_text: 'to',
            field_date_end: { nolabel: 1 }
          },
          field_company_id: {}
        }
      }
    }
  }
}

const view_tree_product_supplierinfo = {
  _odoo_model: 'ir.ui.view',
  model: 'product.supplierinfo',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: {},
      field_partner_id: {},
      field_product_id: {},
      field_product_tmpl_id: {},
      field_product_name: {},
      field_product_code: {},
      field_date_start: {},
      field_date_end: {},
      field_company_id: {},
      field_min_qty: {},
      field_product_uom: {},
      field_price: {},
      field_currency_id: {},
      field_delay: {}
    },
    kanban: {
      card_title: { field_partner_id: {} },
      card_label: { field_product_id: {}, field_product_tmpl_id: {} },
      card_value: {
        field_date_start: {},
        field_date_end: {},
        field_price: {}
      }
    }
  }
}

const view_search_product_supplierinfo = {
  _odoo_model: 'ir.ui.view',
  model: 'product.supplierinfo',
  type: 'search',
  arch: {
    sheet: {
      field_partner_id: { _default: 1 },
      field_product_tmpl_id: {},
      filter_product: {
        active_products: {
          string: 'Active Products',
          domain: [
            '|',
            ['product_tmpl_id.active', '=', true],
            ['product_id.active', '=', true]
          ]
        }
      },

      filter_active: {
        active: {
          string: 'Active',

          //
          // [
          // '|',
          // ('date_end', '=', False),
          // ('date_end', '&gt;=',
          //         (context_today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d'))
          // ]

          domain: [
            '|',
            ['date_end', '=', false],
            ['date_end', '>', { datetime_last_date: {} }]
          ]
        },

        archived: {
          string: 'Archived',
          // [('date_end', '&lt;', (context_today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d'))],
          // todo
          domain: [['date_end', '<', { last_date: {} }]]
        }
      }
    },

    groupby: {
      groupby_product: {
        string: 'Product',
        domain: '[]',
        context: { group_by: 'product_tmpl_id' }
      },
      groupby_vendor: {
        string: 'Vendor',
        domain: '[]',
        context: { group_by: 'partner_id' }
      }
    }
  }
}

const action_product_supplierinfo = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Vendor Pricelists',
  type: 'ir.actions.act_window',
  res_model: 'product.supplierinfo',
  search_view_id: 'view_search_product_supplierinfo',
  domain: [],
  context: { visible_product_tmpl_id: false },
  views: {
    tree: 'view_tree_product_supplierinfo',
    form: 'view_form_product_supplierinfo'
  }
}

export default {
  view_form_product_supplierinfo,
  view_tree_product_supplierinfo,
  view_search_product_supplierinfo,
  action_product_supplierinfo
}
