import product_category from './product_category.js'
import product_tag from './product_tag.js'
import product_pricelist from './product_pricelist.js'
import res_country_group from './res_country_group.js'
import product_supplierinfo from './product_supplierinfo.js'
import product_template from './product_template.js'
// import product_product from './product_product.js'

// import product_attribute from './product_attribute.js'

export default {
  ...product_category,
  ...product_tag,
  ...product_pricelist,
  ...res_country_group,
  ...product_supplierinfo,
  ...product_template

  // ...product_attribute,
  // ...product_product,
}
