const view_form_product_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'product.tag',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group_name: {
          field_name: {}
          // field_color: { widget: 'color_picker' }
        }
      },
      group_product_ids: {
        field_product_ids: { string: '商品', widget: 'many2many_tags' }
      }
    }
  }
}

const view_tree_product_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'product.tag',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {}
      // field_color: { widget: 'color_picker' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: {}
    }
  }
}

const action_product_tag = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品标签',
  type: 'ir.actions.act_window',
  res_model: 'product.tag',
  // search_view_id: '',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_product_tag',
    form: 'view_form_product_tag'
  }
}

export default {
  view_form_product_tag,
  view_tree_product_tag,
  action_product_tag
}
