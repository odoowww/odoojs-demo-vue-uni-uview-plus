const view_form_product_pricelist = {
  _odoo_model: 'ir.ui.view',
  model: 'product.pricelist',
  type: 'form',
  arch: {
    sheet: {
      field_active: {},
      widget: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      group_pricelist_settings: {
        field_name: { placeholder: 'e.g. USD Retailers' },
        field_currency_id: { groups: 'base.group_multi_currency' },
        field_active: { invisible: 1 },
        field_company_id: { groups: 'base.group_multi_company' }
      },

      notebook: {
        page_pricelist_rules: {
          attr: { name: 'pricelist_rules', string: 'Price Rules' },
          field_item_ids: {
            nolabel: '1',
            context: { default_base: 'list_price' },
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_display_name: {}
                  },
                  kanban: {
                    card_title: { field_display_name: {} },
                    card_label: {},
                    card_value: {}
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_display_name: {}
                  }
                }
              }
            }
          }
        },

        page_pricelist_config: {
          attr: { name: 'pricelist_config', string: 'Configuration' },
          group: {
            group_pricelist_availability: {
              attr: {
                name: 'pricelist_availability',
                string: 'Availability'
              },
              field_country_group_ids: { widget: 'many2many_tags' }
            },

            group_pricelist_discounts: {
              attr: {
                name: 'pricelist_discounts',
                string: 'Discounts',
                groups: 'product.group_discount_per_so_line'
              },
              field_discount_policy: { widget: 'radio' }
            }
          }
        }
      }
    }
  }
}

const view_tree_product_pricelist = {
  _odoo_model: 'ir.ui.view',
  model: 'product.pricelist',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_discount_policy: { groups: 'product.group_discount_per_so_line' },
      field_currency_id: { groups: 'base.group_multi_currency' },
      field_company_id: { groups: 'base.group_multi_company' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: {}
    }
  }
}

const view_search_product_pricelist = {
  _odoo_model: 'ir.ui.view',
  model: 'product.pricelist',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},
      field_currency_id: { groups: 'base.group_multi_currency' },
      filter_active: {
        inactive: { string: '已归档', domain: [['active', '=', false]] }
      }
    }
  }
}

const action_product_pricelist = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Pricelists',
  type: 'ir.actions.act_window',
  res_model: 'product.pricelist',
  search_view_id: 'view_search_product_pricelist',
  domain: [],
  context: {},

  views: {
    tree: 'view_tree_product_pricelist',
    form: 'view_form_product_pricelist'
  }
}

export default {
  view_form_product_pricelist,
  view_tree_product_pricelist,
  view_search_product_pricelist,
  action_product_pricelist
}
