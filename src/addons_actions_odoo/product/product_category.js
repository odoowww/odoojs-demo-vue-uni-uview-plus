const view_form_product_category = {
  _odoo_model: 'ir.ui.view',
  model: 'product.category',
  type: 'form',
  arch: {
    sheet: {
      group_first: {
        group_first: {
          field_parent_id: {},
          field_name: { string: '类别名称', placeholder: 'e.g. Lamps' },
          field_product_count: { string: '产品数量' }
        }
      }
    }
  }
}

const view_tree_product_category = {
  _odoo_model: 'ir.ui.view',
  model: 'product.category',
  type: 'tree',
  arch: {
    sheet: {
      field_display_name: { string: '名称' }
    },
    kanban: {
      card_title: { field_display_name: {} },
      card_label: {},
      card_value: {}
    }
  }
}

const view_search_product_category = {
  _odoo_model: 'ir.ui.view',
  model: 'product.category',
  type: 'search',
  arch: {
    sheet: {
      field_name: { string: '产品类别' },
      field_parent_id: {}
    }
  }
}

const action_product_category = {
  _odoo_model: 'ir.actions.act_window',
  name: '商品类别',
  type: 'ir.actions.act_window',
  res_model: 'product.category',
  search_view_id: 'view_search_product_category',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_product_category',
    form: 'view_form_product_category'
  }
}

export default {
  view_form_product_category,
  view_tree_product_category,
  view_search_product_category,
  action_product_category
}
