const view_form_res_country_group = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country.group',
  type: 'form',

  arch: {
    sheet: {
      group_title: {
        group: {
          field_name: {
            string: 'Group Name',
            readonly: 1,
            placeholder: 'e.g. Europe'
          }
        }
      },

      group_country_group: {
        field_country_ids: { readonly: 1, widget: 'many2many_tags' }
      },

      widget_text__pricelist_ids: 'Pricelists for Country Group',
      field_pricelist_ids: {
        nolabel: 1,
        string: 'Pricelists',
        views: {
          tree: {
            arch: {
              sheet: {
                field_name: {}
              },
              kanban: {
                card_title: { field_name: {} },
                card_label: {},
                card_value: {}
              }
            }
          },
          form: {
            arch: {
              sheet: {
                group_name: { field_name: {} }
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_country_group = {
  _odoo_model: 'ir.ui.view',
  model: 'res.country.group',
  type: 'tree',

  arch: {
    sheet: {
      field_name: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: {}
    }
  }
}

const action_res_country_group = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Country Group',
  type: 'ir.actions.act_window',
  buttons: { create: false, edit: true, delete: false },

  res_model: 'res.country.group',

  domain: [],
  context: {},
  views: {
    tree: 'view_tree_res_country_group',
    form: 'view_form_res_country_group'
  }
}

export default {
  view_tree_res_country_group,
  view_form_res_country_group,
  action_res_country_group
}
