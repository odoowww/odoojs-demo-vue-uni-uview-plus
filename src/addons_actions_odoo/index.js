import base from './base/index.js'
import contacts from './contacts/index.js'
import uom from './uom/index.js'
import product from './product/index.js'
import analytic from './analytic/index.js'
import account from './account/index.js'
import sales_team from './sales_team/index.js'
import sale from './sale/index.js'
import purchase from './purchase/index.js'
import stock from './stock/index.js'
import sale_stock from './sale_stock/index.js'
import crm from './crm/index.js'

import mrp from './mrp/index.js'

import hr from './hr/index.js'
import hr_expense from './hr_expense/index.js'
// import l10n_cn from './l10n_cn/index.js'

const modules = {
  base,
  contacts,
  uom,
  product,
  analytic,
  account,
  sales_team,
  sale,
  purchase,
  stock,
  sale_stock,
  crm,
  hr,
  hr_expense,
  // l10n_cn,
  mrp
}

export default modules
