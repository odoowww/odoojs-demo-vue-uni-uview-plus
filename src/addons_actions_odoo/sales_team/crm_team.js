const view_form_crm_team = {
  _odoo_model: 'ir.ui.view',
  model: 'crm.team',
  type: 'form',
  arch: {
    sheet: {
      field_member_warning: {
        //  'invisible': ['|', ('is_membership_multi', '=', True),
        // ('member_warning', '=', False)]

        invisible: [
          '|',
          ['is_membership_multi', '=', true],
          ['member_warning', '=', false]
        ]
      },

      group: {
        group_left: {
          widget_web_ribbon: {
            attr: {
              name: 'web_ribbon',
              title: 'Archived',
              bg_color: 'bg-danger',
              // 'invisible': [('active', '=', True)]
              invisible: [['active', '=', true]]
            }
          },

          field_name: {
            // string: 'Sales Team', placeholder: 'e.g. North America'
          }

          // todo. 必须安装 销售模块才有该字段
          // field_use_quotations: {},
        },
        group_right: {
          attr: { name: 'left', string: '团队信息' },
          field_active: { invisible: '1' },
          field_sequence: { invisible: '1' },
          field_is_membership_multi: { invisible: '1' },

          field_user_id: {
            widget: 'many2one_avatar_user',
            domain: [['share', '=', false]]
          },
          field_company_id: { groups: 'base.group_multi_company' },

          // todo. 必须安装 销售模块才有该字段
          // field_invoiced_target: { widget: 'monetary' },

          field_currency_id: { invisible: '1' },
          field_member_company_ids: { invisible: '1' }
        }
      },

      notebook: {
        page_members_users: {
          attr: { string: '成员', name: 'members_users' },
          field_member_ids: {
            views: {
              tree: {
                arch: {
                  sheet: {
                    field_name: { test: 18 },
                    field_email: {},
                    field_avatar_128: { string: '头像', widget: 'image' }
                  },
                  kanban: {
                    card_title: { field_name: { test: 17 } },
                    card_label: {},
                    card_value: { field_email: {} }
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_name: { test: 16 },
                    field_email: {},
                    field_avatar_128: { string: '头像', widget: 'image' }
                  }
                }
              }
            }
          },
          field_crm_team_member_ids: {
            // 'invisible': ['|', ('is_membership_multi', '=', True),
            // ('is_membership_multi', '=', False)]

            invisible: [
              '|',
              ['is_membership_multi', '=', true],
              ['is_membership_multi', '=', false]
            ],
            context: {
              // kanban_view_ref:
              //   'sales_team.crm_team_member_view_kanban_from_team',
              // form_view_ref: 'sales_team.crm_team_member_view_form_from_team',
              // tree_view_ref: 'sales_team.crm_team_member_view_tree_from_team',
              // default_crm_team_id: active_id
            },

            views: {
              tree: {
                arch: {
                  sheet: { field_name: { test: 15 } },
                  kanban: {
                    card_title: { field_name: { test: 14 } },
                    card_label: {},
                    card_value: {}
                  }
                }
              },
              form: {
                arch: {
                  sheet: {
                    field_name: { test: 13 }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_crm_team = {
  _odoo_model: 'ir.ui.view',
  model: 'crm.team',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: { readonly: '1' },
      field_active: { invisible: '1' },
      field_user_id: {
        domain: [['share', '=', false]],
        widget: 'many2one_avatar_user'
      },
      field_company_id: { groups: 'base.group_multi_company' }
    },

    kanban: {
      card_title: { field_name: { test: 2 } },
      card_label: {},
      card_value: { field_user_id: {} }
    }
  }
}

const view_search_crm_team = {
  _odoo_model: 'ir.ui.view',
  model: 'crm.team',
  type: 'search',
  arch: {
    sheet: {
      field_name: { test: 1 },
      field_user_id: {},
      field_member_ids: {},
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      },

      groupby: {
        team_leader: {
          string: 'Team Leader',
          name: 'team_leader',
          domain: [],
          context: { group_by: 'user_id' }
        }
      }
    }
  }
}

const action_crm_team = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售团队',
  type: 'ir.actions.act_window',
  res_model: 'crm.team',
  search_view_id: 'view_search_crm_team',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_crm_team',
    form: 'view_form_crm_team'
  }
}

export default {
  view_form_crm_team,
  view_tree_crm_team,
  view_search_crm_team,
  action_crm_team
}
