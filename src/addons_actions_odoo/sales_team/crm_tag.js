const view_form_crm_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'crm.tag',
  type: 'form',

  arch: {
    sheet: {
      field_name: { placeholder: 'e.g. Services' },

      group_name: {
        group_name: {
          field_color: { required: true, widget: 'color_picker' }
        }
      }
    }
  }
}

const view_tree_crm_tag = {
  _odoo_model: 'ir.ui.view',
  model: 'crm.tag',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_color: { widget: 'color_picker' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_color: {} }
    }
  }
}

const action_crm_tag = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售标签',
  type: 'ir.actions.act_window',
  res_model: 'crm.tag',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_crm_tag',
    form: 'view_form_crm_tag'
  }
}

export default {
  view_form_crm_tag,
  view_tree_crm_tag,
  action_crm_tag
}
