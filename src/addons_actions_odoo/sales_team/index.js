import crm_tag from './crm_tag.js'
import crm_team from './crm_team.js'

export default {
  ...crm_tag,
  ...crm_team
}
