### addons_odoo

action and view for odoojs

### contact us

contact us: odoojs@outlook.com

### 说明

1. 文件夹命名 以 addons\_ 开头
2. 文件夹内部 二级文件夹名 是 odoo 的模块
3. 二级文件夹内 文件命名 无限制
4. 每个文件内, 定义 action, view, model_field
