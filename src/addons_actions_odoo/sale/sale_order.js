const order_line_form_sheet = {
  field_display_type: { invisible: '1' },
  field_sequence: { invisible: '1' },
  field_product_uom_category_id: { invisible: '1' },

  group: {
    group_product: {
      attr: {
        // 'invisible': [('display_type', '!=', False)]
        invisible: [['display_type', '!=', false]]
      },

      field_product_updatable: { invisible: '1' },
      field_product_id: {
        widget: 'many2one_barcode',
        // 'readonly': [('product_updatable', '=', False)],
        readonly: [['product_updatable', '=', false]],
        // 'required': [('display_type', '=', False)],
        required: [['display_type', '=', false]],
        //   context="{
        //     'partner_id': parent.partner_id,
        //     'quantity': product_uom_qty,
        //     'pricelist': parent.pricelist_id,
        //     'uom':product_uom,
        //     'company_id': parent.company_id,
        //     'default_lst_price': price_unit,
        //     'default_description_sale': name
        // }"

        context: {
          partner_id: { field_parent_partner_id: {} },
          quantity: { field_product_uom_qty: {} },
          pricelist: { field_parent_pricelist_id: {} },
          uom: { field_product_uom: {} },
          company_id: { field_parent_company_id: {} },
          default_lst_price: { field_price_unit: {} },
          default_description_sale: { field_name: {} }
        }
      },

      field_product_type: { invisible: '1' },
      field_invoice_status: { invisible: '1' },
      field_qty_to_invoice: { invisible: '1' },
      field_qty_delivered_method: { invisible: '1' },
      field_price_total: { invisible: '1' },
      field_price_tax: { invisible: '1' },
      field_price_subtotal: { invisible: '1' },
      field_product_uom_readonly: { invisible: '1' },
      field_product_uom_qty: {
        // context="{'partner_id':parent.partner_id,
        // 'quantity':product_uom_qty, 'pricelist':parent.pricelist_id,
        // 'uom':product_uom, 'uom_qty_change':True,
        // 'company_id': parent.company_id}"

        context: {
          partner_id: { field_parent_partner_id: {} },
          quantity: { field_product_uom_qty: {} },
          pricelist: { field_parent_pricelist_id: {} },
          uom: { field_product_uom: {} },
          uom_qty_change: true,
          company_id: { field_parent_company_id: {} }
        }
      },
      field_product_uom: {
        groups: 'uom.group_uom',

        // context="{'company_id': parent.company_id}"
        context: { company_id: { field_parent_company_id: {} } },
        // 'readonly': [('product_uom_readonly', '=', True)],
        readonly: [['product_uom_readonly', '=', true]],

        // 'required': [('display_type', '=', False)],
        required: [['display_type', '=', false]]
      },

      field_qty_delivered: {
        string: 'Delivered',
        // 'invisible': [('parent.state', 'not in', ['sale', 'done'])]
        invisible: [['parent.state', 'not in', ['sale', 'done']]],

        // 'readonly': [('qty_delivered_method', '!=', 'manual')]
        readonly: [['qty_delivered_method', '!=', 'manual']]
      },

      field_qty_invoiced: {
        string: 'Invoiced',
        // 'invisible': [('parent.state', 'not in', ['sale', 'done'])]
        invisible: [['parent.state', 'not in', ['sale', 'done']]]
      },
      field_product_packaging_id: { groups: 'product.group_stock_packaging' },
      field_price_unit: {
        //'readonly': [('qty_invoiced', '&gt;', 0)]
        readonly: [['qty_invoiced', '>', 0]]
      },
      field_tax_id: {
        widget: 'many2many_tags',
        //'readonly': [('qty_invoiced', '&gt;', 0)]
        readonly: [['qty_invoiced', '>', 0]],
        // domain="[('type_tax_use','=','sale'),('company_id','=',parent.company_id),('country_id', '=', parent.tax_country_id)]"
        domain: [
          ['type_tax_use', '=', 'sale'],
          ['company_id', '=', { field_parent_company_id: {} }],
          ['country_id', '=', { field_parent_tax_country_id: {} }]
        ]

        //  context="{'active_test': True}"
        //  context="{'search_view_ref': 'account.account_tax_view_search'}"
      },
      field_discount: { groups: 'product.group_discount_per_so_line' },
      field_sequence: { invisible: 0 }
    },
    group_lead: {
      attr: {
        // 'invisible': [('display_type', '!=', False)]
        invisible: [['display_type', '!=', false]]
      },

      field_customer_lead: {
        // widget  天
      },
      field_analytic_distribution: {
        widget: 'analytic_distribution',
        groups: 'analytic.group_analytic_accounting'
      }
    }
  },

  field_name: {
    widget: 'text',
    string: 'Number',

    label_Description: {
      for: 'name',
      string: 'Description',
      // 'invisible': [('display_type', '!=', False)]
      invisible: [['display_type', '!=', false]]
    },

    label_Section: {
      for: 'name',
      string: 'Section Name (eg. Products, Services)',
      // 'invisible': [('display_type', '!=', 'line_section')]
      invisible: [['display_type', '!=', 'line_section']]
    },

    label_Note: {
      for: 'name',
      string: 'Note',
      //  'invisible': [('display_type', '!=', 'line_note')]
      invisible: [['display_type', '!=', 'line_note']]
    }
  },

  field_invoice_lines: {
    groups: 'base.group_no_one',
    invisible: 1
    // 'invisible': [('display_type', '!=', False)]
    // invisible: [['display_type', '!=', false]]
  },

  field_state: { invisible: '1' },
  field_company_id: { invisible: '1' }
}

const view_form_sale_order_sheet = {
  field_name: { string: '订单号', readonly: '1' },

  group_sale_header: {
    group_partner_details: {
      field_partner_id: {
        widget: 'res_partner_many2one',
        //  context="{'res_partner_search_mode': 'customer',
        // 'show_address': 1, 'show_vat': True}"
        context: {
          res_partner_search_mode: 'customer',
          show_address: 1,
          show_vat: true
        }
      },
      field_partner_invoice_id: {
        groups: 'account.group_delivery_invoice_address',
        context: { default_type: 'invoice' }
      },
      field_partner_shipping_id: {
        groups: 'account.group_delivery_invoice_address',
        context: { default_type: 'delivery' }
      }
    },

    group_order_details: {
      field_validity_date: {
        // 'invisible': [('state', 'in', ['sale', 'done'])]
        invisible: [['state', 'in', ['sale', 'done']]]
      },

      field_date_order__quotation: {
        groups: 'base.group_no_one',
        // 'invisible': [('state', 'in', ['sale', 'done', 'cancel'])]
        invisible: [['state', 'in', ['sale', 'done', 'cancel']]]
      },

      field_date_order__order: {
        // 'invisible': [('state', 'in', ['draft', 'sent'])]
        invisible: [['state', 'in', ['draft', 'sent']]]
      },

      field_show_update_pricelist: { invisible: 1 },

      field_pricelist_id: { groups: 'product.group_product_pricelist' },

      button_action_update_prices: {
        invisible: 1,
        name: 'action_update_prices',
        type: 'object',
        string: ' Update Prices',
        help: 'Recompute all prices based on this pricelist',
        confirm:
          'This will update all unit prices based on the currently set pricelist.'

        //'invisible':  ['|', ('show_update_pricelist', '=', False), ('state', 'in', ['sale', 'done', 'cancel'])]
        // invisible: [
        //   '|',
        //   ['show_update_pricelist', '=', false],
        //   ['state', 'in', ['sale', 'done', 'cancel']]
        // ]
      },

      field_company_id: { invisible: 1 },
      field_currency_id: { invisible: 1 },
      field_pricelist_id__1: { invisible: 1 },
      field_tax_country_id: { invisible: 1 },
      field_payment_term_id: {}
    }
  },

  notebook: {
    page_order_lines: {
      attr: { string: '明细', name: 'order_lines' },
      field_order_line: {
        // 'readonly': [('state', 'in', ('done','cancel'))]
        readonly: [['state', 'in', ['done', 'cancel']]],
        views: {
          tree: {
            arch: {
              sheet: {
                field_sequence: { widget: 'handle', invisible: 1 },
                field_display_type: { invisible: 1 },
                field_product_uom_category_id: { invisible: 1 },
                field_product_type: { invisible: 1 },
                field_currency_id: { invisible: 1 },
                field_product_updatable: { invisible: 1 },
                field_product_id: { widget: 'sol_product_many2one' },
                field_product_template_id: { invisible: '1' },
                field_name: {
                  widget: 'section_and_note_text',
                  optional: 'show'
                },
                field_analytic_distribution: {
                  groups: 'analytic.group_analytic_accounting',
                  widget: 'analytic_distribution',
                  optional: 'hide'
                },
                field_product_uom_qty: {
                  // widget: ''
                  // 查看库存?
                  // context="{'partner_id':parent.partner_id,
                  // 'quantity':product_uom_qty, 'pricelist':parent.pricelist_id,
                  // 'uom':product_uom, 'uom_qty_change':True,
                  // 'company_id': parent.company_id}"

                  context: {
                    partner_id: { field_parent_partner_id: {} },
                    quantity: { field_product_uom_qty: {} },
                    pricelist: { field_parent_pricelist_id: {} },
                    uom: { field_product_uom: {} },
                    uom_qty_change: true,
                    company_id: { field_parent_company_id: {} }
                  }
                },
                field_qty_delivered: {
                  string: '已送货',
                  optional: 'show',
                  // 'column_invisible': [('parent.state', 'not in', ['sale', 'done'])],
                  column_invisible: [
                    ['parent.state', 'not in', ['sale', 'done']]
                  ]
                },
                field_qty_delivered_method: { invisible: 1 },
                field_qty_invoiced: {
                  string: '已开票',
                  optional: 'show',
                  // 'column_invisible': [('parent.state', 'not in', ['sale', 'done'])],
                  column_invisible: [
                    ['parent.state', 'not in', ['sale', 'done']]
                  ]
                },
                field_qty_to_invoice: { invisible: '1' },
                field_product_uom_readonly: { invisible: '1' },
                field_product_uom__1: { invisible: '1' },
                field_product_uom: { optional: 'show' },
                field_customer_lead: { optional: 'hide' },
                field_product_packaging_qty: {
                  groups: 'product.group_stock_packaging',
                  optional: 'show',
                  // 'invisible': ['|', ('product_id', '=', False), ('product_packaging_id', '=', False)]
                  invisible: [
                    '|',
                    ['product_id', '=', false],
                    ['product_packaging_id', '=', false]
                  ]
                },
                field_product_packaging_id: {
                  groups: 'product.group_stock_packaging',
                  optional: 'show',
                  invisible: 1,
                  // 'invisible': [('product_id', '=', False)]}"
                  // invisible: [['product_id', '=', false]],
                  // context="{'default_product_id': product_id, 'tree_view_ref':'product.product_packaging_tree_view', 'form_view_ref':'product.product_packaging_form_view'}
                  context: {
                    default_product_id: { field_product_id: {} }
                    // tree_view_ref: 'product.product_packaging_tree_view',
                    // form_view_ref: 'product.product_packaging_form_view'
                  }
                },
                field_price_unit: {
                  //'readonly': [('qty_invoiced', '&gt;', 0)]
                  readonly: [['qty_invoiced', '>', 0]]
                },
                field_tax_id: {
                  widget: 'many2many_tags',
                  //'readonly': [('qty_invoiced', '&gt;', 0)]
                  readonly: [['qty_invoiced', '>', 0]]

                  //  context="{'active_test': True}"
                  //  context="{'search_view_ref': 'account.account_tax_view_search'}"
                },
                field_discount: {
                  groups: 'product.group_discount_per_so_line',
                  widget: 'sol_discount',
                  optional: 'show'
                },
                field_is_downpayment: { invisible: '1' },
                field_price_subtotal: {
                  groups: 'account.group_show_line_subtotals_tax_excluded',
                  widget: 'monetary',
                  // 'invisible': [('is_downpayment', '=', True)]
                  invisible: [['is_downpayment', '=', true]]
                },
                field_price_total: {
                  groups: 'account.group_show_line_subtotals_tax_included',
                  widget: 'monetary',
                  // 'invisible': [('is_downpayment', '=', True)]
                  invisible: [['is_downpayment', '=', true]]
                },
                field_state: { invisible: '1' },
                field_invoice_status: { invisible: '1' },
                //  field_currency_id: { invisible: '1' },
                field_price_tax: { invisible: '1' },
                field_company_id: { invisible: '1' }
              },
              kanban: {
                card_title: { field_name: {} },
                card_label: { field_tax_id: { widget: 'many2many_tags' } },
                card_value: {
                  widget_sale_order_line_value: {
                    field_display_type: {},
                    field_is_downpayment: {},
                    field_product_uom_qty: {},
                    field_price_unit: {},
                    field_price_total: {},
                    field_qty_to_invoice: { invisible: '1' }
                  }
                }
              }
            }
          },

          form: {
            arch: { sheet: { ...order_line_form_sheet } }
          }
        }
      },
      group_note_group: {
        group: {
          field_note: {
            nolabel: '1',
            placeholder: 'Terms and conditions...'
          }
        },
        group_sale_total: {
          field_tax_totals: {
            readonly: '1',
            nolabel: '1',
            widget: 'account-tax-totals-field'
          }
        }
      }
    },

    page_other_information: {
      attr: { string: '其他信息', name: 'other_information' },
      group: {
        group_sales_person: {
          field_user_id: { widget: 'many2one_avatar_user' },
          field_team_id: {},
          field_company_id: { groups: 'base.group_multi_company' },
          // field_require_signature: {},
          // field_require_payment: {},
          field_reference: {
            readonly: '1',
            // 'invisible': [('reference', '=', False)]
            invisible: [['reference', '=', false]]
          },
          field_client_order_ref: {},
          field_tag_ids: { widget: 'many2many_tags' }
        },

        group_sale_info: {
          field_show_update_fpos: { invisible: '1' },

          field_fiscal_position_id: {},

          button_action_update_taxes: {
            invisible: 1,
            name: 'action_update_taxes',
            type: 'object',
            string: ' Update Taxes',
            help: 'Recompute all taxes based on this fiscal position',
            icon: 'fa-refresh',
            confirm:
              'This will update all taxes based on the currently selected fiscal position.'

            // 'invisible': ['|', ('show_update_fpos', '=', False), ('state', 'in', ['sale', 'done','cancel'])]

            // invisible: [
            //   '|',
            //   ['show_update_fpos', '=', false],
            //   ['state', 'in', ['sale', 'done', 'cancel']]
            // ]
          },

          field_partner_invoice_id: { invisible: '1' },
          field_analytic_account_id: {
            groups: 'analytic.group_analytic_accounting',
            // 'readonly': [('invoice_count','!=',0),('state','=','sale')]
            readonly: [
              ['invoice_count', '!=', 0],
              ['state', '=', 'sale']
            ]
          },
          field_invoice_status: {
            groups: 'base.group_no_one',
            invisible: [['state', 'not in', ['sale', 'done']]]
          }
        }
      },

      group_2: {
        group_sale_shipping: {
          field_commitment_date: {
            //  string: 'Delivery Date'
          },
          field_expected_date: { widget: 'date' }
        },

        group_technical: {
          field_origin: {}
        },

        group_utm_link: {
          attr: { invisible: 1 },
          field_campaign_id: {},
          field_medium_id: {},
          field_source_id: {}
        }
      }
    },

    page_customer_signature: {
      attr: {
        invisible: 1,
        groups: 'base.group_no_one',
        string: 'Customer Signature',
        name: 'customer_signature'
        // 'invisible': [('require_signature', '=', False), ('signed_by', '=', False), ('signature', '=', False), ('signed_on', '=', False)]
        // invisible: [
        //   ['require_signature', '=', false],
        //   ['signed_by', '=', false],
        //   ['signature', '=', false],
        //   ['signed_on', '=', false]
        // ]
      },
      group_customer_signature: {
        field_signed_by: {},
        field_signed_on: {},
        field_signature: { widget: 'image' }
      }
    }
  }
}

const view_form_sale_order = {
  _odoo_model: 'ir.ui.view',
  model: 'sale.order',
  type: 'form',
  title: { field_name: {} },
  arch: {
    // _div_warning: {
    //   _attr: {
    //     groups: 'account.group_account_invoice,account.group_account_readonly',
    //     invisible({ record }) {
    //       // 'invisible': [('partner_credit_warning', '=', '')]
    //       const { partner_credit_warning } = record
    //       return !partner_credit_warning
    //     }
    //   },
    //   partner_credit_warning: {}
    // },

    button_box: {
      button_action_view_invoice: {
        name: 'action_view_invoice',
        type: 'object',
        // 'invisible': [('invoice_count', '=', 0)]
        invisible: [['invoice_count', '=', 0]],
        action_map: {
          'account.move': 'account.action_account_move_out_invoice'
        },

        field_invoice_count: { string: '结算单', widget: 'statinfo' }
      }
      //   _button_action_preview_sale_order: {
      //     _attr: {
      //       // string: 'Customer Preview',
      //       name: 'action_preview_sale_order',
      //       type: 'object',
      //       class: 'oe_stat_button',
      //       icon: 'fa-globe icon'
      //     },
      //     _span: { _attr: { text: 'Customer' } },
      //     _span_2: { _attr: { text: 'Preview' } }
      //   }
    },

    header: {
      button_action_confirm: {
        name: 'action_confirm',
        string: '确认',
        type: 'object',
        btn_type: 'primary',
        context: { validate_analytic: true },
        // 'invisible': [('state', 'not in', ['sent'])]
        invisible: [['state', 'not in', ['sent']]]
      },

      button_action_confirm2: {
        name: 'action_confirm',
        string: '确认',
        type: 'object',
        context: { validate_analytic: true },
        // 'invisible': [('state', 'not in', ['draft'])]
        invisible: [['state', 'not in', ['draft']]]
      },

      button_action_view_sale_advance_payment_inv: {
        name: 'action_view_sale_advance_payment_inv',
        type: 'action',
        string: '创建结算单',
        btn_type: 'primary',
        // 'invisible': [('invoice_status', '!=', 'to invoice')]
        invisible: [['invoice_status', '!=', 'to invoice']]
      },

      button_action_view_sale_advance_payment_inv2: {
        name: 'action_view_sale_advance_payment_inv',
        type: 'action',
        string: '创建预收',
        context: { default_advance_payment_method: 'percentage' },
        // 'invisible': ['|',('invoice_status', '!=', 'no'),  ('state', '!=', 'sale')]
        invisible: [
          '|',
          ['invoice_status', '!=', 'no'],
          ['state', '!=', 'sale']
        ]
      },

      button_action_cancel: {
        name: 'action_cancel',
        string: '取消订单',
        type: 'object',
        // 'invisible': ['|', ('state', 'not in', ['draft', 'sent','sale']), ('id', '=', False)]
        invisible: [
          '|',
          ['state', 'not in', ['draft', 'sent', 'sale']],
          ['id', '=', false]
        ],
        action_map: {
          'sale.order.cancel': 'sale.action_sale_order_cancel_wizard'
        }
      },

      button_action_draft: {
        name: 'action_draft',
        string: 'Set to Quotation',
        type: 'object',
        invisible: [['state', 'not in', ['cancel']]]
      },

      // button_action_quotation_send: {
      //   name: 'action_quotation_send',
      //   type: 'object',
      //   string: 'Send by Email',
      //   btn_type: 'primary',
      //   attrs: {
      //     invisible({ record }) {
      //       // states: 'draft',
      //       const { state } = record
      //       return state !== 'draft'
      //     }
      //   }
      // },
      // button_action_quotation_send2: {
      //   name: 'action_quotation_send',
      //   type: 'object',
      //   string: 'Send PRO-FORMA Invoice',
      //   groups: 'sale.group_proforma_sales',
      //   btn_type: 'primary',
      //   // context="{'proforma': True, 'validate_analytic': True}
      //   context: { proforma: true, validate_analytic: true },
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': ['|', ('state', '!=', 'draft'),
      //       // ('invoice_count','&gt;=',1)]
      //       const { state, invoice_count } = record
      //       return state !== 'draft' || invoice_count >= 1
      //     }
      //   }
      // },

      // button_action_quotation_send22: {
      //   name: 'action_quotation_send',
      //   string: 'Send PRO-FORMA Invoice',
      //   type: 'object',
      //   groups: 'sale.group_proforma_sales',
      //   attrs: {
      //     invisible({ record }) {
      //       // 'invisible': ['|', ('state', '=', 'draft'),
      //       // ('invoice_count','&gt;=',1)]}"
      //       const { state, invoice_count } = record
      //       return state === 'draft' || invoice_count >= 1
      //     }
      //   },
      //   // context="{'proforma': True, 'validate_analytic': True}"/>
      //   context: { proforma: true, validate_analytic: true }
      // },
      // button_action_quotation_send23: {
      //   name: 'action_quotation_send',
      //   string: 'Send by Email',
      //   type: 'object',
      //   // context="{'validate_analytic': True}"
      //   context: { validate_analytic: true },
      //   attrs: {
      //     invisible({ record }) {
      //       // states="sent,sale"
      //       const { state } = record
      //       return !['sent', 'sale'].includes(state)
      //     }
      //   }
      // },

      field_state: {
        widget: 'statusbar',
        statusbar_visible: 'draft,sent,sale'
      }
    },

    sheet: { ...view_form_sale_order_sheet }
  }
}

const view_tree_sale_order = {
  _odoo_model: 'ir.ui.view',
  model: 'sale.order',
  type: 'tree',
  arch: {
    sheet: {
      field_message_needaction: { invisible: '1' },
      field_name: {},
      field_create_date: { widget: 'date' },
      field_date_order: { widget: 'date', string: 'Order Date' },
      field_commitment_date: { optional: 'hide' },
      field_expected_date: { optional: 'hide' },
      field_partner_id: {},
      field_user_id: { optional: 'show', widget: 'many2one_avatar_user' },
      field_team_id: { optional: 'hide' },
      field_company_id: {
        optional: 'show',
        groups: 'base.group_multi_company'
      },
      field_amount_untaxed: { widget: 'monetary', optional: 'hide' },
      field_amount_tax: { widget: 'monetary', optional: 'hide' },
      field_amount_total: { widget: 'monetary', optional: 'show' },
      field_currency_id: { invisible: '1' },
      field_invoice_status: {
        groups: 'base.group_no_one',
        widget: 'badge'
        // decoration-success="invoice_status == 'invoiced'"
        // decoration-info="invoice_status == 'to invoice'"
        // decoration-warning="invoice_status == 'upselling'"
      },
      field_tag_ids: { widget: 'many2many_tags', optional: 'hide' },
      field_state: { invisible: '1' }
    },
    kanban: {
      card_title: {
        widget_one_row: {
          field_name: {},
          widget_text: ' ',
          field_partner_id: {}
        }
      },
      card_label: { field_create_date: { widget: 'date' } },
      card_value: { field_state: {}, field_amount_total: {} }
    }
  }
}

const view_search_sale_order = {
  _odoo_model: 'ir.ui.view',
  model: 'sale.order',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: '订单',
        // filter_domain="['|', '|',
        // ('name', 'ilike', self),
        // ('client_order_ref', 'ilike', self),
        // ('partner_id', 'child_of', self)]"
        filter_domain: [
          '|',
          '|',
          ['name', 'ilike', { self: {} }],
          ['client_order_ref', 'ilike', { self: {} }],
          ['partner_id', 'child_of', { self: {} }]
        ]
      },
      field_partner_id: { operator: 'child_of' },
      field_user_id: {},
      field_team_id: { string: '销售团队' },
      field_order_line: {
        string: '产品',
        // filter_domain="[('order_line.product_id', 'ilike', self)]

        filter_domain: [['order_line.product_id', 'ilike', { self: {} }]]
      },
      field_analytic_account_id: {
        groups: 'analytic.group_analytic_accounting'
      },
      filter_me: {
        my_sale_orders_filter: {
          name: 'my_sale_orders_filter',
          string: '我的订单',
          domain: [['user_id', '=', { session_uid: {} }]]
        }
      },

      filter_state: {
        draft: {
          string: '报价单',
          // domain="[('state','in',('draft', 'sent'))]
          domain: [['state', 'in', ['draft', 'sent']]]
        },
        sales: {
          string: '销售订单',
          // domain="[('state','in',('sale','done'))]
          domain: [['state', 'in', ['sale', 'done']]]
        }
      },

      filter_invoice: {
        to_invoice: {
          string: 'To Invoice',
          // domain="[('invoice_status','=','to invoice')]
          domain: [['invoice_status', '=', 'to invoice']]
        },
        upselling: {
          string: 'To Upsell',
          // domain="[('invoice_status','=','upselling')]
          domain: [['invoice_status', '=', 'upselling']]
        }
      },

      filter_date: {
        filter_create_date: { string: 'Create Date', date: 'create_date' },
        order_date: { string: 'Order Date', date: 'date_order' }
      }
    }
  }
}

const action_sale_order = {
  _odoo_model: 'ir.actions.act_window',
  name: '销售订单',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'sale.order',
  search_view_id: 'view_search_sale_order',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_sale_order',
    form: 'view_form_sale_order'
  }
}

export default {
  view_form_sale_order,
  view_tree_sale_order,
  view_search_sale_order,
  action_sale_order
}
