export default {
  view_sale_advance_payment_inv: {
    _odoo_model: 'ir.ui.view',
    model: 'sale.advance.payment.inv',
    type: 'form',
    arch: {
      footer: {
        button_create_invoices__open_inv: {
          name: 'create_invoices',
          type: 'object',
          string: '创建并查看结算单',
          id: 'create_invoice_open',
          context: { open_invoices: true },
          btn_type: 'btn-primary'
        },

        button_create_invoices__noopen: {
          name: 'create_invoices',
          type: 'object',
          string: '创建结算单',
          id: 'create_invoice'
        }
      },

      sheet: {
        widget_text:
          '草稿结算单将被创建, 之后, 你可以结算单确认之前, 检查结算单.',

        group: {
          group: {
            field_sale_order_ids: { invisible: '1' },
            field_has_down_payments: { invisible: '1' },
            field_count: {
              invisible: [['count', '=', 1]]
            },
            field_advance_payment_method: {
              widget: 'radio',
              invisible: [['count', '>', 1]]
            },

            field_deduct_down_payments: {
              invisible: [
                '|',
                ['has_down_payments', '=', false],
                ['advance_payment_method', '!=', 'delivered']
              ]
            }
          },

          group_down_payment_specification: {
            attr: {
              name: 'down_payment_specification',
              invisible: [
                ['advance_payment_method', 'not in', ['fixed', 'percentage']]
              ]
            },
            field_company_id: { invisible: '1' },
            field_product_id: { invisible: '1' },
            field_currency_id: { invisible: '1' },

            field_fixed_amount: {
              required: [['advance_payment_method', '=', 'fixed']],
              invisible: [['advance_payment_method', '!=', 'fixed']]
            },

            field_amount: {
              required: [['advance_payment_method', '=', 'percentage']],
              invisible: [['advance_payment_method', '!=', 'percentage']]
            },

            widget_text__percentage: {
              attr: {
                invisible: [['advance_payment_method', '!=', 'percentage']],
                text: '%'
              }
            },

            field_deposit_account_id: {
              groups: 'account.group_account_manager',
              invisible: [['product_id', '!=', false]]
            },
            field_deposit_taxes_id: {
              widget: 'many2many_tags',
              invisible: [['product_id', '!=', false]]
            }
          }
        }
      }
    }
  },

  action_view_sale_advance_payment_inv: {
    _odoo_model: 'ir.actions.act_window',
    name: '创建结算单',
    type: 'ir.actions.act_window',
    res_model: 'sale.advance.payment.inv',
    views: {
      form: 'view_sale_advance_payment_inv'
    }
  }
}
