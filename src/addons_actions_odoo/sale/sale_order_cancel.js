export default {
  view_form_sale_order_cancel: {
    _odoo_model: 'ir.ui.view',
    model: 'sale.order.cancel',
    type: 'form',
    arch: {
      footer: {
        button_action_cancel: {
          name: 'action_cancel',
          type: 'object',
          string: '取消订单',
          btn_type: 'primary'
        }
      },
      sheet: {
        widget_text: '确认要取消订单么?'
      }
    }
  },

  action_sale_order_cancel_wizard: {
    _odoo_model: 'ir.actions.act_window',
    name: '取消订单?',
    type: 'ir.actions.act_window',
    res_model: 'sale.order.cancel',
    domain: [],

    context: {},
    views: {
      form: 'view_form_sale_order_cancel'
    }
  }
}
