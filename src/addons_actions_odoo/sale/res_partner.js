const view_form_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_form_res_partner',
  model: 'res.partner',
  type: 'form',
  title: { field_name: {} },
  arch: {
    sheet: {
      notebook: {
        page_contact_addresses: {},
        page_sales_purchases: {
          attr: { name: 'sales_purchases', string: '销售' },
          group_container_row_2: {
            attr: { name: 'container_row_2' },
            group_sale: {
              attr: { name: 'sale', string: 'Sales' },
              field_user_id: { widget: 'many2one_avatar_user' },
              field_team_id: { groups: 'base.group_no_one' },
              field_property_product_pricelist: { string: 'Pricelists' }
            },
            group_purchase: {
              attr: { invisible: 1 }
            },
            group_fiscal_information: {
              attr: {
                name: 'fiscal_information',
                string: 'Fiscal Information'
                // groups:
                //   'account.group_account_invoice,sales_team.group_sale_salesman'
              },
              field_property_account_position_id: {}
            }
          }
        },

        page_internal_notes: {
          group_slot: {},
          group_invoice: {},
          group_sale: {
            attr: { groups: 'sales_team.group_sale_salesman' },
            group_sale2: {
              attr: { groups: 'sale.group_warning_sale' },
              widget_separator: 'Warning on the Sales Order',
              field_sale_warn: { required: '1' },
              field_sale_warn_msg: {
                nolabel: '1',

                // 'invisible':[('sale_warn','in',(False,'no-message'))]
                invisible: [['sale_warn', 'in', [false, 'no-message']]],

                // 'required':[('sale_warn','!=', False), ('sale_warn','!=','no-message')]
                required: [
                  ['sale_warn', '!=', false],
                  ['sale_warn', '!=', 'no-message']
                ],
                placeholder: 'Type a message...'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_tree_res_partner'
}

const view_search_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_search_res_partner'
}

const action_res_partner = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人(客户设置)',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'res.partner',
  search_view_id: 'view_search_res_partner',
  domain: [],
  context: {
    search_default_customer: 1,
    res_partner_search_mode: 'customer',
    default_is_company: true,
    default_customer_rank: 1
  },
  views: {
    tree: 'view_tree_res_partner',
    form: 'view_form_res_partner'
  }
}

export default {
  view_form_res_partner,
  view_tree_res_partner,
  view_search_res_partner,
  action_res_partner
}
