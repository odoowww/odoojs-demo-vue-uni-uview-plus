import res_partner from './res_partner.js'
import product_template from './product_template.js'
import sale_order from './sale_order.js'
import sale_order_cancel from './sale_order_cancel.js'
import sale_advance_payment_inv from './sale_advance_payment_inv.js'

export default {
  ...res_partner,
  ...product_template,
  ...sale_order,
  ...sale_order_cancel,
  ...sale_advance_payment_inv
}
