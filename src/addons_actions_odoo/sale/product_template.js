const view_form_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_form_product_template',
  model: 'product.template',
  type: 'form',
  arch: {
    button_box: {
      button_action_view_sales: {
        name: 'action_view_sales',
        type: 'object',
        icon: 'fa-signal',
        groups: 'sales_team.group_sale_salesman',
        invisible: [['sale_ok', '=', false]],
        field_sales_count: { widget: 'statinfo' },
        field_uom_name: { invisible: 1 }
      }
    },

    sheet: {
      field_service_type: { widget: 'radio', invisible: true },
      field_visible_expense_policy: { invisible: '1' },

      notebook: {
        page_general_information: {
          group_general_information: {
            group_group_general: {
              field_detailed_type: { help2: 'next extend' },
              field_invoice_policy: { required: '1' },
              field_expense_policy: {
                widget: 'radio',
                // invisible:
                // [['visible_expense_policy', '=', false]]
                invisible: [['visible_expense_policy', '=', false]]
              },
              field_product_tooltip: { string: '' },
              widget_text__product_tooltip_order: {
                attr: {
                  text: 'You can invoice them before they are delivered.',

                  invisible: [
                    '|',
                    ['type', 'not in', ['product', 'consu']],
                    ['invoice_policy', '!=', 'order']
                  ]
                }
              },

              widget_text__product_tooltip_delivery: {
                attr: {
                  text: 'Invoice after delivery, based on quantities delivered, not ordered.',

                  invisible: [
                    '|',
                    ['type', 'not in', ['product', 'consu']],
                    ['invoice_policy', '!=', 'delivery']
                  ]
                }
              }
            }
          }
        },
        page_sales: {
          attr: {
            name: 'sales',
            string: '销售',
            invisible: 0
            //  'invisible':[('sale_ok','=',False)]
            // invisible: [['sale_ok', '=', false]]
          },
          group_sale: {
            attr: { name: 'sale' },
            group_upsell: {
              attr: {
                name: 'upsell',
                string: 'Upsell &amp; Cross-Sell',
                invisible: '1'
              }
            }
          },

          group_desc: {
            group_description: {
              attr: {
                name: 'description',
                string: 'Sales Description'
              },
              field_description_sale: {
                nolabel: 1,
                placeholder: 'This note is added to sales orders and invoices.'
              }
            },

            group_warn: {
              attr: {
                groups: 'sales_team.group_sale_salesman'
              },

              group_: {
                attr: {
                  string: 'Warning when Selling this Product',
                  groups: 'sale.group_warning_sale'
                },
                field_sale_line_warn: { string: 'Warning' },
                field_sale_line_warn_msg: {
                  string: 'Message',
                  required: [['sale_line_warn', '!=', 'no-message']],
                  readonly: [['sale_line_warn', '=', 'no-message']],
                  // invisible: [['sale_line_warn', '=', 'no-message']],
                  placeholder: 'Type a message...'
                }
              }
            }
          }
        },
        page_purchase: {
          attr: { invisible: 1 }
        }
      }
    }
  }
}

const view_tree_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_tree_product_template'
}

const view_search_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'account.view_search_product_template'
}

const action_product_template = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品(销售设置)',
  type: 'ir.actions.act_window',
  res_model: 'product.template',
  search_view_id: 'view_search_product_template',
  domain: [],
  context: {
    search_default_filter_to_sell: 1,
    sale_multi_pricelist_product_template: 1
  },
  views: {
    tree: 'view_tree_product_template',
    form: 'view_form_product_template'
  }
}

export default {
  view_form_product_template,
  view_tree_product_template,
  view_search_product_template,
  action_product_template
}
