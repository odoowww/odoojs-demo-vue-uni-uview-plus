const view_form_hr_expense = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.expense',
  type: 'form',
  arch: {
    sheet: {
      group_name: {
        field_name: {},
        field_shortcut: {}
      }
    },
    header: {
      buttons: {
        _button_action_submit_expenses: {
          name: 'action_submit_expenses',
          type: 'object',
          string: 'Create Report',
          attrs: {
            invisible({ record }) {
              // invisible: [
              //   '|',
              //   ['attachment_number', '<=', 0],
              //   ['sheet_id', '!=', false]
              // ],
              const { attachment_number, sheet_id } = record
              return attachment_number <= 0 || !sheet_id
            }
          },
          class: 'oe_highlight o_expense_submit'
        },
        // _widget_attach_document: {
        //   _attr: {
        //     name: 'attach_document',
        //     string: 'Attach Receipt'
        //     // invisible: [['attachment_number', '<', 1]]
        //   }
        // },
        // _widget_attach_document_778: {
        //   _attr: {
        //     name: 'attach_document',
        //     string: 'Attach Receipt'
        //     // invisible: [['attachment_number', '>=', 1]]
        //   }
        // },
        _button_action_submit_expenses_129: {
          name: 'action_submit_expenses',
          type: 'object',
          string: 'Create Report',

          attrs: {
            invisible({ record }) {
              // invisible: [
              //   '|',
              //   ['attachment_number', '>=', 1],
              //   ['sheet_id', '!=', false]
              // ],
              const { attachment_number, sheet_id } = record
              return attachment_number >= 1 || !sheet_id
            }
          },
          class: 'o_expense_submit'
        },

        _button_action_view_sheet: {
          name: 'action_view_sheet',
          type: 'object',
          string: 'View Report',
          attrs: {
            invisible({ record }) {
              // invisible: [['sheet_id', '=', false]],
              const { sheet_id } = record
              return !sheet_id
            }
          },
          class: 'oe_highlight'
        },
        _button_action_split_wizard: {
          name: 'action_split_wizard',
          type: 'object',
          string: 'Split Expense',

          attrs: {
            invisible({ record }) {
              // invisible: [
              //   '|',
              //   ['sheet_id', '!=', false],
              //   ['product_has_cost', '=', true]
              // ]
              const { sheet_id, product_has_cost } = record
              return !sheet_id || product_has_cost
            }
          }
        }
      },

      fields: {
        state: {
          widget: 'statusbar',
          statusbar_visible: 'draft,reported,approved,done,refused'
        }
      }
    },
    sheet: {
      _div: {
        description: { placeholder: 'Notes...' }
      },
      _div_title: {
        _attr: { class: 'oe_title' },
        _label_name: { for: 'name' },
        _h1: {
          name: {
            attrs: {
              readonly({ record }) {
                //  readonly: [['sheet_is_editable', '=', false]]
                const { sheet_is_editable } = record
                return !sheet_is_editable
              }
            },
            placeholder: 'e.g. Lunch with Customer'
          }
        }
      },
      _group: {
        _group: {
          product_has_cost: { invisible: '1' },
          product_has_tax: { invisible: '1' },
          same_currency: { invisible: '1' },
          is_editable: { invisible: '1' },
          is_ref_editable: { invisible: '1' },
          currency_id: { invisible: '1' },
          company_id: { invisible: '1' },
          company_currency_id: { invisible: '1' },
          amount_tax_company: { invisible: '1' },
          unit_amount: {
            invisible: '1',
            attrs: {
              readonly({ record }) {
                // readonly: [
                //   '|',
                //   ['is_editable', '=', false],
                //   ['product_has_cost', '=', true]
                // ],
                const { is_editable, product_has_cost } = record
                return !is_editable || product_has_cost
              }
            },
            required: '1',
            currency_field: 'currency_id',
            field_digits: true
          },
          attachment_number: { invisible: '1' },
          total_amount_company: { invisible: '1' },
          duplicate_expense_ids: { invisible: '1' },
          sheet_is_editable: { invisible: '1' },
          currency_rate: { invisible: '1' },
          _field_product_id: {
            _label_product_id: { for: 'product_id' },
            _div: {
              product_id: {
                class: 'w-100',
                attrs: {
                  readonly({ record }) {
                    //  readonly: [['sheet_is_editable', '=', false]]
                    const { sheet_is_editable } = record
                    return !sheet_is_editable
                  }
                },
                context: {
                  default_can_be_expensed: 1,
                  tree_view_ref: 'hr_expense.product_product_expense_tree_view',
                  form_view_ref: 'hr_expense.product_product_expense_form_view'
                },
                required: '1'
              },
              _div: {
                _attr: {
                  attrs: {
                    invisible({ record }) {
                      //   invisible: [
                      //     '|',
                      //     ['product_description', '=', false],
                      //     ['product_id', '=', false]
                      //   ],
                      const { product_description, product_id } = record
                      return !product_description || !product_id
                    }
                  },
                  class: 'fst-italic'
                },
                product_description: {}
              }
            }
          },
          unit_amount__2: {
            widget: 'monetary',
            attrs: {
              invisible({ record }) {
                // invisible: [['product_has_cost', '=', false]],
                const { product_has_cost } = record
                return !product_has_cost
              }
            },
            force_save: '1'
          },
          product_uom_category_id: { invisible: '1' },
          _field_quantity: {
            _attr: {
              attrs: {
                invisible({ record }) {
                  // invisible: [['product_has_cost', '=', false]],
                  const { product_has_cost } = record
                  return !product_has_cost
                }
              }
            },
            _label_quantity: {
              for: 'quantity'
            },
            _div_866: {
              _div: {
                _attr: { class: 'o_row' },
                quantity: {
                  attrs: {
                    readonly({ record }) {
                      //  readonly: [['sheet_is_editable', '=', false]]
                      const { sheet_is_editable } = record
                      return !sheet_is_editable
                    }
                  }
                },
                product_uom_id: {
                  force_save: '1',
                  no_open: true,
                  no_create: true,
                  groups: 'uom.group_uom',
                  required: '1'
                }
              },
              total_amount_company: {
                widget: 'monetary',
                currency_field: 'company_currency_id'
              }
            }
          },

          _field_total_amount: {
            _attr: {
              attrs: {
                invisible({ record }) {
                  // invisible: [['product_has_cost', '=', true]]
                  const { product_has_cost } = record
                  return product_has_cost
                }
              }
            },

            _label_total_amount: {
              for: 'total_amount',
              string: 'Total'
            },
            _div_133: {
              _div: {
                _attr: {
                  class: 'o_row'
                },
                total_amount: {
                  widget: 'monetary',
                  class: 'oe_inline',
                  attrs: {
                    readonly({ record }) {
                      //  readonly: [['sheet_is_editable', '=', false]]
                      const { sheet_is_editable } = record
                      return !sheet_is_editable
                    }
                  },
                  currency_field: 'currency_id'
                },
                currency_id: { groups: 'base.group_multi_currency' }
              },
              _div_300: {
                _attr: {
                  attrs: {
                    invisible({ record }) {
                      //   invisible: [
                      //     ['same_currency', '=', true],
                      //     ['product_has_cost', '=', false]
                      //   ],
                      const { same_currency, product_has_cost } = record
                      return same_currency && !product_has_cost
                    }
                  },
                  class: 'o_row'
                },
                total_amount_company: {
                  widget: 'monetary',
                  class: 'oe_inline',
                  currency_field: 'company_currency_id'
                },
                label_convert_rate: { class: 'ps-0' }
              }
            }
          },

          _field_tax_ids: {
            _attr: {
              attrs: {
                invisible({ record }) {
                  //   invisible: [['product_has_tax', '=', false]]
                  const { product_has_tax } = record
                  return !product_has_tax
                }
              }
            },
            _label_tax_ids: {
              for: 'tax_ids'
            },
            _div_776: {
              _attr: {
                class: 'd-flex o_row'
              },
              _div: {
                _attr: { class: 'p-2' },
                tax_ids: {
                  groups:
                    'account.group_account_invoice,account.group_account_readonly',
                  widget: 'many2many_tags',
                  force_save: '1',
                  no_create: true,
                  attrs: {
                    readonly({ record }) {
                      // readonly: [
                      //   '|',
                      //   ['is_editable', '=', false],
                      //   ['product_has_cost', '=', true]
                      // ],
                      const { is_editable, product_has_cost } = record
                      return !is_editable || product_has_cost
                    }
                  },

                  context({ record }) {
                    const { company_id } = record
                    return {
                      default_company_id: company_id,
                      default_type_tax_use: 'purchase',
                      default_price_include: 1
                    }
                  }
                }
              },
              _div_214: {
                _attr: { class: 'd-flex pt-2' },
                _span: {
                  _attr: {
                    class: 'oe_inline o_form_label ms-1 me-1',
                    text: '('
                  }
                },
                amount_tax: {
                  class: 'ps-0'
                },
                _span_852: {
                  _attr: {
                    class: 'oe_inline o_form_label ms-1 me-3',
                    text: ')'
                  }
                }
              }
            }
          },
          employee_id: {
            widget: 'many2one_avatar_employee',
            groups: 'hr_expense.group_hr_expense_team_approver',

            context({ record }) {
              // context: "{'default_company_id': company_id}"
              const { company_id } = record
              return { default_company_id: company_id }
            }
          },

          _field_payment_mode: {
            _attr: {
              attrs: {
                invisible({ record }) {
                  // invisible: [['product_has_cost', '=', true]]
                  const { product_has_cost } = record
                  return product_has_cost
                }
              }
            },
            _label_payment_mode: {
              for: 'payment_mode'
            },
            _div_193: {
              payment_mode: { widget: 'radio' }
            }
          }
        },
        _group_261: {
          reference: {
            groups: 'account.group_account_readonly',
            attrs: {
              invisible({ record }) {
                // invisible: [['product_has_cost', '=', true]]
                const { product_has_cost } = record
                return product_has_cost
              },
              readonly({ record }) {
                //readonly: [['is_ref_editable', '=', false]]
                const { is_ref_editable } = record
                return !is_ref_editable
              }
            }
          },
          date: {
            attrs: {
              readonly({ record }) {
                //  readonly: [['sheet_is_editable', '=', false]]
                const { sheet_is_editable } = record
                return !sheet_is_editable
              }
            }
          },
          accounting_date: {
            groups:
              'account.group_account_invoice,account.group_account_readonly',
            readonly: 0,
            attrs: {
              invisible({ record }) {
                // invisible: [
                //   '|',
                //   ['accounting_date', '=', false],
                //   ['state', 'not in', ['approved', 'done']]
                // ],
                const { accounting_date, state } = record
                return !accounting_date || !['approved', 'done'].includes(state)
              }
            }
          },
          account_id: {
            no_create: true,
            groups: 'account.group_account_readonly',
            attrs: {
              readonly({ record }) {
                // readonly: [
                //   '|',
                //   ['is_editable', '=', false],
                //   ['sheet_is_editable', '=', false]
                // ],
                const { is_editable, sheet_is_editable } = record
                return !is_editable || !sheet_is_editable
              }
            },

            context({ record }) {
              // context: "{'default_company_id': company_id}"
              const { company_id } = record
              return { default_company_id: company_id }
            }
          },
          sheet_id: { invisible: '1' },
          analytic_distribution: {
            widget: 'analytic_distribution',
            groups: 'analytic.group_analytic_accounting',
            attrs: {
              readonly({ record }) {
                //  readonly: [['is_editable', '=', false]],
                const { is_editable } = record
                return !is_editable
              }
            },
            product_field: 'product_id',
            account_field: 'account_id',
            business_domain: 'expense'
          },
          company_id: { groups: 'base.group_multi_company', readonly: '1' }
        }
      }
    }
  }
}

const view_tree_hr_expense = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.expense',
  type: 'tree',
  arch: {
    sheet: {
      is_editable: { invisible: '1' },
      field_company_id: { invisible: '1' },
      field_company_currency_id: { invisible: '1' },
      nb_attachment: { invisible: '1' },
      is_multiple_currency: { invisible: '1' },
      product_has_cost: { invisible: '1' },
      date: { optional: 'show' },
      product_id: { optional: 'hide' },
      name: {},
      employee_id: { widget: 'many2one_avatar_employee' },
      sheet_id: {
        invisible: "not context.get['show_report', False]",
        readonly: '1',
        optional: 'show'
      },
      payment_mode: { optional: 'show' },
      is_editable: { invisible: '1' },
      is_editable: {},
      is_editable: {},
      // attachment_number: { invisible: 'True' },
      // activity_ids: {
      //   widget: 'list_activity',
      //   optional: 'show'
      // },
      accounting_date: {
        groups: 'account.group_account_invoice,account.group_account_readonly',
        readonly: 0,
        optional: 'hide'
      },
      reference: { optional: 'hide' },
      analytic_distribution: {
        widget: 'analytic_distribution',
        optional: 'show'
      },
      account_id: { optional: 'hide' },
      company_id: { optional: 'show' },
      unit_amount_display: {
        string: 'Unit Price',
        widget: 'monetary',
        optional: 'hide',
        currency_field: 'company_currency_id'
      },
      quantity: { optional: 'hide' },
      tax_ids: { widget: 'many2many_tags', optional: 'hide' },
      amount_tax_company: {
        optional: 'hide',
        groups: 'account.group_account_invoice,account.group_account_readonly'
      },
      attachment_number: {
        attrs: {
          invisible({ record }) {
            // invisible: [['attachment_number', '=', 0]],
            const { attachment_number } = record
            return !attachment_number
          }
        },
        class: 'fa fa-paperclip pe-0'
      },
      total_amount_company: {
        widget: 'monetary',
        optional: 'show',
        currency_field: 'company_currency_id'
      },
      total_amount: { groups: 'base.group_multi_currency', optional: 'hide' },
      currency_id: { optional: 'hide' },
      state: { widget: 'badge', readonly: '1', optional: 'show' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_shortcut: {} }
    }
  }
}

const view_search_hr_expense = {
  _odoo_model: 'ir.ui.view',
  model: 'hr.expense',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Expense',
        filter_domain: [
          '|',
          '|',
          ['employee_id', 'ilike', { self: {} }],
          ['name', 'ilike', { self: {} }],
          ['product_id', 'ilike', { self: {} }]
        ]
      },
      //  field_date: {},
      field_employee_id: {},

      filter_my: {
        my_expenses: {
          name: 'my_expenses',
          string: 'My Expenses',
          domain: [['employee_id.user_id', '=', { session_uid: {} }]]
        },
        my_team_expenses: {
          name: 'my_team_expenses',
          string: 'My Team',
          help: 'Expenses of Your Team Member',
          groups: 'hr_expense.group_hr_expense_team_approver',
          domain: [['employee_id.parent_id.user_id', '=', { session_uid: {} }]]
        }
      },
      filter_state: {
        no_report: {
          name: 'no_report',
          string: 'To Report',
          domain: [['sheet_id', '=', false]]
        },
        refused: {
          name: 'refused',
          string: 'Refused',
          help: 'Refused Expenses',
          domain: [['state', '=', 'refused']]
        }
      },
      filter_date: {
        date: { name: 'date', string: 'Expense Date', date: 'date' }
      },
      filter_inactive: {
        inactive: {
          name: 'inactive',
          string: 'Former Employees',
          groups:
            'hr_expense.group_hr_expense_user,hr_expense.group_hr_expense_manager',
          domain: [['employee_id.active', '=', false]]
        }
      }
    }
  }
}

const action_hr_expense = {
  _odoo_model: 'ir.actions.act_window',
  name: '费用报销',
  type: 'ir.actions.act_window',
  res_model: 'hr.expense',
  search_view_id: 'view_search_hr_expense',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_hr_expense',
    form: 'view_form_hr_expense'
  }
}

export default {
  view_form_hr_expense,
  view_tree_hr_expense,
  view_search_hr_expense,
  action_hr_expense,

  model_hr_expense: {
    _odoo_model: 'ir.model',
    fields: {
      // account_id: {
      //   domain({ record }) {
      //     // domain: {
      //     // "[('account_type', 'not in',
      //     // ('asset_receivable','liability_payable','asset_cash','liability_credit_card')),
      //     // ('company_id', '=', company_id)]"
      //     // },
      //     const account_types = [
      //       'asset_receivable',
      //       'liability_payable',
      //       'asset_cash',
      //       'liability_credit_card'
      //     ]
      //     const { account_type, company_id } = record
      //     return (
      //       !account_types.includes(account_type) && company_id == company_id
      //     )
      //   }
      // },
      // accounting_date: {},
      // activity_ids: {},
      // amount_tax: {},
      // amount_tax_company: {},
      // analytic_distribution: {},
      // attachment_number: {},
      // company_currency_id: {},
      // company_id: {},
      // currency_id: {},
      // currency_rate: {},
      // date: {},
      // description: {},
      // duplicate_expense_ids: {},
      // employee_id: {
      //   domain({ record }) {
      //     // ['|', ('company_id', '=', False), ('company_id', '=', company_id)]
      //     const { company_id } = record
      //     return [
      //       '|',
      //       ['company_id', '=', false],
      //       ['company_id', '=', company_id]
      //     ]
      //   }
      // },
      // is_editable: {},
      // is_ref_editable: {},
      // label_convert_rate: {},
      // name: {},
      // payment_mode: {},
      // product_description: {},
      // product_has_cost: {},
      // product_has_tax: {},
      // product_id: {
      //   domain({ record }) {
      //     // domain="[('can_be_expensed', '=', True),
      //     //   '|', ('company_id', '=', False),
      //     //   ('company_id', '=', company_id)]",
      //     const { company_id } = record
      //     return [
      //       ['can_be_expensed', '=', true],
      //       '|',
      //       ['company_id', '=', false],
      //       ['company_id', '=', company_id]
      //     ]
      //   }
      // },
      // product_uom_category_id: {},
      // product_uom_id: {
      //   // domain="[('category_id', '=', product_uom_category_id)]"
      // },
      // quantity: {},
      // reference: {},
      // same_currency: {},
      // sheet_id: {
      //   //   domain="[('employee_id', '=', employee_id), ('company_id', '=', company_id)]",
      // },
      // sheet_is_editable: {},
      // state: {
      //   selection: [
      //     ['draft', 'To Submit'],
      //     ['reported', 'Submitted'],
      //     ['approved', 'Approved'],
      //     ['done', 'Paid'],
      //     ['refused', 'Refused']
      //   ]
      // },
      // tax_ids: {
      //   // domain="[('company_id', '=', company_id), ('type_tax_use', '=', 'purchase')]",
      // },
      // total_amount: {},
      // total_amount_company: {},
      // unit_amount: {},
      // unit_amount_display: {}
    }
  }
}
