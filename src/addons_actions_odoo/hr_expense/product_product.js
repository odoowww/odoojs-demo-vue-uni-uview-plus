const view_form_hr_expense_product_product = {
  _odoo_model: 'ir.ui.view',
  model: 'product.product',
  type: 'form',
  arch: {
    sheet: {
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      field_product_variant_count: { invisible: '1' },
      field_image_1920: { widget: 'image', image_preview: 'image_128' },
      field_name: {},

      field_can_be_expensed: { groups: 'base.group_user', invisible: '1' },

      group_product_details: {
        group_info: {
          attr: { string: 'General Information' },

          field_active: { invisible: '1' },
          field_type: { invisible: '1' },
          field_standard_price: {
            help: "When the cost of an expense product is different than 0, then the user using this product won't be able to change the amount of the expense, only the quantity. Use a cost different than 0 for expense categories funded by the company at fixed cost like allowances for mileage, per diem, accommodation or meal."
          },
          field_uom_id: { groups: 'uom.group_uom' },
          field_uom_po_id: { invisible: '1' },
          field_default_code: {},
          field_categ_id: {},
          field_company_id: { groups: 'base.group_multi_company' }
        },

        group_Accounting: {
          attr: { string: 'Accounting' },
          field_property_account_expense_id: {
            groups: 'account.group_account_readonly'
          },

          field_supplier_taxes_id: { widget: 'many2many_tags' }
        }
      },
      field_description: {}
    }
  }
}

const view_tree_hr_expense_product_product = {
  _odoo_model: 'ir.ui.view',
  model: 'product.product',
  type: 'tree',
  arch: {
    sheet: {
      field_name: {},
      field_default_code: {},
      field_description: {},
      field_lst_price: {},
      field_standard_price: {},
      field_supplier_taxes_id: { widget: 'many2many_tags' }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_description: {} }
    }
  }
}

const action_hr_expense_product_product = {
  _odoo_model: 'ir.actions.act_window',
  name: '费用类型',
  type: 'ir.actions.act_window',
  res_model: 'product.product',
  domain: [['can_be_expensed', '=', true]],
  context: {
    default_can_be_expensed: 1,
    default_detailed_type: 'service'
  },
  views: {
    tree: 'view_tree_hr_expense_product_product',
    form: 'view_form_hr_expense_product_product'
  }
}

export default {
  view_form_hr_expense_product_product,
  view_tree_hr_expense_product_product,
  action_hr_expense_product_product,

  model_product_product: {
    _odoo_model: 'ir.model',
    fields: {}
  }
}
