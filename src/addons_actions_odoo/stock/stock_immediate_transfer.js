export default {
  view_immediate_transfer: {
    _odoo_model: 'ir.ui.view',
    model: 'stock.immediate.transfer',
    type: 'form',
    arch: {
      footer: {
        button_process: {
          name: 'process',
          type: 'object',
          string: 'Apply',
          btn_type: 'primary'
        }
      },

      sheet: {
        group: {
          group: {
            widget_text: '您尚未记录完成的数量, 单击应用, OdooJS将处理所有数量.'
          }
        },

        field_pick_ids: { invisible: '1' },
        field_show_transfers: { invisible: '1' },
        field_immediate_transfer_line_ids: {
          nolabel: 1,

          // invisible: [['show_transfers', '=', false]],
          invisible: [['show_transfers', '=', false]],
          views: {
            tree: {
              arch: {
                sheet: {
                  field_picking_id: {},
                  field_to_immediate: { widget: 'boolean_toggle' }
                }
              }
            },
            form: {
              arch: {
                sheet: {
                  field_picking_id: {},
                  field_to_immediate: { widget: 'boolean_toggle' }
                }
              }
            }
          }
        }
      }
    }
  },

  action_immediate_transfer_wizard: {
    _odoo_model: 'ir.actions.act_window',
    name: 'Immediate Transfer?',
    type: 'ir.actions.act_window',
    res_model: 'stock.immediate.transfer',
    domain: [],

    context: {
      // active_model: 'stock.picking'
      // default_show_transfers: true
    },
    views: {
      form: 'view_immediate_transfer'
    }
  }
}
