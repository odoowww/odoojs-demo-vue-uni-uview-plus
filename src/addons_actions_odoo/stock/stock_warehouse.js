const view_form_stock_warehouse = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.warehouse',
  type: 'form',
  arch: {
    sheet: {
      field_active: { invisible: 1 },
      widget_web_ribbon: {
        attr: {
          name: 'web_ribbon',
          title: 'Archived',
          bg_color: 'bg-danger',
          // invisible': [('active', '=', True)]
          invisible: [['active', '=', true]]
        }
      },

      group: {
        group: {
          field_active: { invisible: '1' },
          field_company_id: { invisible: '1' },
          field_name: { placeholder: 'e.g. Central Warehouse' },
          field_code: { placeholder: 'e.g. CW' }
        },
        group_328: {
          field_company_id: { groups: 'base.group_multi_company' },
          field_partner_id: {}
        }
      },

      notebook: {
        attr: {
          groups: 'stock.group_adv_location,stock.group_stock_multi_warehouses'
        },
        page_warehouse_config: {
          attr: {
            name: 'warehouse_config',
            string: 'Warehouse Configuration'
          },
          group: {
            group: {
              attr: {
                string: 'Shipments',
                groups: 'stock.group_adv_location'
              },
              field_reception_steps: { widget: 'radio' },
              field_delivery_steps: { widget: 'radio' }
            },
            group_group_resupply: {
              attr: {
                name: 'group_resupply',
                string: 'Resupply',
                groups: 'stock.group_stock_multi_warehouses'
              },
              field_resupply_wh_ids: {
                widget: 'many2many_checkboxes',
                groups: 'stock.group_stock_multi_warehouses',
                // domain: [['id', '!=', '<built-in function id>']]
                domain: [['id', '!=', { field_id: {} }]]
              }
            }
          }
        },

        page_technical_info: {
          attr: {
            name: 'technical_info',
            string: 'Technical Information',
            groups: 'base.group_no_one'
          },
          group: {
            group: {
              attr: { string: 'Locations' },
              field_view_location_id: {
                string: 'Warehouse view location',
                required: 0,
                readonly: '1'
              },
              field_lot_stock_id: { required: 0, readonly: '1' },
              field_wh_input_stock_loc_id: { readonly: '1' },
              field_wh_qc_stock_loc_id: { readonly: '1' },
              field_wh_pack_stock_loc_id: { readonly: '1' },
              field_wh_output_stock_loc_id: { readonly: '1' }
            },
            group_574: {
              attr: { string: 'Operation Types' },
              field_in_type_id: { readonly: '1' },
              field_int_type_id: { readonly: '1' },
              field_pick_type_id: { readonly: '1' },
              field_pack_type_id: { readonly: '1' },
              field_out_type_id: { readonly: '1' }
            }
          }
        }
      }
    }
  }
}

const view_tree_stock_warehouse = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.warehouse',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: { widget: 'handle' },
      field_name: {},
      field_active: { invisible: '1' },
      field_lot_stock_id: { groups: 'stock.group_stock_multi_locations' },
      field_partner_id: {},
      field_company_id: { groups: 'base.group_multi_company' }
    },

    kanban: {
      card_title: { field_name: {} },
      card_label: {},
      card_value: { field_lot_stock_id: {} }
    }
  }
}

const view_search_stock_warehouse = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.warehouse',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_stock_warehouse = {
  _odoo_model: 'ir.actions.act_window',
  name: 'Warehouses',
  type: 'ir.actions.act_window',
  res_model: 'stock.warehouse',
  search_view_id: 'view_search_stock_warehouse',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_stock_warehouse',
    form: 'view_form_stock_warehouse'
  }
}

export default {
  view_form_stock_warehouse,
  view_tree_stock_warehouse,
  view_search_stock_warehouse,
  action_stock_warehouse
}
