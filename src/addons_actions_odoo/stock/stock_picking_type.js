const view_form_stock_picking_type = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking.type',
  type: 'form',
  arch: {
    sheet: {
      // _widget: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Archived',
      //     bg_color: 'bg-danger',
      //     attrs: {
      //       // 'invisible': [('active', '=', True)]
      //       'invisible': [['active', '=', true]]
      //     }
      //   }
      // },
      field_name: { placeholder: 'e.g. Receptions' },

      group_first: {
        attr: { name: 'first' },
        group: {
          field_code: {},
          field_active: { invisible: '1' },
          field_company_id: { invisible: '1' },
          field_hide_reservation_method: { invisible: '1' },
          field_sequence_id: { groups: 'base.group_no_one' },
          field_sequence_code: {},
          field_warehouse_id: {
            groups: 'stock.group_stock_multi_warehouses',
            force_save: '1'
          },
          field_reservation_method: {
            widget: 'radio',
            //'invisible': [('hide_reservation_method', '=', True)]
            invisible: [['hide_reservation_method', '=', true]]
          },

          field_auto_show_reception_report: {
            groups: 'stock.group_reception_report',
            // 'invisible': [('code', 'not in', ['incoming', 'internal'])]
            invisible: [['code', 'not in', ['incoming', 'internal']]]
          },

          group_reservation_days_before: {
            attr: {
              // 'invisible':
              // ['|', ('code', '=', 'incoming'),
              // ('reservation_method', '!=', 'by_date')]
              invisible: [
                '|',
                ['code', '=', 'incoming'],
                ['reservation_method', '!=', 'by_date']
              ]
            },
            field_reservation_days_before: {
              string: 'Reserve before scheduled date'
            },
            widget_text_span: 'days before/',
            field_reservation_days_before_priority: {},
            widget_text_span_2: ' days before when starred'
          }
        }
      },

      group_2: {
        field_company_id: { groups: 'base.group_multi_company' },
        field_return_picking_type_id: {
          string: 'Returns Type',
          // "invisible": [("code", "not in",
          //   ["incoming", "outgoing", "internal"])]}'

          invisible: [['code', 'not in', ['incoming', 'outgoing', 'internal']]]
        },
        field_create_backorder: {},
        field_show_operations: {},
        field_show_reserved: {
          // 'invisible': [('code', '!=', 'incoming')]
          invisible: [['code', '!=', 'incoming']]
        }
      }
    },

    group_second: {
      attr: { name: 'second' },
      group: {
        attr: {
          string: 'Lots/Serial Numbers',
          groups: 'stock.group_production_lot',
          name: 'stock_picking_type_lot',
          // {"invisible": [("code", "not in",
          //   ["incoming", "outgoing", "internal"])]}'

          invisible: [['code', 'not in', ['incoming', 'outgoing', 'internal']]]
        },
        field_use_create_lots: { string: 'Create New' },
        field_use_existing_lots: { string: 'Use Existing ones' }
      },
      group_Packages: {
        attr: {
          string: 'Packages',
          groups: 'stock.group_tracking_lot',
          // "invisible": [("code", "not in",
          //   ["incoming", "outgoing", "internal"])]}'
          invisible: [['code', 'not in', ['incoming', 'outgoing', 'internal']]]
        },
        field_show_entire_packs: {}
      },
      group_locations: {
        attr: {
          name: 'locations',
          string: 'Locations',
          groups: 'stock.group_stock_multi_locations'
        },

        field_default_location_src_id: {
          // 'required': [('code', 'in', ('internal', 'outgoing'))]
          required: [['code', 'in', ['internal', 'outgoing']]]
        },
        field_default_location_dest_id: {
          // 'required': [('code', 'in', ('internal', 'incoming'))]
          required: [['code', 'in', ['internal', 'incoming']]]
        }
      }
    }
  }
}

const view_tree_stock_picking_type = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking.type',
  type: 'tree',
  arch: {
    sheet: {
      field_sequence: {},
      field_name: {},
      field_active: { invisible: '1' },
      field_warehouse_id: {},
      field_sequence_id: {},
      field_company_id: {}
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_sequence_id: {} },
      card_value: { field_warehouse_id: {} }
    }
  }
}

const view_search_stock_picking_type = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking.type',
  type: 'search',
  arch: {
    sheet: {
      field_name: {},
      field_warehouse_id: {},
      filter_active: {
        inactive: {
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    },

    groupby: {
      groupby_code: {
        string: 'Type of Operation',
        domain: [],
        context: { group_by: 'code' }
      },
      groupby_warehouse_id: {
        string: 'Warehouse',
        domain: [],
        context: { group_by: 'warehouse_id' }
      }
    }
  }
}

const action_stock_picking_type = {
  _odoo_model: 'ir.actions.act_window',
  name: '移库类型',
  type: 'ir.actions.act_window',
  res_model: 'stock.picking.type',
  domain: [],
  context: {},
  search_view_id: 'view_search_stock_picking_type',
  views: {
    tree: 'view_tree_stock_picking_type',
    form: 'view_form_stock_picking_type'
  }
}

export default {
  view_form_stock_picking_type,
  view_tree_stock_picking_type,
  view_search_stock_picking_type,
  action_stock_picking_type
}
