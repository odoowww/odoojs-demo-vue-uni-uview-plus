import res_partner from './res_partner.js'
import product_category from './product_category.js'
import product_template from './product_template.js'

// import product_product from './product_product.js'

import stock_warehouse from './stock_warehouse.js'
import stock_location from './stock_location.js'
import stock_picking_type from './stock_picking_type.js'

// import stock_move_line from './stock_move_line.js'
// import stock_move from './stock_move.js'

import stock_picking from './stock_picking.js'
import stock_quant from './stock_quant.js'
// import stock_immediate_transfer from './stock_immediate_transfer.js'

export default {
  ...res_partner,
  ...product_category,
  ...product_template,
  // ...product_product,

  ...stock_warehouse,
  ...stock_location,
  ...stock_picking_type,

  // ...stock_move_line,
  // ...stock_move,

  ...stock_picking,
  ...stock_quant
  // ...stock_immediate_transfer
}
