const view_form_stock_quant = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.quant',
  type: 'form',
  arch: {
    sheet: {
      group: {
        group: {
          field_tracking: { invisible: '1' },
          field_company_id__1: { invisible: '1' },
          field_product_id: { readonly: 0, no_create: true },
          field_location_id: { readonly: 0, no_create: true },
          field_lot_id: {
            groups: 'stock.group_production_lot'
            //   readonly: [['tracking', 'not in', ['serial', 'lot']]],
            //   required: [['tracking', '!=', 'none']],
            //   context: {
            //     todo_ctx:
            //       "{'default_product_id': product_id, 'default_company_id': company_id}"
            //   }
          },
          field_package_id: {
            groups: 'stock.group_tracking_lot',
            readonly: 0
          },
          field_owner_id: {
            groups: 'stock.group_tracking_owner',
            readonly: 0,
            no_create: true
          },
          field_company_id: { groups: 'base.group_multi_company' }
        },
        group_335: {
          field_product_uom_id: { groups: 'uom.group_uom' },
          field_quantity: { string: 'Quantity On Hand' },
          field_available_quantity: { string: 'Available Quantity' },
          field_reserved_quantity: { string: 'Quantity Reserved' }
        }
      }
    }
  }
}

const view_tree_stock_quant = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.quant',
  type: 'tree',
  toolbar: {
    action: {},
    print: {}
  },
  arch: {
    sheet: {
      field_product_id: {
        //   invisible: "context.get['single_product', False]"
        invisible: { context_single_product: false }
      },
      field_location_id: {
        //   invisible: "context.get['hide_location', False]"
        invisible: { context_hide_location: false }
      },
      field_lot_id: {
        groups: 'stock.group_production_lot',
        //   invisible: "context.get['hide_lot', False]"
        invisible: { context_hide_lot: false }
      },
      field_package_id: { groups: 'stock.group_tracking_lot' },
      field_owner_id: { groups: 'stock.group_tracking_owner' },
      field_available_quantity: {},
      field_quantity: { string: 'On Hand Quantity' },
      field_product_uom_id: { groups: 'uom.group_uom' },
      field_company_id: { groups: 'base.group_multi_company' }
    },
    kanban: {
      card_title: { field_product_id: {} },
      card_label: { field_location_id: {} },
      card_value: { field_quantity: {}, field_available_quantity: {} }
    }
  }
}

// view_stock_quant_tree_editable: {
//   _odoo_model: 'ir.ui.view',
//   model: 'stock.quant',
//   type: 'tree',
//   arch: {
//     sheet: {
//       create_date: { invisible: '1' },
//       write_date: { invisible: '1' },
//       id: { invisible: '1' },
//       tracking: { invisible: '1' },
//       // company_id: { invisible: '1' },
//       location_id: {
//         //   invisible: "context.get['hide_location', False]",
//         //   readonly: [['id', '!=', false]],
//         no_create: true
//       },
//       storage_category_id: { optional: 'hide' },
//       product_id: {
//         widget: 'many2one'
//         //   readonly: "context.get['single_product', False]",
//         //   force_save: '1',
//         //   no_create: true
//       },
//       product_categ_id: { optional: 'hide' },
//       company_id: { groups: 'base.group_multi_company', optional: 'hidden' },
//       package_id: {
//         groups: 'stock.group_tracking_lot'
//         //   readonly: [['id', '!=', false]]
//       },
//       lot_id: {
//         groups: 'stock.group_production_lot'
//         //   invisible: "context.get['hide_lot', False]",
//         //   readonly: [
//         //     '|',
//         //     ['id', '!=', false],
//         //     ['tracking', 'not in', ['serial', 'lot']]
//         //   ],
//         //   required: [['tracking', '!=', 'none']],
//         //   context: {
//         //     todo_ctx:
//         //       "{'default_product_id': product_id, 'default_company_id': company_id}"
//         //   }
//       },
//       owner_id: {
//         groups: 'stock.group_tracking_owner'
//         //   readonly: [['id', '!=', false]],
//         //   no_create: true
//       },
//       inventory_quantity_auto_apply: {
//         string: 'On Hand Quantity',
//         readonly: 0
//       },
//       _button_action_view_inventory_tree: {
//         _attr: {
//           name: 'action_view_inventory_tree',
//           type: 'action',
//           title: 'Inventory Adjustment',
//           icon: 'fa-pencil',
//           context: {
//             //   todo_ctx:
//             //     "{'search_default_product_id': product_id, 'default_product_id': product_id}"
//           },
//           class: 'btn-link'
//         }
//       },
//       reserved_quantity: { optional: 'show' },
//       product_uom_id: { string: 'Unit', groups: 'uom.group_uom' },
//       _button_action_view_stock_moves: {
//         _attr: {
//           name: 'action_view_stock_moves',
//           type: 'object',
//           string: 'History',
//           icon: 'fa-history',
//           class: 'btn-link'
//         }
//       },
//       _button_action_view_orderpoints: {
//         _attr: {
//           name: 'action_view_orderpoints',
//           type: 'object',
//           string: 'Replenishment',
//           icon: 'fa-refresh',
//           context: {
//             //   todo_ctx:
//             //     "{'default_product_id': product_id, 'search_default_location_id': location_id}"
//           },
//           class: 'btn-link'
//         }
//       }
//     }
//   }
// },

const view_search_stock_quant = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.quant',
  type: 'search',
  arch: {
    sheet: {
      field_product_id: { default: 1 },
      field_location_id: {},
      field_warehouse_id: {},
      field_storage_category_id: {
        groups: 'stock.group_stock_storage_categories'
      },
      field_user_id: {},
      // field_inventory_date: {},
      field_product_categ_id: {},
      field_product_tmpl_id: {},
      field_package_id: { groups: 'stock.group_tracking_lot' },
      field_lot_id: { groups: 'stock.group_production_lot' },
      field_owner_id: { groups: 'stock.group_tracking_owner' },

      filter_location_id: {
        internal_loc: {
          name: 'internal_loc',
          string: 'Internal Locations',
          domain: [['location_id.usage', '=', 'internal']]
        },
        transit_loc: {
          name: 'transit_loc',
          string: 'Transit Locations',
          domain: [['location_id.usage', '=', 'transit']]
        }
      },

      filter_inventory: {
        on_hand: {
          name: 'on_hand',
          string: 'On Hand',
          domain: [['on_hand', '=', true]]
        },
        to_count: {
          name: 'to_count',
          string: 'To Count',
          // domain="[('inventory_date', '&lt;=', context_today().strftime('%Y-%m-%d'))]
          domain: [['inventory_date', '<=', { datetime_today: {} }]]
        },
        to_apply: {
          name: 'to_apply',
          string: 'To Apply',
          domain: [['inventory_quantity_set', '=', true]]
        },
        priority_products: {
          name: 'priority_products',
          string: 'Starred Products',
          domain: [['priority', '=', 1]]
        }
      },

      filter_negative: {
        negative: {
          name: 'negative',
          string: 'Negative Stock',
          domain: [['quantity', '<', 0.0]]
        },
        reserved: {
          name: 'reserved',
          string: 'Reservations',
          domain: [['reserved_quantity', '>', 0.0]]
        }
      },

      filter_in_date: {
        filter_in_date: { name: 'filter_in_date', date: 'in_date' }
      },

      filter_my_count: {
        my_count: {
          name: 'my_count',
          string: 'My Counts',
          domain: [['user_id', '=', { session_uid: {} }]]
        }
      }
    },

    groupby: {
      productgroup: {
        string: 'Product',
        domain: [],
        context: { group_by: 'product_id' }
      },
      locationgroup: {
        string: 'Location',
        domain: [],
        context: { group_by: 'location_id' }
      },
      storagecategorygroup: {
        string: 'Storage Category',
        domain: [],
        context: { group_by: 'storage_category_id' }
      },

      owner: {
        groups: 'stock.group_tracking_owner',
        string: 'Owner',
        domain: [],
        context: { group_by: 'owner_id' }
      },
      Lot_Serial_number: {
        groups: 'stock.group_production_lot',
        string: 'Lot/Serial Number',
        domain: [],
        context: { group_by: 'lot_id' }
      },
      package: {
        groups: 'stock.group_tracking_lot',
        string: 'Package',
        domain: [],
        context: { group_by: 'package_id' }
      },
      company: {
        groups: 'base.group_multi_company',
        string: 'Company',
        domain: [],
        context: { group_by: 'company_id' }
      }
      //
    }
  }
}

const action_stock_quant = {
  _odoo_model: 'ir.actions.act_window',
  name: '库存',
  res_model: 'stock.quant',
  buttons: { create: false, edit: false, delete: false },
  search_view_id: 'view_search_stock_quant',
  // domain: [['location_id.usage', 'in', ['internal', 'transit']]],
  order: 'location_id, product_id',
  context: {
    // search_default_internal_loc: 1,
    // search_default_productgroup: 1,
    // search_default_locationgroup: 1,
    // inventory_mode: true
  },
  views: {
    //   tree: 'view_stock_quant_tree_editable',
    tree: 'view_tree_stock_quant',
    form: 'view_form_stock_quant'
  }
}

//   location_open_quants: {
//     _odoo_model: 'ir.actions.act_window',
//     name: 'Current Stock',
//     res_model: 'stock.quant',
//     search_view_id: 'quant_search_view',
//     domain: "[['location_id', 'child_of', active_ids]]",
//     context: {
//       search_default_productgroup: 1
//     },
//     views: {
//       tree: 'view_stock_quant_tree',
//       form: '=======todo=========='
//     }
//   }

export default {
  view_form_stock_quant,
  view_tree_stock_quant,
  view_search_stock_quant,
  action_stock_quant
}
