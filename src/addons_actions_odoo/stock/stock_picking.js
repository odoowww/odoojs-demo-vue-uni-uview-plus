const move_ids_without_package_tree = {
  arch: {
    sheet: {
      field_company_id: { invisible: '1' },
      field_name: { invisible: '1' },
      field_state: { invisible: '1' },
      field_picking_type_id: { invisible: '1' },
      field_location_id: { invisible: '1' },
      field_location_dest_id: { invisible: '1' },
      field_partner_id: { invisible: '1' },
      field_scrapped: { invisible: '1' },
      field_picking_code: { invisible: '1' },
      field_product_type: { invisible: '1' },
      field_show_details_visible: { invisible: '1' },
      // field_show_reserved_availability: { invisible: '1' },
      field_show_operations: { invisible: '1' },
      field_additional: { invisible: '1' },
      field_move_lines_count: { invisible: '1' },
      field_product_uom_category_id: { invisible: '1' },
      field_has_tracking: { invisible: '1' },
      field_display_assign_serial: { invisible: '1' },
      field_product_id: {},
      field_description_picking: { optional: 'hide' },
      field_date: { optional: 'hide' },
      field_date_deadline: { optional: 'hide' },
      field_is_initial_demand_editable: { invisible: '1' },
      field_is_quantity_done_editable: { invisible: '1' },
      field_product_packaging_id: {
        // groups: 'product.group_stock_packaging'
      },
      field_product_uom_qty: {
        string: 'Demand'
        // 'column_invisible':
        // [('parent.immediate_transfer', '=', True)],
        // column_invisible: [['parent.immediate_transfer', '=', true]]
      },

      // _button_action_product_forecast_report: {
      //   _attr: {
      //     type: 'object',
      //     name: 'action_product_forecast_report',
      //     title: 'Forecast Report',
      //     icon: 'fa-area-chart',
      //    attrs: {
      //       // 'invisible': ['|',
      //       // ('forecast_availability', '&lt;', 0), '|',
      //       // ('parent.immediate_transfer', '=', True), '&amp;',
      //       // ('parent.picking_type_code', '=', 'outgoing'),
      //       // ('state', '!=', 'draft')]
      //     invisible({}) {
      //     }
      // }
      //   }
      // },

      // _button_action_product_forecast_report2: {
      //   _attr: {
      //     type: 'object',
      //     name: 'action_product_forecast_report',
      //     title: 'Forecast Report',
      //     icon: 'fa-area-chart text-danger',
      //   attrs: {
      //       // 'invisible': ['|',
      //       // ('forecast_availability', '&lt;', 0), '|',
      //       // ('parent.immediate_transfer', '=', True), '&amp;',
      //       // ('parent.picking_type_code', '=', 'outgoing'),
      //       // ('state', '!=', 'draft')]

      //       //  'invisible': ['|', ('forecast_availability', '&gt;=', 0), '|',
      //       // ('parent.immediate_transfer', '=', True), '&amp;',
      //       // ('parent.picking_type_code', '=', 'outgoing'),
      //       // ('state', '!=', 'draft')]}"/>

      //     invisible({ record }) {

      //       const { forecast_availability, parent: prt, state } = record

      //       return (
      //         forecast_availability >= 0 ||
      //         prt.immediate_transfer ||
      //         (prt.picking_type_code === 'outgoing' && state !== 'draft')
      //       )
      //     }
      // }
      //   }
      // },

      field_forecast_expected_date: { invisible: '1' },

      field_forecast_availability: {
        string: 'Reserved',
        widget: 'forecast_widget'
        //'column_invisible': ['|', '|',
        // ('parent.state', 'in', ['draft', 'done']),
        // ('parent.picking_type_code', '!=', 'outgoing'),
        // ('parent.immediate_transfer', '=', True)]}"
        // column_invisible: [
        //   '|',
        //   '|',
        //   ['parent.state', 'in', ['draft', 'done']],
        //   ['parent.picking_type_code', '!=', 'outgoing'],
        //   ['parent.immediate_transfer', '=', true]
        // ]
      },
      // field_reserved_availability: {
      //   string: 'Reserved',
      //   attrs: {
      //     // 'column_invisible': ['|', '|',
      //     // ('parent.state', 'in', ['draft', 'done']),
      //     // ('parent.picking_type_code', 'in', ),
      //     // ('parent.immediate_transfer', '=', True)]
      //     // column_invisible: [
      //     //   '|',
      //     //   '|',
      //     //   ['parent.state', 'in', ['draft', 'done']],
      //     //   ['parent.picking_type_code', 'in', ['incoming', 'outgoing']],
      //     //   ['parent.immediate_transfer', '=', true]
      //     // ]
      //   }
      // },
      // field_product_qty: { invisible: '1', readonly: '1' },
      // field_quantity_done: {
      //   string: 'Done',
      //   attrs: {
      //     // 'column_invisible':[('parent.state', '=', 'draft'),
      //     //  ('parent.immediate_transfer', '=', False)]
      //     // column_invisible: [
      //     //   ['parent.state', '=', 'draft'],
      //     //   ['parent.immediate_transfer', '=', false]
      //     // ]
      //   }
      // },
      field_product_uom: {},
      field_lot_ids: {
        groups: 'stock.group_production_lot',
        widget: 'many2many_tags',
        optional: 'hide',
        //'invisible': ['|', ('show_details_visible', '=', False),
        // ('has_tracking', '!=', 'serial')]}"

        invisible: [
          '|',
          ['show_details_visible', '=', false],
          ['has_tracking', '!=', 'serial']
        ]
        //  context="{
        // 'default_company_id': company_id,
        // 'default_product_id': product_id,
        // 'active_picking_id': parent.id
        // }"
      }

      // button_action_show_details: {
      //   attr: {
      //     type: 'object',
      //     name: 'action_show_details',
      //     title: 'Details',
      //     icon: 'fa-list',
      //     attrs: {
      //         // 'invisible': [('show_details_visible', '=', False)]}"
      //         // options='{"warn": true}'/>
      //       invisible({ record }) {
      //         const { show_details_visible } = record
      //         return !show_details_visible
      //       }
      //     }
      //   }
      // },
      // button_action_assign_serial: {
      //   attr: {
      //     type: 'object',
      //     name: 'action_assign_serial',
      //     title: 'Assign Serial Numbers',
      //     icon: 'fa-plus-square',
      //     role: 'img',
      //     attrs: {
      //         // 'invisible': ['|', ('', '=', False),
      //         // ('show_operations', '=', False)]}"/>
      //       invisible({ record }) {
      //         const { display_assign_serial, show_operations } = record
      //         return !display_assign_serial || !show_operations
      //       }
      //     }
      //   }
      // }
    },

    kanban: {
      card_title: {
        field_product_id: { string: '产品' }
      },
      card_label: { field_location_id: { string: '源库位' } },
      card_value: {
        field_product_uom: { string: '单位' },
        //  field_location_qty_available: { string: '在手数量' },
        field_product_uom_qty: { string: '保留数量' }
        // field_quantity_done: { string: '完成数量' }
      }
    }
  }
}

const move_ids_without_package_form = {
  arch: {
    sheet: {
      field_state: {},
      field_product_uom_category_id: { invisible: '1' },
      field_additional: { invisible: '1' },
      field_move_lines_count: { invisible: '1' },
      field_company_id: { invisible: '1' },
      field_product_id: {},
      field_is_initial_demand_editable: { invisible: '1' },
      field_is_quantity_done_editable: { invisible: '1' },
      field_product_uom_qty: {
        string: 'Demand'
        // 'column_invisible':
        // [('parent.immediate_transfer', '=', True)],
        // column_invisible: [['parent.immediate_transfer', '=', true]]
      },
      // field_reserved_availability: {
      //   // 'invisible': (['|','|',
      //   // ('parent.state','=', 'done'),
      //   // ('parent.picking_type_code', 'in', ['outgoing', 'incoming']),
      //   // ('parent.immediate_transfer', '=', True)])

      //   string: 'Reserved',
      //   attrs: {
      //     // 'column_invisible': ['|', '|',
      //     // ('parent.state', 'in', ['draft', 'done']),
      //     // ('parent.picking_type_code', 'in', ['incoming', 'outgoing'] ),
      //     // ('parent.immediate_transfer', '=', True)]
      //     // column_invisible: [
      //     //   '|',
      //     //   '|',
      //     //   ['parent.state', 'in', ['draft', 'done']],
      //     //   ['parent.picking_type_code', 'in', ['incoming', 'outgoing']],
      //     //   ['parent.immediate_transfer', '=', true]
      //     // ]
      //   }
      // },
      // field_product_qty: { invisible: '1', readonly: '1' },

      field_forecast_expected_date: { invisible: '1' },

      field_forecast_availability: {
        // string="Reserved" attrs="{

        string: 'Reserved',
        widget: 'forecast_widget'
        //'column_invisible': ['|', '|',
        // ('parent.state', 'in', ['draft', 'done']),
        // ('parent.picking_type_code', '!=', 'outgoing'),
        // ('parent.immediate_transfer', '=', True)]}"
        // column_invisible: [
        //   '|',
        //   '|',
        //   ['parent.state', 'in', ['draft', 'done']],
        //   ['parent.picking_type_code', '!=', 'outgoing'],
        //   ['parent.immediate_transfer', '=', true]
        // ]
      },

      // field_quantity_done: {
      //   string: 'Done',
      //   attrs: {
      //     // 'column_invisible':[('parent.state', '=', 'draft'),
      //     //  ('parent.immediate_transfer', '=', False)]
      //     // column_invisible: [
      //     //   ['parent.state', '=', 'draft'],
      //     //   ['parent.immediate_transfer', '=', false]
      //     // ]
      //     // to check
      //     // for tree view. 'readonly': [('product_id', '=', False)],
      //     // for form view. 'readonly': [('is_quantity_done_editable', '=', False)]
      //   }
      // },
      field_product_uom: {},
      field_description_picking: { string: 'Description' }
    }
  }
}

const view_form_stock_picking = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking',

  title: { field_name: {} },

  rules: {},

  arch: {
    button_box: {
      field_has_scrap_move: { invisible: '1' },
      field_has_tracking: { invisible: '1' }
      // button_action_see_move_scrap: {
      //   _attr: {
      //     name: 'action_see_move_scrap',
      //     string: 'Scraps',
      //     type: 'object',
      //     icon: 'fa-arrows-v',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': [('has_scrap_move', '=', False)]
      //         const { has_scrap_move } = record
      //         return !has_scrap_move
      //       }
      //     }
      //   }
      // },
      // _button_action_see_packages: {
      //   _attr: {
      //     name: 'action_see_packages',
      //     string: 'Packages',
      //     type: 'object',
      //     icon: 'fa-cubes',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': [('has_packages', '=', False)]
      //         const { has_packages } = record
      //         return !has_packages
      //       }
      //     }
      //   }
      // },
      // _button_action_stock_report: {
      //   _attr: {
      //     name: 'action_stock_report',
      //     string: 'Traceability',
      //     groups: 'stock.group_production_lot',
      //     type: 'action',
      //     icon: 'fa-arrow-up',
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': ['|', ('state', '!=', 'done'),
      //         // ('has_tracking', '=', False)]
      //         const { state, has_tracking } = record
      //         return state !== 'done' || !has_tracking
      //       }
      //     }
      //   }
      // },
      // _button_action_view_reception_report: {
      //   _attr: {
      //     name: 'action_view_reception_report',
      //     string: 'Allocation',
      //     groups: 'stock.group_reception_report',
      //     type: 'object',
      //     icon: 'fa-list',
      //     context({ record }) {
      //       // context="{'default_picking_ids': [id]}"
      //       const { id: res_id } = record
      //       return { default_picking_ids: [res_id] }
      //     },
      //     attrs: {
      //       invisible({ record }) {
      //         //'invisible': [('show_allocation', '=', False)]
      //         const { show_allocation } = record
      //         return !show_allocation
      //       }
      //     }
      //   }
      // },
      // _button_action_picking_move_tree: {
      //   _attr: {
      //     help2: 'Use the following button to avoid onchange on one2many',
      //     help: 'List view of operations',
      //     name: 'action_picking_move_tree',
      //     string: 'Allocation',
      //     groups: 'base.group_no_one',
      //     type: 'object',
      //     icon: 'fa-arrows-v',
      //     context({ record }) {
      //       // context="{'picking_type_code': picking_type_code,
      //       // 'default_picking_id': id,
      //       // 'form_view_ref':'stock.view_move_form',
      //       // 'address_in_id': partner_id,
      //       // 'default_picking_type_id': picking_type_id,
      //       // 'default_location_id': location_id,
      //       // 'default_location_dest_id': location_dest_id}
      //       const {
      //         picking_type_code,
      //         id: res_id,
      //         partner_id,
      //         picking_type_id,
      //         location_id,
      //         location_dest_id
      //       } = record
      //       return {
      //         picking_type_code: picking_type_code,
      //         default_picking_id: res_id,
      //         form_view_ref: 'stock.view_move_form',
      //         address_in_id: partner_id,
      //         default_picking_type_id: picking_type_id,
      //         default_location_id: location_id,
      //         default_location_dest_id: location_dest_id
      //       }
      //     }
      //   },
      //   _span: 'Operations'
      // }
    },

    header: {
      button_action_confirm: {
        name: 'action_confirm',
        string: '标记为待办',
        type: 'object',
        btn_type: 'primary',
        groups: 'base.group_user'
        // invisible':
        // [('show_mark_as_todo', '=', False)]}"
        // invisible: [['show_mark_as_todo', '=', false]]
      },

      button_action_assign: {
        name: 'action_assign',
        string: '检查可用量',
        type: 'object',
        btn_type: 'primary',
        groups: 'base.group_user',
        // 'invisible':
        // [('show_check_availability', '=', False)]
        invisible: [['show_check_availability', '=', false]]
      },

      button_validate: {
        name: 'button_validate',
        string: 'Validate',
        type: 'object',
        btn_type: 'primary',
        groups: 'stock.group_stock_user',
        //  'invisible':
        // ['|', ('state', 'in', ('waiting','confirmed')),
        // ('show_validate', '=', False)]}"
        // invisible: [
        //   '|',
        //   ['state', 'in', ['waiting', 'confirmed']],
        //   ['show_validate', '=', false]
        // ]

        action_map: {
          'stock.immediate.transfer': 'stock.action_immediate_transfer_wizard',
          'stock.backorder.confirmation':
            'stock.action_backorder_confirmation_wizard'
        }
      },

      button_validate2: {
        name: 'button_validate',
        string: 'Validate',
        type: 'object',
        groups: 'stock.group_stock_user'
        //'invisible':
        // ['|', ('state', 'not in', ('waiting', 'confirmed')),
        // ('show_validate', '=', False)]
        // invisible: [
        //   '|',
        //   ['state', 'not in', ['waiting', 'confirmed']],
        //   ['show_validate', '=', false]
        // ]
      },

      button_action_cancel: {
        name: 'action_cancel',
        string: 'Cancel',
        type: 'object',
        groups: 'base.group_user',
        help: 'If the picking is unlocked you can edit initial demand (for a draft picking) or done quantities (for a done picking).',

        // 'invisible': [('state', 'not in',
        // ('assigned', 'confirmed', 'draft', 'waiting'))]
        invisible: 1
        // invisible: [
        //   ['state', 'not in', ['assigned', 'confirmed', 'draft', 'waiting']]
        // ]
      },

      button__todo: {
        invisible: 1
        // action_set_quantities_to_reservation: {
        //   name: 'action_set_quantities_to_reservation',
        //   string: 'Set quantities',
        //   type: 'object',
        //   groups: 'stock.group_stock_user',
        //   attrs: {
        //       // 'invisible': [('show_set_qty_button', '=', False)]
        //     invisible({ record }) {
        //       const { show_set_qty_button } = record
        //       return !show_set_qty_button
        //     }
        //   }
        // },
        // action_clear_quantities_to_zero: {
        //   name: 'action_clear_quantities_to_zero',
        //   string: 'Clear quantities',
        //   type: 'object',
        //   groups: 'stock.group_stock_user',
        //   attrs: {
        //       // 'invisible': [('show_clear_qty_button', '=', False)]
        //     invisible({ record }) {
        //       const { show_clear_qty_button } = record
        //       return !show_clear_qty_button
        //     }
        //   }
        // },
        // _widget_signature: {
        //   name: 'signature',
        //   string: 'Sign',
        //   btn_type: 'primary',
        //   full_name: 'partner_id',
        //   groups: 'stock.group_stock_sign_delivery',
        //     // 'invisible': ['|', '|', ('id', '=', False),
        //     // ('picking_type_code', '!=', 'outgoing'),
        //     // ('state', '!=', 'done')]
        //   invisible({ record }) {
        //     const { id: res_id, picking_type_code, state } = record
        //     return (
        //       !res_id || picking_type_code !== 'outgoing' || state !== 'done'
        //     )
        //   }
        // },
        // _widget_signature2: {
        //   name: 'signature',
        //   string: 'Sign',
        //   btn_type: 'primary',
        //   full_name: 'partner_id',
        //   groups: 'stock.group_stock_sign_delivery',
        //   invisible({ record }) {
        //     //'invisible': ['|', '|', ('id', '=', False),
        //     // ('picking_type_code', '!=', 'outgoing'),
        //     // ('state', '=', 'done')]
        //     const { id: res_id, picking_type_code, state } = record
        //     return (
        //       !res_id || picking_type_code !== 'outgoing' || state === 'done'
        //     )
        //   }
        //   }
        // },
        // do_print_picking: {
        //   name: 'do_print_picking',
        //   string: 'Print',
        //   type: 'object',
        //   groups: 'stock.group_stock_user',
        //   attrs: {
        //     //'invisible': [('state', '!=', 'assigned')]
        //     invisible: [['state', '!=', 'assigned']]
        //   }
        // },
        // action_open_label_type: {
        //   name: 'action_open_label_type',
        //   string: 'Print Labels',
        //   type: 'object'
        // },
        // action_report_delivery: {
        //   name: 'action_report_delivery',
        //   string: 'Print',
        //   type: 'action',
        //   groups: 'base.group_user',
        //   attrs: {
        //     //'invisible': [('state', '!=', 'done')]
        //     invisible: [['state', '!=', 'done']]
        //   }
        // },
        // act_stock_return_picking: {
        //   name: 'act_stock_return_picking',
        //   string: 'Return',
        //   type: 'action',
        //   groups: 'base.group_user',
        //   attrs: {
        //     //'invisible': [('state', '!=', 'done')]
        //     invisible: [['state', '!=', 'done']]
        //   }
        // },
        // do_unreserve: {
        //   name: 'do_unreserve',
        //   string: 'Unreserve',
        //   type: 'object',
        //   groups: 'base.group_user',
        //   attrs: {
        //     //  'invisible': ['|', '|', '|',
        //     // ('picking_type_code', '=', 'incoming'),
        //     // ('immediate_transfer', '=', True), '&amp;',
        //     // ('state', '!=', 'assigned'), ('move_type', '!=', 'one'),
        //     // '&amp;', ('state', 'not in', ('assigned', 'confirmed')),
        //     // ('move_type', '=', 'one')]
        //     invisible: [
        //       '|',
        //       '|',
        //       '|',
        //       ['picking_type_code', '=', 'incoming'],
        //       ['immediate_transfer', '=', true],
        //       '&',
        //       ['state', '!=', 'assigned'],
        //       ['move_type', '!=', 'one'],
        //       '&',
        //       ['state', 'not in', ['assigned', 'confirmed']],
        //       ['move_type', '=', 'one']
        //     ]
        //   }
        // },
        // button_scrap: {
        //   name: 'button_scrap',
        //   string: 'Scrap',
        //   type: 'object',
        //   attrs: {
        //     // 'invisible': ['|',
        //     // '&amp;',
        //     // ('picking_type_code', '=', 'incoming'),
        //     // ('state', '!=', 'done'),
        //     // '&amp;',
        //     // ('picking_type_code', '=', 'outgoing'),
        //     // ('state', '=', 'done')]
        //     invisible: [
        //       '|',
        //       '&',
        //       ['picking_type_code', '=', 'incoming'],
        //       ['state', '!=', 'done'],
        //       '&',
        //       ['picking_type_code', '=', 'outgoing'],
        //       ['state', '=', 'done']
        //     ]
        //   }
        // },
        // action_toggle_is_locked: {
        //   name: 'action_toggle_is_locked',
        //   string: 'Unlock',
        //   type: 'object',
        //   groups: 'stock.group_stock_manager',
        //   help: 'If the picking is unlocked you can edit initial demand (for a draft picking) or done quantities (for a done picking).',
        //   attrs: {
        //     // 'invisible':
        //     // ['|', ('state', 'in', ('draft','cancel')),
        //     // ('is_locked', '=', False)]
        //     invisible: [
        //       '|',
        //       ['state', 'in', ['draft', 'cancel']],
        //       ['is_locked', '=', false]
        //     ]
        //   }
        // },
        // action_toggle_is_locked2: {
        //   name: 'action_toggle_is_locked',
        //   string: 'Lock',
        //   type: 'object',
        //   groups: 'stock.group_stock_manager',
        //   help: 'If the picking is unlocked you can edit initial demand (for a draft picking) or done quantities (for a done picking).',
        //   attrs: {
        //     //'invisible': [('is_locked', '=', True)]
        //     invisible: [['is_locked', '=', true]]
        //   }
        // }
      },

      field_state: {
        widget: 'statusbar',
        statusbar_visible: 'draft,confirmed,assigned,done'
      }
    },

    sheet: {
      field_state: { invisible: '1' },

      field_is_locked: { invisible: '1' },
      // field_show_mark_as_todo: { invisible: '1' },
      field_show_check_availability: { invisible: '1' },
      // field_show_validate: { invisible: '1' },
      field_show_lots_text: { invisible: '1' },
      // field_immediate_transfer: { invisible: '1' },
      field_picking_type_code: { invisible: '1' },
      field_hide_picking_type: { invisible: '1' },
      field_show_operations: { invisible: '1' },
      field_show_allocation: { invisible: '1' },
      field_show_reserved: { invisible: '1' },
      field_move_line_exist: { invisible: '1' },
      field_has_packages: { invisible: '1' },
      field_picking_type_entire_packs: { invisible: '1' },
      field_use_create_lots: { invisible: '1' },
      field_show_set_qty_button: { invisible: '1' },
      field_show_clear_qty_button: { invisible: '1' },
      field_company_id: { invisible: '1' },

      field_priority: {
        widget: 'priority',
        // 'invisible': [('name','=','/')]
        invisible: [['name', '=', '/']]
      },

      field_name: {
        string: '名称',
        // 'invisible': [('name','=','/')]
        invisible: [['name', '=', '/']]
      },

      group: {
        group: {
          field_partner_id: {
            // string: 'Delivery Address', //outgoing
            // string: 'Receive From', // incoming
            // string: 'Contact', // not in: incoming,outgoing
            //

            label_outgoing: {
              for: 'partner_id',
              string: 'Delivery Address',
              invisible: [['picking_type_code', '!=', 'outgoing']]
            },
            label_incoming: {
              for: 'partner_id',
              string: 'Receive From',
              invisible: [['picking_type_code', '!=', 'incoming']]
            },

            label_Contact: {
              for: 'partner_id',
              string: 'Contact',
              invisible: [['picking_type_code', 'in', ['incoming', 'outgoing']]]
            }
          },
          field_picking_type_id: {
            string: '分拣类型',
            // 'invisible': [('hide_picking_type', '=', True)]
            invisible: [['hide_picking_type', '=', true]],
            // 'readonly': [('state', '!=', 'draft')]
            readonly: [['state', '!=', 'draft']]
          },
          field_location_id: {
            string: '源库位',
            // 'invisible': [('picking_type_code', '=', 'incoming')]
            invisible: [['picking_type_code', '=', 'incoming']]
          },
          field_location_dest_id: {
            string: '目标库位',
            // 'invisible': [('picking_type_code', '=', 'outgoing')]
            invisible: [['picking_type_code', '=', 'outgoing']]
          },
          field_backorder_id: {
            // 'invisible': [('backorder_id','=',False)]
            invisible: [['backorder_id', '=', false]]
          }
        },

        group_2: {
          field_scheduled_date: {
            // 'required': [('id', '!=', False)]
            required: [['id', '!=', false]]
          },
          // field_json_popover: {
          //   // widget: 'stock_rescheduling_popover',
          //   attrs: {
          //     // invisible: [['json_popover', '=', false]]
          //     invisible: [['json_popover', '=', false]]
          //   }
          // },
          field_date_deadline: {
            // invisible: ['|', ['state', 'in', ('done', 'cancel')], ['date_deadline', '=', false]]
            invisible: [
              '|',
              ['state', 'in', ['done', 'cancel']],
              ['date_deadline', '=', false]
            ]
          },
          field_products_availability_state: { invisible: '1' },
          field_products_availability: {
            // invisible: ['|', ['picking_type_code', '!=', 'outgoing'], ['state', 'not in', ['confirmed', 'waiting', 'assigned']]]
            invisible: [
              '|',
              ['picking_type_code', '!=', 'outgoing'],
              ['state', 'not in', ['confirmed', 'waiting', 'assigned']]
            ]
          },
          field_date_done: {
            string: 'Effective Date',
            //   invisible: [['state', '!=', 'done']]
            invisible: [['state', '!=', 'done']]
          },
          field_origin: { placeholder: 'e.g. PO0032' },
          field_owner_id: {
            groups: 'stock.group_tracking_owner',
            // invisible: [['picking_type_code', '!=', 'incoming']]
            invisible: [['picking_type_code', '!=', 'incoming']]
          }
        }
      },

      notebook: {
        page_detailed_operations: {
          attr: {
            string: 'Detailed Operations',
            name: 'detailed_operations',
            //'invisible': [('show_operations', '=', False)]]
            invisible: [['show_operations', '=', false]]
          },
          // field_move_line_nosuggest_ids: {
          //   attrs: {
          //     // 'invisible': [('show_reserved', '=', True)]
          //     invisible: [['show_reserved', '=', true]],
          //     // 'readonly': ['|', '|',
          //     // ('show_operations', '=', False),
          //     // ('state', '=', 'cancel'),
          //     // '&amp;', ('state', '=', 'done'),
          //     // ('is_locked', '=', True)],

          //     readonly: [
          //       '|',
          //       '|',
          //       ['show_operations', '=', false],
          //       ['state', '=', 'cancel'],
          //       '&',
          //       ['state', '=', 'done'],
          //       ['is_locked', '=', true]
          //     ]
          //   },
          //   // context="{'tree_view_ref': 'stock.view_stock_move_line_detailed_operation_tree',
          //   // 'default_picking_id': id,
          //   // 'default_location_id': location_id,
          //   // 'default_location_dest_id': location_dest_id,
          //   // 'default_company_id': company_id}
          //   context: {
          //     // 'tree_view_ref': 'stock.view_stock_move_line_detailed_operation_tree',
          //     default_picking_id: { field_id: {} },
          //     default_location_id: { field_location_id: {} },
          //     default_location_dest_id: { field_location_dest_id: {} },
          //     default_company_id: { field_company_id: {} }
          //   }
          // },

          field_move_line_ids_without_package: {
            // 'invisible': [('show_reserved', '=', False)]
            invisible: [['show_reserved', '=', false]],

            //  'readonly': ['|', '|',
            // ('show_operations', '=', False),
            // ('state', '=', 'cancel'), '&amp;',
            // ('state', '=', 'done'), ('is_locked', '=', True)]

            readonly: [
              '|',
              '|',
              ['show_operations', '=', false],
              ['state', '=', 'cancel'],
              '&',
              ['state', '=', 'done'],
              ['is_locked', '=', true]
            ],
            // context="{'tree_view_ref': 'stock.view_stock_move_line_detailed_operation_tree',
            // 'default_picking_id': id,
            // 'default_location_id': location_id,
            // 'default_location_dest_id': location_dest_id,
            // 'default_company_id': company_id}

            context: {
              // tree_view_ref: 'stock.view_stock_move_line_detailed_operation_tree',

              default_picking_id: { field_id: {} },
              default_location_id: { field_location_id: {} },
              default_location_dest_id: { field_location_dest_id: {} },
              default_company_id: { field_company_id: {} }
            },

            views: {
              tree: {
                arch: {
                  sheet: {
                    field_product_id: { string: '产品' },
                    field_location_id: { string: '源库位' },

                    // // field_product_customer_code: { string: '产品客户代码' },
                    // // field_packaging_id: { string: '外包装' },
                    // field_product_uom_qty: { string: '需求数量' }
                    // // field_quantity_num: { string: '件数' },
                    // field_reserved_availability: { string: '已保留' },
                    // field_qty_done: { string: '完成数量' },
                    field_product_uom_id: { string: '单位' }
                  },
                  kanban: {
                    card_title: { field_product_id: {} },
                    card_label: { field_location_id: {} },
                    card_value: {
                      // field_qty_done: {},
                      field_product_uom_id: {}
                    }
                  }
                }
              },

              form: {
                arch: {
                  sheet: {
                    field_location_id: { string: '源库位' },
                    field_product_id: { string: '产品' },

                    // field_product_customer_code: { string: '产品客户代码' },
                    // field_packaging_id: { string: '外包装' },
                    // field_product_uom_qty: { string: '需求数量' },
                    // field_quantity_num: { string: '件数' },
                    // field_reserved_availability: { string: '已保留' },
                    // field_qty_done: { string: '完成数量' },
                    field_product_uom_id: { string: '单位' }
                  }
                }
              }
            }
          },

          field_package_level_ids_details: {
            // 'invisible': ['|',
            // ('picking_type_entire_packs', '=', False),
            // ('show_operations', '=', False)]
            invisible: [
              '|',
              ['picking_type_entire_packs', '=', false],
              ['show_operations', '=', false]
            ],

            // 'readonly': [('state', '=', 'done')],
            readonly: [['state', '=', 'done']],
            // context="{'default_location_id': location_id,
            // 'default_location_dest_id': location_dest_id,
            // 'default_company_id': company_id}"
            context: {
              default_location_id: { field_location_id: {} },
              default_location_dest_id: { field_location_dest_id: {} },
              default_company_id: { field_company_id: {} }
            }
          }

          // button_action_put_in_pack: {
          //   attr: {
          //     btn_type: 'primary',
          //     name: 'action_put_in_pack',
          //     type: 'object',
          //     string: 'Put in Pack',
          //     groups: 'stock.group_tracking_lot',
          //     attrs: {
          //       // 'invisible': [('state', 'in', ('draft', 'done', 'cancel'))]
          //       'invisible': [['state', 'in', ['draft', 'done', 'cancel']]]
          //
          //     }
          //   }
          // }
        },

        page_operations: {
          attr: { string: 'Operations', name: 'operations' },
          field_move_ids_without_package: {
            // add-label="Add a Product">
            // widget: 'stock_move_one2many',

            //'readonly': ['&amp;', ('state', '=', 'done'),
            // ('is_locked', '=', True)]
            readonly: ['&', ['state', '=', 'done'], ['is_locked', '=', true]],
            // context="{
            // 'default_company_id': company_id,
            // 'default_date': scheduled_date,
            // 'default_date_deadline': date_deadline,
            // 'picking_type_code': picking_type_code,
            // 'default_picking_id': id,
            // 'form_view_ref':'stock.view_move_form',
            // 'address_in_id': partner_id,
            // 'default_picking_type_id': picking_type_id,
            // 'default_location_id': location_id,
            // 'default_location_dest_id': location_dest_id,
            // 'default_partner_id': partner_id}"

            //   context="{
            // 'default_company_id': company_id,
            // 'default_date': scheduled_date,
            // 'default_date_deadline': date_deadline,
            // 'picking_type_code': picking_type_code,
            // 'default_picking_id': id,
            // 'form_view_ref':'stock.view_move_form',
            // 'address_in_id': partner_id,
            // 'default_picking_type_id': picking_type_id,
            // 'default_location_id': location_id,
            // 'default_location_dest_id': location_dest_id,
            // 'default_partner_id': partner_id
            // }
            context: {
              default_company_id: { field_company_id: {} },
              default_date: { field_scheduled_date: {} },
              default_date_deadline: { field_date_deadline: {} },
              picking_type_code: { field_picking_type_code: {} },
              default_picking_id: { field_id: {} },
              // form_view_ref: 'stock.view_move_form',
              address_in_id: { field_partner_id: {} },
              default_picking_type_id: { field_picking_type_id: {} },
              default_location_id: { field_location_id: {} },
              default_location_dest_id: { field_location_dest_id: {} },
              default_partner_id: { field_partner_id: {} }
            },

            views: {
              tree: move_ids_without_package_tree,

              form: move_ids_without_package_form
            }
          },

          field_package_level_ids: {
            // 'invisible': ['|',
            // ('picking_type_entire_packs', '=', False),
            // ('show_operations', '=', True)]

            invisible: [
              '|',
              ['picking_type_entire_packs', '=', false],
              ['show_operations', '=', true]
            ],

            // 'readonly': [('state', '=', 'done')],
            readonly: [['state', '=', 'done']]

            // context="{
            // 'default_location_id': location_id,
            // 'default_location_dest_id': location_dest_id,
            // 'default_company_id': company_id}"
          }

          // button_action_put_in_pack: {
          //   attr: {
          //     btn_type: 'primary',
          //     name: 'action_put_in_pack',
          //     type: 'object',
          //     string: 'Put in Pack',
          //     groups: 'stock.group_tracking_lot',
          //     attrs: {
          //       //  'invisible': [('state', 'in', ('draft', 'done', 'cancel'))]
          //       invisible: [['state', 'in', ['draft', 'done', 'cancel']]]
          //     }
          //   }
          // }
        },

        page_extra: {
          attr: { string: 'Other Information', name: 'extra' },

          group: {
            group_other_infos: {
              attr: { string: 'Other Information', name: 'other_infos' },
              field_picking_type_code: { invisible: '1' },
              field_move_type: {
                // 'invisible': [('picking_type_code', '=', 'incoming')]
                invisible: [['picking_type_code', '=', 'incoming']]
              },
              field_user_id: {},
              field_group_id: { groups: 'base.group_no_one' },
              field_company_id: {
                groups: 'base.group_multi_company',
                force_save: '1'
              }
            }
          }
        },

        page_note: {
          attr: { string: 'Note', name: 'note' },
          field_note: {
            string: 'Note',
            placeholder:
              'Add an internal note that will be printed on the Picking Operations sheet'
          }
        }
      }
    }
  }
}

const view_tree_stock_picking = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking',

  arch: {
    sheet: {
      field_company_id__1: { invisible: '1' },
      field_priority: { optional: 'show', widget: 'priority' },
      field_name: {},
      field_location_id: {
        string: 'From',
        groups: 'stock.group_stock_multi_locations',
        optional: 'show'
      },
      field_location_dest_id: {
        string: 'To',
        groups: 'stock.group_stock_multi_locations',
        optional: 'show'
      },
      field_partner_id: { optional: 'show' },
      field_is_signed: {
        string: 'Signed',
        optional: 'hide',
        groups: 'stock.group_stock_sign_delivery'
      },
      field_user_id: { optional: 'hide', widget: 'many2one_avatar_user' },
      field_scheduled_date: {
        optional: 'show',
        widget: 'remaining_days',

        // 'invisible':[('state', 'in', ('done', 'cancel'))]
        invisible: [['state', 'in', ['done', 'cancel']]]
      },
      field_picking_type_code: { invisible: '1' },
      field_products_availability_state: { invisible: '1' },
      field_products_availability: {
        optional: 'hide',

        //'invisible':
        // ['|', ('picking_type_code', '!=', 'outgoing'),
        // ('state', 'not in',
        // ['confirmed', 'waiting', 'assigned'])]

        invisible: [
          '|',
          ['picking_type_code', '!=', 'outgoing'],
          ['state', 'not in', ['confirmed', 'waiting', 'assigned']]
        ]

        // decoration-success="state == 'assigned' or products_availability_state == 'available'"
        // decoration-warning="state != 'assigned' and products_availability_state in ('expected', 'available')"
        // decoration-danger="state != 'assigned' and products_availability_state == 'late'"/>
      },
      field_date_deadline: {
        optional: 'hide',
        widget: 'remaining_days',

        // 'invisible':[('state', 'in', ('done', 'cancel'))]
        invisible: [['state', 'in', ['done', 'cancel']]]
      },
      field_date_done: {
        string: 'Effective Date',
        optional: 'hide'
      },
      field_origin: { optional: 'show' },
      field_backorder_id: { optional: 'hide' },
      field_picking_type_id: { optional: 'hide' },
      field_company_id: {
        groups: 'base.group_multi_company',
        optional: 'show'
      },
      field_state: {
        optional: 'show',
        widget: 'badge'
        // decoration-danger="state=='cancel'"
        // decoration-info="state== 'assigned'"
        // decoration-muted="state == 'draft'"
        // decoration-success="state == 'done'"
        // decoration-warning="state not in ('draft','cancel','done','assigned')"/>
      },
      // activity_exception_decoration: { widget: 'activity_exception' },
      field_json_popover: {
        widget: 'stock_rescheduling_popover',
        nolabel: '1',
        // 'invisible': [('json_popover', '=', False)]}"
        invisible: [['json_popover', '=', false]]
      }
    },
    kanban: {
      // card_title: { field_name: {} },
      // card_label: { field_partner_id: {} },
      // card_value: { field_state: {}, field_date: {} },

      card_title: { field_name: {} },
      card_label: { field_location_id: {}, field_location_dest_id: {} },
      card_value: { field_state: {} }
    }
  }
}

const view_search_stock_picking = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.picking',
  type: 'search',
  arch: {
    sheet: {
      field_name: {
        string: 'Transfer',
        filter_domain: [
          '|',
          ['name', 'ilike', { self: {} }],
          ['origin', '=like', { self: {} }]
        ]
      },
      field_partner_id: {
        filter_domain: [['partner_id', 'child_of', { self: {} }]]
      },
      field_origin: {},
      field_product_id: {},
      field_picking_type_id: {},
      field_move_line_ids: {
        string: 'Package',
        groups: 'stock.group_tracking_lot',
        filter_domain: [
          '|',
          ['move_line_ids.package_id.name', 'ilike', { self: {} }],
          ['move_line_ids.result_package_id.name', 'ilike', { self: {} }]
        ]
      },
      filter_me: {
        to_do_transfers: {
          string: 'To Do',
          domain: [['user_id', 'in', [{ env: { uid: {} } }, false]]]
        },

        my_transfers: {
          string: 'My Transfers',

          domain: [['user_id', '=', { env: { uid: {} } }]]
        },
        starred: {
          string: 'Starred',
          domain: [['priority', '=', '1']]
        }
      },
      filter_state: {
        draft: {
          name: 'draft',
          string: 'Draft',
          domain: [['state', '=', 'draft']],
          help: 'Draft Moves'
        },
        waiting: {
          name: 'waiting',
          string: 'Waiting',
          domain: [['state', 'in', ['confirmed', 'waiting']]],
          help: 'Waiting Moves'
        },
        available: {
          name: 'available',
          string: 'Ready',
          domain: [['state', '=', 'assigned']],
          help: 'Assigned Moves'
        },
        done: {
          name: 'done',
          string: 'Done',
          domain: [['state', '=', 'done']],
          help: 'Pickings already processed'
        },
        cancel: {
          name: 'cancel',
          string: 'Cancelled',
          domain: [['state', '=', 'cancel']],
          help: 'Cancelled Moves'
        }
      },

      filter_date: {
        late: {
          name: 'late',
          string: 'Late',
          help: 'Deadline exceed or/and by the scheduled',

          domain: [
            ['state', 'in', ['assigned', 'waiting', 'confirmed']],
            '|',
            '|',
            ['has_deadline_issue', '=', true],
            ['date_deadline', '<', { datetime_today: {} }],
            ['scheduled_date', '<', { datetime_today: {} }]
          ]
        },

        planning_issues: {
          name: 'planning_issues',
          string: 'Planning Issues',
          help: 'Transfers that are late on scheduled time or one of pickings will be late',

          // ('scheduled_date', '&lt;', time.strftime('%Y-%m-%d %H:%M:%S')),
          domain: [
            '|',
            ['delay_alert_date', '!=', false],
            '&',
            ['scheduled_date', '<', { datetime_today: {} }],
            ['state', 'in', ['assigned', 'waiting', 'confirmed']]
          ]
        }
      },

      filter_backorder: {
        backorder: {
          name: 'backorder',
          string: 'Backorders',
          domain: [
            ['backorder_id', '!=', false],
            ['state', 'in', ['assigned', 'waiting', 'confirmed']]
          ],
          help: 'Remaining parts of picking partially processed'
        }
      }
    },

    groupby: {
      status: {
        string: 'Status',
        domain: [],
        context: { group_by: 'state' }
      },
      expected_date: {
        string: 'Scheduled Date',
        domain: [],
        context: { group_by: 'scheduled_date' }
      },
      origin: {
        string: 'Source Document',
        domain: [],
        context: { group_by: 'origin' }
      },
      picking_type: {
        string: 'Operation Type',
        domain: [],
        context: { group_by: 'picking_type_id' }
      }
    }
  }
}

const action_stock_picking_internal = {
  _odoo_model: 'ir.actions.act_window',
  name: '调拨',
  type: 'ir.actions.act_window',
  res_model: 'stock.picking',
  search_view_id: 'view_search_stock_picking',
  domain: [['picking_type_id.code', '=', 'internal']],
  order: 'date desc',
  context: {
    contact_display: 'partner_address',
    default_company_id: { session_current_company_id: {} }
  },

  views: {
    tree: 'view_tree_stock_picking',
    form: 'view_form_stock_picking'
  }
}

const action_stock_picking_incoming = {
  _odoo_model: 'ir.actions.act_window',
  name: '调拨',
  type: 'ir.actions.act_window',
  res_model: 'stock.picking',
  search_view_id: 'view_search_stock_picking',
  domain: [['picking_type_id.code', '=', 'incoming']],
  order: 'date desc',
  context: {
    contact_display: 'partner_address',
    default_company_id: { session_current_company_id: {} }
  },

  views: {
    tree: 'view_tree_stock_picking',
    form: 'view_form_stock_picking'
  }
}

const action_stock_picking_outgoing = {
  _odoo_model: 'ir.actions.act_window',
  name: '出库单',
  type: 'ir.actions.act_window',
  res_model: 'stock.picking',
  search_view_id: 'view_search_stock_picking',
  domain: [['picking_type_id.code', '=', 'outgoing']],
  order: 'date desc',
  context: {
    contact_display: 'partner_address',
    default_company_id: { session_current_company_id: {} }
  },

  views: {
    tree: 'view_tree_stock_picking',
    form: 'view_form_stock_picking'
  }
}

export default {
  view_form_stock_picking,
  view_tree_stock_picking,
  view_search_stock_picking,
  action_stock_picking_internal,
  action_stock_picking_incoming,
  action_stock_picking_outgoing
}
