const view_form_stock_location = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.location',
  type: 'form',
  arch: {
    sheet: {
      field_company_id: { invisible: '1' },
      // _div_button_box: {
      //   _attr: { name: 'button_box', class: 'oe_button_box' },
      //   _button_location_open_putaway: {
      //     _attr: {
      //       name: 'location_open_putaway',
      //       type: 'action',
      //       string: 'Putaway Rules',
      //       icon: 'fa-random',
      //       groups: 'stock.group_stock_multi_locations',
      //       context: {
      //         // todo_ctx: "{'default_company_id': company_id}"
      //       },
      //       class: 'oe_stat_button'
      //     }
      //   },
      //   _button_location_open_quants: {
      //     _attr: {
      //       name: 'location_open_quants',
      //       type: 'action',
      //       string: 'Current Stock',
      //       icon: 'fa-cubes',
      //       class: 'oe_stat_button'
      //     }
      //   }
      // },
      // _widget_web_ribbon: {
      //   _attr: {
      //     name: 'web_ribbon',
      //     title: 'Archived',
      //     bg_color: 'bg-danger'
      //     // invisible: [['active', '=', true]]
      //   }
      // },
      field_name: { placeholder: 'e.g. Spare Stock' },
      field_location_id: { placeholder: 'e.g. Physical Locations' },

      group: {
        group_additional_info: {
          attr: { name: 'additional_info', string: 'Additional Information' },
          field_active: { invisible: '1' },
          field_usage: {},
          field_storage_category_id: {
            groups: 'stock.group_stock_storage_categories',
            // invisible: [['usage', '!=', 'internal']]
            invisible: [['usage', '!=', 'internal']]
          },
          field_company_id: { groups: 'base.group_multi_company' },
          field_scrap_location: {
            invisible: [['usage', 'not in', ['inventory', 'internal']]]
          },
          field_return_location: {},
          field_replenish_location: {
            // invisible: [['usage', '!=', 'internal']]
            invisible: [['usage', '!=', 'internal']]
          }
        },
        group: {
          attr: {
            string: 'Cyclic Counting',
            invisible: [
              '|',
              ['usage', 'not in', ['internal', 'transit']],
              ['company_id', '=', false]
            ]
          },
          field_cyclic_inventory_frequency: {},
          field_last_inventory_date: {},
          field_next_inventory_date: {
            invisible: [['active', '=', false]]
          }
        },
        group_231: {
          attr: { string: 'Logistics', groups: 'stock.group_adv_location' },
          field_removal_strategy_id: {}
        }
      },
      field_comment: { placeholder: 'External note...' }
    }
  }
}

const view_tree_stock_location = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.location',
  type: 'tree',
  arch: {
    sheet: {
      // company_id: { invisible: '1' },
      field_active: { invisible: '1' },
      field_complete_name: { string: 'Location' },
      field_usage: {},
      field_storage_category_id: {
        groups: 'stock.group_stock_storage_categories',
        readonly: [['usage', '!=', 'internal']]
      },
      field_company_id: { groups: 'base.group_multi_company' }
    },
    kanban: {
      card_title: { field_complete_name: {} },
      card_label: {},
      card_value: { field_usage: {} }
    }
  }
}

const view_search_stock_location = {
  _odoo_model: 'ir.ui.view',
  model: 'stock.location',
  type: 'search',
  arch: {
    sheet: {
      field_complete_name: { _default: 1, string: 'Stock Locations' },
      field_location_id: { string: 'Parent Location' },
      filter_usage: {
        in_location: {
          name: 'in_location',
          string: 'Internal',
          domain: [['usage', '=', 'internal']]
        },
        customer: {
          name: 'customer',
          string: 'Customer',
          domain: [['usage', '=', 'customer']]
        },
        prod_inv_location: {
          name: 'prod_inv_location',
          string: 'Production',
          domain: [['usage', 'in', ['inventory', 'production']]]
        },
        supplier: {
          name: 'supplier',
          string: 'Vendor',
          domain: [['usage', '=', 'supplier']]
        }
      },
      filter_active: {
        inactive: {
          name: 'inactive',
          string: 'Archived',
          domain: [['active', '=', false]]
        }
      }
    }
  }
}

const action_stock_location = {
  _odoo_model: 'ir.actions.act_window',
  name: '库位',
  type: 'ir.actions.act_window',
  res_model: 'stock.location',
  search_view_id: 'view_search_stock_location',
  context: {
    // search_default_in_location: 1
  },
  views: {
    tree: 'view_tree_stock_location',
    form: 'view_form_stock_location'
  }
}

export default {
  view_form_stock_location,
  view_tree_stock_location,
  view_search_stock_location,
  action_stock_location
}
