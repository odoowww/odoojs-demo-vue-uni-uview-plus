const view_form_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'base.view_form_res_partner',
  model: 'res.partner',
  type: 'form',
  title: { field_name: {} },
  arch: {
    sheet: {
      notebook: {
        page_contact_addresses: {},
        page_sales_purchases: {
          attr: { name: 'sales_purchases', string: '销售和采购' },

          group_container_row_2: {
            attr: { name: 'container_row_2' },
            group_sale: {
              attr: { invisible: 1 }
            },
            group_purchase: {
              attr: { invisible: 1 }
            }
          },

          group_container_row_stock: {
            attr: {
              name: 'container_row_stock',
              groups: 'base.group_no_one'
            },
            group_inventory: {
              attr: { name: 'inventory', string: 'Inventory' },
              field_property_stock_customer: {},
              field_property_stock_supplier: {}
            }
          }
        },

        page_internal_notes: {
          group_slot: {},
          group_stock: {
            attr: { groups: 'stock.group_warning_stock' },
            group: {
              widget_separator: 'Warning on the Picking',
              field_picking_warn: { required: '1' },
              field_picking_warn_msg: {
                required: [
                  ['picking_warn', '!=', false],
                  ['picking_warn', '!=', 'no-message']
                ],

                invisible: [['picking_warn', 'in', [false, 'no-message']]],
                placeholder: 'Type a message...'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'base.view_tree_res_partner'
}

const view_search_res_partner = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'base.view_search_res_partner'
}

const action_res_partner = {
  _odoo_model: 'ir.actions.act_window',
  name: '联系人(库存设置)',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'res.partner',
  search_view_id: 'view_search_res_partner',
  domain: [],
  context: {
    default_is_company: true,
    default_type: 'contact',
    search_default_type_company: 1,
    search_default_type_person: 1
  },
  views: {
    tree: 'view_tree_res_partner',
    form: 'view_form_res_partner'
  }
}

export default {
  view_form_res_partner,
  view_tree_res_partner,
  view_search_res_partner,
  action_res_partner
}
