const view_form_product_category = {
  _odoo_model: 'ir.ui.view',
  model: 'product.category',
  type: 'form',
  arch: {
    button_box: {
      button_category_open_putaway: {
        invisible: 1,
        name: 'category_open_putaway',
        type: 'action',
        groups: 'stock.group_stock_multi_locations',
        string: 'Putaway Rules'
      }
    },

    sheet: {
      group_first: {},
      group: {
        group_logistics: {
          attr: { name: 'logistics', string: 'Logistics' },
          field_route_ids: {
            widget: 'many2many_tags',
            groups: 'stock.group_adv_location'
          },
          field_total_route_ids: {
            widget: 'many2many_tags',
            groups: 'stock.group_adv_location',
            invisible: [['parent_id', '=', false]]
          },
          field_removal_strategy_id: {},
          field_packaging_reserve_method: {
            widget: 'radio',
            groups: 'product.group_stock_packaging'
          }
        }
      }
    }
  }
}

const view_tree_product_category = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_tree_product_category'
}

const view_search_product_category = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_search_product_category'
}

const action_product_category = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品类别(库存设置)',
  type: 'ir.actions.act_window',
  buttons: { create: true, edit: true, delete: false },
  res_model: 'product.category',
  search_view_id: 'view_search_product_category',
  domain: [],
  context: {},
  views: {
    tree: 'view_tree_product_category',
    form: 'view_form_product_category'
  }
}

export default {
  view_form_product_category,
  view_tree_product_category,
  view_search_product_category,
  action_product_category
}
