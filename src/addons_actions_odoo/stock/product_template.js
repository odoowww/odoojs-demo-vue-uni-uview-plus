const view_form_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_form_product_template',
  model: 'product.template',
  type: 'form',
  arch: {
    sheet: {
      notebook: {
        page_general_information: {},
        page_inventory: {
          attr: {
            name: 'inventory',
            string: 'Inventory',
            // groups: 'stock.group_stock_user,product.group_stock_packaging',

            // 'invisible':[('type', '=', 'service')]
            invisible: [['type', '=', 'service']]
          },

          group_inventory: {
            field_has_available_route_ids: { invisible: '1' },
            group_operations: {
              attr: {
                invisible: 1,
                name: 'operations',
                string: 'Operations'
              },
              field_route_ids: {
                widget: 'many2many_checkboxes',
                invisible: [
                  '|',
                  ['has_available_route_ids', '=', false],
                  ['type', '=', 'service']
                ]
              },

              button_action_open_routes: {
                name: 'action_open_routes',
                type: 'action',
                string: 'View Diagram',
                icon: 'fa-arrow-right',
                invisible: [['type', 'not in', ['product', 'consu']]],
                context: {
                  // default_product_tmpl_id: 'todo===id'
                }
              },

              field_route_from_categ_ids: {
                widget: 'many2many_tags',
                invisible: [['route_from_categ_ids', '=', []]]
              }
            },

            group_group_lots_and_weight: {
              attr: {
                name: 'group_lots_and_weight',
                string: 'Logistics',
                // 'invisible': [('type', 'not in', ['product', 'consu'])
                invisible: [['type', 'not in', ['product', 'consu']]]
              },

              field_sale_delay: {
                invisible: [['sale_ok', '=', false]]
              },

              widget_text: 'days',

              group_weight: {
                attr: {
                  // 'invisible':[('product_variant_count', '>', 1),
                  // ('is_product_variant', '=', False)]

                  invisible: [
                    ['product_variant_count', '>', 1],
                    ['is_product_variant', '=', false]
                  ]
                },
                field_weight: {},
                field_weight_uom_name: {}
              },

              group_volume: {
                attr: {
                  // 'invisible':[('product_variant_count', '>', 1),
                  // ('is_product_variant', '=', False)]

                  invisible: [
                    ['product_variant_count', '>', 1],
                    ['is_product_variant', '=', false]
                  ]
                },
                field_volume: { string: 'Volume' },
                field_volume_uom_name: {}
              }
            },

            group_traceability: {
              attr: {
                name: 'traceability',
                string: 'Traceability',
                groups: 'stock.group_production_lot',
                invisible: [['type', '=', 'consu']]
              },
              field_tracking: {
                widget: 'radio',
                invisible: [['type', '=', 'service']]
              }
            },

            group_stock_property: {
              attr: {
                name: 'stock_property',
                string: 'Counterpart Locations',
                groups: 'base.group_no_one'
              },
              field_property_stock_production: {},
              field_property_stock_inventory: {}
            }
          },

          group_packaging: {
            attr: {
              invisible: 1,
              name: 'packaging',
              string: 'Packaging',
              groups: 'product.group_stock_packaging'

              // 'invisible':[
              // '|',
              // ('type', 'not in', ['product', 'consu']),
              // ('product_variant_count', '>', 1),
              // ('is_product_variant', '=', False)]

              // invisible: [
              //   '|',
              //   ['type', 'not in', ['product', 'consu']],
              //   ['product_variant_count', '>', 1],
              //   ['is_product_variant', '=', false]
              // ]
            },

            field_packaging_ids: {
              nolabel: '1'
              // context({ record }) {
              //   const { company_id } = record
              //   return {
              //     // tree_view_ref: 'product.product_packaging_tree_view2',
              //     default_company_id: company_id
              //   }
              // }
            }
          },

          group_desc: {
            group_desc: {
              attr: {
                string: 'Description for Receipts'
              },
              field_description_pickingin: {
                placeholder:
                  'This note is added to receipt orders (e.g. where to store the product in the warehouse).'
              }
            },
            group_683: {
              attr: {
                string: 'Description for Delivery Orders'
              },
              field_description_pickingout: {
                placeholder: 'This note is added to delivery orders.'
              }
            },
            group_111: {
              attr: {
                string: 'Description for Internal Transfers',
                groups: 'stock.group_stock_multi_locations'
              },
              field_description_picking: {
                placeholder:
                  'This note is added to internal transfer orders (e.g. where to pick the product in the warehouse).'
              }
            }
          }
        }
      }
    }
  }
}

const view_tree_product_template = {
  _odoo_model: 'ir.ui.view',
  inherit_id: 'product.view_tree_product_template',
  model: 'product.template',
  type: 'tree',
  arch: {
    sheet: {
      field_name: { help2: 'next extend' },
      field_default_code: { optional: 'show' },

      field_responsible_id: {
        widget: 'many2one_avatar_user'
      },

      field_type: { invisible: '1', help2: 'next extend' },
      field_show_on_hand_qty_status_button: { invisible: '1' },
      field_qty_available: {
        // invisible: [['show_on_hand_qty_status_button', '=', false]],
        optional: 'show'
      },
      field_virtual_available: {
        // invisible: [['show_on_hand_qty_status_button', '=', false]],
        optional: 'show'
      }
    },
    kanban: {
      card_title: { field_name: {} },
      card_label: { field_default_code: {} },
      card_value: { field_barcode: {} }
    }
  }
}

const view_search_product_template = {
  _odoo_model: 'ir.ui.view',
  model: 'product.template',
  inherit_id: 'product.view_search_product_template',
  arch: {
    sheet: {
      field_location_id: {
        // filter_domain: [],
        context: {
          // todo_ctx: "{'location': self}"
        }
      },
      field_warehouse_id: {
        // filter_domain: [],
        context: {
          // todo_ctx: "{'warehouse': self}"
        }
      },

      filter_real_stock: {
        real_stock_available: {
          name: 'real_stock_available',
          string: 'Available Products',
          domain: [['qty_available', '>', 0]]
        },
        real_stock_negative: {
          name: 'real_stock_negative',
          string: 'Negative Forecasted Quantity',
          domain: [['virtual_available', '<', 0]]
        }
      }
    }
  }
}

const action_product_template = {
  _odoo_model: 'ir.actions.act_window',
  name: '产品(库存设置)',
  type: 'ir.actions.act_window',
  res_model: 'product.template',
  search_view_id: 'view_search_product_template',
  domain: [],
  context: {
    search_default_consumable: 1,
    search_default_products: 1,
    default_detailed_type: 'product'
  },
  views: {
    tree: 'view_tree_product_template',
    form: 'view_form_product_template'
  }
}

export default {
  view_form_product_template,
  view_tree_product_template,
  view_search_product_template,
  action_product_template
}
