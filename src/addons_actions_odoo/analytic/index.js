import account_analytic_account from './account_analytic_account.js'
import account_analytic_distribution_model from './account_analytic_distribution_model.js'
import account_analytic_line from './account_analytic_line.js'
import account_analytic_plan from './account_analytic_plan.js'

export default {
  // ...account_analytic_account,
  // ...account_analytic_distribution_model,
  // ...account_analytic_line,
  // ...account_analytic_plan
}
