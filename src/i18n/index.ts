import { createI18n } from 'vue-i18n'

import zh_CN from './zh_CN'
import en_US from './en_US'

export enum LANGUAGE{
  zh_CN = 'zh_CN',
  en_US = 'en_US'
}


const messages = {
  zh_CN,
  en_US
};


export const i18n = new createI18n({
  locale: LANGUAGE.zh_CN,
  fallbackLocale: LANGUAGE.en_US,
  messages
});

function setLocale(lang:LANGUAGE) {
    i18n.global.locale = lang
}

  
export default{
  t:i18n.global.t,
  setLocale
};
