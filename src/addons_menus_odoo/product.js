export default {
  menu_product: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '产品',
    icon: 'star',
    sequence: 24,
    children: {
      menu_product_divider_config: {
        sequence: 2,
        name: '设置',
        action: 'divider'
      },

      menu_action_product_tag: {
        name: '产品标签',
        sequence: 2,
        action: 'product.action_product_tag'
      },
      menu_action_uom_category: {
        name: '度量单位类别',
        sequence: 2,
        action: 'uom.action_uom_category'
      },
      menu_action_uom_uom: {
        name: '度量单位',
        sequence: 2,
        action: 'uom.action_uom_uom'
      },

      menu_action_product_pricelist: {
        name: '价格表',
        sequence: 2,
        action: 'product.action_product_pricelist'
      },

      menu_action_res_country_group: {
        name: '国家组价格表设置',
        sequence: 2,
        action: 'product.action_res_country_group'
      },

      menu_product_divider: {
        sequence: 11,
        name: '管理',
        action: 'divider'
      },

      menu_action_product_category: {
        name: '产品类别',
        sequence: 11,
        action: 'product.action_product_category'
      },

      menu_action_product_template: {
        name: '产品',
        sequence: 20,
        action: 'product.action_product_template'
      }

      // menu_attribute_action: {
      //   name: '产品属性',
      //   action: 'product.attribute_action'
      // }
    }
  }
}
