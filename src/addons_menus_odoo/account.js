export default {
  menu_account_action_res_partner: {
    _odoo_model: 'ir.ui.menu',
    parent: 'contacts.menu_contacts',
    name: '联系人(开票设置)',
    icon: 'star',
    sequence: 30,
    action: 'account.action_res_partner'
  },

  menu_account_action_product_category: {
    _odoo_model: 'ir.ui.menu',
    parent: 'product.menu_product',
    name: '产品类别(开票设置)',
    sequence: 30,
    action: 'account.action_product_category'
  },

  menu_account_action_product_template: {
    _odoo_model: 'ir.ui.menu',
    parent: 'product.menu_product',
    name: '产品(开票设置)',
    sequence: 30,
    action: 'account.action_product_template'
  },

  // menu_account_action_product_template_sale: {
  //   _odoo_model: 'ir.ui.menu',
  //   parent: 'product.menu_product',
  //   name: '销售产品(开票设置)',
  //   sequence: 23,
  //   action: 'account.action_product_template_sale'
  // },

  // menu_account_action_product_template_purchase: {
  //   _odoo_model: 'ir.ui.menu',
  //   parent: 'product.menu_product',
  //   name: '采购产品(开票设置)',
  //   sequence: 24,
  //   action: 'account.action_product_template_purchase'
  // },

  todo_menu_analytic_setting: {
    name: 'Analytic Config',
    children: {
      // menu_action_analytic_distribution_model: {
      //   name: 'Analytic Distribution Models',
      //   action: 'analytic.action_analytic_distribution_model'
      // },
      // menu_action_account_analytic_account_form: {
      //   name: 'Analytic Accounts',
      //   action: 'analytic.action_account_analytic_account_form'
      // },
      // menu_account_analytic_plan_action: {
      //   name: 'Analytic Plan',
      //   action: 'analytic.account_analytic_plan_action'
      // }
    }
  },

  menu_account_move: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '开票',
    icon: 'star',
    sequence: 33,
    children: {
      menu_account_divider_config: {
        name: '设置',
        action: 'divider'
      },

      // menu_action_incoterms_tree: {
      //   name: '国际贸易术语',
      //   action: 'account.action_incoterms_tree'
      // },
      // menu_action_account_tax_template_form: {
      //   name: '税模版',
      //   action: 'account.action_account_tax_template_form'
      // },
      // menu_action_tax_group: {
      //   name: 'Tax Groups',
      //   action: 'account.action_tax_group'
      // },
      // menu_action_tax_form: {
      //   name: 'Taxes',
      //   action: 'account.action_tax_form'
      // },
      // menu_action_account_group_action: {
      //   name: '科目组',
      //   action: 'account.action_account_group_action'
      // },
      // menu_action_account_fiscal_position_template: {
      //   name: '财务状况模版',
      //   action: 'account.action_account_fiscal_position_template'
      // },
      // menu_action_account_fiscal_position_form: {
      //   name: '财务状况',
      //   action: 'account.action_account_fiscal_position_form'
      // },
      // menu_action_payment_term_form: {
      //   name: '付款条款',
      //   action: 'account.action_payment_term_form'
      // }

      menu_action_account_account_tag: {
        name: '科目标签',
        action: 'account.action_account_account_tag'
      },

      menu_action_account_account: {
        name: '科目表',
        action: 'account.action_account_account'
      },

      // menu_action_account_journal_group_list: {
      //   name: '日记账分组',
      //   action: 'account.action_account_journal_group_list'
      // },

      menu_action_account_journal: {
        name: '日记账',
        action: 'account.action_account_journal'
      },

      menu_move_action_res_partner: {
        _odoo_model: 'ir.ui.menu',
        name: '联系人(开票设置)',
        icon: 'star',
        action: 'account.action_res_partner'
      },

      menu_move_action_product_category: {
        _odoo_model: 'ir.ui.menu',
        name: '产品类别(开票设置)',
        action: 'account.action_product_category'
      },

      menu_move_action_product_template: {
        _odoo_model: 'ir.ui.menu',
        name: '产品(开票设置)',
        action: 'account.action_product_template'
      },

      menu_account_divider_move_readonly: {
        name: '凭证',
        action: 'divider'
      },

      // menu_action_account_move_payment_customer_inbound: {
      //   action: 'account.action_account_move_payment_customer_inbound',
      //   name: '凭证(收款客户)'
      // },

      // menu_action_account_move_payment_supplier_outbound: {
      //   action: 'account.action_account_move_payment_supplier_outbound',
      //   name: '凭证(付款供应商)'
      // },

      // menu_action_account_move_out: {
      //   action: 'account.action_account_move_out',
      //   name: '凭证(销售结算单)'
      // },

      // menu_action_account_move_in: {
      //   action: 'account.action_account_move_in',
      //   name: '凭证(采购账单)'
      // }

      menu_action_account_move: {
        action: 'account.action_account_move',
        name: '凭证(查看)'
      },

      menu_action_account_move_other: {
        action: 'account.action_account_move_other',
        name: '凭证(手工凭证)'
      },

      menu_account_divider_invoice: {
        name: '结算单及账单',
        action: 'divider'
      },

      menu_action_account_move_out_invoice: {
        action: 'account.action_account_move_out_invoice',
        name: '销售结算单'
      },
      menu_action_account_move_in_invoice: {
        action: 'account.action_account_move_in_invoice',
        name: '采购账单'
      },

      menu_product_divider_payment: {
        name: '收付款',
        action: 'divider'
      },
      menu_action_account_payment_customer_inbound: {
        action: 'account.action_account_payment_customer_inbound',
        name: '销售收款'
      },

      menu_action_account_payment_supplier_outbound: {
        action: 'account.action_account_payment_supplier_outbound',
        name: '采购付款'
      },
      menu_action_account_payment_transfer: {
        action: 'account.action_account_payment_transfer',
        name: '内部转账'
      }

      // menu_action_bank_statement_tree: {
      //   action: 'account.action_bank_statement_tree',
      //   name: '银行对账单'
      // },
      // menu_action_view_bank_statement_tree: {
      //   action: 'account.action_view_bank_statement_tree',
      //   name: '现金对账单'
      // }
    }
  },

  menu_account_report: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '报表',
    icon: 'star',
    sequence: 36,
    children: {
      // menu_action_account_moves_all: {
      //   action: 'account.action_account_moves_all',
      //   name: '日记账本明细'
      // },
      // menu_account_analytic_line_action_entries: {
      //   action: 'analytic.account_analytic_line_action_entries',
      //   name: '分析明细'
      // },
    }
  }
}
