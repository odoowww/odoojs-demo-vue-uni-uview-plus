export default {
  menu_web_root: {
    _odoo_model: 'ir.ui.menu',
    name: 'odoo',
    sequence: 10
  },

  menu_base: {
    _odoo_model: 'ir.ui.menu',
    parent: 'menu_web_root',
    name: '系统设置',
    sequence: 10,
    children: {
      menu_open_module_tree: {
        name: 'Apps',
        action: 'base.action_ir_module_module'
      },
      menu_action_res_company_form: {
        name: '公司',
        action: 'base.action_res_company'
      },
      menu_action_res_users: {
        name: '用户',
        action: 'base.action_res_users'
      }
    }
  }
}
