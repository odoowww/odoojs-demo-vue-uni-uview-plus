export default {
  menu_stock_action_res_partner: {
    _odoo_model: 'ir.ui.menu',
    parent: 'contacts.menu_contacts',
    name: '联系人(库存设置)',
    icon: 'star',
    sequence: 80,
    action: 'stock.action_res_partner'
  },

  menu_stock_action_product_category: {
    _odoo_model: 'ir.ui.menu',
    parent: 'product.menu_product',
    name: '产品类别(库存设置)',
    sequence: 80,
    action: 'stock.action_product_category'
  },

  menu_stock_action_product_template: {
    _odoo_model: 'ir.ui.menu',
    parent: 'product.menu_product',
    name: '产品(库存设置)',
    sequence: 80,
    action: 'stock.action_product_template'
  },

  menu_stock_picking: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '库存管理',
    icon: 'star',
    sequence: 80,
    children: {
      menu_divider_config: {
        name: '设置',
        action: 'divider'
      },

      menu_action_stock_warehouse: {
        name: '仓库',
        action: 'stock.action_stock_warehouse'
      },

      menu_action_stock_location: {
        name: '库位',
        action: 'stock.action_stock_location'
      },

      menu_action_stock_picking_type: {
        name: '调拨类型',
        action: 'stock.action_stock_picking_type'
      },

      menu_odoojs_action_res_partner: {
        _odoo_model: 'ir.ui.menu',
        name: '联系人(库存设置)',
        icon: 'star',
        action: 'stock.action_res_partner'
      },

      menu_odoojs_action_product_category: {
        _odoo_model: 'ir.ui.menu',
        name: '产品类别(库存设置)',
        action: 'stock.action_product_category'
      },

      menu_odoojs_action_product_template: {
        _odoo_model: 'ir.ui.menu',
        name: '产品(库存设置)',
        action: 'stock.action_product_template'
      },

      menu_divider_picking: {
        name: '调拨',
        action: 'divider'
      },

      menu_action_stock_picking_outgoing: {
        name: '出库单',
        action: 'stock.action_stock_picking_outgoing'
      },
      menu_action_stock_picking_incoming: {
        name: '入库单',
        action: 'stock.action_stock_picking_incoming'
      },
      menu_action_stock_picking_internal: {
        name: '内部调拨',
        action: 'stock.action_stock_picking_internal'
      },

      menu_divider_quant: {
        name: '库存查询',
        action: 'divider'
      },

      menu_action_stock_quant: {
        name: '库存',
        action: 'stock.action_stock_quant'
      }
    }
  },

  menu_stock_inventory: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '库存',
    icon: 'star',
    sequence: 88,
    children: {
      // menu_dashboard_open_quants: {
      //   name: '库位库存',
      //   action: 'stock.dashboard_open_quants'
      // }
      // menu_action_product_stock_view: {
      //   name: '产品库存',
      //   action: 'stock.action_product_stock_view'
      // },
      // menu_stock_move_action: {
      //   name: '库存移动',
      //   action: 'stock.stock_move_action'
      // },
      // menu_stock_move_line_action: {
      //   name: '移动历史',
      //   action: 'stock.stock_move_line_action'
      // }
    }
  }
}
