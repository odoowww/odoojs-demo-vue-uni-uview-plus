import base from './base.js'
import contacts from './contacts.js'
import product from './product.js'
import account from './account.js'
import sale from './sale.js'
import purchase from './purchase.js'
import stock from './stock.js'
// import hr from './hr.js'

const modules = {
  base,
  contacts,
  product,
  account,
  sale,
  purchase,
  stock
  //   hr,
}

export default modules
