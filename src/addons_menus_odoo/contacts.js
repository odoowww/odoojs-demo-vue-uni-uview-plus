export default {
  menu_contacts: {
    _odoo_model: 'ir.ui.menu',
    parent: 'base.menu_web_root',
    name: '联系人',
    icon: 'star',
    sequence: 14,
    children: {
      menu_contacts_divider_config: {
        sequence: 2,
        name: '设置',
        action: 'divider'
      },

      menu_action_res_currency: {
        sequence: 2,
        name: '货币',
        action: 'base.action_res_currency'
      },

      menu_action_res_lang: {
        sequence: 2,
        name: '语言',
        action: 'base.action_res_lang'
      },

      menu_action_res_country_group: {
        sequence: 2,
        name: '国家组',
        action: 'base.action_res_country_group'
      },

      menu_action_res_country: {
        sequence: 2,
        name: '国家',
        action: 'base.action_res_country'
      },

      menu_action_res_partner_category: {
        sequence: 2,
        name: '联系人标签',
        action: 'base.action_res_partner_category'
      },
      menu_action_res_partner_title: {
        sequence: 2,
        name: '联系人称谓',
        action: 'base.action_res_partner_title'
      },
      menu_action_res_partner_industry: {
        sequence: 2,
        name: '行业类型',
        action: 'base.action_res_partner_industry'
      },

      menu_contacts_divider: {
        sequence: 2,
        name: '管理',
        action: 'divider'
      },

      menu_action_res_bank: {
        name: '银行',
        sequence: 4,
        action: 'base.action_res_bank'
      },
      menu_action_res_partner_bank: {
        name: '银行账户',
        sequence: 6,
        action: 'base.action_res_partner_bank'
      },

      menu_action_res_partner: {
        name: '所有联系人',
        sequence: 8,
        action: 'contacts.action_res_partner'
      }
    }
  }
}
