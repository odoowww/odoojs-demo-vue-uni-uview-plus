### 移动端 注意事项

1. vite.config.js 中的 server.proxy, 改为自己的 odoo 服务器
2. 路由 pages/user/login 登录页面. src/pages/user/login.vue 中 formState 设置默认 数据库 用户名 密码
3. 路由 pages/home/index 主页面
4. 路由 pages/web/index 是 所有的 模型 展示页面

### odoojs 入口

1. src/odoojs/index.ts
2. 简单了解下 src/odoojs/index.ts 中的 代码。初始化 配置 odoojs
